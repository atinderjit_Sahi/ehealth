//
//  AWSAuth.swift
//  E-Health
//
//  Created by Akash Dhiman on 18/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import Foundation
import AWSMobileClient

class AWSAuth: NSObject {
    
    // MARK: - Shared Instance
    static let shared: AWSAuth = {
        let instance = AWSAuth()
        return instance
    }()
    
    func userAWSStatus() -> UserState {
        switch AWSMobileClient.default().currentUserState {
        case .signedIn:
            return .signedIn
        case .signedOut:
            return .signedOut
        case .signedOutFederatedTokensInvalid:
            return .signedOutFederatedTokensInvalid
        case .signedOutUserPoolsTokenInvalid:
            return .signedOutUserPoolsTokenInvalid
        case .guest:
            return .guest
        case .unknown:
            return .unknown
        }
    }
    
    /**
    Sign in with user credentials:
     - Parameters:
        - emailID: Pass Email ID entered
        - password: Pass the password entered.
    */
    func signInWithAWS(isSocial:Bool, emailID: String, password: String, controller: UIViewController, completion: @escaping (_ status: SignInState, _ error: Error?) -> (Void)) {
        
        if !InternetConnection.isConnected {
            Utility.shared.stopLoader()
            Utility.shared.showToast(message: "No internet connection!")
            return
        }
//        Utility.shared.startLoader()
        AWSMobileClient.default().signIn(username: emailID, password: password) { (signInResult, error) in
//            Utility.shared.stopLoader()
            if let error = error  {
                Utility.shared.showAlert(message: self.handleErrors(error: error), controller: controller)
                completion(.unknown, error)
            } else if let signInResult = signInResult {
                if isSocial {
                    switch (signInResult.signInState) {
                    case .signedIn:
                        completion(.signedIn, nil)
                    case .smsMFA:
                        completion(.smsMFA, nil)
                    default:
                        completion(.unknown, nil)
                    }
                } else {
                    switch (signInResult.signInState) {
                    case .signedIn:
                        completion(.signedIn, nil)
                    case .smsMFA:
                        completion(.smsMFA, nil)
                        Utility.shared.showAlert(message: "SMS message sent to \(signInResult.codeDetails!.destination!)", controller: controller)
                    case .unknown:
                        Utility.shared.showAlert(message: "User does not exist.", controller: controller)
                    case .passwordVerifier:
                        Utility.shared.showAlert(message: "Password Verifier", controller: controller)
                    case .customChallenge:
                        Utility.shared.showAlert(message: "Custom Challenge", controller: controller)
                    case .deviceSRPAuth:
                        Utility.shared.showAlert(message: "Device SRP Auth", controller: controller)
                    case .devicePasswordVerifier:
                        Utility.shared.showAlert(message: "Device Password Verifier", controller: controller)
                    case .adminNoSRPAuth:
                        Utility.shared.showAlert(message: "Request Admin for SRP Auth", controller: controller)
                    case .newPasswordRequired:
                        Utility.shared.showAlert(message: "New Password Required", controller: controller)
                    }
                }
            }
        }
    }
    
    /**
    Confirm SignIn (MFA):
     - Parameters:
        - code: Pass code generated after signIn() method is called.
    */
    func confirmUserSignIn(code :String, controller : UIViewController, completion: @escaping(Bool) -> (Void)) {
        
        if !InternetConnection.isConnected {
            Utility.shared.stopLoader()
            Utility.shared.showToast(message: "No internet connection!")
            return
        }
        Utility.shared.startLoader()
        AWSMobileClient.default().confirmSignIn(challengeResponse: code) { (signInResult, error) in
            Utility.shared.stopLoader()
            if let error = error  {
                Utility.shared.showAlert(message: self.handleErrors(error: error), controller: controller)
                completion(false)
            } else if let signInResult = signInResult {
                switch (signInResult.signInState) {
                case .signedIn:
                    completion(true)
                default:
                    print("\(signInResult.signInState.rawValue)")
                }
            }
        }
    }
    
    func getAccessToken(completion: @escaping (_ response: Tokens, _ error: Error?) -> (Void)) {
        AWSMobileClient.default().getTokens { (tokens, error) in
            completion(tokens!, error)
        }
    }
    
    func getUserAWSAttributes(completion: @escaping (_ response: [String : Any], _ error: Error?) -> (Void)) {
        if !InternetConnection.isConnected {
            Utility.shared.stopLoader()
            Utility.shared.showToast(message: "No internet connection!")
            return
        }
        
        AWSMobileClient.default().getUserAttributes { (attributes, error) in
             if error == nil{
                completion(attributes ?? [:], nil)
             } else {
                completion([:], error)
             }
        }
    }
    
    /**
    Creates a new user in Cognito User Pools:
     - Parameters:
        - requestData: request sign up data dictionary to be passes in this parameter.
    */
    func signUpWithAWS(requestData : [String : String], password: String, controller: UIViewController, completionHandler: @escaping(Bool) -> (Void)) {
        
        if !InternetConnection.isConnected {
            Utility.shared.stopLoader()
            Utility.shared.showToast(message: "No internet connection!")
            return
        }
        Utility.shared.startLoader()
        AWSMobileClient.default().signUp(username: requestData["email"]!,
                                         password: password,
                                                userAttributes: requestData) { (signUpResult, error) in
                                                    Utility.shared.stopLoader()
            if let signUpResult = signUpResult {
                switch(signUpResult.signUpConfirmationState) {
                case .confirmed:
                    completionHandler(true)
                    Utility.shared.showAlert(message: "SMS message sent to \(signUpResult.codeDeliveryDetails?.destination ?? "Registered mobile number")", controller: controller)
                    
                case .unconfirmed:
                    completionHandler(true)
//                    Utility.shared.showAlert(message: "SMS message sent to \(signUpResult.codeDeliveryDetails?.destination ?? "Registered mobile number")", controller: controller)
                    
                case .unknown:
                    completionHandler(false)
                }
            } else if let error = error {
                Utility.shared.showAlert(message: self.handleErrors(error: error), controller: controller)
                completionHandler(false)
            }
        }
    }
    
    /**
    Confirms a new user signUP (MFA):
     - Parameters:
        - emailID: Pass the email id entered.
        - code: Pass the code generated after signUp() method.
    */
    func confirmNewUserAfterSignUp(emailID: String, code: String, controller: UIViewController, completionHandler: @escaping(String, Bool) -> (Void)) {
        
        if !InternetConnection.isConnected {
            Utility.shared.stopLoader()
            Utility.shared.showToast(message: "No internet connection!")
            return
        }
        Utility.shared.startLoader()
        AWSMobileClient.default().confirmSignUp(username: emailID, confirmationCode: code) { (signUpResult, error) in
            Utility.shared.stopLoader()
            if let signUpResult = signUpResult {
                switch(signUpResult.signUpConfirmationState) {
                case .confirmed:
//                    Utility.shared.showToast(message: "User is signed up and confirmed.")
                    completionHandler("User is signed up and confirmed", true)
//                    Utility.shared.showAlert(message: "User is signed up and confirmed.", controller: controller)
                    
                case .unconfirmed:
                    print("User is not confirmed and needs verification via \(signUpResult.codeDeliveryDetails!.deliveryMedium) sent at \(signUpResult.codeDeliveryDetails!.destination!)")
//                    Utility.shared.showToast(message: "User is signed up and confirmed.")
                    completionHandler("User is signed up", true)
                case .unknown:
//                    Utility.shared.showToast(message: "Unexpected case")
                    completionHandler("Unexpected case", false)
                }
            } else if let error = error {
//                Utility.shared.showAlert(message: self.handleErrors(error: error), controller: controller)
                completionHandler(self.handleErrors(error: error), false)
            }
        }
    }
    
    /**
    This method is use to resend confirmation code:
     - Parameters:
        - emailID: Pass the email id entered.
    */
    func resendConfirmationCode(emailID: String, controller: UIViewController, completionHandler: @escaping(Bool) -> (Void)) {
        
        if !InternetConnection.isConnected {
            Utility.shared.stopLoader()
            Utility.shared.showToast(message: "No internet connection!")
            return
        }
        Utility.shared.startLoader()
        AWSMobileClient.default().resendSignUpCode(username: emailID, completionHandler: { (result, error) in
            Utility.shared.stopLoader()
            if let signUpResult = result {
                completionHandler(true)
                Utility.shared.showAlert(message: "A verification code has been sent to \(signUpResult.codeDeliveryDetails!.destination!)", controller: controller)
                
                print("A verification code has been sent via \(signUpResult.codeDeliveryDetails!.deliveryMedium) at \(signUpResult.codeDeliveryDetails!.destination!)")
            } else if let error = error {
                print("\(error.localizedDescription)")
                Utility.shared.showAlert(message: self.handleErrors(error: error), controller: controller)
                completionHandler(false)
            }
        })
    }
    
    /**
     Call a forgotPassword() method which sends a confirmation code via email or phone number. The details of how the code was sent are included in the response of forgotPassword().
        - Parameters:
            - emailID: Pass your Email ID
     */
    func forgotAWSPassword(emailID: String, controller: UIViewController, completionHandler: @escaping(Bool, String) -> (Void)) {
         
        if !InternetConnection.isConnected {
            Utility.shared.stopLoader()
            Utility.shared.showToast(message: "No internet connection!")
            return
        }
        Utility.shared.startLoader()
        AWSMobileClient.default().forgotPassword(username: emailID) { (forgotPasswordResult, error) in
            Utility.shared.stopLoader()
            if let forgotPasswordResult = forgotPasswordResult {
                switch(forgotPasswordResult.forgotPasswordState) {
                case .confirmationCodeSent:
                    completionHandler(true, "")
                    Utility.shared.showAlert(message: "Confirmation code sent via \(forgotPasswordResult.codeDeliveryDetails!.deliveryMedium) to: \(forgotPasswordResult.codeDeliveryDetails!.destination!)", controller: controller)
                default:
                    completionHandler(false, "Error: Invalid case")
//                    Utility.shared.showToast(message: "Error: Invalid case.")
                }
            } else if let error = error {
                completionHandler(false, error.localizedDescription)
//                Utility.shared.showToast(message: error.localizedDescription)
            }
        }
    }
    
    /**
    Once the code is given call confirmForgotPassword() with the confirmation code after hitting forgotPassword() method.
     - Parameters:
        - emailID: Pass your Email ID
        - newPassword: Pass new password entered
        - confirmationCode: Pass new confirmation code generated
    */
    func confirmAWSForgotPassword(emailID: String, newPassword: String, confirmationCode: String, completionHandler: @escaping(Bool, String) -> (Void)) {
        
        if !InternetConnection.isConnected {
            Utility.shared.stopLoader()
            Utility.shared.showToast(message: "No internet connection!")
            return
        }
        Utility.shared.startLoader()
        AWSMobileClient.default().confirmForgotPassword(username: emailID, newPassword: newPassword, confirmationCode: confirmationCode) { (forgotPasswordResult, error) in
            Utility.shared.stopLoader()
            if let forgotPasswordResult = forgotPasswordResult {
                switch(forgotPasswordResult.forgotPasswordState) {
                case .done:
//                    Utility.shared.showToast(message: "Password changed successfully")
                    completionHandler(true, "Password changed successfully")
                default:
//                    Utility.shared.showToast(message: "Error: Could not change password")
                    completionHandler(false, "Could not change password")
                }
            } else if let error = error {
                print(self.handleErrors(error: error))
                completionHandler(false, self.handleErrors(error: error))
            }
        }
    }
    
//    func handleErrors(error : Error) -> String{
//        if let error = error as? AWSMobileClientError {
//            var errorMessage = ""
//            switch(error) {
//            case .usernameExists(let message):
//                errorMessage = message
//            case .invalidParameter(let message):
//                errorMessage = message
//            case .notAuthorized(let message):
//                errorMessage = message
//            case .aliasExists(let message):
//                errorMessage = message
//            case .codeDeliveryFailure(let message):
//                errorMessage = message
//            case .codeMismatch(let message):
//                errorMessage = message
//            case .expiredCode(let message):
//                errorMessage = message
//            case .groupExists(let message):
//                errorMessage = message
//            case .internalError(let message):
//                errorMessage = message
//            case .invalidLambdaResponse(let message):
//                errorMessage = message
//            case .invalidOAuthFlow(let message):
//                errorMessage = message
//            case .invalidPassword(let message):
//                errorMessage = message
//            case .invalidUserPoolConfiguration(let message):
//                errorMessage = message
//            case .limitExceeded(let message):
//                errorMessage = message
//            case .mfaMethodNotFound(let message):
//                errorMessage = message
//            case .passwordResetRequired(let message):
//                errorMessage = message
//            case .resourceNotFound(let message):
//                errorMessage = message
//            case .scopeDoesNotExist(let message):
//                errorMessage = message
//            case .softwareTokenMFANotFound(let message):
//                errorMessage = message
//            case .tooManyFailedAttempts(let message):
//                errorMessage = message
//            case .tooManyRequests(let message):
//                errorMessage = message
//            case .unexpectedLambda(let message):
//                errorMessage = message
//            case .userLambdaValidation(let message):
//                errorMessage = message
//            case .userNotConfirmed(let message):
//                errorMessage = message
//            case .userNotFound(let message):
//                errorMessage = message
//            case .unknown(let message):
//                errorMessage = message
//            case .notSignedIn(let message):
//                errorMessage = message
//            case .identityIdUnavailable(let message):
//                errorMessage = message
//            case .guestAccessNotAllowed(let message):
//                errorMessage = message
//            case .federationProviderExists(let message):
//                errorMessage = message
//            case .cognitoIdentityPoolNotConfigured(let message):
//                errorMessage = message
//            case .unableToSignIn(let message):
//                errorMessage = message
//            case .invalidState(let message):
//                errorMessage = message
//            case .userPoolNotConfigured(let message):
//                errorMessage = message
//            case .userCancelledSignIn(let message):
//                errorMessage = message
//            case .badRequest(let message):
//                errorMessage = message
//            case .expiredRefreshToken(let message):
//                errorMessage = message
//            case .errorLoadingPage(let message):
//                errorMessage = message
//            case .securityFailed(let message):
//                errorMessage = message
//            case .idTokenNotIssued(let message):
//                errorMessage = message
//            case .idTokenAndAcceessTokenNotIssued(let message):
//                errorMessage = message
//            case .invalidConfiguration(let message):
//                errorMessage = message
//            case .deviceNotRemembered(let message):
//                errorMessage = message
//            }
//            return errorMessage
//        }
//        return ""
//    }
    /**
    This method loggs out from current session of AWS
    */
    func signOutAWS() {
        AWSMobileClient.default().clearCredentials()
        AWSMobileClient.default().clearKeychain()
        AWSMobileClient.default().invalidateCachedTemporaryCredentials()
        AWSMobileClient.default().signOut()
    }
    
    /**
     Using global signout, you can signout a user from all active login sessions. By doing this, you are invalidating all tokens (id token, access token and refresh token) which means the user is signed out from all devices.
     */
    func signOutAWSGlobally() {
        AWSMobileClient.default().signOut(options: SignOutOptions(signOutGlobally: true)) { (error) in
            print("\(error?.localizedDescription ?? "")")
        }
    }
    
    func changePassword(oldPassword: String, newPassword: String, completionHandler: @escaping(Bool) -> (Void)) {
        if !InternetConnection.isConnected {
            Utility.shared.stopLoader()
            Utility.shared.showToast(message: "No internet connection!")
            return
        }
        
        AWSMobileClient.default().changePassword(currentPassword: oldPassword, proposedPassword: newPassword) { (error) in
            if error != nil {
                Utility.shared.showToast(message: "Old password is incorrect")//"something went wrong")
                completionHandler(false)
            } else {
                completionHandler(true)
            }
        }
    }
    
    func updateUserAttributes(userData: [String: String], completionHandler: @escaping(Bool) -> (Void)) {
         
        if !InternetConnection.isConnected {
            Utility.shared.stopLoader()
            Utility.shared.showToast(message: "No internet connection!")
            completionHandler(false)
            return
        }
        Utility.shared.startLoader()
        AWSMobileClient.default().updateUserAttributes(attributeMap: userData) { (result, error) in
            Utility.shared.stopLoader()
            if result != nil {
//                print("User attributes update successful.")
//                Utility.shared.showToast(message: "Profile Updated successfully")
                completionHandler(true)
            } else if let error = error {
                Utility.shared.showToast(message: self.handleErrors(error: error))
//                Utility.shared.showAlert(message: self.handleErrors(error: error), controller: controller)
                completionHandler(false)
            }
        }
    }
    
    func handleErrors(error : Error) -> String{
        if let error = error as? AWSMobileClientError {
            var errorMessage = ""
            switch(error) {
            case .usernameExists(let message):
                errorMessage = message
            case .invalidParameter(let message):
                errorMessage = message
            case .notAuthorized(let message):
                errorMessage = message
            case .aliasExists(let message):
                errorMessage = message
            case .codeDeliveryFailure(let message):
                errorMessage = message
            case .codeMismatch(let message):
                errorMessage = message
            case .expiredCode(let message):
                errorMessage = message
            case .groupExists(let message):
                errorMessage = message
            case .internalError(let message):
                errorMessage = message
            case .invalidLambdaResponse(let message):
                errorMessage = message
            case .invalidOAuthFlow(let message):
                errorMessage = message
            case .invalidPassword(let message):
                errorMessage = message
            case .invalidUserPoolConfiguration(let message):
                errorMessage = message
            case .limitExceeded(let message):
                errorMessage = message
            case .mfaMethodNotFound(let message):
                errorMessage = message
            case .passwordResetRequired(let message):
                errorMessage = message
            case .resourceNotFound(let message):
                errorMessage = message
            case .scopeDoesNotExist(let message):
                errorMessage = message
            case .softwareTokenMFANotFound(let message):
                errorMessage = message
            case .tooManyFailedAttempts(let message):
                errorMessage = message
            case .tooManyRequests(let message):
                errorMessage = message
            case .unexpectedLambda(let message):
                errorMessage = message
            case .userLambdaValidation(let message):
                errorMessage = message
            case .userNotConfirmed(let message):
                errorMessage = message
            case .userNotFound(let message):
                errorMessage = message
            case .unknown(let message):
                errorMessage = message
            case .notSignedIn(let message):
                errorMessage = message
            case .identityIdUnavailable(let message):
                errorMessage = message
            case .guestAccessNotAllowed(let message):
                errorMessage = message
            case .federationProviderExists(let message):
                errorMessage = message
            case .cognitoIdentityPoolNotConfigured(let message):
                errorMessage = message
            case .unableToSignIn(let message):
                errorMessage = message
            case .invalidState(let message):
                errorMessage = message
            case .userPoolNotConfigured(let message):
                errorMessage = message
            case .userCancelledSignIn(let message):
                errorMessage = message
            case .badRequest(let message):
                errorMessage = message
            case .expiredRefreshToken(let message):
                errorMessage = message
            case .errorLoadingPage(let message):
                errorMessage = message
            case .securityFailed(let message):
                errorMessage = message
            case .idTokenNotIssued(let message):
                errorMessage = message
            case .idTokenAndAcceessTokenNotIssued(let message):
                errorMessage = message
            case .invalidConfiguration(let message):
                errorMessage = message
            case .deviceNotRemembered(let message):
                errorMessage = message
            }
            return errorMessage
        }
        return ""
    }
}
