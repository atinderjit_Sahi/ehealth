//
//  NewPolicyVC.swift
//  E-Health
//
//  Created by Atinder Kaur on 1/27/21.
//  Copyright © 2021 AbhiShek Chugh. All rights reserved.
//

import UIKit
import WebKit
var boolSelectedTerms = false

class NewPolicyVC: UIViewController {
    
    var webUrlString : String = "http://3.236.116.101/legal-terms"
     var web_View = WKWebView()
    @IBOutlet weak var btnTick: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if #available(iOS 11.0, *) {
            
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top
            
            web_View.frame = CGRect(x: 0, y: topPadding! + 44, width: view.frame.size.width, height: view.frame.size.height - 300)
        } else {
            // Fallback on earlier versions
            web_View.frame = CGRect(x: 0, y: 64, width: view.frame.size.width, height: view.frame.size.height - 300)
        }
        
        web_View.navigationDelegate = self
        web_View.isOpaque = true
        
        guard let url = URL(string: webUrlString) else {
            return
        }
        let request = URLRequest(url: url)
        
        if UIApplication.shared.canOpenURL(url) {
            UIPasteboard.general.string = request.description
            web_View.load(request)
        } else {
            UIPasteboard.general.string = request.description
            Utility.shared.showToast(message: request.description)
            return
        }
        Utility.shared.startLoader()
        view.addSubview(web_View)

    }
    
    @IBAction func actionBack(_ sender: Any) {
        
        
        
        self.navigationController?.popViewController(animated: true )
    }
    
    @IBAction func actionLegalPolics(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected =  false
        }
        else {
            sender.isSelected =  true
            boolSelectedTerms = true

        }
    }
    

    @IBAction func actionSubmit(_ sender: UIButton) {
        if !btnTick.isSelected {
            Utility.shared.showToast(message: "Please agree to Legal Terms")
        }
        else {
            
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
}
extension NewPolicyVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Utility.shared.stopLoader()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!) {
        Utility.shared.stopLoader()
    }
    
}
