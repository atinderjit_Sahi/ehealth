//
//  AuthenticationViewController.swift
//  E-Health
//
//  Created by Akash Dhiman on 18/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import GoogleSignIn
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift
import FBSDKLoginKit
import LoginWithAmazon
import AWSMobileClient

class AuthenticationViewController: UIViewController {

    //Outlets
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var registerHereButton: UIButton!
    @IBOutlet weak var passwordDisplayToggleButton: UIButton!
    @IBOutlet weak var socialTop: NSLayoutConstraint!
    @IBOutlet weak var viewCall: UIView!
    @IBOutlet weak var lblCall: UILabel!
    //MARK: - Properties
    var isComingFromSignup = false

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
      //  firstTimeSignup =  false
//        if isComingFromSignup {
            emailTextField.text? = ""
            passwordTextField.text = ""
//        }
        firstTimeSignup =  false
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        if UserDefaults.standard.value(forKey: "emerContactNumber") != nil && UserDefaults.standard.value(forKey: "emerContactNumber")as? String != "" {
            viewCall.isHidden = false
            lblCall.text = UserDefaults.standard.value(forKey: "emerContactNumber") as? String
        }
        else {
            socialTop.constant = 47
            viewCall.isHidden = true
        }
        
    }
    
    func initializeView() {
        
        let registerButtonAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "Montserrat-Bold", size: 17.0)!,
            .foregroundColor: UIColor(red: 86/255, green: 86/255, blue: 86/255, alpha: 1.0),
            .underlineStyle: NSUnderlineStyle.single.rawValue ]
        
        let attributedString = NSAttributedString(string: "Register Here", attributes: registerButtonAttributes)
        registerHereButton.setAttributedTitle(attributedString, for: .normal)
        
        
    }
    
    //MARK:- ButtonAction
    @IBAction func passwordDisplayToggleButtonAction(_ sender: Any) {
       
        if passwordTextField.isSecureTextEntry {
            passwordTextField.isSecureTextEntry = false
            passwordDisplayToggleButton.setImage(UIImage(named: "unhide_password"), for: .normal)
        } else {
            passwordTextField.isSecureTextEntry = true
            passwordDisplayToggleButton.setImage(UIImage(named: "hide_password"), for: .normal)
        }
    }
    
    
    @IBAction func loginButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        let (status, errorMessage) = Utility.shared.loginValidation(emailTextField: emailTextField, passwordtextfield: passwordTextField)
        if status == false{
            Utility.shared.showToast(message: errorMessage ?? "")
        } else {
            DispatchQueue.main.async {
                Utility.shared.startLoader()
                AWSAuth.shared.signInWithAWS(isSocial: false, emailID: self.emailTextField.text!, password: self.passwordTextField.text!, controller: self) { (state, error) -> (Void) in
                    UserDefaults.standard.set(0, forKey: "isSocialLogin")
                    if state == .smsMFA {
                        self.navigateWhenMfaEnabled(userState: state)
                    } else if state == .signedIn {
                        self.navigateWhenMfaNotEnabled()
                    } else {
                        Utility.shared.stopLoader()
                    }
                }
            }
        }
    }
    
    func navigateWhenMfaEnabled(userState: SignInState) {
//        if userState == .smsMFA {
        Utility.shared.stopLoader()
            DispatchQueue.main.async {
                let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: VerificationViewController.className) as? VerificationViewController
                vc?.isFromLogin = true
                vc?.signupEmail = self.emailTextField.text!
                vc?.signinPassword = self.passwordTextField.text!
                self.navigationController?.pushViewController(vc!, animated: true)
            }
//        }
    }
    
    func navigateWhenMfaNotEnabled() {
        self.getUserProfile()
    }
    
    func getUserProfile() {
        if AWSAuth.shared.userAWSStatus() == .signedIn {
//            self.dispatchGroup.enter()
            AWSAuth.shared.getUserAWSAttributes { (data, error) -> (Void) in
//                self.dispatchGroup.leave()
                Utility.shared.stopLoader()
                guard error == nil else {
                    return
                }
                Constant.userProfile = ProfileDetail(value: data)
                
                UserDefaults.standard.set(Constant.userProfile?.isNewUser, forKey: "isNewUser")
                UserDefaults.standard.removeObject(forKey: UserDefaultKey.userProfile)
                let userDetail = NSKeyedArchiver.archivedData(withRootObject: data as Any)
                UserDefaults.standard.set(userDetail, forKey: UserDefaultKey.userProfile)
                
                if Constant.userProfile?.family_name == "consumer" {
                    Constant.loggedInProfile =  .consumer
                                        
                    //API TO POST USER's COGNITO SUB ID TO DATABASE
                    RegisterViewModel.postCognitoUserID(userID: AWSMobileClient.default().username ?? "") { result in
                        if Constant.userProfile?.isNewUser == "0" {
                            self.goToDashboardScreen()
                        } else {
                            self.goToConsumerTutorialScreen()
//                            self.goToWidgetsScreen()
                        }
                    }
                } else if Constant.userProfile?.family_name == "provider"{
                    Constant.loggedInProfile =  .provider
                    self.saveProviderProfileToDB()
                } else {
                    Constant.loggedInProfile = .none
                }
            }
        } else {
            Utility.shared.stopLoader()
        }
    }
    
    func goToConsumerTutorialScreen() {
        DispatchQueue.main.async {
            guard let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: ConsumerTutorialViewController.className) as? ConsumerTutorialViewController else {
                return
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func goToWidgetsScreen() {
        DispatchQueue.main.async {
            guard let vc = UIStoryboard.init(name: StoryboardNames.TabBar, bundle: Bundle.main).instantiateViewController(withIdentifier: SetWidgetViewController.className) as? SetWidgetViewController else {
                return
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
    func goToDashboardScreen() {
        DispatchQueue.main.async {
            guard let vc = UIStoryboard.init(name: StoryboardNames.TabBar, bundle: Bundle.main).instantiateViewController(withIdentifier: TabBarViewController.className) as? TabBarViewController else {
                return
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func actionCall(_ sender: UIButton) {
        
        var phoneNumber = String()
              
                   phoneNumber = lblCall.text ?? ""
               
               
               if let url = URL(string: "tel://\(String(describing: phoneNumber))"), UIApplication.shared.canOpenURL(url) {
                   UIApplication.shared.open(url)
               }
               else {
                
                
                self.view.makeToast("Something went wrong!")
               }    }
    
    
    
    func saveProviderProfileToDB() {
        var params = [String: AnyObject]()
        
        //Provider Full Name
        var providerFullName = String()
        let name = Constant.userProfile?.name
        let middleName = Constant.userProfile?.middleName
        if middleName != "" || middleName != nil {
            let myStringArr = name?.components(separatedBy: " ")
            let firstName = (myStringArr?[0] ?? "")
            var lastName = ""
            if myStringArr?.count == 2 {
                lastName = (myStringArr?[1] ?? "")
            }
            providerFullName = firstName + " " + (middleName ?? "") + " " + lastName
        } else {
            providerFullName = name ?? ""
        }
        
        let providerType = Constant.userProfile?.providerType
        //IF PROVIDER TYPE NOT DOCTOR/HOSPITAL, HIDE FIELDS
        if providerType != "1" && providerType != "2" {
        }
            //SHOW FIELDS
        else {
            //For SPECIALIST FIELD
            if let specialityString = Constant.userProfile?.specialistIn {
                var specialityNamesArray = ""
                let specialitiesArray = specialityString.components(separatedBy: ";")
                if specialitiesArray.count != 0 {
                    for speciality in specialitiesArray {
                        let specialityDetailArray = speciality.components(separatedBy: "-")
                        specialityNamesArray.append((specialityDetailArray.last ?? "") + ",")
                    }
                    specialityNamesArray.removeLast()
                }
                params["speciality"] = specialityNamesArray as AnyObject
            }
            
            //FOR PRACTICE NAME FIELD
            if let practiceNameString = Constant.userProfile?.praticeName {
                var practiceNamesArray = ""
                let praticeArray = practiceNameString.components(separatedBy: ";")
                if praticeArray.count != 0 {
                    for practice in praticeArray {
                        let practiceDetailArray = practice.components(separatedBy: "-")
                        practiceNamesArray.append((practiceDetailArray.last ?? "") + ",")
                    }
                    practiceNamesArray.removeLast()
                }
                params["practice_name"] = practiceNamesArray as AnyObject
            }
        }
        
        params["cognito_id"] = Constant.userProfile?.sub as AnyObject
        params["provider_name"] = providerFullName as AnyObject
        //        params["practice_name"] = Constant.userProfile?.praticeName as AnyObject
        //        params["speciality"] = Constant.userProfile?.specialistIn as AnyObject
        params["email"] = Constant.userProfile?.email as AnyObject
        params["provider_type"] = Constant.userProfile?.providerType as AnyObject
        
        HomeViewModel.saveProvideProfileToDB(params: params) { (result, error) in
            print(result)
            if Constant.userProfile?.isNewUser == "0" {
                self.goToDashboardScreen()
            } else {
                self.goToWidgetsScreen()
            }
        }
    }
    
    @IBAction func forgotPasswordButtonAction(_ sender: Any) {
        guard let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: ForgotPasswordViewController.className) as? ForgotPasswordViewController else {
            return
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectServiceViewController") as? SelectServiceViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func facebookLoginButtonAction(_ sender: Any) {
        SocialAuth.shared.loginWithFaceBook(viewController: self) { (email, profile, id, sucess, error) in
            if sucess! {
                Utility.shared.stopLoader()
                DispatchQueue.main.async {
                    let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: RegisterProfileViewController.className) as? RegisterProfileViewController
                    vc?.federatedId = id ?? ""
                    vc?.socialProfile = profile
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            } else {
                if email != nil {
                    self.signInFederatedUser(email: email ?? "")
                } else {
                    Utility.shared.stopLoader()
                    Utility.shared.showToast(message: error ?? "Something went wrong")
                }
            }
        }
//        Utility.shared.showToast(message: "Facebook login coming soon")
    }
    
    @IBAction func googleLoginButtonAction(_ sender: Any) {
//        GIDSignIn.sharedInstance()?.signOut()
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.signIn()
//        Utility.shared.showToast(message: "Google login coming soon")
    }
    
    @IBAction func amazonLoginButtonAction(_ sender: Any) {
        SocialAuth.shared.loginWithAmazon(viewController: self) { (email, profile, id, success, error) in
            if success! {
                Utility.shared.stopLoader()
                DispatchQueue.main.async {
                    let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: RegisterProfileViewController.className) as? RegisterProfileViewController
                    vc?.federatedId = id ?? ""
                    vc?.socialProfile = profile
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            } else {
                if email != nil {
                    self.signInFederatedUser(email: email ?? "")
                } else {
                    Utility.shared.stopLoader()
                    Utility.shared.showToast(message: error ?? "Something went wrong")
                }
            }
        }
//        Utility.shared.showToast(message: "AWS login coming soon")
    }
    
    func signInFederatedUser(email: String) {
        AWSAuth.shared.signInWithAWS(isSocial: true, emailID: email, password: "Mind@1234", controller: self) { (state, error) -> (Void) in
            Utility.shared.stopLoader()
            UserDefaults.standard.set(1, forKey: "isSocialLogin")
            Constant.isSocialLogin = true
            Constant.loggedInProfile = .consumer
//            self.navigateWhenMfaEnabled(userState: state)
            self.navigateWhenMfaNotEnabled()
        }
    }

}

extension AuthenticationViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard range.location == 0 else {
            return true
        }
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
        return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
    }
}

extension AuthenticationViewController: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        
        // Perform any operations on signed in user here.
        let idToken = user.authentication.idToken // Safe to send to the server
        Utility.shared.startLoader()
        SocialAuth.shared.loginWithGoogle(viewController: self, user: user, token: idToken) { (email, profile, id, success, error) in
            if success! {
                Utility.shared.stopLoader()
                DispatchQueue.main.async {
                    let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: RegisterProfileViewController.className) as? RegisterProfileViewController
                    vc?.federatedId = id ?? ""
                    vc?.socialProfile = profile
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            } else {
                if email != nil {
                    self.signInFederatedUser(email: email ?? "")
                } else {
                    Utility.shared.stopLoader()
                    Utility.shared.showToast(message: error ?? "Something went wrong")
                }
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
      // Perform any operations when the user disconnects from app here.
    }
    
}
