//
//  AuthenticationViewModel.swift
//  E-Health
//
//  Created by Prabjot Kaur on 6/2/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class AuthenticationViewModel: NSObject {
    static func getAccessToken(params: [String: AnyObject], completion: @escaping (_ status: Bool, _ error: String?) -> Void) {
        let url = Constant.URL.getLoginToken
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params, method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                completion(true, nil)
            } else {
                completion(false, error?.localizedDescription)
            }
        }
    }
}
