//
//  ConsumerTutorialViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 24/12/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class ConsumerTutorialViewController: UIViewController {

    @IBOutlet weak var lblTitleName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let userData = UserDefaults.standard.object(forKey: UserDefaultKey.userProfile) as? NSData
        UserDefaults.standard.object(forKey: UserDefaultKey.userProfile) as? NSData
        if let userData = userData {
            
            let userDetails = NSKeyedUnarchiver.unarchiveObject(with: userData as Data)
            let userDetailDict = userDetails as? [String: Any] ?? [:]
            
            Constant.userProfile = ProfileDetail(value: userDetailDict)
            
//            if Constant.userProfile?.family_name == "consumer" {
//
//
//
//            }
            
            if let name = Constant.userProfile?.name {
            
            lblTitleName.text = "Hello, " +  name
            }
            
            if Constant.userProfile?.middleName != nil && Constant.userProfile?.middleName != "" {
            
                let mname = Constant.userProfile?.middleName ?? ""
                
                let str = Constant.userProfile?.name
                let strArray = str?.components(separatedBy: " ")
                
                if let lname = strArray?[1] {
                if let fname = strArray?[0] {
                    lblTitleName.text =   "Hello, " + "\(String(describing: fname))" + " " + mname + " " + "\(String(describing: lname))"

                }
                }

                
                
               
                
            }
            
         
        }
            
        
        
    }
    
    @IBAction func buttonNextTapped(_ sender: Any) {
        goToConsumerGetStartedScreen()
    }
    
    func goToConsumerGetStartedScreen() {
        DispatchQueue.main.async {
            guard let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: ConsumerGetStartedViewController.className) as? ConsumerGetStartedViewController else {
                return
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
