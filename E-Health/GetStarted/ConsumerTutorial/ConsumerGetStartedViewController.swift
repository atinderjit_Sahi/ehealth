//
//  ConsumerGetStartedViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 24/12/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class ConsumerGetStartedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonGetStartedTapped(_ sender: Any) {
        goToWidgetsScreen()
    }
    
    func goToWidgetsScreen() {
        DispatchQueue.main.async {
//            guard let vc = UIStoryboard.init(name: StoryboardNames.TabBar, bundle: Bundle.main).instantiateViewController(withIdentifier: SetWidgetViewController.className) as? SetWidgetViewController else {
//                return
//            }
            firstTimeSignup =  true
            guard let vc = UIStoryboard.init(name: StoryboardNames.Profile, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardIDs.MyProfileViewController) as? MyProfileViewController else {
                                 return
                             }
            
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
