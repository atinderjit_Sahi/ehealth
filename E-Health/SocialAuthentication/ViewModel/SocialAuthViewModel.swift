//
//  SocialAuthViewModel.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 8/18/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class SocialAuthViewModel: NSObject {
    
    static func getFederatedUserByEmail(emailID: String, completion: @escaping (_ email: String?, _ statusCode: Int?, _ message: String?) -> Void) {
        let url = Constant.URL.getFederatedUserByEmail + emailID
        
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            let data1 = data.first
                            completion(data1?["email"] as? String, statusCode, nil)
                        } else {
                           completion(nil, statusCode, nil)
                        }
                    } else if statusCode == 404 {
                        completion(nil, statusCode, nil)
                    } else {
                        completion(nil, statusCode, result["message"] as? String)
                    }
                } else {
                    completion(nil, nil, result["message"] as? String)
                }
            } else {
                completion(nil, nil, error?.localizedDescription)
            }
        }
    }
    
    static func createFederatedUser(parameters: [String: Any], completion: @escaping (_ response: String?, _ error: String?) -> Void) {
        let url = Constant.URL.createFederatedUser
        
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey]
        
        Utility.shared.startLoader()
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [String: AnyObject] {
                            if let cognitoId = data["cognito_user_id"] as? String {
                                print(cognitoId)
                            }
                            completion(result["message"] as? String, nil)
                        } else {
                           completion(result["message"] as? String, nil)
                        }
                    } else {
                        completion(nil, result["message"] as? String)
                    }
                } else {
                    completion(nil, result["message"] as? String)
                }
            } else {
                completion(nil, error?.localizedDescription)
            }
        }
    }
    
}
