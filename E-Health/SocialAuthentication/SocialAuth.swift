//
//  SocialAuth.swift
//  E-Health
//
//  Created by Akash Dhiman on 16/06/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import Foundation
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import AWSCore
import GoogleSignIn
import AWSAuthCore
import AWSMobileClient
import LoginWithAmazon

//Google Login Account: seasia.developer@gmail.com
//Amazon login Account : seasia.testdeveloper2@gmail.com    //Security#123
//Facebook login account :
//AWS console account : devops@seasiainfotech.com    //Security123

class SocialAuth: NSObject {
    
    let fbLoginManager = LoginManager()
    var userProfile: ProfileDetail?
    
    // MARK: - Shared Instance
    static let shared: SocialAuth = {
        let instance = SocialAuth()
        return instance
    }()
    
    func isUserLoggedInWithFB() {
        if let token = AccessToken.current, !token.isExpired {
            // User is logged in, do work such as go to next view controller.
        }
    }
    
    //MARK:- Login with facebook
    func loginWithFaceBook(viewController: UIViewController, completionHandler: @escaping (_ email: String?, _ profile: ProfileDetail?, _ id: String?, _ result:Bool?,  _ error1: String?) -> Void) {
        fbLoginManager.logOut()
        fbLoginManager.logIn(permissions: ["public_profile","email"], from: viewController) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult? = result!
                if fbloginresult!.isCancelled {
                    completionHandler(nil, nil, "", false, "Cancelled by user")
                } else if fbloginresult?.grantedPermissions != nil {
                    self.fetchProfile(completionHandler: { (result, error) in
                        Utility.shared.startLoader()
                        if let token = AccessToken.current, !token.isExpired {
                            // User is logged in, do work such as go to next view controller.
                            if let facebookData = result as? [String: Any] {
                                Constant.userProfile = ProfileDetail(facebookLogin: facebookData, cognitoId: "")
                            }

                            self.federatedSignIn(authToken: token.tokenString, authTokenKey: "graph.facebook.com") { (email, identityId, success, errorDesc) in
                                if success {
                                    completionHandler(email, Constant.userProfile, identityId, success, errorDesc)
                                } else {
                                    if email != nil {
                                        completionHandler(email, nil, nil, success, errorDesc)
                                    } else {
                                        completionHandler(nil, nil, nil, success, errorDesc)
                                    }
                                }
                            }
                        }
                    })
                } else {
                    let permissionError = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey :"Not having permisssion"])
                    completionHandler(nil, nil, nil, false, permissionError.localizedDescription)
                }
            } else {
                completionHandler(nil, nil, nil, false, error?.localizedDescription)
            }
        }
    }
//    func loginWithFaceBook(viewController: UIViewController, completionHandler: @escaping (_ email: String?, _ profile: ProfileDetail?, _ id: String?, _ result:Bool?,  _ error1: String?) -> Void) {
//        fbLoginManager.logOut()
//        fbLoginManager.logIn(permissions: ["public_profile","email"], from: viewController) { (result, error) -> Void in
//            if (error == nil){
//                let fbloginresult : LoginManagerLoginResult? = result!
//                if fbloginresult?.grantedPermissions != nil {
//                    self.fetchProfile(completionHandler: { (result, error) in
//                        Utility.shared.startLoader()
//                        if let token = AccessToken.current, !token.isExpired {
//                            // User is logged in, do work such as go to next view controller.
//                            if let facebookData = result as? [String: Any] {
//                                Constant.userProfile = ProfileDetail(facebookLogin: facebookData, cognitoId: "")
//                            }
//
//                            self.federatedSignIn(authToken: token.tokenString, authTokenKey: "graph.facebook.com") { (email, identityId, success, errorDesc) in
//                                if success {
//                                    completionHandler(email, Constant.userProfile, identityId, success, errorDesc)
//                                } else {
//                                    if email != nil {
//                                        completionHandler(email, nil, nil, success, errorDesc)
//                                    } else {
//                                        completionHandler(nil, nil, nil, success, errorDesc)
//                                    }
//                                }
//                            }
//                        }
//                    })
//                } else {
//                    let permissionError = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey :"Not having permisssion"])
//                    completionHandler(nil, nil, nil, false, permissionError.localizedDescription)
//                }
//            } else {
//                completionHandler(nil, nil, nil, false, error?.localizedDescription)
//            }
//        }
//    }
    
    //MARK: - Login With Google
    func loginWithGoogle(viewController: UIViewController, user: GIDGoogleUser, token: String?, completionHandler: @escaping (_ email: String?, _ profile: ProfileDetail?, _ id: String?, _ result:Bool?,  _ error1: String?) -> Void) {
        if token != nil {
            Constant.userProfile = ProfileDetail(googleLogin: user, cognitoId: "")
            federatedSignIn(authToken: token!, authTokenKey: "accounts.google.com") { (email, identityId, success, errorDesc) in
                if success {
                    completionHandler(email, Constant.userProfile, identityId, success, errorDesc)
                } else {
                    completionHandler(email, nil, nil, success, errorDesc)
                }
            }
        }
    }
    
    func loginWithAmazon(viewController: UIViewController, completionHandler: @escaping (_ email: String?, _ profile: ProfileDetail?, _ id: String?, _ result:Bool?,  _ error1: String?) -> Void) {
        let request = AMZNAuthorizeRequest()
        request.scopes = [AMZNProfileScope.profile(), AMZNProfileScope.postalCode()]
        
        AMZNAuthorizationManager.shared().authorize(request) { (result, userDidCancel, error) in
            if error != nil {
                // Handle errors from the SDK or authorization server.
            } else if userDidCancel {
                // Handle errors caused when user cancels login.
            } else {
                // Obtain the access token and user profile data.
                let accessToken = result?.token
                Utility.shared.startLoader()
                if accessToken != nil {
                    self.federatedSignIn(authToken: accessToken!, authTokenKey: AWSIdentityProviderLoginWithAmazon) { (email, identityId, success, errorDesc) in
                        if success {
                            if let amazonUser = result?.user {
                                Constant.userProfile = ProfileDetail(amazonLogin: amazonUser, cognitoId: identityId)
                            }
                            completionHandler(email, Constant.userProfile, identityId, success, errorDesc)
                        } else {
                            completionHandler(email, nil, nil, success, errorDesc)
                        }
                    }
                }
            }
        }
    }
    
    func federatedSignIn(authToken: String, authTokenKey: String, completionHandler: @escaping (_ email: String?, _ id: String?, _ result: Bool,  _ error1: String?) -> Void) {
        let cognitoRegion = AWSRegionType.USEast1 // Region of your Cognito Identity Pool
        let cognitoIdentityPoolId = Constant.AWS.identityPoolId
        let tokens = [authTokenKey: authToken]
        let customIdentityProvider = FacebookProvider(tokens: tokens)

        let credentialsProvider = AWSCognitoCredentialsProvider(regionType: cognitoRegion, identityPoolId: cognitoIdentityPoolId, identityProviderManager:customIdentityProvider)
        let serviceConfiguration = AWSServiceConfiguration(region: cognitoRegion, credentialsProvider: credentialsProvider)
        let userPoolConfiguration = AWSCognitoIdentityUserPoolConfiguration(clientId: Constant.AWS.clientId, clientSecret: Constant.AWS.clientSecret, poolId: Constant.AWS.poolId)
        AWSCognitoIdentityUserPool.register(with: serviceConfiguration, userPoolConfiguration: userPoolConfiguration, forKey: "E_Health_Dev")
        
        credentialsProvider.getIdentityId().continueWith { (task) in
            guard task.error == nil else {
                
                let error = task.error! as NSError
                print(error.userInfo)
                completionHandler(nil, nil, false, task.error?.localizedDescription)
                return nil
            }
            // We've got a session and now we can access AWS service via default()
            self.callGetFederatedUser(emailId: String(describing: task.result ?? "")) { (email, success, message) in
                if success! {
                    completionHandler(email, String(describing: task.result ?? ""), true, nil)
                } else {
                    completionHandler(email, String(describing: task.result ?? ""), false, message)
                }
            }
            return task
        }
    }
    
    //MARK:- Fetch profile from facebook
    func fetchProfile(completionHandler: @escaping (_ result:Any?,  _ error1: Error?) -> Void)  {
        let parameters = ["fields": "email , first_name, last_name, picture.type(large)"]
        let graphRequest:GraphRequest = GraphRequest(graphPath: "me", parameters: parameters, httpMethod: .get)
        
        graphRequest.start(completionHandler: {(connection, result, error) -> Void in
            let result = result as? NSDictionary
            if error != nil {
                completionHandler(nil,error)
            } else {
                completionHandler(result,nil)
            }
        })
    }
    
    //MARK:- Logout from facebook
    func logoutFromFacebook() {
        fbLoginManager.logOut()
    }
}

extension SocialAuth {
    
    func callGetFederatedUser(emailId: String, completion: @escaping (_ email: String?, _ bool: Bool?, _ message: String?) -> Void) {
        SocialAuthViewModel.getFederatedUserByEmail(emailID: emailId) { (email, statusCode, message) in
            if statusCode != nil {
                if statusCode == 200 {
                    completion(email, false, message)
                } else if statusCode == 404 {
                    completion(email, true, message)
                } else {
                    completion(email, false, message)
                }
            }
        }
    }
    
}

class FacebookProvider: NSObject, AWSIdentityProviderManager {
    var tokens : [String : String]?

    init(tokens: [String : String]) {
        self.tokens = tokens
    }
    
    func logins() -> AWSTask<NSDictionary> {
        if tokens != nil {
            return AWSTask(result: tokens! as NSDictionary)
        }
        return AWSTask(error:NSError(domain: "Social Login", code: -1 , userInfo: ["Social" : "No current social access token"]))
    }
}

extension SocialAuth: AMZNLWAAuthenticationDelegate {
    func requestDidSucceed(_ apiResult: APIResult!) {
        print(apiResult)
    }
    
    func requestDidFail(_ errorResponse: APIError!) {
        print(errorResponse)
    }
    
}
