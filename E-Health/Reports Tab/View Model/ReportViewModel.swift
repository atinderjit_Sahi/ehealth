//
//  ReportViewModel.swift
//  E-Health
//
//  Created by Atinder Kaur on 2/12/21.
//  Copyright © 2021 AbhiShek Chugh. All rights reserved.
//

import Foundation
import AWSMobileClient


class ShowReportViewModel: NSObject {


static func shareDatCustomerMyPdfs(parameters: [String: Any], completion: @escaping (_ response: ReportList?, _ error: String?) -> Void) {
    
    let url = Constant.URL.getReportDetailCustomer
  //  let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
    
    ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
        if let result = result as? [String: AnyObject] {
            
            if let statusCode = result["code"] as? Int {
                if statusCode == 200 {
                    if let data = result["data"] as? [[String: AnyObject]] {
                        
                        print(result)
                        
                        do
                        {
                            let jsonData = try JSONSerialization.data(withJSONObject: result, options: .prettyPrinted)
                            let model = try JSONDecoder().decode(ReportList.self, from: jsonData)
                            completion(model, nil)
                        }
                        catch
                        {
                            Utility.shared.stopLoader()

                            Utility.shared.showToast(message:  "Something went wrong")
                             //  (alrtType: .error, msg: "Json parsing error", title: kOops)
                        }
                        
                        
                        
                        
                    } else {
                        Utility.shared.stopLoader()

                        completion(nil , result["message"] as? String)
                    }
                } else {
                    Utility.shared.stopLoader()

                    completion(nil , result["message"] as? String)
                }
            } else {
                Utility.shared.stopLoader()

                completion(nil , result["message"] as? String)
            }
        } else {
            Utility.shared.stopLoader()

            completion(nil , error?.localizedDescription)
        }
    }
}
 //


static func shareDatProviderMyPdfs(str: String, completion: @escaping (_ response: ReportList?, _ error: String?) -> Void) {
    
    var url = ""
    if AWSMobileClient.default().username != nil {
        
        
        if str != "" {
            url = Constant.URL.getReportDetailProvider + AWSMobileClient.default().username! + "/" + str
        }
        else {
             url = Constant.URL.getReportDetailProvider + AWSMobileClient.default().username! } }
  
    
    ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
        if let result = result as? [String: AnyObject] {
            
            if let statusCode = result["code"] as? Int {
                if statusCode == 200 {
                    if let data = result["data"] as? [[String: AnyObject]] {
                        print(result)
                        
                        do
                        {
                            let jsonData = try JSONSerialization.data(withJSONObject: result, options: .prettyPrinted)
                            let model = try JSONDecoder().decode(ReportList.self, from: jsonData)
                            completion(model, nil)
                        }
                        catch
                        {
                            Utility.shared.stopLoader()

                            Utility.shared.showToast(message:  "Something went wrong")
                             //  (alrtType: .error, msg: "Json parsing error", title: kOops)
                        }
                        
                        
                        
                        
                    } else {
                        Utility.shared.stopLoader()

                        completion(nil , result["message"] as? String)
                    }
                } else {
                    Utility.shared.stopLoader()

                    completion(nil , result["message"] as? String)
                }
            } else {
                Utility.shared.stopLoader()

                completion(nil , result["message"] as? String)
            }
        } else {
            Utility.shared.stopLoader()

            completion(nil , error?.localizedDescription)
        }
    }
}
}
