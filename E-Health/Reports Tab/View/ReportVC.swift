//
//  ReportVC.swift
//  E-Health
//
//  Created by Atinder Kaur on 2/12/21.
//  Copyright © 2021 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient


class ReportVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    var report_List = [ReportListData]()
    var minorId : String = ""
    var senderName: String = ""
    var selectedCareTakerReport : CaretakerModel?
    @IBOutlet weak var searchBar: UISearchBar!
    var searchText = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.setSearchBarUI()
        tblView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Constant.userProfile?.family_name == "provider" {
            getDataForProvider ()
        }
        else{
            getDataForCustomer ()
        }
    }
    
    func getDataForProvider () {
        Utility.shared.startLoader()
        
        ShowReportViewModel.shareDatProviderMyPdfs(str: ""){ (reportlist, error) in
            if error == nil {
                Utility.shared.stopLoader()
                self.report_List.removeAll()
                self.report_List = reportlist?.data ?? [ReportListData]()
                self.tblView.reloadData()
            } else {
                Utility.shared.stopLoader()
                Utility.shared.showToast(message:  "Something went wrong")
            }
        }
    }
    
    func getDataForCustomer () {
        selectedCareTakerReport =  Constant.selectedCareTaker
        
        var params = [String: Any]()
        
        if selectedCareTakerReport != nil {
            
            if selectedCareTakerReport?.minor_id != nil {
                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTakerReport?.minor_id ?? 0, "reciever_name": ""] as [String : Any]
                minorId = String(selectedCareTakerReport?.minor_id ?? 0)
                senderName = selectedCareTakerReport?.name ?? ""
            } else {
                params = ["cognito_user_id": selectedCareTakerReport?.caretakerSub ?? "0", "minor_id": 0,"reciever_name": ""] as [String : Any]
                minorId = "0"
                senderName = Constant.userProfile?.name ?? ""
            }
        } else {
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "reciever_name": ""] as [String : Any]
            minorId = "0"
            senderName = Constant.userProfile?.name ?? ""
        }
        
        
        Utility.shared.startLoader()
        
        ShowReportViewModel.shareDatCustomerMyPdfs(parameters: params) { (reportlist, error) in
            if error == nil {
                Utility.shared.stopLoader()

                self.report_List = reportlist?.data ?? [ReportListData]()
                self.tblView.reloadData()
            }
            else {
                Utility.shared.stopLoader()

                Utility.shared.showToast(message:  "Something went wrong")
            }
        
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if self.searchBar != nil &&  self.tblView != nil {
        self.searchBar.resignFirstResponder()
        
        }
    }
    
    
    @IBAction func actionViewReport(_ sender: UIButton) {
        guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: WebViewController.className) as? WebViewController else {return}
         vc.webUrlString = report_List[sender.tag].file
        navigationController?.pushViewController(vc, animated: true)
    }
    

}


extension ReportVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if report_List.count > 0 {
         return report_List.count
            }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell( withIdentifier: "DocumentListingTableViewCell", for: indexPath) as? DocumentListingTableViewCell else {
            fatalError()
        }
        if report_List.count > 0 {
            
            
            let date = report_List[indexPath.row].createdAt?.convertToDateString(currentDateFormat: "yyyy-MM-dd HH:mm:ss", requiredDateFormat: Constant.dateFormat)
            cell.dateAddedLabel.text = "Added on " + (date ?? "")
                cell.scanUploadLabel.text = "Through Upload"
            
            if Constant.userProfile?.family_name == "provider" {
                cell.reportTitleLabel.text = report_List[indexPath.row].sender_name
            }
            else {
            cell.reportTitleLabel.text = report_List[indexPath.row].recieverName
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 113
    }
}
//MARK:- Search Bar Delegates
extension ReportVC: UISearchBarDelegate {
    
 
   
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
       
        
        
        let trimmedString = searchText.trimmingCharacters(in: .whitespaces)
        self.searchText = trimmedString
        self.report_List.removeAll()
        tblView.reloadData()
        
        if trimmedString.count >= 2 {
            
            if Constant.userProfile?.family_name == "provider" {
                
                ShowReportViewModel.shareDatProviderMyPdfs(str: self.searchText){ (reportlist, error) in
                    if error == nil {

                        self.report_List = reportlist?.data ?? [ReportListData]()
                        self.tblView.reloadData()
                    }
                    else {

                        Utility.shared.showToast(message:  "Something went wrong")
                    }
                
                }
                
            }
            else {
        
        var params = [String: Any]()
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": minorId, "reciever_name": self.searchText] as [String : Any]
        
        ShowReportViewModel.shareDatCustomerMyPdfs(parameters: params) { (reportlist, error) in
            if error == nil {

                self.report_List = reportlist?.data ?? [ReportListData]()
                self.tblView.reloadData()
            }
            else {

                Utility.shared.showToast(message:  "Something went wrong")
            }
        
        }
            }
                
        }
        
        else{
            if Constant.userProfile?.family_name == "provider" {
                getDataForProvider ()
            }
            else{
                getDataForCustomer ()
            }
      }
        tblView.reloadData()

        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
                        
    func deleteProviderApi(recordId: Int?, recordIndex: IndexPath) {
       
    }
}
