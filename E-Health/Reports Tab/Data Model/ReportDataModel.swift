//
//  ReportDataModel.swift
//  E-Health
//
//  Created by Atinder Kaur on 2/12/21.
//  Copyright © 2021 AbhiShek Chugh. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct ReportList: Codable {
    let data: [ReportListData]?
    let message, technicalMessage: String?
    let customCode, code: Int?

    enum CodingKeys: String, CodingKey {
        case data, message
        case technicalMessage = "technical_message"
        case customCode = "custom_code"
        case code
    }
}

// MARK: - ReportListData
struct ReportListData: Codable {
    let id, recieverID: Int?
    let recieverName: String?
    let file: String?
    let createdAt, updatedAt: String?
    let sender_id: String?
    let minor_id: String?
    let sender_name: String?

    enum CodingKeys: String, CodingKey {
        case id
        case recieverID = "reciever_id"
        case recieverName = "reciever_name"
        case file
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case sender_id
        case minor_id
        case sender_name
        
    }
}
