//
//  AllCareTakerTableViewCell.swift
//  E-Health
//
//  Created by Akash Dhiman on 28/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

protocol AllCareTakerTableViewCellDelegate {
    func editCareTaker(item: CaretakerModel)
    func deleteCareTaker(item: CaretakerModel)
}

class AllCareTakerTableViewCell: UITableViewCell {

    ///Properties
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var careTakerNameLabel: UILabel!
    @IBOutlet weak var careTakerRelationLabel: UILabel!
    @IBOutlet weak var careTakerRequestTimeStampLabel: UILabel!
    @IBOutlet weak var careTakerImageView: UIImageView!
    
    ///Reference of delegate
    var delegate : AllCareTakerTableViewCellDelegate?
    ///Reference of of item passed
    var item: CaretakerModel? = CaretakerModel() {
        didSet {
            guard item != nil else {
                return
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.editCaretaker))
            careTakerImageView.addGestureRecognizer(tap)
            
            self.careTakerNameLabel.text = item?.name
            self.careTakerRelationLabel.text = item?.relationship_name
            
            if let profileImage = item?.profile_image {
                if profileImage != "" {
                    let imageUrl = profileImage //Constant.URL.baseURL + 
                    DispatchQueue.main.async {
                        if let url = URL(string: imageUrl) {
                            self.careTakerImageView.kf.indicatorType = .activity
                            self.careTakerImageView.kf.setImage(with: url)
                            self.careTakerImageView.layer.cornerRadius = self.careTakerImageView.frame.height/2
                            self.careTakerImageView.clipsToBounds = true
                        }
                    }
                }
                
            }
            
            if item?.status == 2 {
                self.careTakerRequestTimeStampLabel.text = "REJECTED"
                self.editButton.isHidden = true
            } else {
                self.editButton.isHidden = false
                let date = item?.created_at?.convertToDateString(currentDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", requiredDateFormat: Constant.dateFormat)
                self.careTakerRequestTimeStampLabel.text = "Added on " + (date ?? "")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func editButtonAction(_ sender: UIButton) {
        delegate?.editCareTaker(item: item ?? CaretakerModel())
    }
    
    @IBAction func deleteButtonAction(_ sender: UIButton) {
        delegate?.deleteCareTaker(item: item ?? CaretakerModel())
    }
    
    @objc func editCaretaker() {
        if item?.status == 2 {
            return
        }
        delegate?.editCareTaker(item: item ?? CaretakerModel())
    }
}
