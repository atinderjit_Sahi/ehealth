//
//  SpecialityTableViewCell.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 6/23/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

protocol CountryCellProtocol {
    func buttonCheckBox(sender: UIButton)
}
class SpecialityTableViewCell: UITableViewCell {

    //MARK:  Outlets
    @IBOutlet weak var buttonCheckBox: UIButton!
    @IBOutlet weak var countryLabel: UILabel!
    
    //MARK: - Properties
    var delegate: CountryCellProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func buttonCheckBoxTapped(_ sender: UIButton) {
        delegate?.buttonCheckBox(sender: sender)
    }
}
