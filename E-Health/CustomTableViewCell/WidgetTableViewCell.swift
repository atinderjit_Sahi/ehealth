//
//  WidgetTableViewCell.swift
//  E-Health
//
//  Created by Akash Dhiman on 01/06/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

protocol WidgetProtocol {
    func selectUnselectWidget(sender: UIButton)
}

class WidgetTableViewCell: UITableViewCell {

    @IBOutlet weak var widgetImageView: UIImageView!
    @IBOutlet weak var widgetLabel: UILabel!
    @IBOutlet weak var widgetSwitch: UIButton!
    
    var delegate: WidgetProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func switchButtonAction(_ sender: UIButton) {
        delegate?.selectUnselectWidget(sender: sender)
    }
}
