//
//  CountryCellTableViewCell.swift
//  E-Health
//
//  Created by Akash Dhiman on 25/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class CountryCellTableViewCell: UITableViewCell {

    //MARK:  Outlets
    @IBOutlet weak var countryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
