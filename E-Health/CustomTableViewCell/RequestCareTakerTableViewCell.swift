//
//  RequestCareTakerTableViewCell.swift
//  E-Health
//
//  Created by Akash Dhiman on 28/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

protocol RequestCareTakerTableViewCellDelegate {
    func acceptCareTakerRequest(item: CaretakerModel)
    func rejectCareTakerRequest(item: CaretakerModel)
    func discardCareTakerRequest(item: CaretakerModel)
}

class RequestCareTakerTableViewCell: UITableViewCell {

    @IBOutlet weak var discardButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var careTakerNameLabel: UILabel!
    @IBOutlet weak var careTakerRelationLabel: UILabel!
    @IBOutlet weak var careTakerRequestTimeStampLabel: UILabel!
    @IBOutlet weak var careTakerImageView: UIImageView!
    
    ///Reference of delegate
    var delegate : RequestCareTakerTableViewCellDelegate?
    var isSender = false
    
    ///Reference of of item passed
    var item: CaretakerModel? = CaretakerModel() {
        didSet {
            guard item != nil else {
                return
            }
            self.careTakerNameLabel.text = item?.name
            if !isSender {
                self.careTakerNameLabel.text = item?.sender_name
            }
            self.careTakerRelationLabel.text = item?.relationship_name
            let date = item?.created_at?.convertToDateString(currentDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", requiredDateFormat: Constant.dateFormat)
            self.careTakerRequestTimeStampLabel.text = "Received on " + (date ?? "")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func acceptButtonAction(_ sender: UIButton) {
        delegate?.acceptCareTakerRequest(item: item ?? CaretakerModel())
    }
    
    @IBAction func rejectButtonAction(_ sender: UIButton) {
        delegate?.rejectCareTakerRequest(item: item ?? CaretakerModel())
    }
    
    @IBAction func discardButtonAction(_ sender: UIButton) {
        delegate?.discardCareTakerRequest(item: item ?? CaretakerModel())
    }
}
