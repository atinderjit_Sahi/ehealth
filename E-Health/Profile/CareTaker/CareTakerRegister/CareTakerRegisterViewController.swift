//
//  CareTakerRegisterViewController.swift
//  E-Health
//
//  Created by Akash Dhiman on 29/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class CareTakerRegisterViewController: UIViewController {

    @IBOutlet weak var careTakerTypeTextField: ExtendedTextField!
    @IBOutlet weak var careTakerNameTextField: ExtendedTextField!
    @IBOutlet weak var careTakerRelationshipTextField: ExtendedTextField!
    @IBOutlet weak var careTakerFirstNameTextField: ExtendedTextField!
    @IBOutlet weak var careTakerMiddleNameTextField: ExtendedTextField!
    @IBOutlet weak var careTakerLastNameTextField: ExtendedTextField!
    @IBOutlet weak var careTakerEmailTextField: ExtendedTextField!
    @IBOutlet weak var careTakerResidenceTextField: ExtendedTextField!
    @IBOutlet weak var careTakerContactTextField: ExtendedTextField!
    @IBOutlet weak var enterPasswortTextField: ExtendedTextField!
    @IBOutlet weak var confirmPasswortTextField: ExtendedTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func dotButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        switch sender.tag {
        case 101:
            if sender.isSelected{
                
            } else {
                
            }
            break
        case 102:
            if sender.isSelected{
                
            } else {
                
            }
            break
        default:
            break
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submitButtonAction(_ sender: Any) {
        
    }
}
