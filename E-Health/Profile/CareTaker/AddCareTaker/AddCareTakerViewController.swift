//
//  AddCareTakerViewController.swift
//  E-Health
//
//  Created by Akash Dhiman on 01/06/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient
import DropDown
import IQKeyboardManagerSwift

class AddCareTakerViewController: UIViewController {

    @IBOutlet weak var enterNameTextField: ExtendedTextField!
    @IBOutlet weak var ageTextField: ExtendedTextField!
    @IBOutlet weak var relationShipTextField: ExtendedTextField!
    @IBOutlet weak var emailTextField: ExtendedTextField!
    @IBOutlet weak var txtfieldTelephone: ExtendedTextField!
    @IBOutlet weak var txtfieldType: ExtendedTextField!
    
    
    var relations = [Relation]()
    var selectedRelationship = Relation()
    var typeList = [CareGiverTyp]()
    var selectedTyp = CareGiverTyp()
    var datePicker = UIDatePicker()
    
    //MARK:- View Hierarchy
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizeTextField()
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        relations = Constant.masterData?.relationship ?? [Relation]()
        typeList = Constant.masterData?.careGiverType ?? [CareGiverTyp]()
    }
    
    func customizeTextField() {
//        relationShipTextField.setRightImage(rightImage: UIImage(named: "dropDown")!)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.previousNextDisplayMode = .default
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        addCarataker()
    }
    
    @IBAction func relationshipDropDown(_ sender: Any) {
        var relationsName = [String]()
        for relationship in relations {
            relationsName.append(relationship.name ?? "")
        }
        IQKeyboardManager.shared.resignFirstResponder()
        self.initdropDown(textField: relationShipTextField, dataSource: relationsName)
    }
    
    
    @IBAction func actionSelectType(_ sender: UIButton) {
        var typeName = [String]()
        for type in typeList {
            typeName.append(type.name ?? "")
        }
        IQKeyboardManager.shared.resignFirstResponder()
        self.initdropDown(textField: txtfieldType, dataSource: typeName)
    }
    
}

extension AddCareTakerViewController {
    func addCarataker() {
        IQKeyboardManager.shared.resignFirstResponder()
        if !validateTextFields() {
            return
        }
        
        Utility.shared.startLoader()
        var params = [String: AnyObject]()
        
        var senderFullName = String()
        let name = Constant.userProfile?.name
        let middleName = Constant.userProfile?.middleName
        if middleName != "" && middleName != nil {
            let myStringArr = name?.components(separatedBy: " ")
            let firstName = (myStringArr?[0] ?? "")
            var lastName = ""
            if myStringArr?.count == 2 {
                lastName = (myStringArr?[1] ?? "")
            }
            senderFullName = firstName + " " + (middleName ?? "") + " " + lastName
        } else {
            senderFullName = name ?? ""
        }
        
        //GET AGE OF CARETAKER FROM DATE OF BIRTH
        let age = self.calculateAge()
        let dob = ageTextField.text  //(ageTextField.text)?.convertToDateString(currentDateFormat: Constant.dateFormat, requiredDateFormat: "yyyy-MM-dd")
        
        //CREATE PARAMS TO ADD CARETAKER
        let userId = AWSMobileClient.default().username ?? ""
        params["user_id"] = userId as AnyObject
        params["relationship_id"] = selectedRelationship.id as AnyObject
        params["name"] = enterNameTextField.text as AnyObject
//        params["age"] = ageTextField.text as AnyObject
        params["dob"] = dob as AnyObject //ageTextField.text as AnyObject
        params["minor_id"] = "" as AnyObject
        params["fitness_device_ids"] = "" as AnyObject
        params["health_device_ids"] = "" as AnyObject
        
        //IF MINOR CARETAKER
        if age < 18 {
            params["care_taker_type_id"] = 1 as AnyObject
        }
        //IF ADULT CARETAKER
        else {
            params["care_taker_type_id"] = 2 as AnyObject
            params["email_address"] = emailTextField.text as AnyObject
        }
        params["sender_name"] = senderFullName as AnyObject
      params["telephone"] = txtfieldTelephone.text as AnyObject
  params["caregiver_type"] = txtfieldType.text as AnyObject
        
        CaretakerViewModel.addCaretakerAPI(params: params) { (_ result, _ message, _ error)  in
            Utility.shared.stopLoader()
            if result {
                Utility.shared.showAlert(message: message ?? "", controller: self)
//                self.resetScreen()
                self.navigationController?.popViewController(animated: true)
            }
            //Handling error
            else if error != nil {
                Utility.shared.showToast(message: error!)
            }
        }
    }
}

extension AddCareTakerViewController {
    func initdropDown (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.selectedRelationship = self.relations[index]
                if textField == txtfieldType {
                    self.txtfieldType.text = item }
                else {
                    self.relationShipTextField.text = self.selectedRelationship.name
                }
                dropDown.hide()
            }
            dropDown.show()
        }
    }
    
    func resetScreen() {
        emailTextField.text?.removeAll()
        enterNameTextField.text?.removeAll()
        ageTextField.text?.removeAll()
        relationShipTextField.text?.removeAll()
        emailTextField.isHidden = false
    }
    
    func calculateAge() -> Int {
        //GET AGE OF CARETAKER FROM DATE OF BIRTH
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat //"yyyy-MM-dd" //"dd MMM, yyyy"
        var birthday = Date()
        if ageTextField.text != "" {
            birthday = formater.date(from: ageTextField.text!) ?? Date()
        }
        let calendar = Calendar.current

        let ageComponents = calendar.dateComponents([.year], from: birthday, to: Date())
        let age = ageComponents.year!
        return age
    }
}

extension AddCareTakerViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == relationShipTextField {
//            var raltionsName = [String]()
//            for relationship in relations {
//                raltionsName.append(relationship.name ?? "")
//            }
//            IQKeyboardManager.shared.resignFirstResponder()
//            self.initdropDown(textField: textField, dataSource: raltionsName)
            return false
        }
        
        //DATE OF BIRTH DATE PICKER
        if textField == ageTextField {
            
            //            self.view.endEditing(true)
            self.showDatePicker()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == ageTextField {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 3, newCharacter: string)
            return result
        }
        
        if textField == enterNameTextField {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit:20, newCharacter: string)
            if !result {
                return result
            }
            
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        
       
        
        if textField == txtfieldTelephone {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: txtfieldTelephone.text! as NSString, limit:12, newCharacter: string)
            return result
        }
        else{
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit:50, newCharacter: string)
            return result
        }
        
        
    }
}

extension AddCareTakerViewController {
    func validateTextFields() -> Bool {
        let objValidate   = Validations()
        
        //First name field Validation
        var (status,message) = objValidate.validateField(enterNameTextField)
        if !status{
            switch message {
            case "Empty field":
                Utility.shared.showToast(message: "Please enter Caregiver's name")
                break
                
            case "Short length text":
                Utility.shared.showToast(message: "Caregiver's name must be atleast \(enterNameTextField.minLength) characters")
                break
                
            case "Long length text":
                Utility.shared.showToast(message: "Caregiver's name must be maximum \(enterNameTextField.maxLength) characters")
                break
                
            default:
                break
            }
            return false
        }
        
        //Age Text Field Validation
        (status,message) = objValidate.validateField(ageTextField)
        if !status{
            switch message {
            case "Empty field":
                Utility.shared.showToast(message: "Please enter Caregiver's date of birth")
                break
                
            default:
                break
            }
            return false
        }
        
//        let age = Int(ageTextField.text!)
//        if age! < 18 {
//            Utility.shared.showToast(message: "Caregiver's age cannot be less than 18 years")
//            return false
//        }
//        if age! > 120 {
//            Utility.shared.showToast(message: "Caregiver's age cannot be greater than 120 years")
//            return false
//        }
        
        //Relationship Text Field Validation
        (status,message) = objValidate.validateField(relationShipTextField)
        if !status{
            switch message {
            case "Empty field":
                Utility.shared.showToast(message: "Please select your Relationship with Caregiver")
                break
                
            default:
                break
            }
            return false
        }
        
        //If age is greater than 18, Email Text field Validation
        let age = self.calculateAge()
        if age >= 18 {
            (status,message) = objValidate.validateEmailField(emailTextField)
            if !status{
                switch message {
                case "Empty field":
                    Utility.shared.showToast(message: "Please enter Caregiver's Email")
                    break
                    
                case "Invalid Email format":
                    Utility.shared.showToast(message: "Please enter a valid Email")
                    break
                    
                default:
                    break
                }
                return false
            }
            
            if emailTextField.text == Constant.userProfile?.email {
                Utility.shared.showToast(message: "Caregiver's Email Id cannot be same as your Email Id")
                return false
            }
        }
            

        return true
    }
}

//MARK:- Date Picker Methods
extension AddCareTakerViewController {
    func showDatePicker(){
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat //"yyyy-MM-dd"//"dd MMM, yyyy"
        var date = Date()
        if ageTextField.text != "" {
            date = formater.date(from: ageTextField.text!) ?? Date()
        }
        
        
        let screenWidth = UIScreen.main.bounds.width
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -110, to: Date())
        datePicker.maximumDate = Date() //Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datePicker.setDate(date, animated: true)
        
        if #available(iOS 14.0, *)
        {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()

//            datePicker.datePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        
        ageTextField.inputAccessoryView = toolbar
        ageTextField.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = Constant.dateFormat //"yyyy-MM-dd" //"dd MMM, yyyy"
        ageTextField.text = formatter.string(from: datePicker.date)
        let age = self.calculateAge()
        if age < 18 {
            emailTextField.isHidden = true
        } else {
            emailTextField.isHidden = false
        }
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}
