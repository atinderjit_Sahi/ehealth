//
//  CaretakerModel.swift
//  E-Health
//
//  Created by Prabjot Kaur on 6/2/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class Caretakers {
    var caretakersList: [CaretakerModel] = []
    
    init() {
        
    }
    
    init(value: [[String: Any]]) {
        for data in value{
            let dataToAppend = CaretakerModel(value: data)
            caretakersList.append(dataToAppend)
        }
    }
}


class CaretakerModel {
    var id: Int?
    var userId: Int?
    var care_taker_type_id: Int?
    var caretakerId: Int?
    var userSub: String?
    var caretakerSub: String?
    var status: Int?
    var created_at: String?
    var updated_at: String?
    var relationship_id: Int?
    var email_address: String?
    var name: String?
    var relationship_name: String?
    var age: String?
    var sender_name: String?
    var minor_id: Int?
    var dob: String?
    var healthDevice: String?
    var fitnessDevice: String?
    var profile_image: String?
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.userId = value["userId"] as? Int
        self.caretakerId = value["caretakerId"] as? Int
        self.caretakerSub = value["caretakerSub"] as? String
        self.userSub = value["userSub"] as? String
        self.care_taker_type_id = value["care_taker_type_id"] as? Int
        self.status = value["status"] as? Int
        self.created_at = value["created_at"] as? String
        self.updated_at = value["updated_at"] as? String
        self.relationship_id = value["relationship_id"] as? Int
        self.email_address = value["email_address"] as? String
        self.name = value["name"] as? String
        self.relationship_name = value["relationship_name"] as? String
        self.age = value["age"] as? String
        self.sender_name = value["sender_name"] as? String
        self.minor_id = value["minor_id"] as? Int
        self.dob = value["dob"] as? String
        self.healthDevice = value["health_device_ids"] as? String
        self.fitnessDevice = value["fitness_device_ids"] as? String
        self.profile_image = value["profile_image"] as? String
    }
}
