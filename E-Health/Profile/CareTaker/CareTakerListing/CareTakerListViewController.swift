//
//  CareTakerListViewController.swift
//  E-Health
//
//  Created by Akash Dhiman on 28/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

class CareTakerListViewController: UIViewController {

    @IBOutlet weak var careTakerTableView: UITableView!
    @IBOutlet weak var tabBarScroller: ScrollPager!
    @IBOutlet weak var addDataButton: UIButton!
    
    var isAllSelected = true
    var awsUserID = String()
    
    var allCaretakersList = [CaretakerModel]()
    var pendingCaretakersList = [CaretakerModel]()
    
    let dispatchGroup = DispatchGroup()
    
    //MARK:- View hierarchy
    override func viewDidLoad() {
        super.viewDidLoad()
        careTakerTableView.register(UINib(nibName: "AllCareTakerTableViewCell", bundle: nil), forCellReuseIdentifier: "AllCareTakerTableViewCell")
        careTakerTableView.register(UINib(nibName: "RequestCareTakerTableViewCell", bundle: nil), forCellReuseIdentifier: "RequestCareTakerTableViewCell")
        careTakerTableView.tableFooterView = UIView()
        tabBarScroller.delegate = self
        tabBarScroller.font = UIFont(name: "Montserrat-Medium", size: 16)!
        tabBarScroller.selectedFont = UIFont(name: "Montserrat-Medium", size: 16)!
        tabBarScroller.viewHeight = self.view.frame.height
        tabBarScroller.addSegmentsWithTitles(segmentTitles: ["All", "Requests"])
        
        awsUserID = AWSMobileClient.default().username ?? ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        addDataButton.isHidden = true
        refreshList()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = false
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func addCareTakerButtonAction(_ sender: Any) {
        moveToAddCaregiverScreen()
    }
    
    @IBAction func addDataButtonTapped(_ sender: Any) {
        moveToAddCaregiverScreen()
    }
    
    func moveToAddCaregiverScreen() {
        guard let vc = UIStoryboard.init(name: StoryboardNames.Caregiver, bundle: Bundle.main).instantiateViewController(withIdentifier: AddCareTakerViewController.className) as? AddCareTakerViewController else {return}
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- API Methods to get Listing data
extension CareTakerListViewController {
    func refreshList() {
        Utility.shared.startLoader()
        getAllCaretakersList()
        getPendingCaretakersList()
        
        dispatchGroup.notify(queue: .main) { [unowned self] in
            Utility.shared.stopLoader()
            self.careTakerTableView.reloadData()
        }
    }
    
    func getAllCaretakersList() {
        self.dispatchGroup.enter()
//        let userId = AWSMobileClient.default().username ?? ""
        
        CaretakerViewModel.getAllCaretakersList(userID: awsUserID) { (response, error) in
            self.dispatchGroup.leave()
            self.allCaretakersList.removeAll()
            if response != nil {
//                self.addDataButton.isHidden = false
                self.allCaretakersList = response!
            } else {
//                self.addDataButton.isHidden = true
                if self.isAllSelected {
                    Utility.shared.showToast(message: error ?? "")
                }
            }
        }
    }
    
    func getPendingCaretakersList() {
        self.dispatchGroup.enter()
//        let userId = AWSMobileClient.default().username ?? ""
        
        CaretakerViewModel.getPendingCaretakers(userID: awsUserID) { (response, error) in
            self.dispatchGroup.leave()
            self.pendingCaretakersList.removeAll()
            if response != nil {
//                self.addDataButton.isHidden = false
                self.pendingCaretakersList = response!
            } else {
//                self.addDataButton.isHidden = true
                if !self.isAllSelected {
                    Utility.shared.showToast(message: error ?? "")
                }
            }
        }
    }
}

extension CareTakerListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isAllSelected {
            if allCaretakersList.count == 0 {
                addDataButton.isHidden = false
            }else {
                addDataButton.isHidden = true
            }
            return allCaretakersList.count //3
        }
        if pendingCaretakersList.count == 0 {
            addDataButton.isHidden = false
        }else {
            addDataButton.isHidden = true
        }
        return pendingCaretakersList.count //7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isAllSelected {
            guard let cell = tableView.dequeueReusableCell( withIdentifier: "AllCareTakerTableViewCell", for: indexPath) as? AllCareTakerTableViewCell else {
                fatalError()
            }
            cell.delegate = self
            cell.item = allCaretakersList[indexPath.row]
            
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell( withIdentifier: "RequestCareTakerTableViewCell", for: indexPath) as? RequestCareTakerTableViewCell else {
                fatalError()
            }
            cell.delegate = self
            
            if awsUserID == self.pendingCaretakersList[indexPath.row].caretakerSub ?? "" {
                cell.discardButton.isHidden = true
                cell.acceptButton.isHidden = false
                cell.rejectButton.isHidden = false
            } else {
                cell.isSender = true
                cell.discardButton.isHidden = false
                cell.acceptButton.isHidden = true
                cell.rejectButton.isHidden = true
            }
            cell.item = self.pendingCaretakersList[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension CareTakerListViewController: ScrollPagerDelegate {
    func scrollPager(scrollPager: ScrollPager, changedIndex: Int) {
        switch changedIndex {
            
        case 0:
            isAllSelected = true
        case 1:
            isAllSelected = false
        default:
            isAllSelected = true
        }
        self.refreshList()
        careTakerTableView.reloadData()
    }
}

extension CareTakerListViewController: AllCareTakerTableViewCellDelegate{
    func editCareTaker(item: CaretakerModel) {
//        if item.email_address == "" || item.email_address == nil{
//            Utility.shared.showToast(message: "User profile not available")
//            return
//        }
        let vc = UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: "MyProfileViewController") as? MyProfileViewController
        vc?.isCaretakerProfile = true
        if vc?.caretakerDetails != nil {
            print("not nil")
        }
        removeProfileControllerFromStack()
        vc?.caretakerDetails = item
        vc?.caretakerEmail = item.email_address!
        vc?.caretakerSub = item.caretakerSub ?? ""
        vc?.caretakerRealtion = item.relationship_name ?? ""
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func removeProfileControllerFromStack() {
        if var viewControllers = self.navigationController?.viewControllers
        {
            for controller in viewControllers
            {
                if controller is MyProfileViewController
                {
                    if let index = viewControllers.index(of: controller) {
                        viewControllers.remove(at: index)
                        self.navigationController?.viewControllers = viewControllers
                    }
                }
            }
        }
    }
    
    func deleteCareTaker(item: CaretakerModel) {
        Utility.shared.showCustomAlertWithCompletion("Delete Caregiver", message: "Are you sure you want to delete the caregiver?", withButtonTypes: ["No", "Yes"], withAlertCompletionBlock: { (alertAction, buttonTitle) in
            
            if buttonTitle == "Yes" {
                Utility.shared.startLoader()
                CaretakerViewModel.deleteCaretakerAPI(caretakerID: item.id ?? 0) { (status, message, error) in
                    if status {
                        Utility.shared.showToast(message: message ?? "Caregiver account deleted!")
                        self.refreshList()
                    } else {
                        Utility.shared.stopLoader()
                        Utility.shared.showToast(message: error ?? "Something went wrong")
                    }
                }
            }
        })
    }
}

extension CareTakerListViewController: RequestCareTakerTableViewCellDelegate{
    func acceptCareTakerRequest(item: CaretakerModel) {
        Utility.shared.startLoader()
        let userId = String(item.id ?? 0)
        
        let params: [String: AnyObject] = ["status": 1 as AnyObject]
        
        CaretakerViewModel.acceptRejectCaretaker(userId: userId, params: params) { (status, message, error) in
            if status {
                Utility.shared.showToast(message: message ?? "")
                self.refreshList()
            } else {
                Utility.shared.stopLoader()
                Utility.shared.showToast(message: message ?? "Something went wrong")
            }
        }
    }
    
    func rejectCareTakerRequest(item: CaretakerModel) {
        Utility.shared.startLoader()
        let userId = String(item.id ?? 0)
        
        let params: [String: AnyObject] = ["status": 2 as AnyObject]
        
        CaretakerViewModel.acceptRejectCaretaker(userId: userId, params: params) { (status, message, error) in
            if status {
                Utility.shared.showToast(message: message ?? "")
                self.refreshList()
            } else {
                Utility.shared.stopLoader()
                Utility.shared.showToast(message: message ?? "Something went wrong")
            }
        }
    }
    
    func discardCareTakerRequest(item: CaretakerModel) {
        Utility.shared.startLoader()
        let userId = String(item.id ?? 0)
        
        let params: [String: AnyObject] = ["status": 3 as AnyObject]
        
        CaretakerViewModel.acceptRejectCaretaker(userId: userId, params: params) { (status, message, error) in
            if status {
                Utility.shared.showToast(message: message ?? "")
                self.refreshList()
            } else {
                Utility.shared.stopLoader()
                Utility.shared.showToast(message: message ?? "Something went wrong")
            }
        }
    }
}
