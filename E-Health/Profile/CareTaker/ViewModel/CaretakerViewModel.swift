//
//  CaretakerViewModel.swift
//  E-Health
//
//  Created by Prabjot Kaur on 6/2/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class CaretakerViewModel: NSObject {
    
    //Get pending Care takers by user ID
    static func getAllCaretakersList(userID: String, completion: @escaping (_ response: [CaretakerModel]?, _ error: String?) -> Void) {
        let url = Constant.URL.getCaretakersByUserID + userID
        
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                if let data = result["data"] as? [String: AnyObject] {
                    if let careTakers = data["care_taker"] as? [[String: AnyObject]] {
                        var caretakerList = [CaretakerModel]()
                        for item in careTakers {
                            caretakerList.append(CaretakerModel(value: item))
                        }
                        completion(caretakerList, nil)
                    } else {
                       completion(nil, result["message"] as? String)
                    }
                }
                else {
                   completion(nil, result["message"] as? String)
                }
                
            } else {
                completion(nil, error?.localizedDescription)
            }
        }
    }
    
    //Get Pending Caretakers API
    static func getPendingCaretakers(userID: String, completion: @escaping (_ response: [CaretakerModel]?, _ error: String?) -> Void) {
        let url = Constant.URL.getPendingCaretakersByID + userID
        
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            
            if let result = result as? [String: AnyObject] {
                if let data = result["data"] as? [String: AnyObject] {
                    if let careTakers = data["care_taker"] as? [[String: AnyObject]] {
                        var caretakerList = [CaretakerModel]()
                        for item in careTakers {
                            caretakerList.append(CaretakerModel(value: item))
                        }
                        completion(caretakerList, nil)
                    } else {
                       completion(nil, result["message"] as? String)
                    }
                }
                else {
                   completion(nil, result["message"] as? String)
                }
                
            } else {
                completion(nil, error?.localizedDescription)
            }
        }
    }
    
    
    //Delete Care Taker API
    static func deleteCaretakerAPI(caretakerID: Int, completion: @escaping (_ status: Bool, _ message: String?, _ error: String?) -> Void) {
        let url = Constant.URL.deleteCaretaker + String(caretakerID)
        
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
         ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            
            if let result = result as? [String: AnyObject] {
                let message = result["message"] as! String
                completion(true, message, nil)
            } else {
                completion(false, nil, error?.localizedDescription)
            }
                
            }
    }
    
    //Add Caretaker API
    static func addCaretakerAPI(params: [String: AnyObject], completion: @escaping (_ status: Bool, _ message: String?, _ error: String?) -> Void) {
        let url = Constant.URL.addCaretakerV2
        
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params, method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                let code = result["code"] as? Int
                if 200 == code {
                    completion(true, result["message"] as? String, nil)
                } else {
                    completion(false, nil, result["message"] as? String)
                }
            } else {
                completion(false, nil,error?.localizedDescription)
            }
        }
    }
    
    static func acceptRejectCaretaker(userId: String, params: [String: AnyObject], completion: @escaping (_ status: Bool, _ message: String?, _ error: String?) -> Void) {
        let url = Constant.URL.acceptRejectCaretaker + userId
        
         let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params, method: .patch) { (result, error) in
            if let result = result as? [String: AnyObject] {
                completion(true, result["message"] as? String, nil)
            } else {
                completion(false, nil,error?.localizedDescription)
            }
        }
    }
}
