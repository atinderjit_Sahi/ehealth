//
//  SetWidgetViewController.swift
//  E-Health
//
//  Created by Akash Dhiman on 01/06/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

class SetWidgetViewController: UIViewController {

    //Consumer Wigets
    var imageArray = [UIImage(named: "widget_profieDetails"), UIImage(named: "widget_providerList"), UIImage(named: "widget_insurance"), UIImage(named: "widget_allergies"), UIImage(named: "widget_medical"), UIImage(named: "widget_immunization"), UIImage(named: "widget_medicalDocument"), UIImage(named: "widget_labReport"), UIImage(named: "widget_caretakerList"), UIImage(named: "widget_vitalStats")]
    /*var imageArray = [UIImage(named: "widget_allergies"),UIImage(named: "widget_immunization"),UIImage(named: "widget_insurance"),UIImage(named: "widget_labReport"),UIImage(named: "widget_medical"),UIImage(named: "widget_medicalDocument"),UIImage(named: "widget_profieDetails"),UIImage(named: "widget_caretakerList"),UIImage(named: "widget_vitalStats"),UIImage(named: "widget_device"),UIImage(named: "widget_providerList"),UIImage(named: "widget_medicalCondition")]*/
    
    var labelArray = ["Allergy Reports", "Immunization Reports", "Insurance Documents", "Lab Reports", "Medications", "Medical Safe"]
//    var labelArray = ["Allergies", "Immunization", "Condition", "Lab Results", "Medications", "Vital Score"]
    
    //Provider Widgets
    var providerImageArray = [UIImage(named: "home_patient"), UIImage(named: "home_report")]
    var providerWidgets: [WidgetModel] = []
//    var providerWigets = ["Patient", "Report Requests"]
    var selectedWidgetItems: [Int] = []
    
    @IBOutlet weak var widgetTableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var editWidgetButton: UIButton!
    
    //MARK: - View LIfe Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Constant.masterData == nil {
            getMasterData()
        }
        
        if Constant.userProfile?.family_name == "consumer" {
            
        } else {
            let providerWigets = [["name": "Patient", "id": "1"], ["name": "Report Requests", "id": "2"]]
            for widget in providerWigets {
                let providerWidget = WidgetModel()
                providerWidget.id = Int(widget["id"] ?? "0")
                providerWidget.name = widget["name"]
                providerWidgets.append(providerWidget)
            }
        }
           
        if Constant.userProfile?.isNewUser == "0" {
            backButton.isHidden = false
            let widgetItemsArray = Constant.userProfile?.widgets?.components(separatedBy: ",")
            if widgetItemsArray != nil {
                for widgetItem in widgetItemsArray! {
                    let widget = Int(widgetItem)
                    selectedWidgetItems.append(widget ?? 0)
                }
            }
        } else {
            backButton.isHidden = true
            let widgetItemsArray = Constant.userProfile?.widgets?.components(separatedBy: ",")
            if widgetItemsArray != nil {
                for widgetItem in widgetItemsArray! {
                    let widget = Int(widgetItem)
                    selectedWidgetItems.append(widget ?? 0)
                }
            }
        }
        
        widgetTableView.register(UINib(nibName: "WidgetTableViewCell", bundle: nil), forCellReuseIdentifier: "WidgetTableViewCell")
        widgetTableView.tableFooterView = UIView()
        
        if Constant.userProfile?.isNewUser == "1" {
            backButton.isHidden = true
        }
        else{
            backButton.isHidden = false
        }
        
    }
    
    func getMasterData() {
        RegisterViewModel.getMasterData() { (masterData, error) in
            if error == nil{
                Constant.masterData = masterData
                self.widgetTableView.reloadData()
            }
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = false
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func editWidgetsButtonAction(_ sender: Any) {
        var profileData = [String: String]()
        var selectedWidgetString = String()
        
        for widgetId in selectedWidgetItems {
            if selectedWidgetString == "" {
                selectedWidgetString = String(describing: widgetId)
            } else {
                selectedWidgetString = selectedWidgetString + "," + String(describing: widgetId)
            }
        }
        
        profileData["custom:widget_items"] = selectedWidgetString
       // profileData["custom:new_user"] = "0"
        print(profileData)
        
        WidgetViewModel.shared.updateWidgets(userData: profileData) { (result) in
            if result {
                DispatchQueue.main.async {
                    
//                    if Constant.userProfile?.isNewUser == "1" {
//                        guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: DocumentListingViewController.className) as? DocumentListingViewController else {return}
//
//                                            vc.documentType = "4"
//                                            vc.fileType = "1"
//                                            vc.titleText = ""
//
//                                                vc.selectedCareTaker = nil
//
//                                            UIApplication.shared.topControllerInHierarchy()?.navigationController?.pushViewController(vc, animated: true)
//                                            return
//                    }
//                    else{
                    
                    
                    let vc = UIStoryboard.init(name: "TabBar", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarViewController
                    self.navigationController?.pushViewController(vc!, animated: true)
                        
               //     }
                }
            }
        }
    }
}

extension SetWidgetViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Constant.loggedInProfile == .consumer {
            if Constant.masterData != nil {
                if Constant.masterData?.widgetTypes.count != 0 {
                    return Constant.masterData!.widgetTypes.count
                }
            }
        }
        return providerWidgets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell( withIdentifier: "WidgetTableViewCell", for: indexPath) as? WidgetTableViewCell else {
            fatalError()
        }
        cell.widgetSwitch.tag = indexPath.row
        cell.delegate = self
        let widget = Constant.masterData?.widgetTypes[indexPath.row]
        //For consumer widget screen
        if Constant.loggedInProfile == .consumer {
            cell.widgetImageView.image = imageArray[indexPath.row]
//            if widget?.name == "Caregivers" {
//                cell.widgetLabel.text = "Caregivers/Dependents"
//            } else {
                cell.widgetLabel.text = widget?.name ?? ""
//            }
            if selectedWidgetItems.contains(widget?.id ?? 0) {
                cell.widgetSwitch.isSelected = true
            } else {
                cell.widgetSwitch.isSelected = false
            }
            return cell
        }
        //For provider Widget screen
        else if Constant.loggedInProfile == .provider {
            
            cell.widgetImageView.image = self.providerImageArray[indexPath.row]
            let providerWidget = self.providerWidgets[indexPath.row]
            cell.widgetLabel.text = providerWidget.name
            if selectedWidgetItems.contains(providerWidget.id ?? 0) {
                cell.widgetSwitch.isSelected = true
            } else {
                cell.widgetSwitch.isSelected = false
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension SetWidgetViewController: WidgetProtocol {
    
    func selectUnselectWidget(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        guard let indexPath = widgetTableView.getSelectedIndexPath(sender: sender) else {
            return
        }
        selectUnselectCell(indexPath: indexPath)
    }
    
    func selectUnselectCell(indexPath: IndexPath) {
        var widgetItems = [WidgetModel]()
        
        if Constant.loggedInProfile == .consumer {
            widgetItems = Constant.masterData!.widgetTypes
        } else {
            widgetItems = providerWidgets
        }
        
//        guard let widgetItems = Constant.masterData?.widgetTypes else {
//            return
//        }
        //If already selected, unselect
        if selectedWidgetItems.contains(widgetItems[indexPath.row].id!) {
            
            guard let index = selectedWidgetItems.firstIndex(of: widgetItems[indexPath.row].id ?? 0) else {
                return
            }
            selectedWidgetItems.remove(at: index)
        }
        //If not already select, set selected
        else {
            selectedWidgetItems.append(widgetItems[indexPath.row].id ?? 0)
        }
        self.widgetTableView.reloadData()
    }
    
}
