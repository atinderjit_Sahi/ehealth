//
//  HomeCollectionViewCell.swift
//  E-Health
//
//  Created by Akash Dhiman on 28/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var consumerTitleLabel: UILabel!
    @IBOutlet weak var homeCollectionButton: UIButton!
}
