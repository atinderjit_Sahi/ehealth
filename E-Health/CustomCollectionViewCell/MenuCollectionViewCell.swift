//
//  MenuCollectionViewCell.swift
//  E-Health
//
//  Created by Akash Dhiman on 28/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var menuTitleLabel: UILabel!
}
