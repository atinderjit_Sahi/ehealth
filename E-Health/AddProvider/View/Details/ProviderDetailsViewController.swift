//
//  ProviderDetailsViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/14/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

class ProviderDetailsViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var providerImageView: UIImageView!
    //Labels
    @IBOutlet weak var labelSpeciality: UILabel!
    @IBOutlet weak var labelProviderType: UILabel!
    @IBOutlet weak var labelProviderName: UILabel!
    @IBOutlet weak var labelPracticeName: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelOfficeNumber: UILabel!
    @IBOutlet weak var labelMobileNumber: UILabel!
    @IBOutlet weak var labelFax: UILabel!
    //Buttons
    @IBOutlet weak var buttonOfficeNumber: UIButton!
    @IBOutlet weak var buttonMobileNumber: UIButton!
    @IBOutlet weak var viewProviderInfo: UIView!    //View
    @IBOutlet weak var buttonAddInList: UIButton!
    @IBOutlet weak var buttonCallMobileNumber: UIButton!
    //MARK: - Properties
    var selectedGlobalProvider: SubProvider?
    var selectedProvider: AddProvider?
    var isMyProvider = Int()
    var selectedCareTaker: CaretakerModel?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        if isMyProvider == 1 || isMyProvider == 3 {
            setPrefilledData()
        } else {
            setPrefilledGlobalData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        tabBarController?.tabBar.isHidden = true
    }
    
    // MARK: - UIButton Actions
    @IBAction func buttonBackTapped(_ sender: Any) {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonOfficeNumberTapped(_ sender: Any) {
        var phoneNumber = String()
        if labelOfficeNumber.text == "" || labelOfficeNumber.text == nil || labelOfficeNumber.text == "N.A" {
            view.makeToast("Contact number format is not correct!")
            return
        } else {
            phoneNumber = labelOfficeNumber.text ?? ""
        }
        
        if let url = URL(string: "tel://\(String(describing: phoneNumber))"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
        else {
            view.makeToast("Something went wrong!")
        }
    }
    
    @IBAction func buttonMobilleNumberTapped(_ sender: Any) {
        var phoneNumber = String()
        if labelMobileNumber.text == "" || labelMobileNumber.text == nil || labelMobileNumber.text == "N.A" {
            view.makeToast("Contact number format is not correct!")
            return
        } else {
            phoneNumber = labelMobileNumber.text ?? ""
        }
        
        if let url = URL(string:"tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        } else {
            view.makeToast("Something went wrong!")
        }
    }
    
    @IBAction func buttonCallMobileNumberTapped(_ sender: Any) {
        var phoneNumber = String()
        if labelMobileNumber.text == "" || labelMobileNumber.text == nil || labelMobileNumber.text == "N.A" {
            view.makeToast("Contact number format is not correct!")
            return
        } else {
            phoneNumber = labelMobileNumber.text ?? ""
        }
        
        if let url = URL(string:"tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        } else {
            view.makeToast("Something went wrong!")
        }
    }
    
    @IBAction func buttonAddInListTapped(_ sender: UIButton) {
        var params = [String: Any]()
        
        if isMyProvider == 1 || isMyProvider == 3 {
            if selectedCareTaker != nil {
                if selectedCareTaker?.minor_id != nil {
                    params = ["user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "provider_id": selectedProvider?.id ?? 0] as [String : Any]
                } else {
                    params = ["user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "provider_id": selectedProvider?.id ?? 0] as [String : Any]
                }
            } else {
                params = ["user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "provider_id": selectedProvider?.id ?? 0] as [String : Any]
            }
        }
        else {
            if selectedCareTaker != nil {
                if selectedCareTaker?.minor_id != nil {
                    params = ["user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "provider_id": selectedGlobalProvider?.id ?? 0] as [String : Any]
                } else {
                    params = ["user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "provider_id": selectedGlobalProvider?.id ?? 0] as [String : Any]
                }
            } else {
                params = ["user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "provider_id": selectedGlobalProvider?.id ?? 0] as [String : Any]
            }
        }
                
        Utility.shared.startLoader()
        if isMyProvider == 1 {
            goToEditProviderScreen()
        } else {
            addProviderApi(params: params)
        }
    }
    
    func goToEditProviderScreen() {
        DispatchQueue.main.async {
            guard let vc = UIStoryboard.init(name: StoryboardNames.ProviderStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: AddProviderViewController.className) as? AddProviderViewController else {
                return
            }
            
            vc.selectedProvider = self.selectedProvider
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func addProviderApi(params: [String: Any]) {
        AddProviderViewModel.assignProvider(params: params) { (message, error) in
            Utility.shared.stopLoader()
            if error == nil {
                Utility.shared.showToast(message: message ?? "Provider assigned successfully")
                self.navigationController?.popViewController(animated: true)
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func setPrefilledGlobalData() {
        buttonAddInList.isHidden = false
        if let profileImage = selectedProvider?.profileImage  {
            DispatchQueue.main.async {
                if let url = URL(string: profileImage) {
                    self.providerImageView.kf.indicatorType = .activity
                    self.providerImageView.kf.setImage(with: url)
                    self.providerImageView.layer.cornerRadius = self.providerImageView.frame.height/2
                    self.providerImageView.clipsToBounds = true
                }
            }
        }
        
        labelSpeciality.text = selectedGlobalProvider?.speciality ?? "N.A"
        labelProviderType.text = selectedGlobalProvider?.type ?? "N.A"
        labelProviderName.text = selectedGlobalProvider?.providerName ?? "N.A"
        labelPracticeName.text = selectedGlobalProvider?.practiceName ?? "N.A"
        labelEmail.text = selectedGlobalProvider?.email ?? "N.A"
//        labelOfficeNumber.text = selectedGlobalProvider?.officeContact
        if let homeNumber = selectedGlobalProvider?.homeContact  {
            labelMobileNumber.text = homeNumber
            buttonMobileNumber.isHidden = false
        } else {
            labelMobileNumber.text = "N.A"
            buttonMobileNumber.isHidden = true
        }
//        labelMobileNumber.text = selectedGlobalProvider?.homeContact ?? "N.A"
        labelFax.text = selectedGlobalProvider?.fax ?? "N.A"
    }
    
    func setPrefilledData() {
        if isMyProvider == 1 {
            buttonAddInList.isHidden = false
            buttonAddInList.setImage(UIImage(named: "editProvider"), for: .normal)
        } else {
            buttonAddInList.isHidden = false
        }
        if let profileImage = selectedProvider?.profileImage  {
            DispatchQueue.main.async {
                if let url = URL(string: profileImage) {
                    self.providerImageView.kf.indicatorType = .activity
                    self.providerImageView.kf.setImage(with: url)
                    self.providerImageView.layer.cornerRadius = self.providerImageView.frame.height/2
                    self.providerImageView.clipsToBounds = true
                }
            }
        }
        
        if selectedProvider?.practiceName == "" {
            labelSpeciality.text = "N.A"
        } else {
            labelSpeciality.text = selectedProvider?.speciality ?? "N.A"
        }
        labelSpeciality.text = selectedProvider?.speciality ?? "N.A"
        labelProviderType.text = selectedProvider?.type ?? "N.A"
        labelProviderName.text = selectedProvider?.providerName ?? "N.A"
        if selectedProvider?.practiceName == "" {
            labelPracticeName.text = "Practice Name : " + "N.A"
        } else {
            labelPracticeName.text = "Practice Name : " + (selectedProvider?.practiceName ?? "N.A")
        }
        
        labelEmail.text = selectedProvider?.email ?? "N.A"
//        labelOfficeNumber.text = selectedProvider?.officeContact
        if let homeNumber = selectedProvider?.homeContact  {
            labelMobileNumber.text = homeNumber
            labelMobileNumber.underlinedLabel()
            buttonMobileNumber.isHidden = false
        } else {
            labelMobileNumber.text = "N.A"
            buttonMobileNumber.isHidden = true
        }
        labelFax.text = selectedProvider?.fax ?? "N.A"
    }
}
