//
//  AddProviderViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/11/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import DropDown
import AWSMobileClient
import IQKeyboardManagerSwift

class AddProviderViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var textFieldProviderCategory: CustomTextField!
    @IBOutlet weak var textFieldType: ExtendedTextField!
    @IBOutlet weak var textFieldPracticeName: ExtendedTextField!
    @IBOutlet weak var textFieldSpeciality: ExtendedTextField!
    @IBOutlet weak var textFieldProviderName: ExtendedTextField!
    @IBOutlet weak var textFieldContact: ExtendedTextField!
    @IBOutlet weak var textFieldEmail: ExtendedTextField!
    @IBOutlet weak var textFieldAddress1: CustomTextField!
    @IBOutlet weak var textFieldAddress2: ExtendedTextField!
    @IBOutlet weak var textFieldRegion: CustomTextField!
    @IBOutlet weak var textFieldCity: ExtendedTextField!
    @IBOutlet weak var textFieldState: UITextField!
    @IBOutlet weak var textFieldCountryCode: UITextField!
    @IBOutlet weak var textFieldCountry: UITextField!
    @IBOutlet weak var textFieldZip: UITextField!
    @IBOutlet weak var textFieldFax: UITextField!
    @IBOutlet weak var textFieldIdofCustomer: CustomTextField!
    @IBOutlet weak var textFieldPasswordOfCustomer: CustomTextField!
    @IBOutlet weak var textFieldProviderUrl: CustomTextField!
    @IBOutlet weak var passwordDisplayToggleButton: UIButton!
    @IBOutlet weak var showUrlButton: UIButton!
    
    @IBOutlet weak var doctorTypeView: UIView!
    @IBOutlet weak var practiceNameView: UIView!
    @IBOutlet weak var soecialityView: UIView!
    //Country List view Outlets
    @IBOutlet var specialitySearchView: UIView!
    @IBOutlet var countrySearchView: UIView!
    @IBOutlet weak var countrySearchBar: UISearchBar!
    @IBOutlet weak var specialitySearchBar: UISearchBar!
    @IBOutlet weak var countryListTableView: UITableView!
    @IBOutlet weak var searchSpecialityView: UIView!
    @IBOutlet weak var specialityListTableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    var isCountrySearchedEmpty = true
    
    @IBOutlet weak var saveButton: UIButton!
    
    // MARK: - Properties
    var fileType: String?
    var documentType: String?
    var selectedCareTaker: CaretakerModel?
    var typeId: String?
    var practiceId: String?
    var specialityId: String?
    
    var imagePicker: ImagePicker!
    var isFrontImageSelected: Int?
    
    var providerTypes = [ProviderType]()
    
    //Specialist arrays
    var isSpecialistSelected = false
    var specialistArray = [SpecialistType]()
    var searchSpecialistArray = [SpecialistType]()
    var selectedSpecialistArray = [SpecialistType]()
    
    //Practice Name Arrays
    var isPracticeNameSelected = false
    var practiceNameArray = [SpecialistType]()
    var searchPracticeNameArray = [SpecialistType]()
    var selectedPracticeArray = [SpecialistType]()
    
    //Selected options
    var selectedProviderType = ProviderType()
//    var selectedDoctorType = String()
    
    var selectedCountry = CountryList()
    var countrySearchArray = [CountryList]()
    var countriesListArray = [CountryList]()
    var selectedProvider: AddProvider?
    var selectedDoctorType: DoctorType?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        textFieldSetUp()
        setupSpecialityUI()
        getDropDownList()
        if selectedProvider != nil {
            getProviderDetail()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        tabBarController?.tabBar.isHidden = false
    }
    
    func textFieldSetUp() {
        textFieldProviderCategory.setUnderLine()
        textFieldType.setUnderLine()
        textFieldPracticeName.setUnderLine()
        textFieldSpeciality.setUnderLine()
        textFieldProviderName.setUnderLine()
        textFieldCountryCode.setUnderLine()
        textFieldContact.setUnderLine()
        textFieldEmail.setUnderLine()
        textFieldAddress1.setUnderLine()
        textFieldAddress2.setUnderLine()
        textFieldCity.setUnderLine()
        textFieldState.setUnderLine()
        textFieldCountry.setUnderLine()
        textFieldZip.setUnderLine()
        textFieldFax.setUnderLine()
        textFieldPasswordOfCustomer.setUnderLine()
        textFieldProviderUrl.setUnderLine()
        textFieldIdofCustomer.setUnderLine()
        textFieldRegion.setUnderLine()
        
        
    }
    
    func getDropDownList() {
        if Constant.masterData?.countries.count != 0 {
            self.countriesListArray = Constant.masterData?.countries ?? []
            self.countrySearchArray = self.countriesListArray
            self.countryListTableView.reloadData()
        }
        if Constant.masterData?.providerTypes.count != 0 {
            self.providerTypes = Constant.masterData?.providerTypes ?? []
        }
    }
    
    // MARK: - UIButton Actions
    @IBAction func showUrlButtonAction(_ sender: Any) {
        if textFieldProviderUrl.text == "" {
            Utility.shared.showToast(message: "Please enter a URL")
            return
        }
        guard let url = URL(string: textFieldProviderUrl.text!) else {
            Utility.shared.showToast(message: "Please enter a valid URL")
            return //be safe
        }

        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        else {
            Utility.shared.showToast(message: "Unable to open URL")
        }
    }
    
    @IBAction func passwordDisplayToggleButtonAction(_ sender: Any) {
        if textFieldPasswordOfCustomer.isSecureTextEntry {
            textFieldPasswordOfCustomer.isSecureTextEntry = false
            passwordDisplayToggleButton.setImage(UIImage(named: "unhide_password"), for: .normal)
        } else {
            textFieldPasswordOfCustomer.isSecureTextEntry = true
            passwordDisplayToggleButton.setImage(UIImage(named: "hide_password"), for: .normal)
        }
    }
    @IBAction func buttonSubmitTapped(_ sender: Any) {
        
        var parameters = [String: Any]()
        
        if selectedCareTaker != nil {
            if selectedCareTaker?.minor_id != nil {
                parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0] as [String : Any]
            } else {
                parameters = ["cognito_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0] as [String : Any]
            }
        } else {
            parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": 0] as [String : Any]
        }
        
        //For Practice Name
        var practiceID = String()
        if selectedPracticeArray.count != 0 {
            for practice in selectedPracticeArray {
                let practiceId = String(practice.id ?? 0)
                let practiceName = practice.text ?? ""
                practiceID.append(/*practiceName + "-" + */practiceId + ";")
            }
            practiceID.removeLast()
        } else {
            practiceID = "1"
        }
        
        //For Specialist Name
        var specialistID = String()
        if selectedSpecialistArray.count != 0 {
            for specialist in selectedSpecialistArray {
                let specialistId = String(specialist.id ?? 0)
                let specialityName = specialist.text ?? ""
                specialistID.append(/*specialityName + "-" + */specialistId + ";")
            }
            specialistID.removeLast()
        } else {
            specialistID = "0"
        }
        
        //For doctor type Primary/Secondary
        if self.selectedProviderType.name == "Doctors" {
//            if selectedDoctorType == "Primary Doctor" {
            parameters["doctor_type"] = String(describing: selectedDoctorType?.id ?? 0)
//            } else if selectedDoctorType == "Secondary Doctor" {
//                parameters["doctor_type"] = "2"
//            }
        } else {
            parameters["doctor_type"] = ""
        }
        
        let phoneNumber = "+" + (selectedCountry.phonecode ?? "") + (textFieldContact.text!)
        parameters["type_id"] = typeId ?? ""
        parameters["practice_name"] = practiceID
        parameters["full_name"] = textFieldProviderName.text ?? ""
        parameters["speciality"] = specialistID
        parameters["email"] = textFieldEmail.text ?? ""
        parameters["address1"] = textFieldAddress1.text ?? ""
        parameters["address2"] = textFieldAddress2.text ?? ""
        parameters["zip"] = textFieldZip.text ?? ""
        parameters["city"] = textFieldCity.text ?? ""
        parameters["country"] = String(selectedCountry.id ?? 0)
        parameters["contact"] = textFieldContact.text ?? ""
        parameters["fax"] = textFieldFax.text ?? ""
        parameters["state"] = textFieldState.text ?? ""
        parameters["user_id"] = textFieldIdofCustomer.text ?? ""
        parameters["password"] = textFieldPasswordOfCustomer.text ?? ""
        parameters["url"] = textFieldProviderUrl.text ?? ""
        parameters["region"] = textFieldRegion.text ?? ""
//        parameters["cognito_id"] = AWSMobileClient.default().username ?? ""
        
        if selectedProvider != nil {
            updateProviderDetail()
        } else {
            let provider = AddProvider(data: parameters)
            let result = Validations.shared.validateAddProvider(provider: provider)
            
            if result.0 == false {
                Utility.shared.showToast(message: result.1)
                return
            } else {
                Utility.shared.startLoader()
                addProviderApi(params: parameters)
            }
        }
    }
    
    @IBAction func providerCategoryDropDown(_ sender: Any) {
        //DROP DOWN FOR REGISTER AS FIELD
        if providerTypes.count != 0 {
            let dataSource = self.providerTypes.map({$0.name})
            self.initdropDown(textField: textFieldProviderCategory, dataSource: dataSource as! [String])
        } else {
            Utility.shared.showToast(message: "No provider type available")
        }
    }
    
    @IBAction func providerTypeListDropDown(_ sender: Any) {
        //DROP DOWN FOR REGISTER AS FIELD
        if Constant.masterData?.doctorTypeArray != nil {
            let dataSource = Constant.masterData?.doctorTypeArray.map({$0.name})
            self.initdropDown(textField: textFieldType, dataSource: dataSource as! [String]/*["Primary Doctor", "Secondary Doctor"]*/)
        } else {
            Utility.shared.showToast(message: "No doctor type available")
        }
    }
    
    @IBAction func practiceNameListDropDown(_ sender: Any) {
        isPracticeNameSelected = true
        isSpecialistSelected = false
        specialitySearchBar.placeholder = "Search Practice Name"
        specialitySearchBar.text = ""
        practiceNameArray.removeAll()
        searchPracticeNameArray = selectedPracticeArray
        specialityListTableView.reloadData()
        showAlertWithBounceEffect(myView: specialitySearchView, onWindow: true)
    }
    
    @IBAction func countryListDropDown(_ sender: Any) {
        if countriesListArray.count == 0 {
            Utility.shared.showToast(message: "No countries list available.")
            return
        }
        isSpecialistSelected = false
        isPracticeNameSelected = false
        countrySearchBar.text = ""
        countrySearchArray = countriesListArray
        countryListTableView.reloadData()
        showAlertWithBounceEffect(myView: countrySearchView, onWindow: true)
    }
    
    @IBAction func specialityListDropDown(_ sender: Any) {
        isSpecialistSelected = true
        isPracticeNameSelected = false
        specialitySearchBar.placeholder = "Search Specialisation"
        specialitySearchBar.text = ""
        searchSpecialistArray = selectedSpecialistArray
        specialityListTableView.reloadData()
        showAlertWithBounceEffect(myView: specialitySearchView, onWindow: true)
    }
    
    @IBAction func countryListCrossButton(_ sender: Any) {
        countrySearchBar.endEditing(true)
        hideAlertWithBounceEffect(myView: countrySearchView)
    }
    
    @IBAction func specialityCloseButton(_ sender: Any) {
        countrySearchBar.endEditing(true)
        hideAlertWithBounceEffect(myView: specialitySearchView)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        
        //If is Specialist multiple selection list
        if isSpecialistSelected {
            
            //Set text in text field
            if selectedSpecialistArray.count != 0 {
                if selectedSpecialistArray.count > 1 {
                    self.textFieldSpeciality.text = (selectedSpecialistArray.first?.text ?? "") + " & \(selectedSpecialistArray.count - 1) more"
                } else {
                    self.textFieldSpeciality.text = selectedSpecialistArray.first?.text
                }
            } else {
                self.textFieldSpeciality.text = ""
            }
            countrySearchBar.endEditing(true)
            hideAlertWithBounceEffect(myView: specialitySearchView)
            return
        }
        
        if isPracticeNameSelected {
            
            //Set text in text field
            if selectedPracticeArray.count != 0 {
                
                if selectedPracticeArray.count > 1 {
                    self.textFieldPracticeName.text = (selectedPracticeArray.first?.text ?? "") + " & \(selectedPracticeArray.count - 1) more"
                } else {
                    self.textFieldPracticeName.text = selectedPracticeArray.first?.text
                }
            } else {
                self.textFieldPracticeName.text = ""
            }
            countrySearchBar.endEditing(true)
            hideAlertWithBounceEffect(myView: specialitySearchView)
            return
        }
    }
    
    @IBAction func buttonBackTapped(_ sender: Any) {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
}

extension AddProviderViewController {
    
    func getProviderDetail() {
        
        var parameters = [String: Any]()
        
        if selectedCareTaker != nil {
            if selectedCareTaker?.minor_id != nil {
                parameters = ["user_id": AWSMobileClient.default().username ?? "", "provider_id": selectedProvider?.id ?? 0, "minor_id": selectedCareTaker?.minor_id ?? 0] as [String : Any]
            } else {
                parameters = ["user_id": selectedCareTaker?.caretakerSub ?? "0", "provider_id": selectedProvider?.id ?? 0, "minor_id": 0] as [String : Any]
            }
        } else {
            parameters = ["user_id": AWSMobileClient.default().username ?? "", "provider_id": selectedProvider?.id ?? 0, "minor_id": 0] as [String : Any]
        }
        Utility.shared.startLoader()
        AddProviderViewModel.getProviderDetail(parameters: parameters) { (result, message) in
            Utility.shared.stopLoader()
            if result != nil {
                self.setValues(provider: result!)
            } else {
                
            }
        }
    }
    
    func setValues(provider: AddProvider) {
        typeId = String(describing: provider.providerTypeId ?? 0)
        selectedSpecialistArray = provider.specialitiesArray
        selectedPracticeArray = provider.practicesArray
        
        var specialitiesName = [String]()
        for speciality in selectedSpecialistArray {
            specialitiesName.append(speciality.text ?? "")
        }
        
        var practicesName = [String]()
        for practice in selectedPracticeArray {
            practicesName.append(practice.text ?? "")
        }
        
        textFieldProviderCategory.text = getProviderTYpeFromId(typeId: provider.providerTypeId ?? 0)
        selectedProviderType.name = getProviderTYpeFromId(typeId: provider.providerTypeId ?? 0)
        
        if self.selectedProviderType.name == "Doctors" {
            self.selectedDoctorType?.id = Int(provider.doctorType ?? "")
//            if provider.doctorType == "1" {
                textFieldType.text = "Primary Doctor"
//            } else if provider.doctorType == "2" {
//                textFieldType.text = "Secondary Doctor"
//            }
            self.doctorTypeView.isHidden = false
//            self.textFieldSpeciality.text = ""
//            self.selectedSpecialistArray.removeAll()
//            self.textFieldType.text = ""
//            self.textFieldPracticeName.text = ""
//            self.selectedPracticeArray.removeAll()
            self.soecialityView.isHidden = false
            self.practiceNameView.isHidden = false
        } else {
            textFieldType.text = ""
            self.doctorTypeView.isHidden = true
//            self.textFieldSpeciality.text = ""
//            self.textFieldType.text = ""
            self.selectedSpecialistArray.removeAll()
//            self.textFieldPracticeName.text = ""
            self.selectedPracticeArray.removeAll()
            self.soecialityView.isHidden = true
            self.practiceNameView.isHidden = true
//            self.selectedDoctorType = ""
        }
        //selectedDoctorType?.id = provider.doctorType ?? ""
//        textFieldType.text = provider.doctorType ?? ""
        textFieldPracticeName.text = practicesName.joined(separator: ",")
        textFieldSpeciality.text = specialitiesName.joined(separator: ",")
        textFieldProviderName.text = provider.providerName ?? ""
        textFieldContact.text = provider.homeContact ?? ""
        textFieldCity.text = provider.city ?? ""
        textFieldState.text = provider.state ?? ""
        
        let (countrySelected, phoneCode) = getCountryFromId(countryId: provider.countryId ?? 0)
        
        textFieldCountry.text = countrySelected
        textFieldCountryCode.text = phoneCode
        selectedCountry.id = provider.countryId ?? 0
        selectedCountry.name = textFieldCountry.text ?? ""
        textFieldEmail.text = provider.email ?? ""
//        textFieldEmail.isUserInteractionEnabled = false
        textFieldAddress1.text = provider.address1
        textFieldAddress2.text = provider.address2
        textFieldRegion.text = provider.region
        textFieldProviderUrl.text = provider.providerUrl
        textFieldIdofCustomer.text = provider.userIdofCustomer
        textFieldZip.text = provider.zip
        textFieldFax.text = provider.fax
        textFieldPasswordOfCustomer.text = provider.passwordOfCustomer
    }
    
    func getCountryFromId(countryId: Int) -> (String, String) {
        let filteredArray = countriesListArray.filter({ $0.id == countryId }).first
        let id = String(describing: filteredArray?.name ?? "")
        let countryPhoneCode = filteredArray?.phonecode ?? ""
        return (id, countryPhoneCode)
    }
    
    func getProviderTYpeFromId(typeId: Int) -> String {
        let filteredArray = providerTypes.filter({ $0.id == typeId }).first
        let id = String(describing: filteredArray?.name ?? "")
        return id
    }
    
    func updateProviderDetail() {
        
        var parameters = [String: Any]()
        
        if selectedCareTaker != nil {
            if selectedCareTaker?.minor_id != nil {
                parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "id": selectedProvider?.id ?? 0, "minor_id": selectedCareTaker?.minor_id ?? 0] as [String : Any]
            } else {
                parameters = ["cognito_id": selectedCareTaker?.caretakerSub ?? "0", "id": selectedProvider?.id ?? 0, "minor_id": 0] as [String : Any]
            }
        } else {
            parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "id": selectedProvider?.id ?? 0, "minor_id": 0] as [String : Any]
        }
        
        var specialistID = String()
        if selectedSpecialistArray.count != 0 {
            for specialist in selectedSpecialistArray {
                let specialistId = String(specialist.id ?? 0)
                let specialityName = specialist.text ?? ""
                specialistID.append(/*specialityName + "-" + */specialistId + ";")
            }
            specialistID.removeLast()
        } else {
            specialistID = "0"
        }
        
        //For Practice Name
        var practiceID = String()
        if selectedPracticeArray.count != 0 {
            for practice in selectedPracticeArray {
                let practiceId = String(practice.id ?? 0)
                let practiceName = practice.text ?? ""
                practiceID.append(/*practiceName + "-" + */practiceId + ";")
            }
            practiceID.removeLast()
        } else {
            practiceID = "1"
        }
        
        //For doctor type Primary/Secondary
        if self.selectedProviderType.name == "Doctors" {
//            if selectedDoctorType == "Primary Doctor" {
            parameters["doctor_type"] = String(describing: selectedDoctorType?.id ?? 0)
//            } else if selectedDoctorType == "Secondary Doctor" {
//                parameters["doctor_type"] = selectedDoctorType
//            }
        } else {
            parameters["doctor_type"] = ""
        }
        
        parameters["provider_type"] = typeId ?? ""
        parameters["practice_name"] = practiceID
        parameters["provider_name"] = textFieldProviderName.text ?? ""
        parameters["specialty"] = specialistID
        parameters["email_address"] = textFieldEmail.text ?? ""
        parameters["practice_address_line_1"] = textFieldAddress1.text ?? ""
        parameters["practice_address_line_2"] = textFieldAddress2.text ?? ""
        parameters["zip"] = textFieldZip.text ?? ""
        parameters["city"] = textFieldCity.text ?? ""
        parameters["country"] = String(selectedCountry.id ?? 0)
        parameters["main_office_telephone"] = textFieldContact.text ?? ""
        parameters["fax"] = textFieldFax.text ?? ""
        parameters["state"] = textFieldState.text ?? ""
        parameters["user_id"] = textFieldIdofCustomer.text ?? ""
        parameters["password"] = textFieldPasswordOfCustomer.text ?? ""
        parameters["url"] = textFieldProviderUrl.text ?? ""
        parameters["region"] = textFieldRegion.text ?? ""
        
        Utility.shared.startLoader()
        AddProviderViewModel.updateProvider(params: parameters) { (message, error) in
            Utility.shared.stopLoader()
            if error == nil {
                Utility.shared.showToast(message: message ?? "Provider updated successfully")
                self.resetTextField()
                self.popToListingController()
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func addProviderApi(params: [String: Any]) {
        AddProviderViewModel.addProvider(params: params) { (message, error) in
            Utility.shared.stopLoader()
            if error == nil {
                Utility.shared.showToast(message: message ?? "Provider added successfully")
                self.resetTextField()
                self.popToListingController()
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func popToListingController() {
        guard let viewControllers = navigationController?.viewControllers else { return }
        
        // Check if disease registry controller is already into the navigation stack. It will be in the stack incase user is saving follow up and disease registry forms
        for documentListingVC in viewControllers {
            if let vc = documentListingVC as? ProviderListingViewController {
                if self.selectedCareTaker != nil {
                    vc.selectedCareTaker = self.selectedCareTaker
                }
                navigationController?.popToViewController(vc, animated: true)
                return
            }
        }
    }
    
    func resetTextField() {
        textFieldPracticeName.text = ""
        textFieldSpeciality.text = ""
        textFieldFax.text = ""
        textFieldZip.text = ""
        textFieldCity.text = ""
        textFieldType.text = ""
        textFieldEmail.text = ""
        textFieldState.text = ""
        textFieldContact.text = ""
        textFieldCountry.text = ""
        textFieldAddress1.text = ""
        textFieldAddress2.text = ""
        textFieldCountryCode.text = ""
        textFieldProviderName.text = ""
        textFieldProviderCategory.text = ""
    }
    
}

extension AddProviderViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //All Text Field Maximum Limitation
        if textField == textFieldProviderUrl {
            return true
        }
        
         if textField == textFieldFax {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 10, newCharacter: string)
            
            if !result {
                return result
            }
            
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
         
         else if textField == textFieldZip {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 6, newCharacter: string)
            return result
         }
         
         else if textField == textFieldContact {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 12, newCharacter: string)
            return result
         }
         
         else {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 32, newCharacter: string)
            return result
         }
        
       
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //DROP DOWN FOR REGISTER AS FIELD
        if textField == textFieldType{
            IQKeyboardManager.shared.resignFirstResponder()
            return false
        }
        if textField == textFieldProviderCategory{
            IQKeyboardManager.shared.resignFirstResponder()
            return false
        }
        //DROP DOWN FOR PRACTICE NAME FIELD
        if textField == textFieldPracticeName{
            IQKeyboardManager.shared.resignFirstResponder()
            return false
        }
        
        //DROP DOWN FOR SPECIALIST FIELD
        if textField == textFieldSpeciality{
            IQKeyboardManager.shared.resignFirstResponder()
            return false
        }
        
        //COUNTRY LIST VIEW
        if textField == textFieldCountry{
            self.view.endEditing(true)
            return false
        }
        return true
    }
    
    func initdropDown (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                textField.text = item
                if textField == self.textFieldProviderCategory {
                    self.textFieldProviderCategory.text  = item
                    self.typeId = String(describing: self.providerTypes[index].id ?? 0)
                    self.selectedProviderType = self.providerTypes[index]
                    if item != "Doctors" {
                        self.doctorTypeView.isHidden = true
                        self.textFieldSpeciality.text = ""
                        self.textFieldType.text = ""
                        self.selectedSpecialistArray.removeAll()
                        self.textFieldPracticeName.text = ""
                        self.selectedPracticeArray.removeAll()
                        self.soecialityView.isHidden = true
                        self.practiceNameView.isHidden = true
                        self.selectedDoctorType = nil
                    } else {
                        self.doctorTypeView.isHidden = false
                        self.textFieldSpeciality.text = ""
                        self.selectedSpecialistArray.removeAll()
                        self.textFieldType.text = ""
                        self.textFieldPracticeName.text = ""
                        self.selectedPracticeArray.removeAll()
                        self.soecialityView.isHidden = false
                        self.practiceNameView.isHidden = false
                    }
                } else if textField == self.textFieldType {
                    self.textFieldType.text  = item
                    self.selectedDoctorType = Constant.masterData?.doctorTypeArray[index]
                }
                else if textField == self.textFieldPracticeName {
                    self.practiceId = String(describing: index)
                } else {
                    self.specialityId = String(describing: index)
                }
                dropDown.hide()
            }
            dropDown.show()
        }
    }
}

//MARK:- CountryList Table view delegates
extension AddProviderViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections = 1
        if tableView == specialityListTableView {
            if isSpecialistSelected {
                numOfSections = showNoRecordFound(dataArray: searchSpecialistArray, tableview: tableView)
                return numOfSections
            }
            else {
                numOfSections = showNoRecordFound(dataArray: searchPracticeNameArray, tableview: tableView)
                return numOfSections
            }
        } else {
            return numOfSections
        }
    }
    
    func showNoRecordFound(dataArray: [SpecialistType], tableview: UITableView) -> Int {
        var noOfSections: Int = 0
        noDataLabel.isHidden = true
        if !dataArray.isEmpty {
            tableview.separatorStyle = .singleLine
            noOfSections            = 1
            tableview.isScrollEnabled = true
        }
        else {
            tableview.isScrollEnabled = false
            noDataLabel.numberOfLines = 0
            if specialitySearchBar.text == "" {
                noDataLabel.text          = "You have to type in something, you are looking for."
            } else {
                noDataLabel.text          = "No Records Found"
            }
            
            noDataLabel.font = UIFont(name: "Montserrat", size: 17)
            noDataLabel.textColor     = UIColor.darkGray
            noDataLabel.textAlignment = .center
            noDataLabel.isHidden = false
            tableview.separatorStyle  = .none
        }
        return noOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == specialityListTableView {
            if isSpecialistSelected {
                return searchSpecialistArray.count
            }
            else {
                return searchPracticeNameArray.count
            }
        } else {
            return countrySearchArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        if tableView == specialityListTableView {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SpecialityTableViewCell") as? SpecialityTableViewCell else {
                return UITableViewCell()
            }
            
            cell.delegate = self
            
            //For Specialist List
            if isSpecialistSelected {
                cell.countryLabel.text = self.searchSpecialistArray[indexPath.row].text
                if selectedSpecialistArray.contains(where: {$0.text == self.searchSpecialistArray[indexPath.row].text} ) {
                    cell.buttonCheckBox.isSelected = true
                    cell.countryLabel.textColor = UIColor(red: 31/255, green: 181/255, blue: 245/255, alpha: 1.0)
                    //                cell.accessoryType = .checkmark
                } else {
                    cell.countryLabel.textColor = .darkGray
                    cell.buttonCheckBox.isSelected = false
                    //                cell.accessoryType = .none
                }
            }
            else if isPracticeNameSelected {
                cell.countryLabel.text = self.searchPracticeNameArray[indexPath.row].text
                if selectedPracticeArray.contains(where: {$0.text == self.searchPracticeNameArray[indexPath.row].text} ) {
                    cell.countryLabel.textColor = UIColor(red: 31/255, green: 181/255, blue: 245/255, alpha: 1.0)
                    cell.buttonCheckBox.isSelected = true
                    //                cell.accessoryType = .checkmark
                } else {
                    cell.countryLabel.textColor = .darkGray
                    cell.buttonCheckBox.isSelected = false
                    //                cell.accessoryType = .none
                }
            }
            return cell
            
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCellTableViewCell") as? CountryCellTableViewCell else {
                return UITableViewCell()
            }
            
            cell.countryLabel.text = countrySearchArray[indexPath.row].name
            if selectedCountry.name == countrySearchArray[indexPath.row].name {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerView = UIView()
        headerView.backgroundColor = .clear
        if tableView == specialityListTableView {
            headerView = specialitySearchBar
        } else {
            headerView = countrySearchBar
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Get Selected Cell
        var selectedCell: UITableViewCell?
        if let cell = tableView.cellForRow(at: indexPath) {
            selectedCell = cell
        }
        
        if tableView == countryListTableView {
            //For Country list, when a country is selected
            countrySearchBar.endEditing(true)
            hideAlertWithBounceEffect(myView: countrySearchView)
            if isCountrySearchedEmpty {
                print(countriesListArray[indexPath.row].name as Any)
            }else{
                print(countrySearchArray[indexPath.row].name as Any)
            }
            
            self.selectedCountry = countrySearchArray[indexPath.row]
            textFieldCountry.text = self.selectedCountry.name
            textFieldCountryCode.text = "+" + (selectedCountry.phonecode ?? "")
        }
        
    }
}
//MARK: - Country Search Bar Protocol
extension AddProviderViewController: CountryCellProtocol {
    
    func buttonCheckBox(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        selectUnselectCell(sender: sender)
    }
    
    func getSelectedIndexPath(sender: UIButton?, tableView: UITableView?) -> IndexPath? {
        let point = sender?.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView?.indexPathForRow(at: point!)
        return indexPath!
    }

    func selectUnselectCell(sender: UIButton) {
        
        //Get Selected Cell
        var selectedCell: UITableViewCell?
        
        guard let indexPath = getSelectedIndexPath(sender: sender, tableView: specialityListTableView) else {
            return
        }
        
        if let cell = specialityListTableView.cellForRow(at: indexPath) as? SpecialityTableViewCell {
            selectedCell = cell
        }
        
        //If is Specialist multiple selection list
        if isSpecialistSelected {
            
            //If already selected, unselect
            if selectedSpecialistArray.contains(where: {$0.text == self.searchSpecialistArray[indexPath.row].text}) {
                
                let index = selectedSpecialistArray.firstIndex(where: {$0.text == self.searchSpecialistArray[indexPath.row].text})!
                
                selectedSpecialistArray.remove(at: index)
            }
            //If not already select, set selected
            else {
                selectedSpecialistArray.append(self.searchSpecialistArray[indexPath.row])
            }
            
            self.specialityListTableView.reloadData()
            return
        }
        
        if isPracticeNameSelected {
            
            //If already selected, unselect
            
            if selectedPracticeArray.contains(where: {$0.text == self.searchPracticeNameArray[indexPath.row].text}) {
                
                let index = selectedPracticeArray.firstIndex(where: {$0.text == self.searchPracticeNameArray[indexPath.row].text})!
                
                selectedPracticeArray.remove(at: index)
            }
            //If not already select, set selected
            else {
                selectedPracticeArray.append(self.searchPracticeNameArray[indexPath.row])
            }
            self.specialityListTableView.reloadData()
            return
        }
    }
}

//MARK:- Search Bar Delegates
extension AddProviderViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //For Specialist search list
        if isSpecialistSelected {
            if searchText.count >= 2 {
                self.searchSpecialist(text: searchText)
            } else {
                if searchText.count == 0 {
                    searchSpecialistArray = selectedSpecialistArray
                    specialityListTableView.reloadData()
                } else {
                    specialistArray.removeAll()
                    searchSpecialistArray = specialistArray
                    specialityListTableView.reloadData()
                }
            }
        }
        if isPracticeNameSelected {
            if searchText.count >= 2 {
                self.searchPracticeName(text: searchText)
            } else {
                if searchText.count == 0 {
                    searchPracticeNameArray = selectedPracticeArray
                    specialityListTableView.reloadData()
                } else {
                    practiceNameArray.removeAll()
                    searchPracticeNameArray = practiceNameArray
                    specialityListTableView.reloadData()
                }
            }
        }
        //For Country Search List
        else {
            countrySearchArray = countriesListArray.filter({ country -> Bool in
                if searchText.isEmpty {
                    isCountrySearchedEmpty = true
                    return true
                }
                isCountrySearchedEmpty = false
                return country.name?.lowercased().contains(searchText.lowercased()) ?? false
            })
            countryListTableView.reloadData()
        }
    }
    
    func searchSpecialist(text: String) {
        ProviderSignupViewModel.searchSpecialist(text: text){ (specialists, error) in
            if specialists?.count != 0 && specialists != nil {
                self.specialistArray = specialists!
                self.searchSpecialistArray = self.specialistArray
            }
            self.specialityListTableView.reloadData()
        }
    }
    
    func searchPracticeName(text: String) {
        ProviderSignupViewModel.searchPracticeName(text: text){ (practiceName, error) in
            if practiceName?.count != 0 && practiceName != nil {
                self.practiceNameArray = practiceName!
                self.searchPracticeNameArray = self.practiceNameArray
            }
            self.specialityListTableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        switch selectedScope {
        case 0:
            countrySearchArray = countriesListArray
        default:
            break
        }
        countryListTableView.reloadData()
    }
}
//MARK:- Show/Hide list view
extension AddProviderViewController {
    
    func setupUI() {
        countrySearchView.frame = self.view.frame
        countrySearchView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        countrySearchBar.delegate = self
        
        countryListTableView.tableFooterView = UIView()
        countrySearchBar.showsScopeBar = false
        countrySearchBar.placeholder = "Search Country Name"
        
        showUrlButton.layer.cornerRadius = showUrlButton.frame.height/2
    }
    
    func setupSpecialityUI() {
        specialitySearchView.frame = self.view.frame
        specialitySearchView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        specialitySearchBar.delegate = self
        
        specialityListTableView.tableFooterView = UIView()
        specialitySearchBar.showsScopeBar = false
        specialitySearchBar.placeholder = "Search Speciality Name"
    }
    func showAlertWithBounceEffect(myView : UIView,onWindow:Bool) {
        let window = UIApplication.shared.keyWindow!
        myView.isHidden = false
        if onWindow == true {
            window.addSubview(myView)
        }  else {
            self.view.addSubview(myView)
            self.view.bringSubviewToFront(myView)
        }
        myView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            myView.transform = .identity
        }, completion: {(finished: Bool) -> Void in
        })
    }
    
    func hideAlertWithBounceEffect(myView : UIView) {
        self.isSpecialistSelected = false
        self.isPracticeNameSelected = false
        myView.transform = .identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            myView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: {(finished: Bool) -> Void in
            myView.isHidden = true
        })
    }
    
    func setCountryCode(forIndex: Int) {
        selectedCountry = countriesListArray[forIndex]
        textFieldCountryCode.text = "+" + (selectedCountry.phonecode ?? "")
    }
}
