//
//  ProviderListingViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/11/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient


class ProviderListingViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var documentListingTableView: UITableView!
    @IBOutlet weak var addDataButton: UIButton!
    @IBOutlet weak var textFieldToDate: UITextField!
    @IBOutlet weak var textFieldFromDate: UITextField!
    @IBOutlet weak var searchDone: UIButton!
    @IBOutlet weak var searchClear: UIButton!
    @IBOutlet weak var fromToDateView: UIView!
    @IBOutlet weak var doneClearButtonView: UIView!
    @IBOutlet weak var tabBarScroller: ScrollPager!
    
    //MARK: - Properties
    var searchText = String()
    var pageSize = 500
    var pageNumber = 1
    var documentType: String?
    var fileType: String?
    var isDataLoading = false
    var titleText: String?
    var selectedCareTaker: CaretakerModel?
    var documentsArray: [DocumentListingModel]?
    var insuranceRecordAray: [InsuranceRecord]?
    
    var datePicker = UIDatePicker()
    var activeTextField = UITextField()
    
    var myProvidersArray = [AddProvider]()
    var globalProvidersArray = [AddProvider]()
    
    var selectedMyProvidersArray = [AddProvider]()
    var selectedGlobalProvidersArray = [AddProvider]()
    
    var isMyProviderSelected = false
        
    let dispatchGroup = DispatchGroup()
    
    var expandedRowsArrays = [Int]()
    
    
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        documentListingTableView.tableFooterView = UIView()
        documentListingTableView.separatorStyle = .none
        searchBar.setSearchBarUI()
        customizeTextFields()
        tabBarScroller.delegate = self
        tabBarScroller.font = UIFont(name: "Montserrat-Medium", size: 16)!
        tabBarScroller.selectedFont = UIFont(name: "Montserrat-Medium", size: 16)!
        tabBarScroller.viewHeight = self.view.frame.height
        tabBarScroller.addSegmentsWithTitles(segmentTitles: ["Provider Directory", "My Provider"])
//        refreshList()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        searchBar.delegate = self
        refreshList()
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.searchBar.resignFirstResponder()
        tabBarController?.tabBar.isHidden = false
    }
    
    func customizeTextFields() {
        textFieldFromDate.setLeftImage(leftImage: UIImage(named: "calendar")!)
        textFieldToDate.setLeftImage(leftImage: UIImage(named: "calendar")!)
        textFieldFromDate.attributedPlaceholder = NSAttributedString(string: "From", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        textFieldToDate.attributedPlaceholder = NSAttributedString(string: "To", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
    }
    
    //MARK:- UIButton Actions
    @IBAction func addDataButtonAction(_ sender: Any) {
        view.endEditing(true)
        goToAddProviderScreen()
    }
    
    @IBAction func filterButton(_ sender: UIButton) {
        view.endEditing(true)
        searchBar.text = ""
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            hideShowSearchView(isHidden: false)
        } else {
            hideShowSearchView(isHidden: true)
            emptySearchByDateFields()
            refreshList()
        }
    }
    
    @IBAction func searchDoneButton(_ sender: Any) {
        view.endEditing(true)
        if textFieldFromDate.text == "" {
            Utility.shared.showToast(message: "Please select from date")
            return
        } else if textFieldToDate.text == "" {
            Utility.shared.showToast(message: "Please select to date")
            return
        }
    }
    
    @IBAction func searchClearButton(_ sender: Any) {
        view.endEditing(true)
        hideShowSearchView(isHidden: true)
        emptySearchByDateFields()
        refreshList()
    }
    
    func hideShowSearchView(isHidden: Bool) {
        DispatchQueue.main.async {
            self.fromToDateView.isHidden = isHidden
            self.doneClearButtonView.isHidden = isHidden
        }
    }
    
    func emptySearchByDateFields() {
        textFieldToDate.text = ""
        textFieldFromDate.text = ""
    }
    
    @IBAction func backButton(_ sender: Any) {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addButton(_ sender: Any) {
        view.endEditing(true)
        goToAddProviderScreen()
    }
        
    func goToAddProviderScreen() {
        guard let vc = UIStoryboard.init(name: StoryboardNames.ProviderStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: AddProviderViewController.className) as? AddProviderViewController else {return}
        
        vc.selectedCareTaker = selectedCareTaker
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension ProviderListingViewController: ScrollPagerDelegate {
    func scrollPager(scrollPager: ScrollPager, changedIndex: Int) {
        switch changedIndex {
        case 0:
            isMyProviderSelected = false
        case 1:
            isMyProviderSelected = true
        default:
            isMyProviderSelected = false
        }
        view.endEditing(true)
        searchBar.text = ""
        refreshList()
    }
    
    func refreshList() {
        if isMyProviderSelected {
            getMyProviderList(searchText: "")
        } else {
            getGlobalProviderList(searchText: "")
        }
           
        dispatchGroup.notify(queue: .main) { [unowned self] in
//            Utility.shared.stopLoader()
            self.documentListingTableView.reloadData()
        }
    }
    
    func getMyProviderList(searchText: String) {
        
        var params = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0] as [String : Any]
            } else {
                params = ["user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0] as [String : Any]
            }
        } else {
            params = ["user_id": AWSMobileClient.default().username ?? "", "minor_id": 0] as [String : Any]
        }
        
        Utility.shared.startLoader()
        AddProviderViewModel.getMyProviders(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.isDataLoading = false
                self.myProvidersArray = response!
                self.selectedMyProvidersArray = response!
                self.documentListingTableView.reloadData()
            } else {
                if searchText != "" {
                    self.myProvidersArray.removeAll()
                    self.selectedMyProvidersArray.removeAll()
                    self.documentListingTableView.reloadData()
                }
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func getGlobalProviderList(searchText: String) {
        
        let params = ["searchBy": "name", "searchVal": searchText] as [String : Any]
        
        Utility.shared.startLoader()
        AddProviderViewModel.getGlobalProviders(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.isDataLoading = false
                self.globalProvidersArray = response!
                self.selectedGlobalProvidersArray = response!
                self.documentListingTableView.reloadData()
            } else {
                if searchText != "" {
                    self.globalProvidersArray.removeAll()
                    self.selectedGlobalProvidersArray.removeAll()
                    self.documentListingTableView.reloadData()
                }
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
}
extension ProviderListingViewController: DocumentListingProtocol {
    
    func viewReport(sender: UIButton) {
        view.endEditing(true)
        let indexpath = getSelectedIndexPath(sender: sender, tableView: documentListingTableView)
        
        if documentType == "3" {
            if indexpath != nil && insuranceRecordAray != nil {
                let insuranceRecord = insuranceRecordAray![indexpath!.row]
                
                if insuranceRecord.uploadType == "upload" {
                    guard let detailVC = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: InsuranceReportDetailViewController.className) as? InsuranceReportDetailViewController else {return}
                    
                    detailVC.selectedInsuranceRecord = insuranceRecord
                    navigationController?.pushViewController(detailVC, animated: true)
                } else {
                    guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: WebViewController.className) as? WebViewController else {return}
                    vc.webUrlString = insuranceRecord.viewFront
                    navigationController?.pushViewController(vc, animated: true)
                }
            }
        } else {
            if indexpath != nil && documentsArray != nil {
                let document = documentsArray![indexpath!.row]
                
                guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: WebViewController.className) as? WebViewController else {return}
                vc.webUrlString = document.filePath
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func getSelectedIndexPath(sender: UIButton?, tableView: UITableView?) -> IndexPath? {
        let point = sender?.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView?.indexPathForRow(at: point!)
        return indexPath!
    }
}

//MARK: - Table View Delegate
extension ProviderListingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: - Table View Data Source
extension ProviderListingViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isMyProviderSelected {
            if myProvidersArray.count == 0 {
                addDataButton.isHidden = false
            } else {
                addDataButton.isHidden = true
            }
            return selectedMyProvidersArray.count
        } else {
            if globalProvidersArray.count == 0 {
                addDataButton.isHidden = false
            }else {
                addDataButton.isHidden = true
            }
            return selectedGlobalProvidersArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isMyProviderSelected {
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: MyProviderTableViewCell.className)! as? MyProviderTableViewCell else {
                fatalError()
            }
            
            cell.delegate = self
            cell.provider = selectedMyProvidersArray[indexPath.row]
            
            return cell
        } else {
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: GlobalProviderTableViewCell.className)! as? GlobalProviderTableViewCell else {
                fatalError()
            }
            if expandedRowsArrays.contains(indexPath.row) {
                print("contains: \(indexPath.row)")
            } else {
                
            }
            cell.delegate = self
            cell.tag = indexPath.row
            cell.layoutMargins = UIEdgeInsets.zero
            cell.preservesSuperviewLayoutMargins = false
            cell.provider = selectedGlobalProvidersArray[indexPath.row]
            
            return cell
        }
    }
        
    /*func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            Utility.shared.showCustomAlertWithCompletion("Delete Record", message: "Are you sure you want to delete this record?", withButtonTypes: ["No", "Yes"], withAlertCompletionBlock: { (alertAction, buttonTitle) in
                
                if buttonTitle == "Yes" {
                    self.deleteDocument(indexpath: indexPath)
                }
            })
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }*/
    
    func deleteDocument(indexpath: IndexPath) {
        self.deleteProviderApi(recordId: 0, recordIndex: indexpath)
    }
    
}

//MARK:- Search Bar Delegates
extension ProviderListingViewController: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        //code added by atinder
        if let vc = UIStoryboard(name: StoryboardNames.ProviderStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: GlobalSubProvidersViewController.className) as? GlobalSubProvidersViewController {
            searchBar.resignFirstResponder()
            vc.bool_ComingFromSearch = true
            navigationController?.pushViewController(vc, animated: true)
       
        }
        
         return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
       
        
        
//        let trimmedString = searchText.trimmingCharacters(in: .whitespaces)
//        self.searchText = trimmedString
//
//        if trimmedString.count >= 2 {
//            if isMyProviderSelected {
//
//                selectedMyProvidersArray = myProvidersArray.filter({ medication -> Bool in
//                    if searchText.isEmpty {
//                        return true
//                    }
//                    return medication.providerName?.lowercased().contains(searchText.lowercased()) ?? false
//                })
//                print(selectedMyProvidersArray)
//            } else {
//                selectedGlobalProvidersArray = globalProvidersArray.filter({ medication -> Bool in
//                    if searchText.isEmpty {
//                        return true
//                    }
//                    return medication.name?.lowercased().contains(searchText.lowercased()) ?? false
//                })
//                print(selectedGlobalProvidersArray)
//            }
//            documentListingTableView.reloadData()
//        } else {
//            if trimmedString.count == 0 {
//                searchBar.endEditing(true)
//                if isMyProviderSelected {
//                    selectedMyProvidersArray = myProvidersArray
//                } else {
//                    selectedGlobalProvidersArray = globalProvidersArray
//                }
//            }
//            documentListingTableView.reloadData()
//        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
                        
    func deleteProviderApi(recordId: Int?, recordIndex: IndexPath) {
        
        if isMyProviderSelected {
            let provider = selectedMyProvidersArray[recordIndex.row]
            
            var params = [String: Any]()
            if selectedCareTaker != nil {
                if selectedCareTaker?.minor_id != nil {
                    params = ["user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "provider_id": provider.id ?? 0] as [String : Any]
                } else {
                    params = ["user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "provider_id": provider.id ?? 0] as [String : Any]
                }
            } else {
                params = ["user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "provider_id": provider.id ?? 0] as [String : Any]
            }
            
            AddProviderViewModel.removeProvider(parameters: params) { (response, message) in
                Utility.shared.stopLoader()
                if response {
                    self.myProvidersArray.remove(at: recordIndex.row)
                    self.selectedMyProvidersArray.remove(at: recordIndex.row)
                    self.documentListingTableView.reloadData()
                    Utility.shared.showToast(message: message ?? "Provider Deleted successfully")
                } else {
                    Utility.shared.showToast(message: message ?? "Something went wrong")
                }
            }
        } else {
            Utility.shared.showToast(message: "Coming Soon")
        }
    }
}
extension ProviderListingViewController: ProviderListProtocol {
    
    func getSubProviderList(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        guard let indexPath = getSelectedIndexPath(sender: sender, tableView: documentListingTableView) else {
            return
        }
        
        let provider = selectedGlobalProvidersArray[indexPath.row]
        
        if sender.isSelected {
            
            if let count = provider.subProviders?.count {
                if count > 0 {
                    provider.isProviderSelected = true
                    documentListingTableView.reloadData()
                }
            }
        } else {
            provider.isProviderSelected = false
            documentListingTableView.reloadData()
        }
    }
    
    func getProviderDetail(sender: UIButton) {
        
    }
    
    func moveToSubProviderList(sender: UIButton, selectedProvider: AddProvider?) {
        if let vc = UIStoryboard(name: StoryboardNames.ProviderStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: GlobalSubProvidersViewController.className) as? GlobalSubProvidersViewController {
            
            vc.selectedProvider = selectedProvider
            vc.selectedCareTaker = selectedCareTaker
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
extension ProviderListingViewController: MyProviderProtocol {
    
    func emailTapped(sender: UIButton) {
        view.makeToast("Coming Soon")
    }
    
    func viewDetailsTapped(sender: UIButton) {
        guard let indexPath = getSelectedIndexPath(sender: sender, tableView: documentListingTableView) else {
            return
        }
        let provider = myProvidersArray[indexPath.row]
        if let vc = UIStoryboard(name: StoryboardNames.ProviderStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: ProviderDetailsViewController.className) as? ProviderDetailsViewController {
            
            vc.selectedProvider = provider
            vc.isMyProvider = 1
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}



