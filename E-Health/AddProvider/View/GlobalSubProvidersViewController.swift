//
//  GlobalSubProvidersViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/17/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class GlobalSubProvidersViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var providerListingTableView: UITableView!
    @IBOutlet weak var addDataButton: UIButton!
    @IBOutlet weak var textFieldToDate: UITextField!
    @IBOutlet weak var textFieldFromDate: UITextField!
    @IBOutlet weak var searchDone: UIButton!
    @IBOutlet weak var searchClear: UIButton!
    @IBOutlet weak var fromToDateView: UIView!
    @IBOutlet weak var doneClearButtonView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: - Outlets
    var pageSize = 500
    var pageNumber = 1
    var searchText = String()
    var selectedCareTaker: CaretakerModel?
    var selectedProvider: AddProvider?
    var subProviderArray: [AddProvider]?
    var selectedSubProviderArray: [AddProvider]?
    var bool_ComingFromSearch : Bool?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        tabBarController?.tabBar.isHidden = true
        
        if bool_ComingFromSearch == true {
            lblTitle.text = "Search Providers"
            getProviderList(searchText: "")
            searchBar.text = ""
            
        }
        else {
            getGlobalSubProviderList(searchText: "")

        }
        
    }
    
    //MARK:- UIButton Actions
    @IBAction func addDataButtonAction(_ sender: Any) {
        view.endEditing(true)
        goToAddProviderScreen()
    }
    
    func goToAddProviderScreen() {
        guard let vc = UIStoryboard.init(name: StoryboardNames.ProviderStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: AddProviderViewController.className) as? AddProviderViewController else {return}
        
        vc.selectedCareTaker = selectedCareTaker
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backButton(_ sender: Any) {
        view.endEditing(true)
        bool_ComingFromSearch = false
        navigationController?.popViewController(animated: true)
    }
    
}
extension GlobalSubProvidersViewController {
    
    func getGlobalSubProviderList(searchText: String) {
        
        let params = ["specialityId": selectedProvider?.speciality_id ?? 0, "type_id": selectedProvider?.providerTypeId ?? 0, "page": pageNumber, "per_page": pageSize] as [String : Any]
        
        Utility.shared.startLoader()
        AddProviderViewModel.getGlobalSubProviders(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.subProviderArray = response
                self.selectedSubProviderArray = response
                self.providerListingTableView.reloadData()
            } else {
                if searchText != "" {
                    self.subProviderArray?.removeAll()
                    self.selectedSubProviderArray?.removeAll()
                    self.providerListingTableView.reloadData()
                }
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func getProviderList(searchText: String) {
        
        let params = ["searchBy": "name", "searchVal": searchText] as [String : Any]
        
        Utility.shared.startLoader()
        AddProviderViewModel.getSearchProviderList(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.subProviderArray = response
                self.selectedSubProviderArray = response
                self.providerListingTableView.reloadData()
            } else {
                if searchText != "" {
                    self.subProviderArray?.removeAll()
                    self.selectedSubProviderArray?.removeAll()
                    self.providerListingTableView.reloadData()
                }
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
        
    }
}
//MARK: - Table View Delegate
extension GlobalSubProvidersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
//MARK: - Table View Data Source
extension GlobalSubProvidersViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedSubProviderArray?.count == 0 {
            addDataButton.isHidden = false
        } else {
            addDataButton.isHidden = true
        }
        return selectedSubProviderArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: MyProviderTableViewCell.className)! as? MyProviderTableViewCell else {
            fatalError()
        }
        
        cell.delegate = self
        cell.provider = selectedSubProviderArray?[indexPath.row]
        return cell
    }
    
    /*func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            Utility.shared.showCustomAlertWithCompletion("Delete Record", message: "Are you sure you want to delete this record?", withButtonTypes: ["No", "Yes"], withAlertCompletionBlock: { (alertAction, buttonTitle) in
                
                if buttonTitle == "Yes" {
                    self.deleteDocument(indexpath: indexPath)
                }
            })
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    func deleteDocument(indexpath: IndexPath) {
        
    }*/
    
}
extension GlobalSubProvidersViewController: MyProviderProtocol {
    
    func getSelectedIndexPath(sender: UIButton?, tableView: UITableView?) -> IndexPath? {
        let point = sender?.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView?.indexPathForRow(at: point!)
        return indexPath!
    }
    
    func emailTapped(sender: UIButton) {
        view.makeToast("Coming Soon")
    }
    
    func viewDetailsTapped(sender: UIButton) {
        guard let indexPath = getSelectedIndexPath(sender: sender, tableView: providerListingTableView) else {
            return
        }
        let provider = selectedSubProviderArray?[indexPath.row]
        if let vc = UIStoryboard.init(name: StoryboardNames.ProviderStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: ProviderDetailsViewController.className) as? ProviderDetailsViewController {
            
            vc.selectedCareTaker = selectedCareTaker
            vc.selectedProvider = provider
            vc.isMyProvider = 3
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
//MARK:- Search Bar Delegates
extension GlobalSubProvidersViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let trimmedString = searchText.trimmingCharacters(in: .whitespaces)
        self.searchText = trimmedString
        
        if trimmedString.count >= 2 {
            selectedSubProviderArray = subProviderArray?.filter({ provider -> Bool in
                if searchText.isEmpty {
                    return true
                }
                return provider.providerName?.lowercased().contains(searchText.lowercased()) ?? false
            })
            print(selectedSubProviderArray)
            providerListingTableView.reloadData()
        } else {
            if trimmedString.count == 0 {
                searchBar.endEditing(true)
                selectedSubProviderArray = subProviderArray
            }
            providerListingTableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
