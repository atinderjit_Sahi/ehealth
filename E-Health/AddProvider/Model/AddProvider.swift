//
//  AddProvider.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/11/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class AddProvider: NSObject {

    var id: Int?
    var userId: Int?
    var minorId: Int?
    var cognitoId: String?
    var type: String?
    var typeId: String?
    var providerTypeId: Int?
    var practiceName: String?
    var speciality: String?
    var providerName: String?
    var officeContact: String?
    var homeContact: String?
    var email: String?
    var address1: String?
    var address2: String?
    var region: String?
    var category: String?
    var count: Int?
    var speciality_id: Int?
    var name: String?
    var city: String?
    var state: String?
    var country: String?
    var countryId: Int?
    var zip: String?
    var fax: String?
    var profileImage: String?
    var isVerified: Int?
    var isCognito: Int?
    var dateAdded: String?
    var doctorType: String?
    var subProviders: [SubProvider]?
    var isProviderSelected: Bool?
    var userIdofCustomer: String?
    var passwordOfCustomer: String?
    var providerUrl: String?
    var specialitiesArray = [SpecialistType]()
    var practicesArray = [SpecialistType]()
    var selected : String?
    
    init(value: [String: Any]) {
        id = value["id"] as? Int
        userId = value["user_id"] as? Int
        minorId = value["minor_id"] as? Int
        cognitoId = value["cognito_id"] as? String
        type = value["type_name"] as? String
        doctorType = value["doctor_type"] as? String
        typeId = value["type_id"] as? String
        providerTypeId = value["type_id"] as? Int
        practiceName = value["practice_name"] as? String
        speciality = value["specialty"] as? String
        providerName = value["provider_name"] as? String
        officeContact = value[""] as? String
        homeContact = value["main_office_telephone"] as? String
        email = value["email_address"] as? String
        address1 = value["practice_address_line_1"] as? String
        address2 = value["practice_address_line_2"] as? String
        region = value["region"] as? String
        city = value["city"] as? String
        category = value["category"] as? String
        count = value["count"] as? Int
        speciality_id = value["specialty_id"] as? Int
        name = value["name"] as? String
        state = value["state"] as? String
        country = value["country"] as? String
        zip = value["zip"] as? String
        fax = value["fax"] as? String
        profileImage = value["profile_image"] as? String
        isVerified = value["verified"] as? Int
        isCognito = value["is_cognito"] as? Int
        dateAdded = value["created_at"] as? String
        isProviderSelected = false
        selected = "no"
    }
    
    init(data: [String: Any]) {
        typeId = (data["type_id"] ?? "") as? String
        doctorType = (data["doctor_type"] ?? "") as? String
        practiceName = (data["practice_name"] ?? "") as? String
        speciality = (data["speciality"] ?? "") as? String
        providerName = data["full_name"] as? String
        officeContact = data["contact"] as? String
        homeContact = data["contact"] as? String
        email = data["email"] as? String
        address1 = data["address1"] as? String
        address2 = data["address2"] as? String
        region = data["region"] as? String
        city = data["city"] as? String
        state = data["state"] as? String
        country = data["country"] as? String
        zip = data["zip"] as? String
        fax = data["fax"] as? String
        userIdofCustomer = data["user_id"] as? String
        passwordOfCustomer = data["password"] as? String
        providerUrl = data["url"] as? String
    }
    
    init(providerDetail: [String: Any]) {
        id = providerDetail["id"] as? Int
        providerTypeId = (providerDetail["provider_type"] ?? "") as? Int
        doctorType = (providerDetail["doctor_type"] ?? "") as? String
        practiceName = (providerDetail["practice_name"] ?? "") as? String
        
        let specialityArray = providerDetail["specialty"] as! [[String: AnyObject]]
        let practiceArray = providerDetail["practice_name"] as! [[String: AnyObject]]
//        var specialities = [String]()
//        var specilaityIds = [Int]()
        for speciality in specialityArray {
            specialitiesArray.append(SpecialistType(data: speciality))
//            specialities.append(speciality["name"] as! String)
//            specilaityIds.append(speciality["id"] as! Int)
        }
//        specialitiesArray = specialities
//        specialitiesIds = specilaityIds
        
        for practice in practiceArray {
            practicesArray.append(SpecialistType(dict: practice))
//            specialities.append(speciality["name"] as! String)
//            specilaityIds.append(speciality["id"] as! Int)
        }
        
        providerName = providerDetail["provider_name"] as? String
        officeContact = providerDetail[""] as? String
        homeContact = providerDetail["main_office_telephone"] as? String
        email = providerDetail["email_address"] as? String
        address1 = providerDetail["practice_address_line_1"] as? String
        address2 = providerDetail["practice_address_line_2"] as? String
        region = providerDetail["region"] as? String
        city = providerDetail["city"] as? String
        state = providerDetail["state"] as? String
        countryId = providerDetail["country"] as? Int
        zip = providerDetail["zip"] as? String
        fax = providerDetail["fax"] as? String
        userIdofCustomer = String(describing: providerDetail["user_id"] as? Int ?? 0)
        passwordOfCustomer = providerDetail["password"] as? String
        providerUrl = providerDetail["url"] as? String
    }
}
