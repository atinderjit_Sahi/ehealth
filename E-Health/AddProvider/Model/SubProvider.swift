//
//  SubProvider.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/15/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class SubProvider: NSObject {

    var id: Int?
    var userId: Int?
    var minorId: Int?
    var cognitoId: String?
    var type: String?
    var typeId: Int?
    var practiceName: String?
    var speciality: String?
    var providerName: String?
    var officeContact: String?
    var homeContact: String?
    var email: String?
    var address1: String?
    var address2: String?
    var category: String?
    var count: Int?
    var speciality_id: Int?
    var name: String?
    var city: String?
    var state: String?
    var country: String?
    var zip: String?
    var fax: String?
    var profileImage: String?
    var isVerified: Int?
    var isCognito: Int?
    var dateAdded: String?
    var subProviders: [AddProvider]?
    
    init(value: [String: Any]) {
        id = value["id"] as? Int
        userId = value["cognito_id"] as? Int
        minorId = value["minor_id"] as? Int
        cognitoId = value["cognito_id"] as? String
        type = value["type_name"] as? String
        typeId = value["provider_type"] as? Int
        practiceName = value["practice_name"] as? String
        speciality = value["specialty"] as? String
        providerName = value["provider_name"] as? String
        officeContact = value["main_office_telephone"] as? String
        homeContact = value["main_office_telephone"] as? String
        email = value["email_address"] as? String
        address1 = value["practice_address_line_1"] as? String
        address2 = value["practice_address_line_2"] as? String
        city = value["city"] as? String
        category = value["category"] as? String
        count = value["count"] as? Int
        speciality_id = value["speciality_id"] as? Int
        name = value["name"] as? String
        state = value["state"] as? String
        country = value["country"] as? String
        zip = value["zip"] as? String
        fax = value["fax"] as? String
        profileImage = value["profile_image"] as? String
        isVerified = value["verified"] as? Int
        isCognito = value["is_cognito"] as? Int
        dateAdded = value["created_at"] as? String
    }
}
