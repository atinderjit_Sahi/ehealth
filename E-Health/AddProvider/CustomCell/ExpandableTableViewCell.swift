//
//  ExpandableTableViewCell.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/15/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

protocol ExpandableDetailProtocol {
    func getProviderDetail(sender: UIButton)
}

class ExpandableTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var labelProviderName: UILabel!
    @IBOutlet weak var labelOfficeNumber: UILabel!
    @IBOutlet weak var labelMobileNumber: UILabel!
    @IBOutlet weak var buttonDetails: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    //MARK: - Properties
    var delegate: ExpandableDetailProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.dropShadow()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - UIButton Actions
    @IBAction func buttonDetailsTapped(_ sender: UIButton) {
        delegate?.getProviderDetail(sender: sender)
    }
}
