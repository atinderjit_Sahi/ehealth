//
//  MyProviderTableViewCell.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/14/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
protocol MyProviderProtocol {
    func emailTapped(sender: UIButton)
    func viewDetailsTapped(sender: UIButton)
}
class MyProviderTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var providerProfileImageView: UIImageView!
    @IBOutlet weak var labelProviderName: UILabel!
    @IBOutlet weak var labelProviderCategory: UILabel!
    @IBOutlet weak var labelAddedOn: UILabel!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var expandProviderList: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var delegate: MyProviderProtocol?
    
    //MARK: - View Life CYcle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.dropShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonEmailTapped(_ sender: UIButton) {
        delegate?.emailTapped(sender: sender)
    }
    
    @IBAction func buttonExpandProvidersTapped(_ sender: UIButton) {
        delegate?.viewDetailsTapped(sender: sender)
    }
    
    var provider: AddProvider? {
        didSet {
            labelProviderName.text = provider?.providerName
            labelProviderCategory.text = "Category : " + (provider?.type ?? "N.A")
            let date = provider?.dateAdded?.convertToDateString(currentDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", requiredDateFormat: Constant.dateFormat)
            labelAddedOn.text = "Added " + (date ?? "")
            
            if let profileImage = provider?.profileImage  {
                DispatchQueue.main.async {
                    if let url = URL(string: profileImage) {
                        self.providerProfileImageView.kf.indicatorType = .activity
                        self.providerProfileImageView.kf.setImage(with: url)
                        self.providerProfileImageView.layer.cornerRadius = self.providerProfileImageView.frame.height/2
                        self.providerProfileImageView.clipsToBounds = true
                    }
                }
            }
        }
    }
}
