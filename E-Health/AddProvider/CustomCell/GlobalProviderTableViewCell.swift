//
//  GlobalProviderTableViewCell.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/14/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
protocol ProviderListProtocol {
    func getSubProviderList(sender: UIButton)
    func getProviderDetail(sender: UIButton)
    func moveToSubProviderList(sender: UIButton, selectedProvider: AddProvider?)
}
class GlobalProviderTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var labelSpeciality: UILabel!
    @IBOutlet weak var labelCategory: UILabel!
    @IBOutlet weak var buttonExpandGlobalProviders: UIButton!
    @IBOutlet weak var expandableTableView: UITableView!
    @IBOutlet weak var expandableTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    
    //MARK: - Properties
    var subProviderArray: [SubProvider]?
    var delegate: ProviderListProtocol?
    var selectedCareTaker: CaretakerModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.dropShadow()
        expandableTableView.delegate = self
        expandableTableView.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    weak var provider: AddProvider? {
        didSet {
            
            labelSpeciality.setString(strings: [(provider?.name ?? ""), ("(Total "), String(describing: provider?.count ?? 0), (")")], fontSize: [15,15,15,15], color: [.black, Color.textFieldPlaceholderColor, Color.textFieldPlaceholderColor, Color.textFieldPlaceholderColor], fonts: ["Poppins-Medium", "Poppins-Regular", "Poppins-Regular", "Poppins-Regular"], alignment: .left)
            labelCategory.setString(strings: ["Category : ", (provider?.category ?? "")], fontSize: [13,13], color: [.black, Color.textFieldPlaceholderColor], fonts: ["Poppins-Regular", "Poppins-Regular"], alignment: .left)
            
            
            if provider?.isProviderSelected != buttonExpandGlobalProviders.isSelected {
                print("\(tag)" + " provider: \(provider?.isProviderSelected)" + " Button:\(buttonExpandGlobalProviders.isSelected)")
//                print(provider?.isProviderSelected)
            }
            
            buttonExpandGlobalProviders.isSelected = provider?.isProviderSelected ?? false
            if let count = provider?.subProviders?.count {
                if count > 0 {
                    if provider?.isProviderSelected == true {
//                        buttonExpandGlobalProviders.setImage(UIImage(named: "collapseProvider"), for: .normal)
                        
                        if count == 2 {
                            expandableTableViewHeight.constant = 320
                        } else {
                            expandableTableViewHeight.constant = 190
                        }
                    } else {
//                        buttonExpandGlobalProviders.setImage(UIImage(named: "expandProvider"), for: .normal)
                        expandableTableViewHeight.constant = 0
                    }
                    expandableTableView.reloadData()
                }
            }
        }
    }
    
    //MARK: - UIButton Actions
    @IBAction func buttonExpandGlobalProvidersTapped(_ sender: UIButton) {
        delegate?.getSubProviderList(sender: sender)
    }
}

extension GlobalProviderTableViewCell: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView.init(frame: CGRect(x:0, y:0, width :tableView.frame.size.width , height:60))
        
        let viewMoreButton = UIButton()
        
        viewMoreButton.frame = CGRect(x:0, y:0, width: footerView.frame.size.width ,height:60)
        viewMoreButton.titleLabel?.font = UIFont(name: "Poppins-Regular", size: 16)
        viewMoreButton.setTitle("View More", for: .normal)
        viewMoreButton.setTitleColor(UIColor(red: 10/255, green: 144/255, blue: 203/255, alpha: 1.0), for: .normal)
        footerView.addSubview(viewMoreButton)
        viewMoreButton.tag = section
        viewMoreButton.addTarget(self, action: #selector(didTapSection(sender:)), for: .touchUpInside)
        
        return footerView
    }
    
    @objc func didTapSection(sender : UIButton) {
        delegate?.moveToSubProviderList(sender: sender, selectedProvider: provider)
    }
}

extension GlobalProviderTableViewCell: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return provider?.subProviders?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ExpandableTableViewCell.className, for: indexPath) as? ExpandableTableViewCell else {
            fatalError()
        }
        
        let subProvider = provider?.subProviders?[indexPath.row]
        cell.buttonDetails.tag = indexPath.row
        cell.delegate = self
        cell.labelProviderName.text = subProvider?.providerName
        
        cell.labelOfficeNumber.setString(strings: ["Mobile ", (subProvider?.homeContact ?? "")], fontSize: [12,12], color: [.black, Color.textFieldPlaceholderColor], fonts: ["Poppins-Medium", "Poppins-Regular"], alignment: .left)
        
//        cell.labelOfficeNumber.text = "Mobile" + (subProvider?.homeContact ?? "")
        //cell.labelMobileNumber.text = subProvider?.homeContact
        
        return cell
    }
    
}
extension GlobalProviderTableViewCell: ExpandableDetailProtocol {
    
    func getProviderDetail(sender: UIButton) {
        if let subproviders = provider?.subProviders {
            if let vc = UIStoryboard(name: StoryboardNames.ProviderStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: ProviderDetailsViewController.className) as? ProviderDetailsViewController {
                
                vc.selectedGlobalProvider = subproviders[sender.tag]
                vc.isMyProvider = 2
                let nav = self.window?.rootViewController as? UINavigationController
                nav?.pushViewController(vc, animated: true)
            }
        }
    }
}
