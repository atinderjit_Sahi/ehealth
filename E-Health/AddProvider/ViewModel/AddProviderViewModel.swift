//
//  AddProviderViewModel.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/11/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class AddProviderViewModel: NSObject {

    static func getMyProviders(parameters: [String: Any], completion: @escaping (_ response: [AddProvider]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.getMyProviders
        let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            var providerListingArray = [AddProvider]()
                            for item in data {
                                providerListingArray.append(AddProvider(value: item))
                            }
                            completion(providerListingArray, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func shareDatMyProviders(parameters: [String: Any], completion: @escaping (_ response: [AddProvider]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.getShareDataMyProvider
        let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            var providerListingArray = [AddProvider]()
                            for item in data {
                                providerListingArray.append(AddProvider(value: item))
                            }
                            completion(providerListingArray, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func getGlobalProviders(parameters: [String: Any], completion: @escaping (_ response: [AddProvider]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.getGlobalProviderList
        //let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            var providerListingArray = [AddProvider]()
                            for item in data {
                                print(data)
                                let provider = AddProvider(value: item)
                                
                                if let providers = item["providers"] as? [[String: AnyObject]] {
                                    var subProvidersArray = [SubProvider]()
                                    for item in providers {
                                        subProvidersArray.append(SubProvider(value: item))
                                    }
                                    provider.subProviders = subProvidersArray
                                }
                                providerListingArray.append(provider)
                            }
                            completion(providerListingArray, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func getGlobalSubProviders(parameters: [String: Any], completion: @escaping (_ response: [AddProvider]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.getProviderListBySpeciality
        //let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            var providerListingArray = [AddProvider]()
                            for item in data {
                                providerListingArray.append(AddProvider(value: item))
                            }
                            completion(providerListingArray, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func getSearchProviderList(parameters: [String: Any], completion: @escaping (_ response: [AddProvider]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.getProviderListSearch
        //let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            var providerListingArray = [AddProvider]()
                            for item in data {
                                providerListingArray.append(AddProvider(value: item))
                            }
                            completion(providerListingArray, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func getProviderDetail(parameters: [String: Any], completion: @escaping (_ response: AddProvider?, _ error: String?) -> Void) {
        
        let url = Constant.URL.getProviderDetail
//        let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [String: AnyObject] {
//                            var providerListingArray = [AddProvider]()
//                            for item in data {
                               let provider = AddProvider(providerDetail: data)
//                            }
                            completion(provider, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func removeProvider(parameters: [String: Any], completion: @escaping (_ response: Bool, _ error: String?) -> Void) {
        
        let url = Constant.URL.deleteProvider
        //let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
//                        if let data = result["data"] as? [[String: AnyObject]] {
                            completion(true, nil)
//                        } else {
//                            completion(false , result["message"] as? String)
//                        }
                    } else {
                        completion(false , result["message"] as? String)
                    }
                } else {
                    completion(false , result["message"] as? String)
                }
            } else {
                completion(false , error?.localizedDescription)
            }
        }
    }
    
    static func addProvider(params: [String: Any], completion: @escaping (_ response: String?, _ error: String?) -> Void) {

        let url = Constant.URL.addProvider
        //let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]

        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            print(data)
                            completion(result["message"] as? String, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func updateProvider(params: [String: Any], completion: @escaping (_ response: String?, _ error: String?) -> Void) {

        let url = Constant.URL.updateProvider
        //let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]

        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
//                        if let data = result["data"] as? [String: AnyObject] {
//                            print(data)
                            completion(result["message"] as? String, nil)
//                        } else {
//                            completion(nil , result["message"] as? String)
//                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func sendPdfDetail(params: [String: Any], completion: @escaping (_ response: String?, _ error: String?) -> Void) {

        let url = Constant.URL.sendPdfDetail
        //let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]

        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
//                        if let data = result["data"] as? [String: AnyObject] {
//                            print(data)
                            completion(result["message"] as? String, nil)
//                        } else {
//                            completion(nil , result["message"] as? String)
//                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func assignProvider(params: [String: Any], completion: @escaping (_ response: String?, _ error: String?) -> Void) {

        let url = Constant.URL.assignProvider
        //let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]

        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            completion(result["message"] as? String, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
}
