//
//  ProviderSignupViewModel.swift
//  E-Health
//
//  Created by Prabjot Kaur on 6/16/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class ProviderSignupViewModel: NSObject {
    static func searchSpecialist(text: String, completion: @escaping (_ response: [SpecialistType]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.autocompleteSpecialist + text
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                if let data = result["data"] as? [[String: AnyObject]] {
                    var specialistArray = [SpecialistType]()
                    for item in data {
                        specialistArray.append(SpecialistType(value: item))
                    }
                    completion(specialistArray, nil)
                } else {
                    completion(nil , result["message"] as? String)
                }
                
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func searchPracticeName(text: String, completion: @escaping (_ response: [SpecialistType]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.autocompletePracticeName + text
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                if let data = result["data"] as? [[String: AnyObject]] {
                    var specialistArray = [SpecialistType]()
                    for item in data {
                        specialistArray.append(SpecialistType(value: item))
                    }
                    completion(specialistArray, nil)
                } else {
                    completion(nil , result["message"] as? String)
                }
                
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
}
