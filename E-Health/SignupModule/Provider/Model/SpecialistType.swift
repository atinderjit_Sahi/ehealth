//
//  SpecialistType.swift
//  E-Health
//
//  Created by Prabjot Kaur on 6/16/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class SpecialistType {
    var code: String?
    var id: Int?
    var text: String?
    
    init() {
        
    }
    
    init(value: [String: AnyObject]) {
        self.id = value["id"] as? Int
        self.text = value["text"] as? String
        self.code = value["code"] as? String
    }
    
    init(data: [String: AnyObject]) {
        self.id = data["id"] as? Int
        self.text = data["name"] as? String
        self.code = data["code"] as? String
    }
    
    init(dict: [String: AnyObject]) {
        self.id = dict["id"] as? Int
        self.text = dict["provider_practice_name"] as? String
        self.code = dict["code"] as? String
    }
}
