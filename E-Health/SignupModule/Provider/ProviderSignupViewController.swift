//
//  ProviderSignupViewController.swift
//  E-Health
//
//  Created by Prabjot Kaur on 6/11/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import DropDown
import IQKeyboardManagerSwift

class ProviderSignupViewController: UIViewController {

    //Country List view Outlets
    @IBOutlet var specialitySearchView: UIView!
    @IBOutlet var countrySearchView: UIView!
    @IBOutlet weak var countrySearchBar: UISearchBar!
    @IBOutlet weak var specialitySearchBar: UISearchBar!
    @IBOutlet weak var countryListTableView: UITableView!
    @IBOutlet weak var specialityListTableView: UITableView!
    
    @IBOutlet weak var registerAsTextField: ExtendedTextField!
    @IBOutlet weak var primaryDoctorTextField: ExtendedTextField!
    @IBOutlet weak var practiceNameTextField: ExtendedTextField!
    @IBOutlet weak var specialistInTextField: ExtendedTextField!
    @IBOutlet weak var firstNameTextField: ExtendedTextField!
    @IBOutlet weak var middleNameTextField: ExtendedTextField!
    @IBOutlet weak var lastNameTextfield: ExtendedTextField!
    @IBOutlet weak var emailTextField: ExtendedTextField!
    @IBOutlet weak var dateOfBirthTextField: ExtendedTextField!
    @IBOutlet weak var countryTextField: ExtendedTextField!
    @IBOutlet weak var contactNumberTextField: ExtendedTextField!
    @IBOutlet weak var passwordTextField: ExtendedTextField!
    @IBOutlet weak var confirmPasswordTextField: ExtendedTextField!
    @IBOutlet weak var countryCodeTextField: ExtendedTextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var primaryDoctorView: UIView!
    @IBOutlet weak var specialistInView: UIView!
    @IBOutlet weak var practiceNameView: UIView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var searchSpecialityView: UIView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var passwordDisplayToggleButton: UIButton!
    @IBOutlet weak var confirmPassDisplayToggleButton: UIButton!
    
    //Country list variables
    var countriesArray = MasterData()
    var countriesListArray = [CountryList]()
    var selectedCountry = CountryList()
    var countrySearchArray = [CountryList]()
    var isCountrySearchedEmpty = true
    
    var datePicker = UIDatePicker()
    
    var providerTypes = [ProviderType]()
    
    //Specialist arrays
    var isSpecialistSelected = false
    var specialistArray = [SpecialistType]()
    var searchSpecialistArray = [SpecialistType]()
    var selectedSpecialistArray = [SpecialistType]()
    
    //Practice Name Arrays
    var isPracticeNameSelected = false
    var practiceNameArray = [SpecialistType]()
    var searchPracticeNameArray = [SpecialistType]()
    var selectedPracticeArray = [SpecialistType]()
    
    //Selected options
    var selectedProviderType = ProviderType()
    var selectedDoctorType = String()
    var isAgreeToTermCondition = Bool()
        
    //MARK:- View Hierarchy
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        countrySearchBar.setSearchBarUI()
        specialitySearchBar.setSearchBarUI()
        setupSpecialityUI()
        setupTextFieldsProperties()
        
        getMasterData()
        registerAsTextField.attributedPlaceholder = NSAttributedString(string: registerAsTextField.placeholder ?? "Provider Category*",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    @IBAction func passwordDisplayToggleButtonAction(_ sender: Any) {
        if passwordTextField.isSecureTextEntry {
            passwordTextField.isSecureTextEntry = false
            passwordDisplayToggleButton.setImage(UIImage(named: "unhide_password"), for: .normal)
        } else {
            passwordTextField.isSecureTextEntry = true
            passwordDisplayToggleButton.setImage(UIImage(named: "hide_password"), for: .normal)
        }
    }
    @IBAction func confirmPasswordDisplayToggleButtonAction(_ sender: Any) {
        if confirmPasswordTextField.isSecureTextEntry {
            confirmPasswordTextField.isSecureTextEntry = false
            confirmPassDisplayToggleButton.setImage(UIImage(named: "unhide_password"), for: .normal)
        } else {
            confirmPasswordTextField.isSecureTextEntry = true
            confirmPassDisplayToggleButton.setImage(UIImage(named: "hide_password"), for: .normal)
        }
    }
    @IBAction func submitButtonAction(_ sender: Any) {
        IQKeyboardManager.shared.resignFirstResponder()
        submitButton.isUserInteractionEnabled = false
        if !validateTextFields() {
            submitButton.isUserInteractionEnabled = true
            return
        }
        
        self.signupProvider()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countryListCrossButton(_ sender: Any) {
        countrySearchBar.endEditing(true)
        hideAlertWithBounceEffect(myView: countrySearchView)
    }
    
    @IBAction func specialityCloseButton(_ sender: Any) {
        countrySearchBar.endEditing(true)
        hideAlertWithBounceEffect(myView: specialitySearchView)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        
        //If is Specialist multiple selection list
        if isSpecialistSelected {
            
            //Set text in text field
            if selectedSpecialistArray.count != 0 {
                if selectedSpecialistArray.count > 1 {
                    self.specialistInTextField.text = (selectedSpecialistArray.first?.text ?? "") + " & \(selectedSpecialistArray.count - 1) more"
                } else {
                    self.specialistInTextField.text = selectedSpecialistArray.first?.text
                }
            } else {
                self.specialistInTextField.text = ""
            }
            countrySearchBar.endEditing(true)
            hideAlertWithBounceEffect(myView: specialitySearchView)
            return
        }
        
        if isPracticeNameSelected {
            
            //Set text in text field
            if selectedPracticeArray.count != 0 {
                
                if selectedPracticeArray.count > 1 {
                    self.practiceNameTextField.text = (selectedPracticeArray.first?.text ?? "") + " & \(selectedPracticeArray.count - 1) more"
                } else {
                    self.practiceNameTextField.text = selectedPracticeArray.first?.text
                }
            } else {
                self.practiceNameTextField.text = ""
            }
            countrySearchBar.endEditing(true)
            hideAlertWithBounceEffect(myView: specialitySearchView)
            return
        }
    }
    
    @IBAction func buttonReadTermsConditionsTapped(_ sender: Any) {
        view.endEditing(true)
        guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: WebViewController.className) as? WebViewController else {return}
        
        vc.webUrlString = "http://3.236.116.101/provider/terms-conditions"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonPrivacyPolicyTapped(_ sender: Any) {
        view.endEditing(true)
        guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: WebViewController.className) as? WebViewController else {return}
        
        vc.webUrlString = "http://3.236.116.101/provider/privacy-policy"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonAgreeTermsConditionsTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        isAgreeToTermCondition = sender.isSelected
    }
}

extension ProviderSignupViewController {
    func setupUI() {
        countrySearchView.frame = self.view.frame
        countrySearchView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        countrySearchBar.delegate = self
        
        countryListTableView.tableFooterView = UIView()
        countrySearchBar.showsScopeBar = false
        countrySearchBar.placeholder = "Search Country Name"
    }
    
    func setupSpecialityUI() {
        specialitySearchView.frame = self.view.frame
        specialitySearchView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        specialitySearchBar.delegate = self
        
        specialityListTableView.tableFooterView = UIView()
        specialitySearchBar.showsScopeBar = false
        specialitySearchBar.placeholder = "Search Speciality Name"
    }
    
    func setupTextFieldsProperties() {
        countryTextField.setRightImage(rightImage: UIImage(named: "dropDown")!)
        firstNameTextField.sizeLimit = true
        middleNameTextField.sizeLimit = true
        lastNameTextfield.sizeLimit = true
        confirmPasswordTextField.sizeLimit = true
        passwordTextField.sizeLimit = true
        
        firstNameTextField.minLength = 2
        middleNameTextField.minLength = 2
        lastNameTextfield.minLength = 2
        confirmPasswordTextField.minLength = 8
        passwordTextField.minLength = 8
        
        firstNameTextField.maxLength = 15
        middleNameTextField.maxLength = 15
        lastNameTextfield.maxLength = 15
        confirmPasswordTextField.maxLength = 16
        passwordTextField.maxLength = 16
    }
}

//MARK:- API methods
extension ProviderSignupViewController {
    func getMasterData() {
        Utility.shared.startLoader()
        RegisterViewModel.getMasterData() { (masterData, error) in
            Utility.shared.stopLoader()
            if error == nil{
                if masterData.countries.count != 0 {
                    self.countriesListArray = masterData.countries
                    self.countrySearchArray = self.countriesListArray
                    self.countryListTableView.reloadData()
                }
                if masterData.providerTypes.count != 0 {
                    self.providerTypes = masterData.providerTypes
                }
            }
        }
    }
    
    func signupProvider() {
        var userData = [String: String]()
        
        userData["custom:provider_type"] = String(selectedProviderType.id ?? 0)
        
        if selectedProviderType.name == "Doctor" || selectedProviderType.name == "Hospital" {
            //For Practice Name
            var practiceID = String()
            if selectedPracticeArray.count != 0 {
                for practice in selectedPracticeArray {
                    let practiceId = String(practice.id ?? 0)
                    let practiceName = practice.text ?? ""
                    practiceID.append(practiceName + "-" + practiceId + ";")
                }
                practiceID.removeLast()
            }
            userData["custom:practice_name"] = practiceID
    
            //For Specialist Name
            var specialistID = String()
            if selectedSpecialistArray.count != 0 {
                for specialist in selectedSpecialistArray {
                    let specialistId = String(specialist.id ?? 0)
                    let specialityName = specialist.text ?? ""
                    specialistID.append(specialityName + "-" + specialistId + ";")
                }
                specialistID.removeLast()
            }
            userData["custom:specialist_in"] = specialistID
            
            //For doctor type Primary/Secondary
            if selectedDoctorType == "Primary Doctor" {
                userData["custom:doctor_type"] = "1"
            } else {
                userData["custom:doctor_type"] = "2"
            }
        }
        
        let firstName = firstNameTextField.text
        let middleName = middleNameTextField.text
        let lastName = lastNameTextfield.text
        let phoneNumber = "+" + (selectedCountry.phonecode!) + (contactNumberTextField.text!)
        
        userData["name"] = firstName! + " " + lastName!
        //userData["middle_name"] = middleName
        userData["email"] = emailTextField.text
        userData["phone_number"] = phoneNumber
        userData["family_name"] = "provider"
        userData["custom:country"] = String(selectedCountry.id ?? 0)
        userData["custom:mid_name"] = middleName
        userData["custom:dob"] = dateOfBirthTextField.text
        userData["custom:new_user"] = "1"
        userData["custom:widget_items"] = "1,2"
        userData["custom:mfa_enabled"] = "false"
        
        AWSAuth.shared.signUpWithAWS(requestData: userData, password: passwordTextField.text!, controller: self) { (status) in
            Utility.shared.stopLoader()
            DispatchQueue.main.async {
                self.submitButton.isUserInteractionEnabled = true
            }
            if status {
//                self.navigateWhenMfaNotEnabled()
                self.navigateWhenMfaEnabled()
            } else {
                print(status)
            }
        }
    }
    
    func navigateWhenMfaEnabled() {
        DispatchQueue.main.async {
            guard let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: VerificationViewController.className) as? VerificationViewController else {
                return
            }
            vc.signupEmail = self.emailTextField.text!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func navigateWhenMfaNotEnabled() {
        DispatchQueue.main.async {
            guard let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: AuthenticationViewController.className) as? AuthenticationViewController else {
                return
            }
            vc.isComingFromSignup = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func searchSpecialist(text: String) {
        ProviderSignupViewModel.searchSpecialist(text: text){ (specialists, error) in
            if specialists?.count != 0 && specialists != nil {
                self.specialistArray = specialists!
                self.searchSpecialistArray = self.specialistArray
            }
            self.specialityListTableView.reloadData()
        }
    }
    
    func searchPracticeName(text: String) {
        ProviderSignupViewModel.searchPracticeName(text: text){ (practiceName, error) in
            if practiceName?.count != 0 && practiceName != nil {
                self.practiceNameArray = practiceName!
                self.searchPracticeNameArray = self.practiceNameArray
            }
            self.specialityListTableView.reloadData()
        }
    }
}

//MARK:- Drop Down
extension ProviderSignupViewController {
    func initdropDown (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                if textField == self.registerAsTextField {
                    self.registerAsTextField.text  = item
                    self.selectedProviderType = self.providerTypes[index]
                    if item != "Doctors" /*&& item != "Hospital"*/ {
                        self.specialistInTextField.text = ""
                        self.primaryDoctorTextField.text = ""
                        self.selectedSpecialistArray.removeAll()
                        self.practiceNameTextField.text = ""
                        self.selectedPracticeArray.removeAll()
//                        self.specialistInTextField.isUserInteractionEnabled = false
//                        self.primaryDoctorTextField.isUserInteractionEnabled = false
                        self.primaryDoctorView.isHidden = true
                        self.specialistInView.isHidden = true
                        self.practiceNameView.isHidden = true
                    } else {
                        self.specialistInTextField.text = ""
                        self.selectedSpecialistArray.removeAll()
                        self.primaryDoctorTextField.text = ""
                        self.practiceNameTextField.text = ""
                        self.selectedPracticeArray.removeAll()
//                        self.specialistInTextField.isUserInteractionEnabled = true
//                        self.primaryDoctorTextField.isUserInteractionEnabled = true
                        self.primaryDoctorView.isHidden = false
                        self.specialistInView.isHidden = false
                        self.practiceNameView.isHidden = false
                    }
                }
                else if textField == self.primaryDoctorTextField {
                    self.primaryDoctorTextField.text  = item
                    self.selectedDoctorType = item
                }
                else if textField == self.specialistInTextField {
                    self.specialistInTextField.text  = item
                }
                
                dropDown.hide()
            }
            dropDown.show()
        }
    }
}

//MARK:- Show/Hide Country list view
extension ProviderSignupViewController {
    func showAlertWithBounceEffect(myView : UIView,onWindow:Bool) {
        //        let indexPath = IndexPath(row: 0, section: 0)
        //        countryListTableView.scrollToRow(at: indexPath, at: .top, animated: true)
        let window = UIApplication.shared.keyWindow!
        myView.isHidden = false
        if onWindow == true {
            window.addSubview(myView)
        }  else {
            self.view.addSubview(myView)
            self.view.bringSubviewToFront(myView)
        }
        myView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            myView.transform = .identity
        }, completion: {(finished: Bool) -> Void in
        })
    }
    
    func hideAlertWithBounceEffect(myView : UIView) {
        self.isSpecialistSelected = false
        self.isPracticeNameSelected = false
        myView.transform = .identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            myView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: {(finished: Bool) -> Void in
            myView.isHidden = true
        })
    }
    
    func setCountryCode(forIndex: Int) {
        selectedCountry = countriesListArray[forIndex]
        countryCodeTextField.text = "+" + (selectedCountry.phonecode ?? "")
    }
}

//MARK:- Date Picker Methods
extension ProviderSignupViewController {
    func showDatePicker(){
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat //"dd MMM, yyyy"
        var date = Date()
        if dateOfBirthTextField.text != "" {
            date = formater.date(from: dateOfBirthTextField.text!) ?? Date()
        }
        
        let screenWidth = UIScreen.main.bounds.width
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -110, to: Date())
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datePicker.setDate(datePicker.maximumDate ?? date, animated: true)
        
        if #available(iOS 14.0, *)
        {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()

//            datePicker.datePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        dateOfBirthTextField.inputAccessoryView = toolbar
        dateOfBirthTextField.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = Constant.dateFormat //"dd MMM, yyyy"
        dateOfBirthTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}

//MARK:- CountryList Table view delegates
extension ProviderSignupViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections = 1
        if tableView == specialityListTableView {
            if isSpecialistSelected {
                numOfSections = showNoRecordFound(dataArray: searchSpecialistArray, tableview: tableView)
                return numOfSections
            }
            else {
                numOfSections = showNoRecordFound(dataArray: searchPracticeNameArray, tableview: tableView)
                return numOfSections
            }
        } else {
            return numOfSections
        }
    }
    
    func showNoRecordFound(dataArray: [SpecialistType], tableview: UITableView) -> Int {
        var noOfSections: Int = 0
        noDataLabel.isHidden = true
        if !dataArray.isEmpty {
            tableview.separatorStyle = .singleLine
            noOfSections            = 1
            tableview.isScrollEnabled = true
        }
        else {
            tableview.isScrollEnabled = false
            noDataLabel.numberOfLines = 0
            if specialitySearchBar.text == "" {
                noDataLabel.text          = "You have to type in something, you are looking for."
            } else {
                noDataLabel.text          = "No Records Found"
            }
            
            noDataLabel.font = UIFont(name: "Montserrat", size: 17)
            noDataLabel.textColor     = UIColor.darkGray
            noDataLabel.textAlignment = .center
            noDataLabel.isHidden = false
            tableview.separatorStyle  = .none
        }
        return noOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == specialityListTableView {
            if isSpecialistSelected {
                return searchSpecialistArray.count
            }
            else {
                return searchPracticeNameArray.count
            }
        } else {
            return countrySearchArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        if tableView == specialityListTableView {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SpecialityTableViewCell") as? SpecialityTableViewCell else {
                return UITableViewCell()
            }
            
            cell.delegate = self
            
            //For Specialist List
            if isSpecialistSelected {
                cell.countryLabel.text = self.searchSpecialistArray[indexPath.row].text
                if selectedSpecialistArray.contains(where: {$0.text == self.searchSpecialistArray[indexPath.row].text} ) {
                    cell.buttonCheckBox.isSelected = true
                    cell.countryLabel.textColor = UIColor(red: 31/255, green: 181/255, blue: 245/255, alpha: 1.0)
                    //                cell.accessoryType = .checkmark
                } else {
                    cell.countryLabel.textColor = .darkGray
                    cell.buttonCheckBox.isSelected = false
                    //                cell.accessoryType = .none
                }
            }
            else if isPracticeNameSelected {
                cell.countryLabel.text = self.searchPracticeNameArray[indexPath.row].text
                if selectedPracticeArray.contains(where: {$0.text == self.searchPracticeNameArray[indexPath.row].text} ) {
                    cell.countryLabel.textColor = UIColor(red: 31/255, green: 181/255, blue: 245/255, alpha: 1.0)
                    cell.buttonCheckBox.isSelected = true
                    //                cell.accessoryType = .checkmark
                } else {
                    cell.countryLabel.textColor = .darkGray
                    cell.buttonCheckBox.isSelected = false
                    //                cell.accessoryType = .none
                }
            }
            return cell
            
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCellTableViewCell") as? CountryCellTableViewCell else {
                return UITableViewCell()
            }
            
            cell.countryLabel.text = countrySearchArray[indexPath.row].name
            if selectedCountry.name == countrySearchArray[indexPath.row].name {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerView = UIView()
        headerView.backgroundColor = .clear
        if tableView == specialityListTableView {
            headerView = specialitySearchBar
        } else {
            headerView = countrySearchBar
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Get Selected Cell
        var selectedCell: UITableViewCell?
        if let cell = tableView.cellForRow(at: indexPath) {
            selectedCell = cell
        }
        
        if tableView == countryListTableView {
            //If is Specialist multiple selection list
            /*if isSpecialistSelected {
                //If already selected, unselect
                if selectedSpecialistArray.contains(where: {$0.text == self.searchSpecialistArray[indexPath.row].text}) {
                    let index = selectedSpecialistArray.firstIndex(where: {$0.text == self.searchSpecialistArray[indexPath.row].text})!
                    selectedSpecialistArray.remove(at: index)
                    selectedCell?.accessoryType = .none
                }
                //If not already select, set selected
                else {
                    selectedSpecialistArray.append(self.searchSpecialistArray[indexPath.row])
                    selectedCell?.accessoryType = .checkmark
                }
                //Set text in text field
                if selectedSpecialistArray.count != 0 {
                    if selectedSpecialistArray.count > 1 {
                        self.specialistInTextField.text = (selectedSpecialistArray.first?.text ?? "") + " & \(selectedSpecialistArray.count - 1) more"
                    } else {
                        self.specialistInTextField.text = selectedSpecialistArray.first?.text
                    }
                } else {
                    self.specialistInTextField.text = ""
                }
                return
            }
            
            if isPracticeNameSelected {
                //If already selected, unselect
                if selectedPracticeArray.contains(where: {$0.text == self.searchPracticeNameArray[indexPath.row].text}) {
                    let index = selectedPracticeArray.firstIndex(where: {$0.text == self.searchPracticeNameArray[indexPath.row].text})!
                    selectedPracticeArray.remove(at: index)
                    selectedCell?.accessoryType = .none
                }
                //If not already select, set selected
                else {
                    selectedPracticeArray.append(self.searchPracticeNameArray[indexPath.row])
                    selectedCell?.accessoryType = .checkmark
                }
                //Set text in text field
                if selectedPracticeArray.count != 0 {
                    if selectedPracticeArray.count > 1 {
                        self.practiceNameTextField.text = (selectedPracticeArray.first?.text ?? "") + " & \(selectedPracticeArray.count - 1) more"
                    } else {
                        self.practiceNameTextField.text = selectedPracticeArray.first?.text
                    }
                } else {
                    self.practiceNameTextField.text = ""
                }
                return
            }*/
            
            //For Country list, when a country is selected
            countrySearchBar.endEditing(true)
            hideAlertWithBounceEffect(myView: countrySearchView)
            if isCountrySearchedEmpty {
                print(countriesListArray[indexPath.row].name as Any)
            }else{
                print(countrySearchArray[indexPath.row].name as Any)
            }
            
            self.selectedCountry = countrySearchArray[indexPath.row]
            countryTextField.text = self.selectedCountry.name
            countryCodeTextField.text = "+" + (selectedCountry.phonecode ?? "")
        }
        
    }
}

//MARK: - Country Search Bar Protocol
extension ProviderSignupViewController: CountryCellProtocol {
    
    func buttonCheckBox(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        selectUnselectCell(sender: sender)
    }
    
    func getSelectedIndexPath(sender: UIButton?, tableView: UITableView?) -> IndexPath? {
        let point = sender?.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView?.indexPathForRow(at: point!)
        return indexPath!
    }

    func selectUnselectCell(sender: UIButton) {
        
        //Get Selected Cell
        var selectedCell: UITableViewCell?
        
        guard let indexPath = getSelectedIndexPath(sender: sender, tableView: specialityListTableView) else {
            return
        }
        
        if let cell = specialityListTableView.cellForRow(at: indexPath) as? SpecialityTableViewCell {
            selectedCell = cell
        }
        
        //If is Specialist multiple selection list
        if isSpecialistSelected {
            
            //If already selected, unselect
            if selectedSpecialistArray.contains(where: {$0.text == self.searchSpecialistArray[indexPath.row].text}) {
                
                let index = selectedSpecialistArray.firstIndex(where: {$0.text == self.searchSpecialistArray[indexPath.row].text})!
                
                selectedSpecialistArray.remove(at: index)
            }
            //If not already select, set selected
            else {
                selectedSpecialistArray.append(self.searchSpecialistArray[indexPath.row])
            }
            
            self.specialityListTableView.reloadData()
            return
        }
        
        if isPracticeNameSelected {
            
            //If already selected, unselect
            
            if selectedPracticeArray.contains(where: {$0.text == self.searchPracticeNameArray[indexPath.row].text}) {
                
                let index = selectedPracticeArray.firstIndex(where: {$0.text == self.searchPracticeNameArray[indexPath.row].text})!
                
                selectedPracticeArray.remove(at: index)
            }
            //If not already select, set selected
            else {
                selectedPracticeArray.append(self.searchPracticeNameArray[indexPath.row])
            }
            self.specialityListTableView.reloadData()
            return
        }
    }
    
}

//MARK:- Search Bar Delegates
extension ProviderSignupViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //For Specialist search list
        if isSpecialistSelected {
            if searchText.count >= 2 {
                self.searchSpecialist(text: searchText)
            } else {
                if searchText.count == 0 {
                    searchSpecialistArray = selectedSpecialistArray
                    specialityListTableView.reloadData()
                } else {
                    specialistArray.removeAll()
                    searchSpecialistArray = specialistArray
                    specialityListTableView.reloadData()
                }
            }
        }
        
        if isPracticeNameSelected {
            if searchText.count >= 2 {
                self.searchPracticeName(text: searchText)
            } else {
                if searchText.count == 0 {
                    searchPracticeNameArray = selectedPracticeArray
                    specialityListTableView.reloadData()
                } else {
                    practiceNameArray.removeAll()
                    searchPracticeNameArray = practiceNameArray
                    specialityListTableView.reloadData()
                }
            }
        }
        
        //For Country Search List
        else {
            countrySearchArray = countriesListArray.filter({ country -> Bool in
                if searchText.isEmpty {
                    isCountrySearchedEmpty = true
                    return true
                }
                isCountrySearchedEmpty = false
                return country.name?.lowercased().contains(searchText.lowercased()) ?? false
            })
            countryListTableView.reloadData()
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        switch selectedScope {
        case 0:
            countrySearchArray = countriesListArray
        default:
            break
        }
        countryListTableView.reloadData()
    }
}

//MARK:- Validation
extension ProviderSignupViewController {
    func validateTextFields() -> Bool {
        let objValidate   = Validations()
        
        //Register as Validation
        var (status,message) = objValidate.validateField(registerAsTextField)
        if !status{
        switch message {
        case "Empty field":
            self.view.makeToast("Please select a provider category")
            break
            
            default:
                break
            }
            return false
        }
        
        //Doctor type Validation
        if selectedProviderType.name == "Doctors" || selectedProviderType.name == "Hospital" {
            (status,message) = objValidate.validateField(primaryDoctorTextField)
            if !status{
            switch message {
            case "Empty field":
                self.view.makeToast("Please select a doctor type")
                break
                
                default:
                    break
                }
                return false
            }
        }
        
        
        //Practice Name Validation
        if selectedProviderType.name == "Doctors" || selectedProviderType.name == "Hospital" {
            (status,message) = objValidate.validateField(practiceNameTextField)
            if !status{
            switch message {
            case "Empty field":
                self.view.makeToast("Please select practice name")
                break
                
                default:
                    break
                }
                return false
            }
        }
        
        
        //Specialist Validation
        if selectedProviderType.name == "Doctors" || selectedProviderType.name == "Hospital"{
            (status,message) = objValidate.validateField(specialistInTextField)
            if !status{
            switch message {
            case "Empty field":
                self.view.makeToast("Please select specialisation")
                break
                
                default:
                    break
                }
                return false
            }
        }
        
        
        //First name field Validation
        (status,message) = objValidate.validateField(firstNameTextField)
        if !status{
            switch message {
            case "Empty field":
                self.view.makeToast("Please enter provider name")
                break
                
            case "Short length text":
                self.view.makeToast("Provider name must be atleast \(firstNameTextField.minLength) characters")
                break
                
            case "Long length text":
                self.view.makeToast("Provider name must be maximum \(firstNameTextField.maxLength) characters")
                break
                
            default:
                break
            }
            return false
        }
        
        //Middle Name Field Validation
        /*if middleNameTextField.text?.count != 0{
            (status,message) = objValidate.validateField(middleNameTextField)
            if !status{
                switch message {
                case "Empty field":
                    self.view.makeToast("Please enter your Middle name")
                    break
                    
                case "Short length text":
                    self.view.makeToast("Middle name must be atleast \(middleNameTextField.minLength) characters")
                    break
                    
                case "Long length text":
                    self.view.makeToast("Middle name must be maximum \(middleNameTextField.maxLength) characters")
                    break
                    
                default:
                    break
                }
                return false
            }
        }*/
        
        //Last Name Field Validation
        /*(status,message) = objValidate.validateField(lastNameTextfield)
        if !status{
            switch message {
            case "Empty field":
                self.view.makeToast("Please enter your Last name")
                
                break
            case "Short length text":
                self.view.makeToast("Last name must be atleast \(lastNameTextfield.minLength) characters")
                
                break
            case "Long length text":
                self.view.makeToast("Last name must be maximum \(lastNameTextfield.maxLength) characters")
                
                break
            default:
                break
            }
            return false
        }*/
        
        //Email Field Validation
        (status,message) = objValidate.validateEmailField(emailTextField)
        if !status{
            switch message {
            case "Empty field":
                self.view.makeToast("Please enter your Email")
                break
                
            case "Invalid Email format":
                self.view.makeToast("Please enter a valid Email")
                break
                                
            default:
                break
            }
            return false
        }
        
        //DOB Field Validation
        /*if dateOfBirthTextField.text == "" {
            self.view.makeToast("Please enter your Date of Birth")
            return false
        }*/
        
        //Country Field Validation
        if countryTextField.text!.isEmpty{
            self.view.makeToast("Please select your country")
            return false
        }
        if (countryTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0 {
            countryTextField.text = ""
            self.view.makeToast("Please select your country")
            return false
        }
        
        if contactNumberTextField.text!.isEmpty {
            self.view.makeToast("Please enter your contact number")
            return false
        } else if contactNumberTextField.text!.count > 14 {
            self.view.makeToast("Contact number cannot be more than 14 digits")
            return false
        } else if contactNumberTextField.text!.count < 9 {
            self.view.makeToast("Contact number cannot be less than 9 digits")
            return false
        }
        
        //Password Field validation
        (status,message) = objValidate.validatePasswordField(passwordTextField)
        if !status{
            switch message {
            case "Empty field":
                self.view.makeToast("Please enter a password")
                break
                
            case "Short length text":
                self.view.makeToast("Password must be atleast \(passwordTextField.minLength) characters")
                break
                
            case "Long length text":
                self.view.makeToast("Password must be maximum \(passwordTextField.maxLength) characters")
                break
                
            case "Invalid password format":
                self.view.makeToast("Password should contain 1 uppercase, 1 lowercase, 1 special character and min length as 8")
                break
                
            default:
                break
            }
            return false
        }
        
        //Confirm Password field validation
        if confirmPasswordTextField.text != passwordTextField.text {
            self.view.makeToast("Password and Confirm Password must be same")
            return false
        }
        if !isAgreeToTermCondition {
            self.view.makeToast("Please agree to the terms and conditions")
            return false
        }
        return true
    }
}

extension ProviderSignupViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //DROP DOWN FOR REGISTER AS FIELD
        if textField == registerAsTextField{
            IQKeyboardManager.shared.resignFirstResponder()
            if providerTypes.count != 0 {
                let dataSource = self.providerTypes.map({$0.name})
                self.initdropDown(textField: textField, dataSource: dataSource as! [String])
            } else {
                Utility.shared.showToast(message: "No list available")
            }
            
            return false
        }
        
        //DROP DOWN FOR PRIMARY DOCTOR FIELD
        if textField == primaryDoctorTextField{
            IQKeyboardManager.shared.resignFirstResponder()
            self.initdropDown(textField: textField, dataSource: ["Primary Doctor", "Secondary Doctor"])
            return false
        }
        
        //DROP DOWN FOR PRACTICE NAME FIELD
        if textField == practiceNameTextField{
            IQKeyboardManager.shared.resignFirstResponder()
            
            self.isPracticeNameSelected = true
            self.isSpecialistSelected = false
            specialitySearchBar.placeholder = "Search Practice Name"
            specialitySearchBar.text = ""
            practiceNameArray.removeAll()
            searchPracticeNameArray = selectedPracticeArray
            specialityListTableView.reloadData()
            showAlertWithBounceEffect(myView: specialitySearchView, onWindow: true)
            return false
        }
        
        //DROP DOWN FOR SPECIALIST FIELD
        if textField == specialistInTextField{
            IQKeyboardManager.shared.resignFirstResponder()
            self.isSpecialistSelected = true
            self.isPracticeNameSelected = false
            specialitySearchBar.placeholder = "Search Specialisation"
            specialitySearchBar.text = ""
//            specialistArray.removeAll()
            searchSpecialistArray = selectedSpecialistArray
            specialityListTableView.reloadData()
            showAlertWithBounceEffect(myView: specialitySearchView, onWindow: true)
            return false
        }
        
        //COUNTRY LIST VIEW
        if textField == countryTextField{
            self.view.endEditing(true)
            if countriesListArray.count == 0 {
                Utility.shared.showToast(message: "No countries list available.")
                return false
            }
            self.isSpecialistSelected = false
            self.isPracticeNameSelected = false
            countrySearchBar.placeholder = "Search Country Name"
            countrySearchBar.text = ""
            countrySearchArray = countriesListArray
            countryListTableView.reloadData()
            showAlertWithBounceEffect(myView: countrySearchView, onWindow: true)
            return false
        }
        
        //DATE OF BIRTH DATE PICKER
        if textField == dateOfBirthTextField {
            //            self.view.endEditing(true)
            self.showDatePicker()
        }
        
        
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
            return false
        }
        
        //Check textfield limit for characters
        if textField != emailTextField {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: nil, newCharacter: string)
            if !result {
                return result
            }
        }
        
        if textField == firstNameTextField || textField == middleNameTextField || textField == lastNameTextfield {
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        else if textField == contactNumberTextField {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 14, newCharacter: string)
//            let result  = Utility.shared.limitContactNumberTextFieldCharacters(range: range, oldString: textField.text! as NSString, minLimit: 9, maxLimit: 14, newCharacter: string)
            if !result {
                return result
            }
            
            let ACCEPTABLE_CHARACTERS = "1234567890"
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
}
