//
//  ProviderProfileViewController.swift
//  E-Health
//
//  Created by Prabjot Kaur on 6/11/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import DropDown
import AWSMobileClient

class ProviderProfileViewController: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var firstNameTextField: ExtendedTextField!
    @IBOutlet weak var middleNameTextField: ExtendedTextField!
    @IBOutlet weak var lastNameTextField: ExtendedTextField!
    
    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var femaleRadioButton: UIButton!
    @IBOutlet weak var dobTextField: ExtendedTextField!
    @IBOutlet weak var emailTextField: ExtendedTextField!
    @IBOutlet weak var countryTextField: ExtendedTextField!
    @IBOutlet weak var address1TextField: ExtendedTextField!
    @IBOutlet weak var address2TextField: ExtendedTextField!
    @IBOutlet weak var regionTextField: ExtendedTextField!
    @IBOutlet weak var stateTextField: ExtendedTextField!
    @IBOutlet weak var cityTextField: ExtendedTextField!
    @IBOutlet weak var raceTextField: ExtendedTextField!
    @IBOutlet weak var ethnicityTextField: ExtendedTextField!
    @IBOutlet weak var zipTextField: ExtendedTextField!
    @IBOutlet weak var primaryContactTextField: ExtendedTextField!
    @IBOutlet weak var phoneCodeTextField: ExtendedTextField!
    @IBOutlet weak var alternateNumberTextField: ExtendedTextField!
    
    @IBOutlet weak var smsRecieveButton: UIButton!
    @IBOutlet weak var groupStudiesButton: UIButton!
    
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var doctorTypeView: UIView!
    @IBOutlet weak var doctorTypeTextField: ExtendedTextField!
    @IBOutlet weak var practiceNameTextField: ExtendedTextField!
    @IBOutlet weak var specialistInView: UIView!
    @IBOutlet weak var specialistTextField: ExtendedTextField!
    
    var masterData = MasterData()
    
    //Drop Down selected values model
    var selectedCountry = CountryList()
    var selectedRace = Race()
    var selectedEthnicity = Ethnicity()
    
    var imagePicker: ImagePicker!
    
    var datePicker = UIDatePicker()
    
    //MARK:- View Hierarchy
    override func viewDidLoad() {
        super.viewDidLoad()

        masterData = Constant.masterData ?? MasterData()
        setupUI()
        setupTextFieldsProperties()
        getUserAttributesFromAWS()
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
    }
    
    
    //Button Action
    @IBAction func backButtonAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addimageButtonAction(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    
    @IBAction func maleButtonAction(_ sender: Any) {
        self.maleRadioButton.isSelected = true
        self.femaleRadioButton.isSelected = false
    }
    
    @IBAction func femaleButtonAction(_ sender: Any) {
        self.maleRadioButton.isSelected = false
        self.femaleRadioButton.isSelected = true
    }
    
    @IBAction func smsRecieveButtonAction(_ sender: Any) {
        if smsRecieveButton.isSelected {
            smsRecieveButton.isSelected = false
        } else {
            smsRecieveButton.isSelected = true
        }
    }
    
    @IBAction func groupStudiesButtonAction(_ sender: Any) {
        if groupStudiesButton.isSelected {
            groupStudiesButton.isSelected = false
        } else {
            groupStudiesButton.isSelected = true
        }
    }
    @IBAction func changePasswordButtonAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardIDs.ResetPasswordViewController) as? ResetPasswordViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        IQKeyboardManager.shared.resignFirstResponder()
        if !validateTextFields() {
            return
        }
        
        var userData = [String: String]()
        
        let firstName = firstNameTextField.text
        let middleName = middleNameTextField.text ?? ""
        let lastName = lastNameTextField.text
        let altPhoneNumber = alternateNumberTextField.text ?? ""
        
        userData["name"] = firstName! + " " + lastName!
        userData["custom:mid_name"] = middleName
        
        if maleRadioButton.isSelected {
            userData["custom:gender"] = "Male"
        } else if femaleRadioButton.isSelected {
            userData["custom:gender"] = "Female"
        }
        
        userData["custom:dob"] = dobTextField.text
//        let trimmedString = string.trimmingCharacters(in: .whitespaces)

        userData["custom:address1"] = (address1TextField.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:address2"] = (address2TextField.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:region"] = (regionTextField.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:state"] = (stateTextField.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:city"] = (cityTextField.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:race"] = String(self.selectedRace.id ?? 0)
        userData["custom:ethnicity"] = String(self.selectedEthnicity.id ?? 0)
        userData["custom:zip"] = zipTextField.text ?? ""    //zip
        userData["custom:alt_pn"] = altPhoneNumber
        
        let smsRecieve = self.smsRecieveButton.isSelected ? "true" : "false"
        let groupStudies = self.groupStudiesButton.isSelected ? "true" : "false"
        userData["custom:sms_r"] = smsRecieve
        userData["custom:grp_stdy"] = groupStudies
        
        //SAVE PROFILE IMAGE TO DOCUMENT DIRECTORY
        let noImageData = UIImage(named: "profile_no_image")?.pngData()
        let profileImage = profileImageView.image?.pngData()
        if profileImage != noImageData {
            Utility.shared.saveImageToDocumentDirectory(image: profileImageView.image!)
        }
        
        self.saveButton.isUserInteractionEnabled = false
        
        AWSAuth.shared.updateUserAttributes(userData: userData) { (result) in
            DispatchQueue.main.async {
                self.saveButton.isUserInteractionEnabled = true
            }
            if result {
                Utility.shared.showToast(message: "Profile Updated successfully")
            }
        }
    }
}

//MARK:- Initial View Setup Methods
extension ProviderProfileViewController {
    func setupUI() {
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        profileImageView.clipsToBounds = true
        
        let changePasswordButtonAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "Montserrat-Bold", size: 14.0)!,
            .foregroundColor: UIColor(red: 10/255, green: 144/255, blue: 203/255, alpha: 1.0),
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        
        let attributedString = NSAttributedString(string: "Change Password", attributes: changePasswordButtonAttributes)
        
        changePasswordButton.setAttributedTitle(attributedString, for: .normal)
    }
    
    func setupTextFieldsProperties() {
        firstNameTextField.minLength = 2
        middleNameTextField.minLength = 2
        lastNameTextField.minLength = 2
        emailTextField.minLength = 6
        zipTextField.minLength = 5
        primaryContactTextField.minLength = 9
        alternateNumberTextField.minLength = 9
        
        firstNameTextField.maxLength = 15
        middleNameTextField.maxLength = 15
        lastNameTextField.maxLength = 15
        zipTextField.maxLength = 8
        primaryContactTextField.maxLength = 15
        alternateNumberTextField.maxLength = 12
        
        firstNameTextField.sizeLimit = true
        middleNameTextField.sizeLimit = true
        lastNameTextField.sizeLimit = true
        zipTextField.sizeLimit = true
        primaryContactTextField.sizeLimit = true
        alternateNumberTextField.sizeLimit = true
    }
    
    func setPrefilled(profile: ProfileDetail) {
        
        DispatchQueue.main.async {
            //FETCH PROFILE IMAGE FROM DOCUMENT DIRECTORY
            if let profileImage = profile.userImageUrl  { //Utility.shared.fetchImageFromDocumentDirectory() {
                let imageURLString = Constant.URL.baseURL + "uploads/profile_pictures/" + (profile.sub ?? AWSMobileClient.default().username!) + "/\(profileImage)"
                DispatchQueue.main.async {
                    if let url = URL(string: imageURLString) {
                        self.profileImageView.kf.indicatorType = .activity
                        self.profileImageView.kf.setImage(with: url)
                    }
                }
            }
            
            let providerType = profile.providerType
            //IF PROVIDER TYPE NOT DOCTOR/HOSPITAL, HIDE FIELDS
            if providerType != "1" && providerType != "2" {
                self.doctorTypeView.isHidden = true
                self.specialistInView.isHidden = true
                self.practiceNameTextField.isHidden = true
            }
            //SHOW FIELDS
            else {
                self.doctorTypeView.isHidden = false
                self.specialistInView.isHidden = false
                self.practiceNameTextField.isHidden = false
                
                //For SPECIALIST FIELD
                if let specialityString = profile.specialistIn {
                    var specialityNamesArray = [String]()
                    let specialitiesArray = specialityString.components(separatedBy: ";")
                    if specialitiesArray.count != 0 {
                        for speciality in specialitiesArray {
                            let specialityDetailArray = speciality.components(separatedBy: "-")
                            specialityNamesArray.append(specialityDetailArray.first ?? "")
                        }
                        if specialityNamesArray.count > 1 {
                            self.specialistTextField.text = (specialityNamesArray.first  ?? "") + " & \(specialityNamesArray.count - 1) more"
                        }
                        else {
                            self.specialistTextField.text = (specialityNamesArray.first  ?? "")
                        }
                    }
                }
                
                //FOR PRACTICE NAME FIELD
                if let practiceNameString = profile.praticeName {
                    var practiceNamesArray = [String]()
                    let praticeArray = practiceNameString.components(separatedBy: ";")
                    if praticeArray.count != 0 {
                        for practice in praticeArray {
                            let practiceDetailArray = practice.components(separatedBy: "-")
                            practiceNamesArray.append(practiceDetailArray.first ?? "")
                        }
                        
                        if practiceNamesArray.count > 1 {
                            self.practiceNameTextField.text = (practiceNamesArray.first  ?? "") + " & \(practiceNamesArray.count - 1) more"
                        } else {
                            self.practiceNameTextField.text = (practiceNamesArray.first  ?? "")
                        }
                    }
                }
                
                //FOR DOCTOR TYPE (PRIMARY/SECONDARY)
                if profile.doctorType == "1" {
                    self.doctorTypeTextField.text = "Primary Doctor"
                } else {
                    self.doctorTypeTextField.text = "Secondary Doctor"
                }
            }
            
            //First Name and Last Name
            let name = profile.name
            let myStringArr = name?.components(separatedBy: " ")
            if let firstName = myStringArr?[0] {
                self.firstNameTextField.text = firstName
            }
            if let lastName = myStringArr?[1] {
                self.lastNameTextField.text = lastName
            }
            
            self.middleNameTextField.text = profile.middleName ?? ""
            
            //Set gender radio button
            if let gender = profile.gender {
                if gender == "Male" {
                    self.maleRadioButton.isSelected = true
                    self.femaleRadioButton.isSelected = false
                } else {
                    self.maleRadioButton.isSelected = false
                    self.femaleRadioButton.isSelected = true
                }
            }
            
            //Country name from code
            let countryCode = profile.country ?? "0"
            self.getSelectedCountryFromCode(code: Int(countryCode)!)
            
            //Race Name From code
            let raceCode = profile.race ?? "0"
            self.getRaceFromCode(code: Int(raceCode)!)
            
            //Ethnicity name from Code
            let ethnicityCode = profile.ethnicity ?? "0"
            self.getEthnicityFromCode(code: Int(ethnicityCode)!)
            
            self.emailTextField.text = profile.email ?? ""
            self.countryTextField.text = self.selectedCountry.name ?? ""
            self.primaryContactTextField.text = profile.primaryPhone ?? ""
            self.phoneCodeTextField.text = "+" + (self.selectedCountry.phonecode ?? "1")
            self.address1TextField.text = profile.address1 ?? ""
            self.address2TextField.text = profile.address2 ?? ""
            self.regionTextField.text = profile.region ?? ""
            self.raceTextField.text = self.selectedRace.name ?? ""
            self.ethnicityTextField.text = self.selectedEthnicity.name ?? ""
            self.zipTextField.text = profile.zip ?? ""
            self.alternateNumberTextField.text = profile.altPhone ?? ""
            self.dobTextField.text = profile.dob ?? ""
            self.stateTextField.text = profile.state ?? ""
            self.cityTextField.text = profile.city ?? ""
            
            //SMS recieve Switch
            if profile.smsRecieve == "true" {
                self.smsRecieveButton.isSelected = true
            } else {
                self.smsRecieveButton.isSelected = false
            }
            
            //Group Studies Switch
            if profile.groupStudies == "true" {
                self.groupStudiesButton.isSelected = true
            } else {
                self.groupStudiesButton.isSelected = false
            }
        }
        
    }
}

//MARK:- Get from ID's
extension ProviderProfileViewController {
    func getSelectedCountryFromCode(code: Int) {
        for country in masterData.countries {
            if country.id == code {
                self.selectedCountry = country
                return
            }
        }
    }
    
    func getRaceFromCode(code: Int) {
        for race in masterData.race {
            if race.id == code {
                self.selectedRace = race
            }
        }
    }
    
    func getEthnicityFromCode(code: Int) {
        for ethnicity in masterData.ethnicity {
            if ethnicity.id == code {
                self.selectedEthnicity = ethnicity
            }
        }
    }
}

//MARK:- Text Fields Drop down methods
extension ProviderProfileViewController {
//    func initCaretakerTypeDropDown() {
//        if masterData.caretakerTypes.count == 0 {
//            Utility.shared.showToast(message: "No Caretaker types list available.")
//            return
//        }
//
//        let caretakerTypesNames = masterData.caretakerTypes.map{$0.name}
//        if caretakerTypesNames.count == 0 {
//            Utility.shared.showToast(message: "No Caretaker types list available.")
//            return
//        }
//
//        self.initdropDown(textField: caretakerTypeTextfield, dataSource: caretakerTypesNames as! [String])
//    }
    
//    func initRelationshipDropDown() {
//        if countriesArray.relationship.count == 0 {
//            Utility.shared.showToast(message: "No Relationship list available.")
//            return
//        }
//
//        let relationsNames = countriesArray.relationship.map{$0.name}
//        if relationsNames.count == 0 {
//            Utility.shared.showToast(message: "No Relationship list available.")
//            return
//        }
//
//        self.initdropDown(textField: relationshipTextField, dataSource: relationsNames as! [String])
//    }
    
    func initRaceDropDown() {
        if masterData.race.count == 0 {
            Utility.shared.showToast(message: "No race list available.")
            return
        }
        
        let raceNames = masterData.race.map{$0.name}
        if raceNames.count == 0 {
            Utility.shared.showToast(message: "No race list available.")
            return
        }
        
        self.initdropDown(textField: raceTextField, dataSource: raceNames as! [String])
    }
    
    func initEthnicityDropDown() {
        if masterData.ethnicity.count == 0 {
            Utility.shared.showToast(message: "No ethnicity list available.")
            return
        }
        
        let ethnicityNames = masterData.ethnicity.map{$0.name}
        if ethnicityNames.count == 0 {
            Utility.shared.showToast(message: "No ethnicity list available.")
            return
        }
        self.initdropDown(textField: ethnicityTextField, dataSource: ethnicityNames as! [String])
    }
    
    func initdropDown (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                if textField == self.regionTextField {
                    self.regionTextField.text  = item
                }
                else if textField == self.raceTextField {
                    self.raceTextField.text  = item
                    self.selectedRace = self.masterData.race[index]
                }
                else if textField == self.ethnicityTextField {
                    self.ethnicityTextField.text  = item
                    self.selectedEthnicity = self.masterData.ethnicity[index]
                }
                
                dropDown.hide()
            }
            dropDown.show()
        }
    }
}

//MARK:- API methods
extension ProviderProfileViewController {
    func getUserAttributesFromAWS() {
        if AWSAuth.shared.userAWSStatus() == .signedIn {
            Utility.shared.startLoader()
            AWSAuth.shared.getUserAWSAttributes { (data, error) -> (Void) in
                Utility.shared.stopLoader()
                guard error == nil else {
                    return
                }
                let profile = ProfileDetail(value: data)
                self.setPrefilled(profile: profile)
            }
        }
    }
    
    func updateProfileImage(image: UIImage) {
        let noImageData = UIImage(named: "profile_no_image")?.pngData()
        let profileImage = image.pngData()//profileImageView.image?.pngData()
        
        if profileImage != noImageData {
            var params = [String: AnyObject]()
            params["email"] = emailTextField.text as AnyObject
            params["cognito_user_id"] = (AWSMobileClient.default().username ?? "") as AnyObject
            params["minor_id"] = "" as AnyObject
            params["user_type"] = "provider" as AnyObject
            Utility.shared.startLoader()
            MyProfileViewModel.uploadProfileImage(params: params, profileImage: image) { (data, error) in
                Utility.shared.stopLoader()
                DispatchQueue.main.async {
                    if error != nil {
                        Utility.shared.showToast(message: error!)
                    } else {
                        self.profileImageView.image = image
                        Utility.shared.showToast(message: data!)
                    }
                }
            }
        }
    }
}

//MARK:- Image Picker Delegate
extension ProviderProfileViewController: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        if let selectedImage = image {
            self.updateProfileImage(image: selectedImage)
        }
        Utility.shared.showToast(message: "Something went wrong!")
    }
}

//MARK:- TextField Delegates
extension ProviderProfileViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == doctorTypeTextField || textField == practiceNameTextField || textField == specialistTextField {
            return false
        }
        //Race text field -  drop down init
        if textField == raceTextField {
            IQKeyboardManager.shared.resignFirstResponder()
            self.initRaceDropDown()
            return false
        }
        //Ethnicity Text field
        else if textField == ethnicityTextField {
            IQKeyboardManager.shared.resignFirstResponder()
            self.initEthnicityDropDown()
            return false
        }
        else if textField == dobTextField {
            self.showDatePicker()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //RESTRICT INITIAL EMPTY SPACES
        if (textField.text == "" && string == " ") && (textField == address1TextField || textField == address2TextField || stateTextField == textField || cityTextField == textField || textField == regionTextField) {
            return false
        }
        
        //RESTRICT MORE THAN ONE SPACE CONSECUTIVELY
        if (string == " " && textField.text?.last == " ") && (textField == address1TextField || textField == address2TextField || stateTextField == textField || cityTextField == textField || textField == regionTextField) {
            return false
        }
        
        //RESTRICT SPACE IN TEXT FIELD OTHER THAN MENTIONED BELOW
        if string == " " && textField != address1TextField && textField != address2TextField && stateTextField == textField && cityTextField == textField{
            return false
        }
        
        //ADDRESS TEXT FIELD LIMITATIONS
        if textField == address1TextField || textField == address2TextField {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 60, newCharacter: string)
            return result
        }
        
        //STATE AND CITY FIELD LIMITATIONS
        if textField == stateTextField || textField == cityTextField {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 32, newCharacter: string)
            
            if !result {
                return result
            }
            
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        
        //Check textfield limit for characters
        let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: nil, newCharacter: string)
        if !result {
            return result
        }
        
        //NAME FIELD LIMITATIONS
        if textField == firstNameTextField || textField == middleNameTextField || textField == lastNameTextField {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 32, newCharacter: string)
            
            if !result {
                return result
            }
            
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
            
        //CONTACT AND ZIP CODE FIELD LIMITATTIONS
        else if textField == primaryContactTextField || textField == alternateNumberTextField || textField == zipTextField {
            var result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 14, newCharacter: string)
            
            if textField == zipTextField {
                result = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 8, newCharacter: string)
            }
            
            if !result {
                return result
            }
            
            let ACCEPTABLE_CHARACTERS = "1234567890"
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        
        
        return true
    }
}

//MARK:- Validate text fields
extension ProviderProfileViewController {
    func validateTextFields() -> Bool {
            let objValidate   = Validations()
            
            //First name field Validation
            var (status,message) = objValidate.validateField(firstNameTextField)
            if !status{
                switch message {
                case "Empty field":
                    Utility.shared.showToast(message: "Please enter your First name")
                    break
                    
                case "Short length text":
                    Utility.shared.showToast(message: "First name must be atleast \(firstNameTextField.minLength) characters")
                    break
                    
                case "Long length text":
                    Utility.shared.showToast(message: "First name must be maximum \(firstNameTextField.maxLength) characters")
                    break
                    
                default:
                    break
                }
                return false
            }
            
            //Middle Name Field Validation
            if middleNameTextField.text?.count != 0{
                (status,message) = objValidate.validateField(middleNameTextField)
                if !status{
                    switch message {
                    case "Empty field":
                        Utility.shared.showToast(message: "Please enter your Middle name")
                        break
                        
                    case "Short length text":
                        Utility.shared.showToast(message: "Middle name must be atleast \(middleNameTextField.minLength) characters")
                        break
                        
                    case "Long length text":
                        Utility.shared.showToast(message: "Middle name must be maximum \(middleNameTextField.maxLength) characters")
                        break
                        
                    default:
                        break
                    }
                    return false
                }
            }
            
            //Last Name Field Validation
            (status,message) = objValidate.validateField(lastNameTextField)
            if !status{
                switch message {
                case "Empty field":
                    Utility.shared.showToast(message: "Please enter your Last name")
                    
                    break
                case "Short length text":
                    Utility.shared.showToast(message: "Last name must be atleast \(lastNameTextField.minLength) characters")
                    
                    break
                case "Long length text":
                    Utility.shared.showToast(message: "Last name must be maximum \(lastNameTextField.maxLength) characters")
                    
                    break
                default:
                    break
                }
                return false
            }
            
            //Email Field Validation
            (status,message) = objValidate.validateEmailField(emailTextField)
            if !status{
                switch message {
                case "Empty field":
                    Utility.shared.showToast(message: "Please enter your Email")
                    break
                    
                case "Invalid Email format":
                    Utility.shared.showToast(message: "Please enter a valid Email")
                    break
                    
                case "Short length text":
                    Utility.shared.showToast(message: "Email must be atleast \(emailTextField.minLength) characters")
                    break
                    
                case "Long length text":
                    Utility.shared.showToast(message: "Email must be maximum \(emailTextField.maxLength) characters")
                    break
                    
                default:
                    break
                }
                return false
            }
            
            //Country Field Validation
            if countryTextField.text!.isEmpty || (countryTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0{
                Utility.shared.showToast(message: "Please enter your country")
                return false
            }
            
            //Address Line 1
            if address1TextField.text!.isEmpty || (address1TextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0{
    //            self.makeToast("Please enter your Address Line 1")
    //            return false
                address1TextField.text = ""
            }
            
            //Address Line 2
            if address2TextField.text!.isEmpty || (address2TextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0{
    //            self.makeToast("Please enter your Address Line 1")
    //            return false
                address2TextField.text = ""
            }
            
            
            //Zip Field Validation
            if !(zipTextField.text?.isEmpty)! {
                (status,message) = objValidate.validateField(zipTextField)
                if !status{
                    switch message {
                    case "Empty field":
                        Utility.shared.showToast(message: "Please enter your Zip")
                        break
                        
                    case "Short length text":
                        Utility.shared.showToast(message: "Zip must be atleast \(zipTextField.minLength) digits")
                        break
                        
                    case "Long length text":
                        Utility.shared.showToast(message: "Zip must be maximum \(zipTextField.maxLength) digits")
                        break
                        
                    default:
                        break
                    }
                    return false
                }
            }
            
            
            if alternateNumberTextField.text?.count != 0 {
                if alternateNumberTextField.text!.count > alternateNumberTextField.maxLength {
                    Utility.shared.showToast(message: "Alternate Contact number cannot be more than \(alternateNumberTextField.maxLength) digits")
                    return false
                } else if alternateNumberTextField.text!.count < alternateNumberTextField.minLength {
                    Utility.shared.showToast(message: "Alternate Contact number cannot be less than \(alternateNumberTextField.minLength) digits")
                    return false
                }
            }
            
            return true
        }
}

//MARK:- Date Picker Methods
extension ProviderProfileViewController {
    func showDatePicker(){
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat         //"dd MMM, yyyy"
        var date = Date()
        if dobTextField.text != "" {
            date = formater.date(from: dobTextField.text!) ?? Date()
        }
        
        let screenWidth = UIScreen.main.bounds.width
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -110, to: Date())
        datePicker.setDate(date, animated: true)
        if #available(iOS 14.0, *)
        {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()

//            datePicker.datePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        dobTextField.inputAccessoryView = toolbar
        dobTextField.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
//        if self.caretakerDetails?.care_taker_type_id == 1 {
//            formatter.dateFormat = "yyyy-MM-dd"
//        }
//        else {
            formatter.dateFormat = Constant.dateFormat
//        }
        dobTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }

}
