//
//  SelectServiceViewController.swift
//  E-Health
//
//  Created by Abhishek Chugh on 5/14/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class SelectServiceViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func providerButtonAction(_ sender: Any) {
//        Constant.loggedInProfile = Profile.provider
//        Utility.shared.showToast(message: "Provider login coming soon")
        let vc = UIStoryboard.init(name: "ProviderStoryboard", bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardIDs.ProviderSignupViewController) as? ProviderSignupViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func consumerButtonAction(_ sender: Any) {
//        Constant.loggedInProfile = Profile.consumer
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardIDs.RegisterProfileViewController) as? RegisterProfileViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
//    @IBAction func caregiverButtonAction(_ sender: Any) {
//        Constant.loggedInProfile = Profile.careGiver
//        let vc = UIStoryboard.init(name: "CareGiver", bundle: Bundle.main).instantiateViewController(withIdentifier: "CareTakerRegisterViewController") as? CareTakerRegisterViewController
//        self.navigationController?.pushViewController(vc!, animated: true)
//    }
}
