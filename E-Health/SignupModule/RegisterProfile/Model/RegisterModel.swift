//
//  RegisterModel.swift
//  E-Health
//
//  Created by Akash Dhiman on 19/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import Foundation

class MasterData {
    
    var region: [Region] = []
    var race: [Race] = []
    var ethnicity: [Ethnicity] = []
    var countries: [CountryList] = []
    var relationship: [Relation] = []
    var caretakerTypes: [CaretakerType] = []
    var providerTypes: [ProviderType] = []
    var documentTypes: [DocumentListingModel] = []
    var allergyTypes: [DocumentListingModel] = []
    var widgetTypes: [WidgetModel] = []
    var insuranceDocuments: [InsuranceType] = []
    var insuranceTypes: [InsuranceType] = []
    var bloodtypes: [BloodType] = []
    var labReports: [DocumentListingModel] = []
    var doctorTypeArray: [DoctorType] = []
    var careGiverType: [CareGiverTyp] = []

    
    init() {
        
    }
    
    init(value: [String: Any]) {
        if let regions = value["region"] as? [[String : Any]] {
            for data in regions{
                let dataToAppend = Region(value: data)
                region.append(dataToAppend)
            }
        }
        if let races = value["race"] as? [[String : Any]] {
            for data in races{
                let dataToAppend = Race(value: data)
                race.append(dataToAppend)
            }
        }
        if let ethnicitys = value["ethnicity"] as? [[String : Any]] {
            for data in ethnicitys{
                let dataToAppend = Ethnicity(value: data)
                ethnicity.append(dataToAppend)
            }
        }
        if let countriesArray = value["countries"] as? [[String : Any]] {
            for data in countriesArray{
                let dataToAppend = CountryList(value: data)
                countries.append(dataToAppend)
            }
        }
        if let relationArray = value["relationships"] as? [[String : Any]] {
            for data in relationArray{
                let dataToAppend = Relation(value: data)
                relationship.append(dataToAppend)
            }
        }
        if let typeArray = value["caregiver_types"] as? [[String : Any]] {
            for data in typeArray{
                let dataToAppend = CareGiverTyp(value: data)
                careGiverType.append(dataToAppend)
            }
        }
        
        if let caretakerArray = value["care_taker_types"] as? [[String : Any]] {
            for data in caretakerArray{
                let dataToAppend = CaretakerType(value: data)
                caretakerTypes.append(dataToAppend)
            }
        }
        if let providersArray = value["provider_categories"] as? [[String : Any]] {
            for data in providersArray{
                let dataToAppend = ProviderType(value: data)
                providerTypes.append(dataToAppend)
            }
        }
        if let documentTypeArray = value["document_types"] as? [[String : Any]] {
            for data in documentTypeArray{
                let dataToAppend = DocumentListingModel(value: data)
                documentTypes.append(dataToAppend)
            }
        }
        if let allergyTypesArray = value["allergies"] as? [[String : Any]] {
            for allergy in allergyTypesArray{
                let dataToAppend = DocumentListingModel(value: allergy)
                allergyTypes.append(dataToAppend)
            }
        }
        if let widgetType = value["report"] as? [[String : Any]] {
            for widget in widgetType{
                let dataToAppend = WidgetModel(value: widget)
                widgetTypes.append(dataToAppend)
            }
        }
        if let insuranceDocumets = value["insurance_document_listing"] as? [[String: Any]] {
            for insuranceDocument in insuranceDocumets {
                let dataToAppend = InsuranceType(value: insuranceDocument)
                insuranceDocuments.append(dataToAppend)
            }
        }
        if let insuranceType = value["insurance_type_listing"] as? [[String: Any]] {
            for insuranceTyp in insuranceType {
                let dataToAppend = InsuranceType(value: insuranceTyp)
                insuranceTypes.append(dataToAppend)
            }
        }
        if let bloodTypes = value["blood_type"] as? [[String: Any]] {
            for bloodType in bloodTypes {
                let dataToAppend = BloodType(value: bloodType)
                bloodtypes.append(dataToAppend)
            }
        }
        if let lab_Reports = value["lab_reports"] as? [[String: Any]] {
            for labReport in lab_Reports {
                let dataToAppend = DocumentListingModel(value: labReport)
                labReports.append(dataToAppend)
            }
        }
        if let doctor_types = value["doctor_type"] as? [[String: Any]] {
            for doctorType in doctor_types {
                let dataToAppend = DoctorType(value: doctorType)
                doctorTypeArray.append(dataToAppend)
            }
        }
    }
}

class Region {
    var id: Int?
    var name: String?
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.name = value["name"] as? String
    }
}

class Race {
    var countryId: Int?
    var id: Int?
    var name: String?
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.countryId = value["country_id"] as? Int
        self.id = value["id"] as? Int
        self.name = value["name"] as? String
    }
}

class Ethnicity {
    var countryId: Int?
    var id: Int?
    var name: String?
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.countryId = value["country_id"] as? Int
        self.id = value["id"] as? Int
        self.name = value["name"] as? String
    }
}

class ListOfCountries {
    
}

class CountryList {
    var id : Int?
    var sortname: String?
    var name: String?
    var phonecode: String?
    var status: Int?
    var region_id: Int?
//    var created_at": null,
//    "updated_at": null
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.sortname = value["sortname"] as? String
        self.name = value["name"] as? String
        self.phonecode = value["phonecode"] as? String
        self.status = value["status"] as? Int
        self.region_id = value["region_id"] as? Int
    }
}

class Relation {
    var id : Int?
    var name: String?
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.name = value["name"] as? String
    }
}
class CareGiverTyp {
    var id : Int?
    var name: String?
    var createdAt : String?
    var updatedAt : String?
    
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.name = value["name"] as? String
        self.createdAt = value["created_at"] as? String
        self.updatedAt = value["updated_at"] as? String
    }
}

class CaretakerType {
    var id : Int?
    var name: String?
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.name = value["name"] as? String
    }
}

class ProviderType {
    var id : Int?
    var name: String?
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.name = value["name"] as? String
    }
}

class DoctorType {
    var id : Int?
    var name: String?
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.name = value["type_name"] as? String
    }
}


class BloodType {
    var id : Int?
    var name: String?
    var updated_at: String?
    var created_at: String?
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.name = value["name"] as? String
        self.created_at = value["created_at"] as? String
        self.updated_at = value["updated_at"] as? String
    }
}
