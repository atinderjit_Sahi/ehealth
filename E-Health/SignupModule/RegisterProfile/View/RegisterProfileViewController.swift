//
//  RegisterProfileViewController.swift
//  E-Health
//
//  Created by Abhishek Chugh on 5/15/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import FlagPhoneNumber
import DropDown
import IQKeyboardManagerSwift
import AWSUserPoolsSignIn
import AWSMobileClient

var firstTimeSignup = false

class RegisterProfileViewController: UIViewController {
    
    @IBOutlet var countrySearchView: UIView!
    @IBOutlet weak var countrySearchBar: UISearchBar!
    @IBOutlet weak var countryListTableView: UITableView!
    
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var firstNameTextField: ExtendedTextField!
    @IBOutlet weak var middleNameTextField: ExtendedTextField!
    @IBOutlet weak var lastNameTextField: ExtendedTextField!
    @IBOutlet weak var emailTextField: ExtendedTextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var primaryContactTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: ExtendedTextField!
    @IBOutlet weak var passwordTextField: ExtendedTextField!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var countryCodeTextField: UITextField!
//    @IBOutlet var countryListSearchView: SearchTableView!
    @IBOutlet weak var passwordDisplayToggleButton: UIButton!
    @IBOutlet weak var confirmPassDisplayToggleButton: UIButton!
    
    @IBOutlet weak var passwordHeight: NSLayoutConstraint!
    @IBOutlet weak var confirmPasswordHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnTick: UIButton!
    
    
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    var countriesArray = MasterData()
    var countriesListArray = [CountryList]()
    var selectedCountry = CountryList()
    var countrySearchArray = [CountryList]()
    var isCountrySearchedEmpty = true
    
    var datePicker = UIDatePicker()
    var selectedCountryCode = String()
    var federatedId = String()
    var socialProfile: ProfileDetail?
    var isAgreeToTermCondition = Bool()
    
    //MARK:- View controller Hierarchy
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupUI()
        setupTextFieldsProperties()
        getCountriesAPIList()
        
        countryListTableView.backgroundColor = .white
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        let tapView = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnView(_:)))
        self.view.addGestureRecognizer(tapView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if boolSelectedTerms == true {
            boolSelectedTerms = false
            btnTick.isSelected = true
        }
        else{
            btnTick.isSelected = false

        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.previousNextDisplayMode = .default
    }
    
    @objc func handleTapOnView(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
        hideAlertWithBounceEffect(myView: countrySearchView)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
        firstTimeSignup = false
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        saveButton.isUserInteractionEnabled = false
        signupUser()
    }
    
    @IBAction func countryTextfieldAction(_ sender: Any) {
//        countrySearchBar.becomeFirstResponder()
//        if countriesListArray.count == 0 {
//            Utility.shared.showToast(message: "No countries list available.")
//            return
//        }
//
//        showAlertWithBounceEffect(myView: countrySearchView, onWindow: true)
    }
    
    @IBAction func countryListCrossButtonAction(_ sender: Any) {
        countrySearchBar.endEditing(true)
        hideAlertWithBounceEffect(myView: countrySearchView)
    }
    
    func showAlertWithBounceEffect(myView : UIView,onWindow:Bool) {
//        let indexPath = IndexPath(row: 0, section: 0)
//        countryListTableView.scrollToRow(at: indexPath, at: .top, animated: true)
        let window = UIApplication.shared.keyWindow!
        myView.isHidden = false
        if onWindow == true {
            window.addSubview(myView)
        }  else {
            self.view.addSubview(myView)
            self.view.bringSubviewToFront(myView)
        }
        myView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            myView.transform = .identity
        }, completion: {(finished: Bool) -> Void in
        })
    }

    func hideAlertWithBounceEffect(myView : UIView) {
        myView.transform = .identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            myView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: {(finished: Bool) -> Void in
            myView.isHidden = true
        })
    }
    
    func setCountryCode(forIndex: Int) {
        selectedCountry = countriesListArray[forIndex]
        countryCodeTextField.text = "+" + (selectedCountry.phonecode ?? "")
    }
    
    @IBAction func passwordDisplayToggleButtonAction(_ sender: Any) {
        if passwordTextField.isSecureTextEntry {
            passwordTextField.isSecureTextEntry = false
            passwordDisplayToggleButton.setImage(UIImage(named: "unhide_password"), for: .normal)
        } else {
            passwordTextField.isSecureTextEntry = true
            passwordDisplayToggleButton.setImage(UIImage(named: "hide_password"), for: .normal)
        }
    }
    @IBAction func confirmPasswordDisplayToggleButtonAction(_ sender: Any) {
        if confirmPasswordTextField.isSecureTextEntry {
            confirmPasswordTextField.isSecureTextEntry = false
            confirmPassDisplayToggleButton.setImage(UIImage(named: "unhide_password"), for: .normal)
        } else {
            confirmPasswordTextField.isSecureTextEntry = true
            confirmPassDisplayToggleButton.setImage(UIImage(named: "hide_password"), for: .normal)
        }
    }
    
    
    @IBAction func countryDropDown(_ sender: Any) {
        IQKeyboardManager.shared.resignFirstResponder()
        if countriesListArray.count == 0 {
            Utility.shared.showToast(message: "No countries list available.")
            return
        }
        countrySearchBar.text = ""
        countrySearchArray = countriesListArray
        countryListTableView.reloadData()
        showAlertWithBounceEffect(myView: countrySearchView, onWindow: true)
    }
    
    @IBAction func buttonReadTermsConditionsTapped(_ sender: Any) {
        view.endEditing(true)
        guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: WebViewController.className) as? WebViewController else {return}
        
        vc.webUrlString = "http://3.236.116.101/consumer/terms-conditions"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonPrivacyPolicyTapped(_ sender: Any) {
        view.endEditing(true)
        guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: WebViewController.className) as? WebViewController else {return}
        
        vc.webUrlString = "http://3.236.116.101/consumer/privacy-policy"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonAgreeTermsConditionsTapped(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected =  false
        }
        else {
            sender.isSelected =  true
            isAgreeToTermCondition = sender.isSelected
            guard let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: "NewPolicyVC") as? NewPolicyVC else {return}
           // vc.webUrlString = "http://3.236.116.101/consumer/terms-conditions"
            navigationController?.pushViewController(vc, animated: true)
        }
      
        
    }
}

//MARK:- Class Methods
extension RegisterProfileViewController {
    
    func setupUI() {
        countrySearchView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        countrySearchBar.delegate = self
        countrySearchView.frame = self.view.frame
        
//        countryListTableView.tableHeaderView = UIView()
        countryListTableView.tableFooterView = UIView()
        countrySearchBar.showsScopeBar = false
        countrySearchBar.placeholder = "Search Country Name"
        
        countryTextField.setUnderLine()
        primaryContactTextField.setUnderLine()
        dateOfBirthTextField.setUnderLine()
        countryCodeTextField.setUnderLine()
        
        if federatedId != "" {
            passwordHeight.constant = 0
            confirmPasswordHeight.constant = 0
            passwordTextField.isHidden = true
            confirmPasswordTextField.isHidden = true
            passwordDisplayToggleButton.isHidden = true
            confirmPassDisplayToggleButton.isHidden = true
            
            if socialProfile?.primaryPhone != nil {
                
                primaryContactTextField.isUserInteractionEnabled = false
                primaryContactTextField.text = socialProfile?.primaryPhone ?? ""
            }
            
            if socialProfile?.email != nil {
                emailTextField.isUserInteractionEnabled = false
                emailTextField.text = socialProfile?.email ?? ""
            }
            
            if socialProfile?.firstName != nil {
                firstNameTextField.text = socialProfile?.firstName ?? ""
            }
            
            if socialProfile?.lastName != nil {
                lastNameTextField.text = socialProfile?.lastName ?? ""
            }
        } else {
//            passwordHeight.constant = 50
//            confirmPasswordHeight.constant = 50
            passwordTextField.isHidden = false
            confirmPasswordTextField.isHidden = false
            passwordDisplayToggleButton.isHidden = false
            confirmPassDisplayToggleButton.isHidden = false
        }
    }
    
    func setupTextFieldsProperties() {
        firstNameTextField.minLength = 2
        middleNameTextField.minLength = 2
        lastNameTextField.minLength = 2
        confirmPasswordTextField.minLength = 8
        passwordTextField.minLength = 8
        
        firstNameTextField.maxLength = 15
        middleNameTextField.maxLength = 15
        lastNameTextField.maxLength = 15
        confirmPasswordTextField.maxLength = 16
        passwordTextField.maxLength = 16
    }
    
    func setScreenPrefilled(userData: [String: Any]) {
        firstNameTextField.text = userData["name"] as? String ?? ""
        emailTextField.text = userData["email"] as? String ?? ""
        countryTextField.text = userData["custom:country"] as? String ?? ""
        primaryContactTextField.text = userData["phone_number"] as? String ?? ""
    }

    func initDropDown(textField: UITextField, options: [String]) {
        view.endEditing(true)
        Utility.shared.initDropDown(textField: textField, array: options)
    }
    
    func getCountriesAPIList() {
        Utility.shared.startLoader()
        RegisterViewModel.getMasterData() { (countries, error) in
            Utility.shared.stopLoader()
            if error == nil{
                if countries.countries.count != 0 {
                    self.countriesListArray = countries.countries
                    self.countrySearchArray = self.countriesListArray
                    self.countryListTableView.reloadData()
                }
            }
        }
    }
    
}

//MARK:- Date Picker Methods
extension RegisterProfileViewController {
    func showDatePicker(){
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat //"dd MMM, yyyy"
        var date = Date()
        if dateOfBirthTextField.text != "" {
            date = formater.date(from: dateOfBirthTextField.text!) ?? Date()
        }
        
        let screenWidth = UIScreen.main.bounds.width
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -110, to: Date())
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datePicker.setDate(datePicker.maximumDate ?? date, animated: true)
        if #available(iOS 14.0, *)
        {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()

//            datePicker.datePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        dateOfBirthTextField.inputAccessoryView = toolbar
        dateOfBirthTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = Constant.dateFormat //"dd MMM, yyyy"
        dateOfBirthTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}

extension RegisterProfileViewController {
    
    func signupUser() {
        if !validateTextFields(federatedId: federatedId) {
            saveButton.isUserInteractionEnabled = true
            return
        }
        var userData = [String: String]()
        
        let firstName = firstNameTextField.text
        let middleName = middleNameTextField.text
        let lastName = lastNameTextField.text
        let phoneNumber = "+" + (selectedCountry.phonecode!) + (primaryContactTextField.text!)
        
        userData["name"] = firstName! + " " + lastName!
        userData["email"] = emailTextField.text
        userData["phone_number"] = phoneNumber
        userData["family_name"] = "consumer"
        userData["custom:widget_items"] = "1,2,3,4,5,6,7,8,9" // "1,2,3,4,5,6,991,992,993,994,995,996"
        userData["custom:new_user"] = "1"
        userData["custom:dob"] = dateOfBirthTextField.text
        userData["custom:country"] = String(selectedCountry.id ?? 0)
        userData["custom:mid_name"] = middleName
        
        if federatedId != "" {
            userData["user_federated_id"] = federatedId
            userData["is_from_social"] = "1"
            userData["password"] = "Mind@1234"
            
            self.createFederatedUser(parameters: userData)
        } else {
            userData["custom:mfa_enabled"] = "false"
            AWSAuth.shared.signUpWithAWS(requestData: userData, password: passwordTextField.text!, controller: self) { (status) in
                Utility.shared.stopLoader()
                DispatchQueue.main.async {
                    self.saveButton.isUserInteractionEnabled = true
                }
                if status {
//                    self.navigateWhenMfaNotEnabled()
                    self.navigateWhenMfaEnabled()
                    
                } else {
                    print(status)
                }
            }
        }
    }
    
    func createFederatedUser(parameters: [String: Any]) {
        SocialAuthViewModel.createFederatedUser(parameters: parameters) { (message, error) in
            Utility.shared.stopLoader()
            if error == nil {
                DispatchQueue.main.async {
                    self.saveButton.isUserInteractionEnabled = true
                }
                self.navigateWhenMfaEnabled()
//                self.signInFederatedUser()
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func signInFederatedUser() {
        AWSAuth.shared.signInWithAWS(isSocial: true, emailID: self.emailTextField.text!, password: "Mind@1234", controller: self) { (state, error) -> (Void) in
            UserDefaults.standard.set(1, forKey: "isSocialLogin")
            Constant.isSocialLogin = true
            Constant.loggedInProfile = .consumer
            self.navigateWhenSocialMfaNotEnabled()
//            self.navigateWhenMfaEnabled()
        }
    }
    
    func navigateWhenSocialMfaNotEnabled() {
        self.getUserProfile()
    }
    
    func getUserProfile() {
        if AWSAuth.shared.userAWSStatus() == .signedIn {
            //            self.dispatchGroup.enter()
            AWSAuth.shared.getUserAWSAttributes { (data, error) -> (Void) in
                //                self.dispatchGroup.leave()
                guard error == nil else {
                    return
                }
                Constant.userProfile = ProfileDetail(value: data)
                
                UserDefaults.standard.set(Constant.userProfile?.isNewUser, forKey: "isNewUser")
                UserDefaults.standard.removeObject(forKey: UserDefaultKey.userProfile)
                let userDetail = NSKeyedArchiver.archivedData(withRootObject: data as Any)
                UserDefaults.standard.set(userDetail, forKey: UserDefaultKey.userProfile)
                
                if Constant.userProfile?.family_name == "consumer" {
                    Constant.loggedInProfile =  .consumer
                    
                    //API TO POST USER's COGNITO SUB ID TO DATABASE
                    RegisterViewModel.postCognitoUserID(userID: AWSMobileClient.default().username ?? "") { result in
//                        if Constant.userProfile?.isNewUser == "0" {
//                            self.goToDashboardScreen()
//                        } else {
                            self.goToWidgetScreen()
//                        }
                    }
                } else {
                    Constant.loggedInProfile = .none
                }
            }
        }
    }
    
    func goToWidgetScreen() {
        DispatchQueue.main.async {
            guard let vc = UIStoryboard.init(name: StoryboardNames.TabBar, bundle: Bundle.main).instantiateViewController(withIdentifier: SetWidgetViewController.className) as? SetWidgetViewController else {
                return
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func goToDashboardScreen() {
        DispatchQueue.main.async {
            guard let vc = UIStoryboard.init(name: StoryboardNames.TabBar, bundle: Bundle.main).instantiateViewController(withIdentifier: TabBarViewController.className) as? TabBarViewController else {
                return
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func navigateWhenMfaEnabled() {
        DispatchQueue.main.async {
            let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: VerificationViewController.className) as? VerificationViewController
            vc?.signupEmail = self.emailTextField.text!
            vc?.federatedId = self.federatedId
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    func navigateWhenMfaNotEnabled() {
        DispatchQueue.main.async {
            let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: AuthenticationViewController.className) as? AuthenticationViewController
            vc?.isComingFromSignup = true
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
}

//MARK:- Text Field Delegates
extension RegisterProfileViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == countryTextField{
            /*IQKeyboardManager.shared.resignFirstResponder()
            if countriesListArray.count == 0 {
                Utility.shared.showToast(message: "No countries list available.")
                return false
            }
            countrySearchBar.text = ""
            countrySearchArray = countriesListArray
            countryListTableView.reloadData()
            showAlertWithBounceEffect(myView: countrySearchView, onWindow: true)*/
            return false
        }
        
        if textField == dateOfBirthTextField {
//            self.view.endEditing(true)
            self.showDatePicker()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
            return false
        }
        
        //Check textfield limit for characters
        if textField != emailTextField {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: nil, newCharacter: string)
            if !result {
                return result
            }
        }
                
        if textField == firstNameTextField || textField == middleNameTextField || textField == lastNameTextField {
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        else if textField == primaryContactTextField {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 12, newCharacter: string)
            if !result {
                return result
            }
            
            let ACCEPTABLE_CHARACTERS = "1234567890"
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


//MARK:- CountryList Table view delegates
extension RegisterProfileViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countrySearchArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCellTableViewCell") as? CountryCellTableViewCell else {
            return UITableViewCell()
        }
        cell.countryLabel.text = countrySearchArray[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerView = UIView()
        headerView.backgroundColor = .clear
        headerView = countrySearchBar
//        headerView.frame.size = countrySearchBar.frame.size
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        countrySearchBar.endEditing(true)
        hideAlertWithBounceEffect(myView: countrySearchView)
        if isCountrySearchedEmpty {
            print(countriesListArray[indexPath.row].name as Any)
        }else{
            print(countrySearchArray[indexPath.row].name as Any)
        }
        
        self.selectedCountry = countrySearchArray[indexPath.row]
        countryTextField.text = self.selectedCountry.name
        countryCodeTextField.text = "+" + (selectedCountry.phonecode ?? "")
    }
}

//MARK:- TextField Validations
extension RegisterProfileViewController {
    
    func validateTextFields(federatedId: String) -> Bool {
        let objValidate   = Validations()
        
        //First name field Validation
        var (status,message) = objValidate.validateField(firstNameTextField)
        if !status{
            switch message {
            case "Empty field":
                self.view.makeToast("Please enter your First name")
                break
            case "Short length text":
                self.view.makeToast("First name must be atleast \(firstNameTextField.minLength) characters")
                break
            case "Long length text":
                self.view.makeToast("First name must be maximum \(firstNameTextField.maxLength) characters")
                break
            default:
                break
            }
            return false
        }
        
        //Middle Name Field Validation
        if middleNameTextField.text?.count != 0{
            (status,message) = objValidate.validateField(middleNameTextField)
            if !status{
                switch message {
                case "Empty field":
                    self.view.makeToast("Please enter your Middle name")
                    break
                    
                case "Short length text":
                    self.view.makeToast("Middle name must be atleast \(middleNameTextField.minLength) characters")
                    break
                    
                case "Long length text":
                    self.view.makeToast("Middle name must be maximum \(middleNameTextField.maxLength) characters")
                    break
                    
                default:
                    break
                }
                return false
            }
        }
        
        
        //Last Name Field Validation
        (status,message) = objValidate.validateField(lastNameTextField)
        if !status{
            switch message {
            case "Empty field":
                self.view.makeToast("Please enter your Last name")
                
                break
            case "Short length text":
                self.view.makeToast("Last name must be atleast \(lastNameTextField.minLength) characters")
                
                break
            case "Long length text":
                self.view.makeToast("Last name must be maximum \(lastNameTextField.maxLength) characters")
                
                break
            default:
                break
            }
            return false
        }
        
        //DOB Field Validation
        if dateOfBirthTextField.text == "" {
            self.view.makeToast("Please enter your Date of Birth")
            return false
        }
        
        //Email Field Validation
        (status,message) = objValidate.validateEmailField(emailTextField)
        if !status{
            switch message {
            case "Empty field":
                self.view.makeToast("Please enter your Email")
                break
                
            case "Invalid Email format":
                self.view.makeToast("Please enter a valid Email")
                break
                
                //            case "Short length text":
                //                self.view.makeToast("Email must be atleast \(emailTextField.minLength) characters")
                //                break
                //
                //            case "Long length text":
                //                self.view.makeToast("Email must be maximum \(emailTextField.maxLength) characters")
                //                break
                
            default:
                break
            }
            return false
        }
        
        //Country Field Validation
        if countryTextField.text!.isEmpty{
            self.view.makeToast("Please enter your country")
            return false
        }
        if (countryTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0 {
            countryTextField.text = ""
            self.view.makeToast("Please enter your country")
            return false
        }
        
        if primaryContactTextField.text!.isEmpty {
            self.view.makeToast("Please enter your contact number")
            return false
        } else if primaryContactTextField.text!.count > 15 {
            self.view.makeToast("Contact number cannot be more than 15 digits")
            return false
        } else if primaryContactTextField.text!.count < 9 {
            self.view.makeToast("Contact number cannot be less than 9 digits")
            return false
        }
        
        //Password Field validation
        if federatedId == "" {
            (status,message) = objValidate.validatePasswordField(passwordTextField)
            if !status{
                switch message {
                case "Empty field":
                    self.view.makeToast("Please enter a password")
                    break
                    
                case "Short length text":
                    self.view.makeToast("Password must be atleast \(passwordTextField.minLength) characters")
                    break
                    
                case "Long length text":
                    self.view.makeToast("Password must be maximum \(passwordTextField.maxLength) characters")
                    break
                    
                case "Invalid password format":
                    self.view.makeToast("Password should contain 1 uppercase, 1 lowercase, 1 special character and min length as 8")
                    break
                    
                default:
                    break
                }
                return false
            }
            
            //Confirm Password field validation
            if confirmPasswordTextField.text != passwordTextField.text {
                self.view.makeToast("Password and Confirm Password must be Same")
                return false
            }
        }
        
        if !isAgreeToTermCondition {
            self.view.makeToast("Please agree to the terms and conditions")
            return false
        }
        return true
    }
}

extension RegisterProfileViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        countrySearchArray = countriesListArray.filter({ country -> Bool in
            if searchText.isEmpty {
                isCountrySearchedEmpty = true
                return true
            }
            isCountrySearchedEmpty = false
            return country.name?.lowercased().contains(searchText.lowercased()) ?? false
        })
        countryListTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        switch selectedScope {
        case 0:
            countrySearchArray = countriesListArray
        default:
            break
        }
        countryListTableView.reloadData()
    }
}
