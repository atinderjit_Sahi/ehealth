//
//  RegisterViewModel.swift
//  E-Health
//
//  Created by Abhishek Chugh on 5/18/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

class RegisterViewModel: NSObject {
    
    static func getCityFromServer(completion: @escaping (_ response: MasterData, _ error: String?) -> Void) {
        
        let url = Constant.URL.getCountriesList
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                completion(MasterData(value: result), nil)
            } else {
                completion(MasterData(value: [:]) , error?.localizedDescription)
            }
        }
    }
    
    static func getCountriesList(controller: UIViewController?, completion: @escaping (_ response: [CountryList]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.getCountriesListAPI
        
        ApiService.shared.triggerRequest(controller: controller, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [[String: AnyObject]] {
                print(result)
                var countriesArray = [CountryList]()
                
                for country in result {
                    let countryData = CountryList(value: country)
                    countriesArray.append(countryData)
                }
                completion(countriesArray, nil)
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func getMasterData(completion: @escaping (_ response: MasterData, _ error: String?) -> Void) {
        let url = Constant.URL.getMasterData
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                completion(MasterData(value: result), nil)
            } else {
                completion(MasterData(value: [:]) , error?.localizedDescription)
            }
        }
    }
    
    static func postCognitoUserID(userID: String, completionHandler: @escaping (_ result: Bool) -> Void) {
        let userId = AWSMobileClient.default().username ?? ""
        let url = Constant.URL.addCognitoUserID
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        let param: [String: AnyObject] = ["cognito_user_id": userId as AnyObject]
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: param, method: .post) { (result, error) in
            if error != nil {
                print(error ?? "")
                completionHandler(false)
            } else {
                print(result as Any)
                completionHandler(true)
            }
            self.saveARNToDB()
        }
    }
    
    static func saveARNToDB() {
        let userId = AWSMobileClient.default().username ?? ""
        let url = Constant.URL.addCognitoUserID + "/" + userId
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        let param: [String: AnyObject] = ["arn": Constant.AWS.endPointID as AnyObject]
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: param, method: .put) { (result, error) in
            if error != nil {
                print(error ?? "")
            } else {
                print(result as Any)
            }
        }
    }
    
    static func verifyEmail(parameters : [String: Any], completionHandler: @escaping (_ message: String?, _ success: Bool) -> Void) {
        
        let url = Constant.URL.verifyEmail
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        completionHandler(nil, true)
                    } else {
                        completionHandler(result["message"] as? String, false)
                    }
                } else {
                    completionHandler(result["message"] as? String, false)
                }
            } else {
                completionHandler(error?.localizedDescription, false)
            }
        }
    }
}
