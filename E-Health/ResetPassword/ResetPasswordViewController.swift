//
//  ResetPasswordViewController.swift
//  E-Health
//
//  Created by Akash Dhiman on 03/06/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ResetPasswordViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var oldPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var newPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPasswordTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var oldPassDisplayToggleButton: UIButton!
    @IBOutlet weak var newPassDisplayToggleButton: UIButton!
    @IBOutlet weak var confirmPassDisplayToggleButton: UIButton!
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func oldPasswordDisplayToggleButtonAction(_ sender: Any) {
        if oldPasswordTextField.isSecureTextEntry {
            oldPasswordTextField.isSecureTextEntry = false
            oldPassDisplayToggleButton.setImage(UIImage(named: "unhide_password"), for: .normal)
        } else {
            oldPasswordTextField.isSecureTextEntry = true
            oldPassDisplayToggleButton.setImage(UIImage(named: "hide_password"), for: .normal)
        }
    }
    
    @IBAction func newPasswordDisplayToggleButtonAction(_ sender: Any) {
        if newPasswordTextField.isSecureTextEntry {
            newPasswordTextField.isSecureTextEntry = false
            newPassDisplayToggleButton.setImage(UIImage(named: "unhide_password"), for: .normal)
        } else {
            newPasswordTextField.isSecureTextEntry = true
            newPassDisplayToggleButton.setImage(UIImage(named: "hide_password"), for: .normal)
        }
    }
    
    @IBAction func confirmPasswordDisplayToggleButtonAction(_ sender: Any) {
        if confirmPasswordTextField.isSecureTextEntry {
            confirmPasswordTextField.isSecureTextEntry = false
            confirmPassDisplayToggleButton.setImage(UIImage(named: "unhide_password"), for: .normal)
        } else {
            confirmPasswordTextField.isSecureTextEntry = true
            confirmPassDisplayToggleButton.setImage(UIImage(named: "hide_password"), for: .normal)
        }
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        let (status, errorMessage) = Utility.shared.resetPasswordValidation(oldPasswordTextField: oldPasswordTextField, newPasswordTextfield: newPasswordTextField, confirmPasswordTextfield: confirmPasswordTextField)
        if status == false{
            Utility.shared.showToast(message: errorMessage ?? "")
        } else {
            Utility.shared.startLoader()
            AWSAuth.shared.changePassword(oldPassword: oldPasswordTextField.text!, newPassword: newPasswordTextField.text!) { (status) -> (Void) in
                Utility.shared.stopLoader()
                if status {
                    DispatchQueue.main.async {
                        self.oldPasswordTextField.text = ""
                        self.newPasswordTextField.text = ""
                        self.confirmPasswordTextField.text = ""
                        
                        guard let controller = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: AuthenticationViewController.className) as? AuthenticationViewController else { return }
                        AWSAuth.shared.signOutAWS()
                        let navigationController = UINavigationController(rootViewController: controller)
                        navigationController.navigationBar.isHidden = true
                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                        appdelegate.window!.rootViewController = navigationController
                        Utility.shared.showAlert(message: "Password changed successfully. \n Please signin again", controller: controller)
                    }
                }
            }
        }
    }
}

extension ResetPasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard range.location == 0 else {
            return true
        }
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
        return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
