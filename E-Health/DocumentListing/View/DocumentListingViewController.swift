//
//  DocumentListingViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 6/30/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

class DocumentListingViewController: UIViewController {

    @IBOutlet weak var bckbutton: UIButton!
    //MARK: - Outlets
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var documentListingTableView: UITableView!
    @IBOutlet var allergyDetailContainerView: UIView!
    @IBOutlet weak var allergyDetailBlurView: UIView!
    @IBOutlet weak var labelAllergyTypeName: UILabel!
    @IBOutlet weak var textViewAllergyDEscription: UITextView!
    @IBOutlet weak var addDataButton: UIButton!
    @IBOutlet weak var textFieldToDate: UITextField!
    @IBOutlet weak var textFieldFromDate: UITextField!
    @IBOutlet weak var searchDone: UIButton!
    @IBOutlet weak var searchClear: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    var activeTextField = UITextField()
    
    @IBOutlet weak var fromToDateView: UIView!
    @IBOutlet weak var doneClearButtonView: UIView!
    @IBOutlet weak var lblSeverity: UILabel!
    @IBOutlet weak var lblEpipen: UILabel!
    
    //MARK: - Properties
    var searchText = String()
    var pageSize = 500
    var pageNumber = 1
    var documentType: String?
    var fileType: String?
    var isDataLoading = false
    var titleText: String?
    var selectedCareTaker: CaretakerModel?
    var documentsArray: [DocumentListingModel]?
    var insuranceRecordAray: [InsuranceRecord]?
    
    var datePicker = UIDatePicker()
    var toDate = Date()
    var fromDate = Date()
    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var KconstraintbackBottom: NSLayoutConstraint!
    
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tapeGestureRecogniser()
        labelTitle.text = titleText
        documentListingTableView.tableFooterView = UIView()
        documentListingTableView.separatorStyle = .none
        searchBar.setSearchBarUI()
        customizeTextFields()
        if documentType == "7" {
            self.addDataButton.setImage(UIImage(named: "addMedicalSafe"), for: .normal)
        } else if documentType == "4" {
            self.addDataButton.setImage(UIImage(named: "addAllergies"), for: .normal)
        } else if documentType == "3" {
            self.addDataButton.setImage(UIImage(named: "addInsurance"), for: .normal)
        } else if documentType == "6" {
            self.addDataButton.setImage(UIImage(named: "addImmunization"), for: .normal)
        } else if documentType == "8" {
            self.addDataButton.setImage(UIImage(named: "addLabReports"), for: .normal)
        }
        
        
        if Constant.userProfile?.isNewUser ==  "1" {
            btnNext.isHidden = false
            KconstraintbackBottom.constant = 67
            bckbutton.isHidden = true
        }
        else{
            btnNext.isHidden = true
            KconstraintbackBottom.constant = 0
            bckbutton.isHidden = false

        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        callSearchApi()
    }
    
    func customizeTextFields() {
        textFieldFromDate.delegate = self
        textFieldToDate.delegate = self
        textFieldFromDate.setLeftImage(leftImage: UIImage(named: "calendar")!)
        textFieldToDate.setLeftImage(leftImage: UIImage(named: "calendar")!)
        textFieldFromDate.attributedPlaceholder = NSAttributedString(string: "From", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        textFieldToDate.attributedPlaceholder = NSAttributedString(string: "To", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
    }
    
    func tapeGestureRecogniser() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        allergyDetailContainerView.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.allergyDetailContainerView.removeFromSuperview()
    }
    
    //MARK:- UIButton Actions
    @IBAction func addDataButtonAction(_ sender: Any) {
        view.endEditing(true)
        
        switch documentType {
        case "4":
            goToAddAllergyScreen()
        case "2":
            goToAddDocumentScreen()
        case "3":
            goToAddDocumentScreen()
        case "4":
            goToAddDocumentScreen()
        case "5":
            goToAddDocumentScreen()
        default:
            goToAddDocumentScreen()
        }
    }
    
    
    @IBAction func actionNext(_ sender: UIButton) {
        DispatchQueue.main.async {
         
            var profileData = [String: String]()
            
       
            
            profileData["custom:new_user"] = "0"
           // profileData["custom:new_user"] = "0"
            print(profileData)
            
            WidgetViewModel.shared.updateUserStatus(userData: profileData) { (result) in
                if result {
                    DispatchQueue.main.async {
                        
                        
                        if let aa = UserDefaults.standard.value(forKey: "isNewUser") as? String {
                            if aa == "0" {
                            guard let vc = UIStoryboard.init(name: StoryboardNames.TabBar, bundle: Bundle.main).instantiateViewController(withIdentifier: TabBarViewController.className) as? TabBarViewController else {
                                return
                            }
                            
                            self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        else{
                            
                      
                        }
                    }
                }
            }
            
            
            
            
        
        }
    }
    
    @IBAction func filterButton(_ sender: UIButton) {
        view.endEditing(true)
        self.searchBar.text = ""
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            print(sender.isSelected)
            hideShowSearchView(isHidden: false)
        } else {
            print(sender.isSelected)
            hideShowSearchView(isHidden: true)
            emptySearchByDateFields()
            callSearchApi()
        }
    }
    
    @IBAction func searchDoneButton(_ sender: Any) {
        view.endEditing(true)
        if textFieldFromDate.text == "" {
            Utility.shared.showToast(message: "Please select from date")
            return
        } else if textFieldToDate.text == "" {
            Utility.shared.showToast(message: "Please select to date")
            return
        } else if fromDate > toDate {
            Utility.shared.showToast(message: "To date should be greater than From date")
            return
        }
        searchDocumentByDate(toDate: textFieldToDate.text ?? "", fromDate: textFieldFromDate.text ?? "")
    }
    
    @IBAction func searchClearButton(_ sender: Any) {
        view.endEditing(true)
        hideShowSearchView(isHidden: true)
        filterButton.isSelected = false
        emptySearchByDateFields()
        callSearchApi()
    }
    
    func hideShowSearchView(isHidden: Bool) {
        DispatchQueue.main.async {
            self.fromToDateView.isHidden = isHidden
            self.doneClearButtonView.isHidden = isHidden
        }
    }
    
    func emptySearchByDateFields() {
        textFieldToDate.text = ""
        textFieldFromDate.text = ""
    }
    
    func callSearchApi() {
        if documentType == "3" {
            searchInsuranceDocument(text: "")
        } else {
            searchDocument(text: "")
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addButton(_ sender: Any) {
        view.endEditing(true)
        
        switch documentType {
        case "4":
            goToAddAllergyScreen()
        case "6":
            goToAddDocumentScreen()
        case "3":
            goToAddDocumentScreen()
        case "8":
            goToAddDocumentScreen()
        case "5":
            goToAddDocumentScreen()
        default:
            goToAddDocumentScreen()
        }
    }
    
    func goToAddAllergyScreen() {
        guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: AddAllergyReportViewController.className) as? AddAllergyReportViewController else {return}
        vc.documentType = documentType ?? "0"
        vc.fileType = fileType ?? "0"
        if selectedCareTaker != nil {
            vc.selectedCareTaker = selectedCareTaker!
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToAddDocumentScreen() {
        guard let vc = UIStoryboard.init(name: StoryboardNames.Caregiver, bundle: Bundle.main).instantiateViewController(withIdentifier: AddDocumentViewController.className) as? AddDocumentViewController else {return}
        vc.documentType = documentType ?? "0"
        vc.fileType = fileType ?? "0"
        if documentType == "8" {
            vc.isLabReport = 1
        } else {
            vc.isLabReport = 0
        }
        if selectedCareTaker != nil {
            vc.selectedCareTaker = selectedCareTaker!
        }
        navigationController?.pushViewController(vc, animated: true)
    }

}

extension DocumentListingViewController: DocumentListingProtocol {
    
    func viewReport(sender: UIButton) {
        view.endEditing(true)
        let indexpath = getSelectedIndexPath(sender: sender, tableView: documentListingTableView)
        
        if documentType == "3" {
            if indexpath != nil && insuranceRecordAray != nil {
                let insuranceRecord = insuranceRecordAray![indexpath!.row]
                
                if insuranceRecord.uploadType == "upload" {
                    guard let detailVC = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: InsuranceReportDetailViewController.className) as? InsuranceReportDetailViewController else {return}
                    
                    detailVC.selectedInsuranceRecord = insuranceRecord
                    navigationController?.pushViewController(detailVC, animated: true)
                } else {
                    guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: WebViewController.className) as? WebViewController else {return}
                    vc.webUrlString = insuranceRecord.viewFront
                    vc.strNotes = insuranceRecord.additional_notes ?? ""
                    navigationController?.pushViewController(vc, animated: true)
                }
            }
        } else if documentType == "4" {
            if indexpath != nil && documentsArray != nil {
                let document = documentsArray![indexpath!.row]
                
                allergyDetailContainerView.frame = view.frame
                loadAllergyDetailView(allergyRecord: document)
                
                UIView.animate(withDuration: 0.4, delay: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                    self.view.addSubview(self.allergyDetailContainerView)
                }, completion: nil)
            }
        } else {
            if indexpath != nil && documentsArray != nil {
                let document = documentsArray![indexpath!.row]
                
                guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: WebViewController.className) as? WebViewController else {return}
                let fileUrl = (document.filePath ?? "") //Constant.URL.baseURL + 
                vc.webUrlString = fileUrl //document.filePath
                vc.docType = documentType ?? ""
                if document.medical_original_date != nil &&  document.medical_original_date != "" {
                    vc.strDate = document.medical_original_date ?? ""
                }
                
                if document.medical_categorization != nil &&  document.medical_categorization != "" {
                    vc.strPurpose = document.medical_categorization ?? ""
                }
                
                if document.vaccine_date != nil &&  document.vaccine_date != "" {
                    vc.strDate = document.vaccine_date ?? ""
                }
                
                if document.vaccine_purpose != nil &&  document.vaccine_purpose != "" {
                    vc.strPurpose = document.vaccine_purpose ?? ""
                }
                
                if document.vaccine_for != nil &&  document.vaccine_for != "" {
                    vc.strFor = document.vaccine_for ?? ""
                }
                if document.additional_notes != nil &&  document.additional_notes != "" {
                    vc.strNotes = document.additional_notes ?? ""
                }
                
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func getSelectedIndexPath(sender: UIButton?, tableView: UITableView?) -> IndexPath? {
        let point = sender?.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView?.indexPathForRow(at: point!)
        return indexPath!
    }
    
    func loadAllergyDetailView(allergyRecord: DocumentListingModel) {
        
        self.labelAllergyTypeName.text = allergyRecord.allergyType
        self.textViewAllergyDEscription.text = allergyRecord.description
        if allergyRecord.severity != nil &&  allergyRecord.severity != "" {
            
        self.lblSeverity.text = "Severity: " + allergyRecord.severity!
            
            if allergyRecord.epipenUsed != nil && allergyRecord.epipenUsed != "" {
            self.lblEpipen.text = "Epipen used: " + allergyRecord.epipenUsed!
            }
            else{
                self.lblEpipen.text = "Epipen used: " + "NA"
            }
        }
        else {
            self.lblEpipen.text = "Epipen used: " + "NA"
            self.lblSeverity.text = "Severity: " + "NA"
        }
    }
    
}

//MARK: - Table View Delegate
extension DocumentListingViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK: - Table View Data Source
extension DocumentListingViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections = Int()
        if documentType == "3" {
            numOfSections = showNoRecordFound(dataArray: insuranceRecordAray ?? [], tableview: tableView)
        } else {
            numOfSections = showNoRecordFound(dataArray: documentsArray ?? [], tableview: tableView)
        }
        
        return numOfSections
    }
    
    func showNoRecordFound(dataArray: [Any], tableview: UITableView) -> Int {
        var noOfSections: Int = 0
//        let noDataLabel = UILabel()
//        let backgroundView = UIView()
//        let addScanButton = UIButton()
        

        if !dataArray.isEmpty {
            tableview.separatorStyle = .singleLine
            noOfSections            = 1
//            tableview.backgroundView = nil
            addDataButton.isHidden = true
        } else {
//            noDataLabel.text          = "NO DATA AVAILABLE"
//            noDataLabel.font = UIFont.boldSystemFont(ofSize: 18)
//            noDataLabel.textColor     = UIColor.darkGray
//            noDataLabel.textAlignment = .center
//            tableview.backgroundView  = noDataLabel
//            tableview.separatorStyle  = .none
            addDataButton.isHidden = false
        }
        return noOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if documentType == "3" {
            if insuranceRecordAray != nil {
                return insuranceRecordAray!.count
            }
        } else {
            if documentsArray != nil {
            //            if documentsArray?.count == 0 {
            //                NoDataImage().remove(view: tableView)
            //                NoDataImage().set(view: tableView, alertMsg: "NO DATA AVAILABLE")
            //            } else {
            //                NoDataImage().remove(view: tableView)
            //            }
                        return documentsArray!.count
            }
            //        else {
            //            NoDataImage().remove(view: tableView)
            //            NoDataImage().set(view: tableView, alertMsg: "NO DATA AVAILABLE")
            //        }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DocumentListingTableViewCell.className, for: indexPath) as? DocumentListingTableViewCell else {
            fatalError()
        }
        cell.documentType = documentType ?? "0"
        if documentType == "3" {
            let document = insuranceRecordAray![indexPath.row]
            //Added on june 5 2020
            let date = document.createdAt?.convertToDateString(currentDateFormat: "yyyy-MM-dd HH:mm:ss", requiredDateFormat: Constant.dateFormat)
            cell.dateAddedLabel.text = "Added on " + (date ?? "")
            if document.uploadType == "upload" {
                cell.scanUploadLabel.text = "Manually Added"
            } else {
                cell.scanUploadLabel.text = "Through Upload"
            }
            cell.reportTitleLabel.text = document.documentName
            cell.delegate = self
            return cell
        } else {
            let document = documentsArray![indexPath.row]
            //Added on june 5 2020
            let date = document.createdAt?.convertToDateString(currentDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", requiredDateFormat: Constant.dateFormat)
            cell.dateAddedLabel.text = "Added on " + (date ?? "")
            if documentType == "4" {
                cell.scanUploadLabel.text = ""
                cell.reportTitleLabel.text = document.allergyType
            } else if documentType == "6" {
                cell.scanUploadLabel.text = "Through Upload"
                cell.reportTitleLabel.text = document.name
            } else {
                cell.scanUploadLabel.text = "Through Upload"
                cell.reportTitleLabel.text = document.name
            }
            
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            Utility.shared.showCustomAlertWithCompletion("Delete Record", message: "Are you sure you want to delete this record?", withButtonTypes: ["No", "Yes"], withAlertCompletionBlock: { (alertAction, buttonTitle) in
                
                if buttonTitle == "Yes" {
                    self.deleteDocument(indexpath: indexPath)
                }
            })
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    func deleteDocument(indexpath: IndexPath) {
        if documentType == "3" {
            let insuranceRecord = insuranceRecordAray![indexpath.row]
            self.deleteInsuranceRecordApi(recordId: insuranceRecord.id, recordIndex: indexpath)
        } else if documentType == "6" {
            let document = documentsArray![indexpath.row]
            self.deleteImmunizationRecordApi(recordId: document.id, recordIndex: indexpath)
        } else if documentType == "4" {
            let document = documentsArray![indexpath.row]
            self.deleteAllergyRecordApi(recordId: document.id, recordIndex: indexpath)
        } else if documentType == "8" {
            let document = documentsArray![indexpath.row]
            self.deleteLabReportApi(recordId: document.id, recordIndex: indexpath)
        } else if documentType == "7" {
            let document = documentsArray![indexpath.row]
            self.deleteMedicalSafeApi(recordId: document.id, recordIndex: indexpath)
//            Utility.shared.showToast(message: "Coming Soon")
        }
    }
    
    /*func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            
            if !isDataLoading {
                isDataLoading = true
                pageNumber += 1
                searchDocument(text: "")
            }
        }
    }*/
    
}

//MARK:- Search Bar Delegates
extension DocumentListingViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count >= 2 {
            if documentType == "3" {
                self.searchInsuranceDocument(text: searchText)
            } else {
                self.searchDocument(text: searchText)
            }
        } else {
            if searchText.count == 0 {
                searchBar.endEditing(true)
                if documentType == "3" {
                    self.searchInsuranceDocument(text: searchText)
                    documentListingTableView.reloadData()
                } else {
                    self.searchDocument(text: searchText)
                    documentListingTableView.reloadData()
                }
            } else {
                documentListingTableView.reloadData()
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchDocument(text: String) {
        
        var params = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": text, "page": pageNumber, "size": pageSize] as [String : Any]
            } else {
                params = ["cognito_user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": text, "page": pageNumber, "size": pageSize] as [String : Any]
            }
        } else {
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": text, "page": pageNumber, "size": pageSize] as [String : Any]
        }
        
        if documentType == "7" {
            params["document_type"] = ""
        }
        
        Utility.shared.startLoader()
        DocumentListingViewModel.searchDocument(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
//                self.filterButton.isUserInteractionEnabled = true
                self.isDataLoading = false
                self.documentsArray = response
                self.documentListingTableView.reloadData()
            } else {
//                self.filterButton.isUserInteractionEnabled = false
                if text != "" {
                    self.documentsArray?.removeAll()
                    self.documentListingTableView.reloadData()
                }
//                if error != "No data found." {
                    Utility.shared.showToast(message: error ?? "Something went wrong")
//                }
            }
        }
    }
    
    func searchInsuranceDocument(text: String) {
        
        var params = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": text, "page": pageNumber, "size": pageSize] as [String : Any]
            } else {
                params = ["cognito_user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": text, "page": pageNumber, "size": pageSize] as [String : Any]
            }
        } else {
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": text, "page": pageNumber, "size": pageSize] as [String : Any]
        }
        
        if documentType == "7" {
            params["document_type"] = "0"
        }
        
        Utility.shared.startLoader()
        DocumentListingViewModel.searchInsuranceDocument(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                //self.filterButton.isUserInteractionEnabled = true
                self.isDataLoading = false
                self.insuranceRecordAray = response
                self.documentListingTableView.reloadData()
            } else {
                //self.filterButton.isUserInteractionEnabled = false
                if text != "" {
                    self.insuranceRecordAray?.removeAll()
                    self.documentListingTableView.reloadData()
                }
                if error != "No data found." {
                    Utility.shared.showToast(message: error ?? "Something went wrong")
                }
            }
        }
    }
    
    func searchDocumentByDate(toDate: String, fromDate: String) {
        var params = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": "", "page": pageNumber, "size": pageSize, "from_date" : fromDate, "to_date": toDate] as [String : Any]
            } else {
                params = ["cognito_user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": "", "page": pageNumber, "size": pageSize, "from_date" : fromDate, "to_date": toDate] as [String : Any]
            }
        } else {
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": "", "page": pageNumber, "size": pageSize, "from_date" : fromDate, "to_date": toDate] as [String : Any]
        }
        
        if documentType == "7" {
            params["document_type"] = ""
        }
        
        Utility.shared.startLoader()
        //        if documentType == "3" {
        DocumentListingViewModel.searchDocumentsByDate(parameters: params) { (response, records, error) in
            Utility.shared.stopLoader()
            if response != nil {
                //self.filterButton.isUserInteractionEnabled = true
                if self.documentType == "3" {
                    self.insuranceRecordAray = records
                    self.documentListingTableView.reloadData()
                } else {
                    self.documentsArray = response
                    self.documentListingTableView.reloadData()
                }
            } else {
                //self.filterButton.isUserInteractionEnabled = false
                if self.documentType == "3" {
                    self.insuranceRecordAray?.removeAll()
                } else {
                    self.documentsArray?.removeAll()
                }
                
                self.documentListingTableView.reloadData()
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func deleteInsuranceRecordApi(recordId: Int?, recordIndex: IndexPath) {
        
        var params = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "id": recordId ?? 0] as [String : Any]
            } else {
                params = ["cognito_user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "id": recordId ?? 0] as [String : Any]
            }
        } else {
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "id": recordId ?? 0] as [String : Any]
        }
        
        InsuaranceRecordViewModel.deleteInsuranceRecord(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.insuranceRecordAray?.remove(at: recordIndex.row)
                self.documentListingTableView.reloadData()
                Utility.shared.showToast(message: error ?? "Insurance Record Deleted successfully")
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func deleteImmunizationRecordApi(recordId: Int?, recordIndex: IndexPath) {
        
        var params = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "id": recordId ?? 0] as [String : Any]
            } else {
                params = ["cognito_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "id": recordId ?? 0] as [String : Any]
            }
        } else {
            params = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "id": recordId ?? 0] as [String : Any]
        }
        
        ImmunizationViewModel.deleteImunizationRecord(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.documentsArray?.remove(at: recordIndex.row)
                self.documentListingTableView.reloadData()
                Utility.shared.showToast(message: error ?? "Immunization Record Deleted successfully")
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func deleteAllergyRecordApi(recordId: Int?, recordIndex: IndexPath) {
        
        var params = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "id": recordId ?? 0] as [String : Any]
            } else {
                params = ["cognito_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "id": recordId ?? 0] as [String : Any]
            }
        } else {
            params = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "id": recordId ?? 0] as [String : Any]
        }
        
        AllergyViewModel.deleteAllergyRecord(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.documentsArray?.remove(at: recordIndex.row)
                self.documentListingTableView.reloadData()
                Utility.shared.showToast(message: error ?? "Allergy Record Deleted successfully")
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func deleteLabReportApi(recordId: Int?, recordIndex: IndexPath) {
        
        var params = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "id": recordId ?? 0] as [String : Any]
            } else {
                params = ["cognito_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "id": recordId ?? 0] as [String : Any]
            }
        } else {
            params = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "id": recordId ?? 0] as [String : Any]
        }
        
        LabReportViewModel.deleteLabReport(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.documentsArray?.remove(at: recordIndex.row)
                self.documentListingTableView.reloadData()
                Utility.shared.showToast(message: error ?? "Lab Report Deleted successfully")
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func deleteMedicalSafeApi(recordId: Int?, recordIndex: IndexPath) {
        
        var params = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "id": recordId ?? 0] as [String : Any]
            } else {
                params = ["cognito_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "id": recordId ?? 0] as [String : Any]
            }
        } else {
            params = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "id": recordId ?? 0] as [String : Any]
        }
        
//atinder        DocumentListingViewModel.deleteMedicalSafeReport(parameters: params) { (response, error) in
//            Utility.shared.stopLoader()
//            if response != nil {
//                self.documentsArray?.remove(at: recordIndex.row)
//                self.documentListingTableView.reloadData()
//                Utility.shared.showToast(message: error ?? "Medical Safe Document Deleted successfully")
//            } else {
//                Utility.shared.showToast(message: error ?? "Something went wrong")
//            }
//        }
    }
}

extension DocumentListingViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        if textField == textFieldFromDate || textField == textFieldToDate {
            self.showDatePicker()
        }
        return true
    }
    
    func showDatePicker(){
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat //"MM/dd/yyyy"
        var date = Date()
        if activeTextField.text != "" {
            date = formater.date(from: activeTextField.text!) ?? Date()
        }
        
        let screenWidth = UIScreen.main.bounds.width
       
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -110, to: Date())
        datePicker.maximumDate = Date() //Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datePicker.setDate(date, animated: true)
        
        if #available(iOS 14.0, *)
        {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()

//            datePicker.datePickerStyle = UIDatePickerStyle.wheels
        } else {
           
        }
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        textFieldFromDate.inputAccessoryView = toolbar
        textFieldToDate.inputAccessoryView = toolbar
        textFieldFromDate.inputView = datePicker
        textFieldToDate.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = Constant.dateFormat //"yyyy-MM-dd" //"dd MMM, yyyy"
        if activeTextField == textFieldFromDate {
            fromDate = datePicker.date
        } else {
            toDate = datePicker.date
        }
        activeTextField.text = formatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}
