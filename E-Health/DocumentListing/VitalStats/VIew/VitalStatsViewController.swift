//
//  VitalStatsViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/29/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient
import DropDown

class VitalStatsViewController: UIViewController {

    // MARK: - Properties
    var fileType: String?
    var documentType: String?
    var selectedCareTaker: CaretakerModel?
    var userVitalStats: VitalStats?
    var selectedBloodType: BloodType?
    var syncVitalsVC = SyncVitalsViewController()
    var selectedVitalStats: VitalStats?
    
    //MARK: - Outlets
    @IBOutlet weak var textFieldBloodType: ExtendedTextField!
    @IBOutlet weak var textFieldBaselineTemperature: ExtendedTextField!
    @IBOutlet weak var textFieldRestingHeartRate: ExtendedTextField!
    @IBOutlet weak var textFieldPulse: ExtendedTextField!
    @IBOutlet weak var textFieldBloodPressure: ExtendedTextField!
    @IBOutlet weak var textFieldRespiratoryRate: ExtendedTextField!
    @IBOutlet weak var textFieldA1C: ExtendedTextField!
    @IBOutlet weak var textFieldMorningSugar: ExtendedTextField!
    @IBOutlet weak var textFieldCholestrolScore: ExtendedTextField!
    @IBOutlet weak var textFieldLDL: ExtendedTextField!
    @IBOutlet weak var textFieldHDL: ExtendedTextField!
    @IBOutlet weak var textFieldTriglycerides: ExtendedTextField!
    @IBOutlet weak var textFieldOxygenSaturation: ExtendedTextField!
    @IBOutlet weak var textFieldPH: ExtendedTextField!
    @IBOutlet weak var textFieldCo2Level: ExtendedTextField!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        
        if Constant.selectedCareTaker != nil {
            self.selectedCareTaker = Constant.selectedCareTaker
        }
        getVitalStatsDetailApi()
    }
    
    func setUI() {
        textFieldBloodType.setDarkBorderColor()
        textFieldBaselineTemperature.setDarkBorderColor()
        textFieldRestingHeartRate.setDarkBorderColor()
        textFieldPulse.setDarkBorderColor()
        textFieldBloodPressure.setDarkBorderColor()
        textFieldRespiratoryRate.setDarkBorderColor()
        textFieldA1C.setDarkBorderColor()
        textFieldMorningSugar.setDarkBorderColor()
        textFieldCholestrolScore.setDarkBorderColor()
        textFieldLDL.setDarkBorderColor()
        textFieldHDL.setDarkBorderColor()
        textFieldTriglycerides.setDarkBorderColor()
        textFieldOxygenSaturation.setDarkBorderColor()
        textFieldPH.setDarkBorderColor()
        textFieldCo2Level.setDarkBorderColor()
        
        textFieldBloodType.attributedPlaceholder = NSAttributedString(string: "Blood Type", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldBaselineTemperature.attributedPlaceholder = NSAttributedString(string: "Baseline Temperature(> 95.0 *F <105.8 *F)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldRestingHeartRate.attributedPlaceholder = NSAttributedString(string: "Resting Heart Rate(10-120)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldPulse.attributedPlaceholder = NSAttributedString(string: "Pulse(10-100 beat/minute)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldBloodPressure.attributedPlaceholder = NSAttributedString(string: "Blood Pressure(50-300 mmHg)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldRespiratoryRate.attributedPlaceholder = NSAttributedString(string: "Respiratory Rate(5-25 Breath/Minute)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldA1C.attributedPlaceholder = NSAttributedString(string: "A1C(5.0-10%)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldMorningSugar.attributedPlaceholder = NSAttributedString(string: "Morning Sugar(10-900 mg/dl)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldCholestrolScore.attributedPlaceholder = NSAttributedString(string: "Cholesterol Score(100-400 mg/dl)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldLDL.attributedPlaceholder = NSAttributedString(string: "LDL(50-300 mg/dl)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldHDL.attributedPlaceholder = NSAttributedString(string: "HDL(0-200 mg/dl)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldTriglycerides.attributedPlaceholder = NSAttributedString(string: "Triglycerides(100-1000 mg/dl)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldOxygenSaturation.attributedPlaceholder = NSAttributedString(string: "Oxygen Saturation(0-300 mm/hg)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldPH.attributedPlaceholder = NSAttributedString(string: "PH(0-14 pHs)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldCo2Level.attributedPlaceholder = NSAttributedString(string: "CO2 Level(0-600 ppm)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
      
    }
    
    
    func getVitalStatsDetailApi() {
        var parameters = [String: Any]()
        if Constant.selectedCareTaker != nil {
            
            if Constant.selectedCareTaker?.minor_id != nil {
                parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": Constant.selectedCareTaker?.minor_id ?? 0] as [String : Any]
              //  labelUSerName.text = Constant.selectedCareTaker?.name
             //   labelAge.text = "Age " + self.calculateAge(dob: Constant.selectedCareTaker?.dob ?? "")
                
            } else {
              //  labelUSerName.text = Constant.selectedCareTaker?.name
                parameters = ["cognito_id": Constant.selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0] as [String : Any]
             //   labelAge.text = "Age " + self.calculateAge(dob: Constant.selectedCareTaker?.dob ?? "")
            }
        } else {
            parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": 0] as [String : Any]
         //   labelUSerName.text = getName(name: Constant.userProfile?.name ?? "" , middleName: Constant.userProfile?.middleName)
          //  labelAge.text = "Age " + self.calculateAge(dob: Constant.userProfile?.dob ?? "")
        }
        Utility.shared.startLoader()
        VitalStatsViewModel.getVitalStatsDetail(params: parameters) { (response, error) in
            Utility.shared.stopLoader()
            let isDarkMode = self.checkIfDarkMode()
            if response != nil {
                if isDarkMode {
                    self.setData(response: response, textColor: .white)
                } else {
                    self.setData(response: response, textColor: .black)
                }
            } else {
                if isDarkMode {
                    self.setData(response: nil, textColor: .white)
                } else {
                    self.setData(response: nil, textColor: .black)
                }
                
                if error?.lowercased() == "no data found" {
                    return
                }
                Utility.shared.showToast(message: error ?? "Something went wrong!")
            }
        }
    }
    
    
    
    func setData(response: VitalStats?, textColor: UIColor) {
        self.userVitalStats = response
        
        
        if userVitalStats != nil {
            self.prefilledFields()
        }
        
//        self.getbloodTypeCode(code: response?.blood_type_id ?? 0)
//
//        if let bloodType = self.selectedBloodType?.name {
//            self.textFieldBloodType.text = bloodType
//        }
//
//
//        if let temperature = response?.baselineTemperature {
//            self.textFieldBaselineTemperature.text =  temperature
//        }
//
//        if let heartRate = response?.restingHeartRate {
//            self.textFieldRestingHeartRate.text = heartRate
//        }
//
//        if let pulse = response?.pulse {
//            self.textFieldPulse.text = pulse
//        }
//
//
//        if let bloodPressure = response?.bloodPressure {
//            self.textFieldBloodPressure.text = bloodPressure
//
//        }
//
//
//        if let respiratoryRate = response?.respiratoryRate {
//            self.textFieldRespiratoryRate.text = respiratoryRate
//        }
//
//        if let a1c = response?.a1c {
//            self.textFieldA1C.text = a1c
//        }
//
//
//        if let mornignSugar = response?.mornignSugar {
//            self.textFieldMorningSugar.text = mornignSugar
//        }
//
//
//        if let cholestrolScore = response?.cholestrolScore {
//            self.textFieldCholestrolScore.text = cholestrolScore
//        }
//
//
//        if let ldl = response?.ldl {
//            self.textFieldLDL.text = ldl
//        }
//
//        if let hdl = response?.hdl {
//            self.textFieldHDL.text = hdl
//        }
//
//        if let triglycerides = response?.triglycerides {
//            self.textFieldTriglycerides.text = triglycerides
//        }
//
//        if let oxygenSaturation = response?.oxygenSaturation {
//            self.textFieldOxygenSaturation.text = oxygenSaturation
//        }
//
//        if let ph = response?.ph {
//            self.textFieldPH.text = ph
//        }
//
//        if let co2Level = response?.co2Level {
//            self.textFieldCo2Level.text = co2Level
//        }
    }
    
    func prefilledFields() {
        self.getbloodTypeCode(code: userVitalStats!.blood_type_id ?? 0)
        DispatchQueue.main.async {
            self.textFieldBloodType.text = self.selectedBloodType?.name
            if let temperature = self.userVitalStats?.baselineTemperature {
                self.textFieldBaselineTemperature.text =  String(describing: temperature)
            }
            
            if let heartRate = self.userVitalStats?.restingHeartRate {
                self.textFieldRestingHeartRate.text = String(describing: heartRate)
            }
            if let pulse = self.userVitalStats?.pulse {
                self.textFieldPulse.text = String(describing: pulse)
            }
            if let bloodPressure = self.userVitalStats?.bloodPressure {
                self.textFieldBloodPressure.text = String(describing: bloodPressure)
            }
            if let respiratoryRate = self.userVitalStats?.respiratoryRate {
                self.textFieldRespiratoryRate.text = String(describing: respiratoryRate)
            }
            if let a1c = self.userVitalStats?.a1c {
                self.textFieldA1C.text = String(describing: a1c)
            }
            if let mornignSugar = self.userVitalStats?.mornignSugar {
                self.textFieldMorningSugar.text = String(describing: mornignSugar)
            }
            if let cholestrolScore = self.userVitalStats?.cholestrolScore {
                self.textFieldCholestrolScore.text =  String(describing: cholestrolScore)
            }
            if let ldl = self.userVitalStats?.ldl {
                self.textFieldLDL.text = String(describing: ldl)
            }
            if let hdl = self.userVitalStats?.hdl {
                self.textFieldHDL.text = String(describing: hdl)
            }
            if let triglycerides = self.userVitalStats?.triglycerides {
                self.textFieldTriglycerides.text = String(describing: triglycerides)
            }
            if let oxygenSaturation = self.userVitalStats?.oxygenSaturation {
                self.textFieldOxygenSaturation.text = String(describing: oxygenSaturation)
            }
            if let ph = self.userVitalStats?.ph {
                self.textFieldPH.text = String(describing: ph)
            }
            if let co2Level = self.userVitalStats?.co2Level {
                self.textFieldCo2Level.text = String(describing: co2Level)
            }
        }
        
    }
    
    func getbloodTypeCode(code: Int) {
        if Constant.masterData == nil {
            return
        }
        for bloodtype in Constant.masterData!.bloodtypes {
            if bloodtype.id == code {
                self.selectedBloodType = bloodtype
            }
        }
    }
    
    // MARK: - UIButton Actions
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func syncButtonAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: SyncVitalsViewController.className) as? SyncVitalsViewController
        vc?.delegate = self
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func saveRecordsButtonAction(_ sender: Any) {
        
        var parameters = [String: Any]()
        if Constant.selectedCareTaker != nil {
            
            if Constant.selectedCareTaker?.minor_id != nil {
                parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": Constant.selectedCareTaker?.minor_id ?? 0] as [String : Any]
            } else {
                parameters = ["cognito_id": Constant.selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0] as [String : Any]
            }
        } else {
            parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": 0] as [String : Any]
        }
        if userVitalStats != nil {
            parameters["id"] = userVitalStats?.id ?? ""
        }
        if selectedBloodType?.id != nil {
            parameters["blood_type_id"] = selectedBloodType?.id }
        else {
            parameters["blood_type_id"] = "0"
        }
        //textFieldBloodType.text ?? ""
        parameters["baseline_temperature"] = textFieldBaselineTemperature.text ?? ""
        parameters["resting_heart_rate"] = textFieldRestingHeartRate.text ?? ""
        parameters["pulse"] = textFieldPulse.text ?? ""
        parameters["blood_pressure"] = textFieldBloodPressure.text ?? ""
        parameters["respiratory_rate"] = textFieldRespiratoryRate.text ?? ""
        parameters["A1C"] = textFieldA1C.text ?? ""
        parameters["morning_sugar"] = textFieldMorningSugar.text ?? ""
        parameters["cholestrol"] = textFieldCholestrolScore.text ?? ""
        parameters["LDL"] = textFieldLDL.text ?? ""
        parameters["HDL"] = textFieldHDL.text ?? ""
        parameters["oxygen_saturation"] = textFieldOxygenSaturation.text ?? ""
        parameters["PH"] = textFieldPH.text ?? ""
        parameters["CO2_level"] = textFieldCo2Level.text ?? ""
        parameters["triglycerides"] = textFieldTriglycerides.text ?? ""
        
        _ = VitalStats(value: parameters)
        let result = self.validateTextField()//Validations.shared.validateAddInsuranceRecord(insuranceRecord: insuranceRecord)
        
        if result == false {
            return
        } else {
            Utility.shared.startLoader()
            addVitalStatsRecordApi(params: parameters)
        }
    }
}

//MARK:- Sync Vitals Delegates
extension VitalStatsViewController: VitalsFromHealtkit {
    
    func displayVitals(HeartRate: String?, userVitals: VitalStats?) {
        
        if HeartRate == nil && userVitals == nil {
            DispatchQueue.main.async {
                self.view.makeToast("No Data available from device!")
            }
            return
        }
        if HeartRate != nil {
            self.userVitalStats?.restingHeartRate = HeartRate
            DispatchQueue.main.async {
                self.textFieldRestingHeartRate.text = HeartRate
            }
        }
        if userVitals != nil {
            self.userVitalStats = userVitals
            self.prefilledFields()
        }
    }
}

extension VitalStatsViewController {
    
    func addVitalStatsRecordApi(params: [String: Any]) {
        VitalStatsViewModel.addVitalStatsRecord(isUploadDoc: false, params: params) { (message, error) in
            Utility.shared.stopLoader()
            if error == nil {
                Utility.shared.showToast(message: message!)
                self.navigationController?.popViewController(animated: true)
//                self.resetTextField()
            } else {
                Utility.shared.showToast(message: error!)
            }
        }
    }
    
    func resetTextField() {
        textFieldBloodType.text = ""
        textFieldBaselineTemperature.text = ""
        textFieldRestingHeartRate.text = ""
        textFieldPulse.text = ""
        textFieldBloodPressure.text = ""
        textFieldRespiratoryRate.text = ""
        textFieldA1C.text = ""
        textFieldMorningSugar.text = ""
        textFieldCholestrolScore.text = ""
        textFieldLDL.text = ""
        textFieldHDL.text = ""
        textFieldTriglycerides.text = ""
        textFieldOxygenSaturation.text = ""
        textFieldPH.text = ""
        textFieldCo2Level.text = ""
    }
    
    func initdropDown (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                textField.text = item
                self.selectedBloodType = Constant.masterData?.bloodtypes[index]
                dropDown.hide()
            }
            dropDown.show()
        }
    }
}

extension VitalStatsViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == textFieldBloodType {
            var bloodTypesNames = [String]()
            for bloodTypes in Constant.masterData!.bloodtypes {
                bloodTypesNames.append(bloodTypes.name ?? "N.A.")
            }
            if bloodTypesNames.count == 0 {
                Utility.shared.showToast(message: "No blood types list available!")
                return false
            }
            self.initdropDown(textField: textField, dataSource: bloodTypesNames)
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
            return false
        }
        if string == "" {
            return true
        }
        
        //TEMPERATURE
        if textField == textFieldBaselineTemperature || textField == textFieldCo2Level {
            var result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 6, newCharacter: string)
            
            if (textField.text?.contains("."))! && string == "."{
                return false
            } else if (textField.text?.contains("."))! && string != "."{
                if textField.text?.components(separatedBy: ".")[1].count == 2 {
                            return false
                }
//                let textCount = textField.text?.count
//                result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: textCount!+3, newCharacter: string)
            } else if  !(textField.text?.contains("."))! && string != "."{
                result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 3, newCharacter: string)
            }
            return result
        }
        
        if textField == textFieldRestingHeartRate || textField == textFieldPulse || textField == textFieldBloodPressure || textField == textFieldMorningSugar || textField == textFieldCholestrolScore || textField == textFieldLDL || textField == textFieldHDL || textField == textFieldOxygenSaturation {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 3, newCharacter: string)
            
            return result
        }
        
        if textField == textFieldRespiratoryRate || textField == textFieldA1C || textField == textFieldPH  {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 2, newCharacter: string)
            
            return result
        }
        
        if textField == textFieldTriglycerides {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 4, newCharacter: string)
            
            return result
        }
        
        return true
    }
}

//MARK:- VALIDATIONS
extension VitalStatsViewController {
    func validateTextField() -> Bool {
//        if textFieldBloodType.text == "" && textFieldBaselineTemperature.text == "" && textFieldRestingHeartRate.text == "" && textFieldPulse.text == "" && textFieldBloodPressure.text == "" && textFieldRespiratoryRate.text == "" && textFieldA1C.text == "" && textFieldMorningSugar.text == "" && textFieldCholestrolScore.text == "" && textFieldLDL.text == "" && textFieldHDL.text == "" && textFieldTriglycerides.text == "" && textFieldOxygenSaturation.text == "" && textFieldPH.text == "" && textFieldCo2Level.text == "" {
//            Utility.shared.showToast(message: "All fields are empty")
//            return false
//        }
//        //Triglycerides Maximum Validation
//        if textFieldBloodType.text == "" || textFieldBloodType.text == nil {
//            Utility.shared.showToast(message: "Please select Blood Type")
//            return false
//        }
        if textFieldBaselineTemperature.text != "" && textFieldBaselineTemperature.text != nil {
            let temperature = Double(textFieldBaselineTemperature.text!)
            if temperature! > 105.8 {
                Utility.shared.showToast(message: "Temperature cannot be more than 105.8 *F")
                return false
            }
            if temperature! < 95.0 {
                Utility.shared.showToast(message: "Temperature should be minimum 95.0 *F")
                return false
            }
        }
      /*  else {
//            if userVitalStats?.baselineTemperature == nil && textFieldBaselineTemperature.text == ""{
                Utility.shared.showToast(message: "Please enter Baseline Temperature")
                return false
//            }
        } */
        //Heart Rate Maximum Validation
        if textFieldRestingHeartRate.text != "" && textFieldRestingHeartRate.text != nil {
            let heartRate = Int(textFieldRestingHeartRate.text!)
            if heartRate! > 120 {
                Utility.shared.showToast(message: "Heart Rate cannot be more than 120")
                return false
            }
            if heartRate! < 10 {
                Utility.shared.showToast(message: "Heart Rate should be minimum 10")
                return false
            }
        } /* else {
//            if userVitalStats?.restingHeartRate != nil && textFieldRestingHeartRate.text == ""{
                Utility.shared.showToast(message: "Please enter Resting Heart Rate")
                return false
//            }
        } */
        //Pulse Maximum Validation
        if textFieldPulse.text != ""  && textFieldPulse.text != nil  {
            let pulse = Int(textFieldPulse.text!)
            if pulse! > 100{
                Utility.shared.showToast(message: "Pulse rate cannot be more than 100 beats/minute")
                return false
            }
            if pulse! < 10 {
                Utility.shared.showToast(message: "Pulse rate should be minimum 10 beat/minute")
                return false
            }
        }
/* else {
//            if userVitalStats?.pulse == nil && textFieldPulse.text == ""{
                Utility.shared.showToast(message: "Please enter Pulse Rate")
                return false
//            }
        } */
        //Blood Pressure Maximum Validation
        if textFieldBloodPressure.text != "" && textFieldBloodPressure.text != nil {
            let bloodPressure = Int(textFieldBloodPressure.text!)
            if bloodPressure! > 300 {
                Utility.shared.showToast(message: "Blood Pressure cannot be more than 300 mmHg")
                return false
            }
            if bloodPressure! < 50 {
                Utility.shared.showToast(message: "Blood Pressure should be minimum 50 mmHg")
                return false
            }
        }  /* elseelse {
//            if userVitalStats?.bloodPressure != nil && textFieldBloodPressure.text == ""{
                Utility.shared.showToast(message: "Please enter Blood Pressure")
                return false
//            }
        } */
        //Respiratory Rate Maximum Validation
        if textFieldRespiratoryRate.text != "" && textFieldRespiratoryRate.text != nil {
            let respiratioryRate = Int(textFieldRespiratoryRate.text!)
            if respiratioryRate! > 25 {
                Utility.shared.showToast(message: "Respiratory Rate cannot be more than 25 Breath/Minute")
                return false
            }
            if respiratioryRate! < 5 {
                Utility.shared.showToast(message: "Respiratory Rate should be minimum 5 Breath/Minute")
                return false
            }
        } /* else {
//            if userVitalStats?.respiratoryRate != nil && textFieldRespiratoryRate.text == ""{
                Utility.shared.showToast(message: "Please enter Respiratory Rate")
                return false
//            }
        } */
        //A1C Rate Maximum Validation
        if textFieldA1C.text != "" && textFieldA1C.text != nil {
            let string = textFieldA1C.text ?? "0"
            let A1c = Int(string) ?? 0
            if A1c > 10 {
                Utility.shared.showToast(message: "A1C cannot be more than 10%")
                return false
            }
            if A1c < 5 {
                Utility.shared.showToast(message: "A1C should be minimum 5.0%")
                return false
            }
        }
        
/*else {
//            if userVitalStats?.a1c != nil && textFieldA1C.text == ""{
                Utility.shared.showToast(message: "Please enter A1C Rate")
                return false
//            }
        } */
        //Morning Sugar Maximum Validation
        if textFieldMorningSugar.text != "" && textFieldMorningSugar.text != nil {
            let morningSugar = Int(textFieldMorningSugar.text!)
            if morningSugar! > 900 {
                Utility.shared.showToast(message: "Morning Sugar cannot be more than 900 mg/dl")
                return false
            }
            if morningSugar! < 10 {
                Utility.shared.showToast(message: "Morning Sugar should be minimum 10 mg/dl")
                return false
            }
        } /*else {
//            if userVitalStats?.mornignSugar != nil && textFieldMorningSugar.text == ""{
                Utility.shared.showToast(message: "Please enter Morning Sugar")
                return false
//            }
        } */
        //Cholestrol Score Maximum Validation
        if textFieldCholestrolScore.text != "" && textFieldCholestrolScore.text != nil {
            let cholestrolScore = Int(textFieldCholestrolScore.text!)
            if cholestrolScore! > 400 {
                Utility.shared.showToast(message: "Cholesterol level cannot be more than 400 mg/dl")
                return false
            }
            if cholestrolScore! < 100 {
                Utility.shared.showToast(message: "Cholesterol level should be minimum 100 mg/dl")
                return false
            }
        } /* else {
//            if userVitalStats?.cholestrolScore != nil && textFieldCholestrolScore.text == ""{
                Utility.shared.showToast(message: "Please enter Cholesterol level")
                return false
//            }
        }*/
        //LDL Maximum Validation
        if textFieldLDL.text != "" && textFieldLDL.text != nil {
            let ldl = Int(textFieldLDL.text!)
            if ldl! > 300 {
                Utility.shared.showToast(message: "LDL cannot be more than 300 mg/dl")
                return false
            }
            if ldl! < 50 {
                Utility.shared.showToast(message: "LDL should be minimum 50 mg/dl")
                return false
            }
        }
/* else {
//            if userVitalStats?.ldl != nil && textFieldLDL.text == ""{
                Utility.shared.showToast(message: "Please enter LDL")
                return false
//            }
        }*/
        //HDL Maximum Validation
        if textFieldHDL.text != "" && textFieldHDL.text != nil {
            let hdl = Int(textFieldHDL.text!)
            if hdl! > 200 {
                Utility.shared.showToast(message: "HDL cannot be more than 200 mg/dl")
                return false
            }
            if hdl! < 0 {
                Utility.shared.showToast(message: "HDL should be minimum 0 mg/dl")
                return false
            }
        }/* else {
//            if userVitalStats?.hdl != nil && textFieldHDL.text == ""{
                Utility.shared.showToast(message: "Please enter HDL")
                return false
//            }
        } */
        //Triglycerides Maximum Validation
        if textFieldTriglycerides.text != "" && textFieldTriglycerides != nil {
            let triglycerides = Int(textFieldTriglycerides.text!)
            if triglycerides! > 1000 {
                Utility.shared.showToast(message: "Triglycerides cannot be more than 1000 mg/dl")
                return false
            }
            if triglycerides! < 100 {
                Utility.shared.showToast(message: "Triglycerides should be minimum 100 mg/dl")
                return false
            }
        } /* else {
//            if userVitalStats?.triglycerides != nil && textFieldTriglycerides.text == ""{
                Utility.shared.showToast(message: "Please enter Triglycerides")
                return false
//            }
        } */
        //Triglycerides Maximum Validation
        if textFieldOxygenSaturation.text != "" && textFieldOxygenSaturation.text != nil {
            let oxygenSaturation = Int(textFieldOxygenSaturation.text!)
            if oxygenSaturation! > 300 {
                Utility.shared.showToast(message: "Oxygen Saturation level cannot be more than 300 mm/hg")
                return false
            }
            if oxygenSaturation! < 0 {
                Utility.shared.showToast(message: "Oxygen Saturation level should be minimum 0 mm/hg")
                return false
            }
        } /* else {
//            if userVitalStats?.oxygenSaturation != nil && textFieldOxygenSaturation.text == ""{
                Utility.shared.showToast(message: "Please enter Oxygen Saturation level")
                return false
//            }
        } */
        //Triglycerides Maximum Validation
        if textFieldPH.text != "" && textFieldPH.text != nil {
            let ph = Int(textFieldPH.text!)
            if ph! > 14 {
                Utility.shared.showToast(message: "Ph level cannot be more than 14 pHs")
                return false
            }
            if ph! < 0 {
                Utility.shared.showToast(message: "Ph level should be minimum 0 pHs")
                return false
            }
        } /* else {
//            if userVitalStats?.ph != nil && textFieldPH.text == ""{
                Utility.shared.showToast(message: "Please enter Ph level")
                return false
//            }
        } */
        //Triglycerides Maximum Validation
        if textFieldCo2Level.text != "" && textFieldCo2Level.text != nil {
            let co2Level = Double(textFieldCo2Level.text!)
            if co2Level! > 600.0 {
                Utility.shared.showToast(message: "CO2 level cannot be more than 600 ppm")
                return false
            }
            if co2Level! < 0.0 {
                Utility.shared.showToast(message: "CO2 level should be minimum 0 ppm")
                return false
            }
        }
        /* else {
//            if userVitalStats?.co2Level != nil && textFieldCo2Level.text == ""{
                Utility.shared.showToast(message: "Please enter CO2 level")
                return false
//            }
        } */
        return true
    }
}
