//
//  VitalStatsDetailViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/29/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

class VitalStatsDetailViewController: UIViewController {

    var selectedCareTaker: CaretakerModel?
    var selectedVitalStats: VitalStats?
    var selectedBloodType: BloodType?
    
    //MARK: - Outlets
    @IBOutlet weak var labelUSerName: UILabel!
    @IBOutlet weak var labelAge: UILabel!
    @IBOutlet weak var labelBloodType: UILabel!
    @IBOutlet weak var labelBaseTemperature: UILabel!
    @IBOutlet weak var labelRestingHeartRate: UILabel!
    @IBOutlet weak var labelPulse: UILabel!
    @IBOutlet weak var labelBloodPressure: UILabel!
    @IBOutlet weak var labelRespiratoryRate: UILabel!
    @IBOutlet weak var labelA1C: UILabel!
    @IBOutlet weak var labelMorningSugar: UILabel!
    @IBOutlet weak var labelCholestrolScore: UILabel!
    @IBOutlet weak var labelLDL: UILabel!
    @IBOutlet weak var labelHDL: UILabel!
    @IBOutlet weak var labelTriglycerides: UILabel!
    @IBOutlet weak var labelOxygenSaturation: UILabel!
    @IBOutlet weak var labelPH: UILabel!
    @IBOutlet weak var labelCo2Level: UILabel!
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Constant.selectedCareTaker != nil {
            self.selectedCareTaker = Constant.selectedCareTaker
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getVitalStatsDetailApi()
    }
    
    //MARK: - ButtonActions
    @IBAction func buttonBackAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = false
        navigationController?.popViewController(animated: true)
    }
                              
    @IBAction func updateVitalsButtonAction(_ sender: Any) {
//        let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: VitalStatsViewController.className) as? VitalStatsViewController
//        vc?.userVitalStats = selectedVitalStats
//        vc?.selectedCareTaker = self.selectedCareTaker
//        navigationController?.pushViewController(vc!, animated: true)
    }
}

extension VitalStatsDetailViewController {
    func calculateAge(dob: String) -> String {
        //GET AGE OF CARETAKER FROM DATE OF BIRTH
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat //"yyyy-MM-dd" //"dd MMM, yyyy"
        var birthday = Date()
        birthday = formater.date(from: dob) ?? Date()
        
        let calendar = Calendar.current

        let ageComponents = calendar.dateComponents([.year, .month, .day], from: birthday, to: Date())
        var age = String(ageComponents.year!) + " years"
        if ageComponents.year! == 1 {
            age = String(ageComponents.year!) + " year"
        }
        else if ageComponents.year! < 1 {
            age = String(ageComponents.month!) + " months"
            if ageComponents.month! == 1 {
                age = String(ageComponents.month!) + " month"
            }
            if ageComponents.month! < 1 {
                age = String(ageComponents.day!) + " days"
                if ageComponents.day! == 1 {
                    age = String(ageComponents.day!) + " day"
                }
            }
        }
        return age
    }
    
    func getbloodTypeCode(code: Int) {
        if Constant.masterData == nil {
            return
        }
        for bloodtype in Constant.masterData!.bloodtypes {
            if bloodtype.id == code {
                self.selectedBloodType = bloodtype
            }
        }
    }
    
    func getName(name: String, middleName: String?) -> String {
        var senderFullName = String()
        let name = name
        let middleName = middleName
        if middleName != "" && middleName != nil {
            let myStringArr = name.components(separatedBy: " ")
            let firstName = (myStringArr[0])
            var lastName = ""
            if myStringArr.count == 2 {
                lastName = (myStringArr[1])
            }
            senderFullName = firstName + " " + (middleName ?? "") + " " + lastName
        } else {
            senderFullName = name
        }
        return senderFullName
    }
    
    func getVitalStatsDetailApi() {
        var parameters = [String: Any]()
        if Constant.selectedCareTaker != nil {
            
            if Constant.selectedCareTaker?.minor_id != nil {
                parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": Constant.selectedCareTaker?.minor_id ?? 0] as [String : Any]
                labelUSerName.text = Constant.selectedCareTaker?.name
                labelAge.text = "Age " + self.calculateAge(dob: Constant.selectedCareTaker?.dob ?? "")
                
            } else {
                labelUSerName.text = Constant.selectedCareTaker?.name
                parameters = ["cognito_id": Constant.selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0] as [String : Any]
                labelAge.text = "Age " + self.calculateAge(dob: Constant.selectedCareTaker?.dob ?? "")
            }
        } else {
            parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": 0] as [String : Any]
            labelUSerName.text = getName(name: Constant.userProfile?.name ?? "" , middleName: Constant.userProfile?.middleName)
            labelAge.text = "Age " + self.calculateAge(dob: Constant.userProfile?.dob ?? "")
        }
        Utility.shared.startLoader()
        VitalStatsViewModel.getVitalStatsDetail(params: parameters) { (response, error) in
            Utility.shared.stopLoader()
            let isDarkMode = self.checkIfDarkMode()
            if response != nil {
                if isDarkMode {
                    self.setData(response: response, textColor: .white)
                } else {
                    self.setData(response: response, textColor: .black)
                }
            } else {
                if isDarkMode {
                    self.setData(response: nil, textColor: .white)
                } else {
                    self.setData(response: nil, textColor: .black)
                }
                
                if error?.lowercased() == "no data found" {
                    return
                }
                Utility.shared.showToast(message: error ?? "Something went wrong!")
            }
        }
    }
    
    func setData(response: VitalStats?, textColor: UIColor) {
        self.selectedVitalStats = response
        
        self.getbloodTypeCode(code: response?.blood_type_id ?? 0)
        if let bloodType = self.selectedBloodType?.name {
            self.labelBloodType.setString(strings: ["Blood Type : ", bloodType], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        } else {
            self.labelBloodType.setString(strings: ["Blood Type : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        
        if let temperature = response?.baselineTemperature {
            self.labelBaseTemperature.setString(strings: ["Baseline Temperature : ", temperature, " *F"], fontSize: [16,15,15], color: [textColor, Color.labelGreyColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular", "Poppins-Regular"], alignment: .left)
        } else {
            self.labelBaseTemperature.setString(strings: ["Baseline Temperature : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        if let heartRate = response?.restingHeartRate {
            self.labelRestingHeartRate.setString(strings: ["Resting Heart Rate : ", heartRate], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }else {
            self.labelRestingHeartRate.setString(strings: ["Resting Heart Rate : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        
        if let pulse = response?.pulse {
            self.labelPulse.setString(strings: ["Pulse : ", pulse], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        } else {
            self.labelPulse.setString(strings: ["Pulse : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        
        if let bloodPressure = response?.bloodPressure {
            self.labelBloodPressure.setString(strings: ["Blood Pressure : ", bloodPressure, " mmHg"], fontSize: [16,15,15], color: [textColor, Color.labelGreyColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular", "Poppins-Regular"], alignment: .left)
        } else {
            self.labelBloodPressure.setString(strings: ["Blood Pressure : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        
        if let respiratoryRate = response?.respiratoryRate {
            self.labelRespiratoryRate.setString(strings: ["Respiratory Rate : ", respiratoryRate, " Breath/Minute"], fontSize: [16,15,15], color: [textColor, Color.labelGreyColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular", "Poppins-Regular"], alignment: .left)
        } else {
            self.labelRespiratoryRate.setString(strings: ["Respiratory Rate : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        
        if let a1c = response?.a1c {
            self.labelA1C.setString(strings: ["A1C Rate : ", a1c, " %"], fontSize: [16,15,15], color: [textColor, Color.labelGreyColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular", "Poppins-Regular"], alignment: .left)
        } else {
            self.labelA1C.setString(strings: ["A1C : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        
        if let mornignSugar = response?.mornignSugar {
            self.labelMorningSugar.setString(strings: ["Morning Sugar : ", mornignSugar, " mg/dl"], fontSize: [16,15,15], color: [textColor, Color.labelGreyColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular", "Poppins-Regular"], alignment: .left)
        } else {
            self.labelMorningSugar.setString(strings: ["Morning Sugar : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        
        if let cholestrolScore = response?.cholestrolScore {
            self.labelCholestrolScore.setString(strings: ["Cholestrol Score : ", cholestrolScore, " mg/dl"], fontSize: [16,15,15], color: [textColor, Color.labelGreyColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular", "Poppins-Regular"], alignment: .left)
        } else {
            self.labelCholestrolScore.setString(strings: ["Cholestrol Score : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        
        if let ldl = response?.ldl {
            self.labelLDL.setString(strings: ["LDL : ", ldl, " mg/dl"], fontSize: [16,15,15], color: [textColor, Color.labelGreyColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular", "Poppins-Regular"], alignment: .left)
        } else {
            self.labelLDL.setString(strings: ["LDL : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        if let hdl = response?.hdl {
            self.labelHDL.setString(strings: ["HDL : ", hdl, " mg/dl"], fontSize: [16,15,15], color: [textColor, Color.labelGreyColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular", "Poppins-Regular"], alignment: .left)
        }else {
            self.labelHDL.setString(strings: ["HDL : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        if let triglycerides = response?.triglycerides {
            self.labelTriglycerides.setString(strings: ["Triglycerides : ", triglycerides, " mg/dl"], fontSize: [16,15,15], color: [textColor, Color.labelGreyColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular", "Poppins-Regular"], alignment: .left)
        } else {
            self.labelTriglycerides.setString(strings: ["Triglycerides : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        if let oxygenSaturation = response?.oxygenSaturation {
            self.labelOxygenSaturation.setString(strings: ["Oxygen Saturation : ", oxygenSaturation, " mg/dl"], fontSize: [16,15,15], color: [textColor, Color.labelGreyColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular", "Poppins-Regular"], alignment: .left)
        } else {
            self.labelOxygenSaturation.setString(strings: ["Oxygen Saturation : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        if let ph = response?.ph {
            self.labelPH.setString(strings: ["PH : ", ph, " pHs"], fontSize: [16,15,15], color: [textColor, Color.labelGreyColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular", "Poppins-Regular"], alignment: .left)
        } else {
            self.labelPH.setString(strings: ["PH : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
        
        if let co2Level = response?.co2Level {
            self.labelCo2Level.setString(strings: ["Co2 Level : ", co2Level, " PPM"], fontSize: [16,15,15], color: [textColor, Color.labelGreyColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular", "Poppins-Regular"], alignment: .left)
        } else {
            self.labelCo2Level.setString(strings: ["Co2 Level : ", "N.A."], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
        }
    }
}


extension ExtendedTextField {
    func setDarkBorderColor(color: UIColor = UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0)) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 0.8
        self.layer.cornerRadius = 4
        let leftView = UIView(frame: CGRect(x: 8.0, y: 0.0, width: 8.0, height: 2.0))
        self.leftView = leftView
        self.leftViewMode = .always
    }
}
