//
//  SyncVitalsViewController.swift
//  E-Health
//
//  Created by Prabjot Kaur on 8/11/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import DropDown
import HealthKit
import SafariServices

protocol VitalsFromHealtkit {
    func displayVitals(HeartRate: String?, userVitals: VitalStats?)
}

class SyncVitalsViewController: UIViewController {
    
    var userVitalStats: VitalStats?
    var userHeartRate: String?
    
    let health: HKHealthStore = HKHealthStore()
    
    var heartRateQuery:HKSampleQuery?
    
    //    let respiratoryUnit = HKUnit(from: "pound/liter")
    let heartRateUnit:HKUnit = HKUnit(from: "count/min")
    let glucoseUnit = HKUnit(from: "mg/dL")
    let heightUnit = HKUnit.foot()
    let bodyTemperatureUnit = HKUnit.degreeFahrenheit()
    let oxygenSaturationUnit = HKUnit.percent()
    
    var syncDispatchGroup : DispatchGroup?
    var delegate: VitalsFromHealtkit? = nil
    var isLoadingFirstTime = true
    
    var isHeartRate = false
    var isBloodPessure = false
    var isRespiratoryRate = false
    var isBodyTemperature = false
    var isOxySaturation = false
    var isFitbit = false
    
    var authenticationController: FitbitAuthenticationViewController?
    //MARK: - Outlets
    @IBOutlet weak var fromDeviceButton: UIButton!
    @IBOutlet weak var providerButtonView: UIView!
    @IBOutlet weak var fromDeviceButtonview: UIView!
    @IBOutlet weak var providerButton: UIButton!
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fromDeviceButton.customizeVitalsButton(titleTopInset: 80)
        providerButton.customizeVitalsButton(titleTopInset: 80)
        fromDeviceButtonview.dropShadow(cornerRadius: 5, shadowOpacity: 0.25)
        providerButtonView.dropShadow(cornerRadius: 5, shadowOpacity: 0.25)
        authenticationController = FitbitAuthenticationViewController(delegate: self)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func fromDeviceButtonAction(_ sender: UIButton) {
        initdropDown(sender: sender, dataSource: ["Apple Watch", "Fitbit"])
    }
    
    @IBAction func providerButtonAction(_ sender: Any) {
        self.view.makeToast("Coming Soon")
    }
    
    func fetchedDataFromHealthkit() {
        if isFitbit {
            self.delegate?.displayVitals(HeartRate: self.userHeartRate, userVitals: self.userVitalStats)
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            if isHeartRate && isBloodPessure && isRespiratoryRate && isBodyTemperature && isOxySaturation {
                self.delegate?.displayVitals(HeartRate: self.userHeartRate, userVitals: self.userVitalStats)
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

extension SyncVitalsViewController {
    func autorizeHealthKit() {
        //        let vo2MaxObjectType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.vo2Max)!
        
        var healthKitTypes: Set = [
            HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!, HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureSystolic)!, HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureDiastolic)!,
            HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.respiratoryRate)!,
            HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodGlucose)!,
            HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyTemperature)!,
            HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.oxygenSaturation)!]
        //        HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMassIndex)!
        //        HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!,
        if #available(iOS 11.0, *) {
            healthKitTypes.insert(HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.restingHeartRate)!)
        }
        health.requestAuthorization(toShare: nil, read: healthKitTypes) { (result, error) in
            if result {
                self.getTodaysHeartRates()
                self.getBloodPressure()
                self.getBodyTemperature()
                self.fetchRespiratoryRate()
                self.getOxygenSaturation()
            } else {
                DispatchQueue.main.async {
                    self.view.makeToast(error?.localizedDescription ?? "Something went wrong!")
                }
            }
        }
    }
    
    func getTodaysHeartRates() {
        var heartRateType:HKQuantityType?
        if #available(iOS 11.0, *) {
            heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.restingHeartRate)!
        } else {
            self.isHeartRate = true
            self.fetchedDataFromHealthkit()
            return
        }
        
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: Date())
        guard let startDate = calendar.date(from: components) else { return }
        //startDate = calendar.startOfDay(for: startDate)
        let endDate = calendar.date(byAdding: .day, value: 1, to: startDate)
        let predicate = HKQuery.predicateForSamples(withStart: startDate as Date, end: endDate as Date?, options: [])
        let sortDescriptors = [
            NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: true)
        ]
        
        //        self.syncDispatchGroup?.enter()
        heartRateQuery = HKSampleQuery(sampleType: heartRateType!, predicate: nil, limit: 25, sortDescriptors: sortDescriptors)
        { (query:HKSampleQuery, results:[HKSample]?, error:Error?) -> Void in
            guard error == nil else {
                //                self.syncDispatchGroup?.leave()
                self.isHeartRate = true
                self.fetchedDataFromHealthkit()
                print("error");
                return
            }
            self.printHeartRateInfo(results: results)
        }
        health.execute(heartRateQuery!)
    }
    
    /*used only for testing, prints heart rate info */
    private func printHeartRateInfo(results:[HKSample]?)
    {
        if let result = results?.last {
            guard let currData:HKQuantitySample = result as? HKQuantitySample else {
                self.isHeartRate = true
                self.fetchedDataFromHealthkit()
                return
            }
            let doubleHeartRate = currData.quantity.doubleValue(for: heartRateUnit)
            self.userHeartRate = String(describing: Int(doubleHeartRate))
        }
        self.isHeartRate = true
        self.fetchedDataFromHealthkit()
    }
    
    func getBloodPressure() {
        guard let type = HKQuantityType.correlationType(forIdentifier: HKCorrelationTypeIdentifier.bloodPressure),
            let systolicType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureSystolic),
            let diastolicType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodPressureDiastolic)
            else { return }
        
        //        let predicate = HKQuery.predicateForSamples(withStart: startDate as Date, end: endDate as Date?, options: [])
        
        //        self.syncDispatchGroup?.enter()
        let sampleQuery = HKSampleQuery(sampleType: type, predicate: nil, limit: 0, sortDescriptors: nil) { (sampleQuery, results, error) in
            if let dataList = results as? [HKCorrelation] {
                
                if let data = dataList.last
                {
                    if self.userVitalStats == nil {
                        self.userVitalStats = VitalStats()
                    }
                    
                    //Blood Pressure
                    if let data1 = data.objects(for: systolicType).first as? HKQuantitySample,
                        let data2 = data.objects(for: diastolicType).first as? HKQuantitySample {
                        
                        let value1 = data1.quantity.doubleValue(for: HKUnit.millimeterOfMercury())
                        let value2 = data2.quantity.doubleValue(for: HKUnit.millimeterOfMercury())
                        
                        let bloodPressure = "\(Int(value1))" // / \(value2)"
                        self.userVitalStats?.bloodPressure = bloodPressure
                        print("\(value1) / \(value2)")
                    }
                }
            }
            self.isBloodPessure = true
            self.fetchedDataFromHealthkit()
            //            self.syncDispatchGroup?.leave()
        }
        health.execute(sampleQuery)
    }
    
    func fetchRespiratoryRate() {
        guard let respiratoryRate = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.respiratoryRate) else { return }
        
        let sampleQuery = HKSampleQuery(sampleType: respiratoryRate, predicate: nil, limit: 10, sortDescriptors: nil) { (sampleQuery, results, error) in
            if let dataList = results as? [HKQuantitySample] {
                if let respiratoryRateValue = dataList.last {
                    if self.userVitalStats == nil {
                        self.userVitalStats = VitalStats()
                    }
                    let respiratory = respiratoryRateValue.quantity.doubleValue(for: self.heartRateUnit)
                    self.userVitalStats?.respiratoryRate = String(Int(respiratory))
                    print(respiratory)
                }
            }
            self.isRespiratoryRate = true
            self.fetchedDataFromHealthkit()
        }
        health.execute(sampleQuery)
    }
    
    func getBodyTemperature() {
        guard let bodyTemperatureType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyTemperature) else {return}
        let sampleQuery = HKSampleQuery(sampleType: bodyTemperatureType, predicate: nil, limit: 10, sortDescriptors: nil) { (sampleQuery, results, error) in
            if let dataList = results as? [HKQuantitySample] {
                if let bodyTemp = dataList.last
                {
                    if self.userVitalStats == nil {
                        self.userVitalStats = VitalStats()
                    }
                    let bodyTemperature = bodyTemp.quantity.doubleValue(for: self.bodyTemperatureUnit)
                    let floatTemp = CGFloat(bodyTemperature)
                    self.userVitalStats?.baselineTemperature = String(format: "%.2f", floatTemp)
                    print(bodyTemperature)
                }
            }
            self.isBodyTemperature = true
            self.fetchedDataFromHealthkit()
        }
        health.execute(sampleQuery)
    }
    
    func getOxygenSaturation() {
        guard let oxygenSaturationType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.oxygenSaturation) else {return}
        let sampleQuery = HKSampleQuery(sampleType: oxygenSaturationType, predicate: nil, limit: 10, sortDescriptors: nil) { (sampleQuery, results, error) in
            if let dataList = results as? [HKQuantitySample] {
                if let oxySat = dataList.last
                {
                    if self.userVitalStats == nil {
                        self.userVitalStats = VitalStats()
                    }
                    let o2Saturation = oxySat.quantity.doubleValue(for: self.oxygenSaturationUnit)
                    let intSaturation = Int(o2Saturation*100)
                    self.userVitalStats?.oxygenSaturation = String(intSaturation)
                    print(intSaturation)
                }
            }
            self.isOxySaturation = true
            self.fetchedDataFromHealthkit()
        }
        health.execute(sampleQuery)
    }
    
    func getBloodGlucose() {
        guard let bloodGlucoseType = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bloodGlucose) else {return}
        let sampleQuery = HKSampleQuery(sampleType: bloodGlucoseType, predicate: nil, limit: 10, sortDescriptors: nil) { (sampleQuery, results, error) in
            if let dataList = results as? [HKQuantitySample] {
                if let bloodGlucose = dataList.last
                {
                    
                    let bGlucose = bloodGlucose.quantity.doubleValue(for: self.glucoseUnit)
                    //                    self.userVitalStats?.g = String(bGlucose)
                    print(bGlucose)
                }
            }
        }
        health.execute(sampleQuery)
    }
    
    func initdropDown (sender: UIButton, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height + 30)
        dropDown.anchorView = sender
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                if index == 0 {
                    self.isFitbit = false
                    self.autorizeHealthKit()
                } else {
                    self.isFitbit = true
                    self.authorizeFitbit()
                }
                dropDown.hide()
            }
            dropDown.show()
        }
    }
    
    func authorizeFitbit() {
        authenticationController?.login(fromParentViewController: self)
    }
}

extension SyncVitalsViewController: AuthenticationProtocol {
    
    func authorizationDidFinish(_ success: Bool) {
        guard let authToken = authenticationController?.authenticationToken else {
            return
        }
        FitbitApi.sharedInstance.authorize(with: authToken)
        let startDate = Date().dateString()
        let datepath = "/\("2020-10-06")/1d.json"
        getRestingHeartRate(for: datepath)
        //        getHeartrate(for: startDate)
    }
    
    func getHeartrate(for datePath: String) {
        //        https://api.fitbit.com/1/user/-/activities/heart/date/2020-10-03/1d.json
        let url = "https://api.fitbit.com/1/user/-/activities/heart/date/" + datePath + "/1d.json"
        VitalStatsViewModel.getRestingHeartRateFromFitibit(url: url) { (response, message) in
            if response != nil {
                
            } else {
                
            }
        }
    }
    
    func getRestingHeartRate(for datePath: String) {
        guard let session = FitbitApi.sharedInstance.session,
          //    let stepURL = URL(string: "https://api.fitbit.com/1/user/-/activities/heart/date/2020-10-06/1d.json")
           let stepURL = URL(string: "https://api.fitbit.com/1/user/-/activities/heart/date\(datePath)")
            else {
                return
        }
        
//        Utility.shared.startLoader()
        let dataTask = session.dataTask(with: stepURL) { (data, response, error) in
            guard let response = response as? HTTPURLResponse, response.statusCode < 300 else {
                DispatchQueue.main.async {
                    Utility.shared.showToast(message: "No Data available from device!")
//                    Utility.shared.stopLoader()
                }
                return
            }
            
            guard let data = data,
                let dictionary = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: AnyObject] else {
                    DispatchQueue.main.async {
                        Utility.shared.showToast(message: "No Data available from device!")
//                        Utility.shared.stopLoader()
                    }
                    return
            }
            print(dictionary)
            guard let heartRate = dictionary["activities-heart"] as? [[String: AnyObject]] else { return }
            
            if let heartRateDict = heartRate.first!["value"] as? [String: AnyObject] {
//                Utility.shared.stopLoader()
                print(heartRateDict["restingHeartRate"] as? Int)
                if let heartRate = heartRateDict["restingHeartRate"] as? Int {
                    DispatchQueue.main.async {
                        self.userHeartRate = String(describing: heartRate)
                        if self.userVitalStats == nil {
                            self.userVitalStats = VitalStats()
                        }
                        self.userVitalStats?.restingHeartRate = self.userHeartRate
                        self.fetchedDataFromHealthkit()
                    }
                }
            } else {
                Utility.shared.showToast(message: "No Data available from device!")
            }
        }
        dataTask.resume()
    }
}
