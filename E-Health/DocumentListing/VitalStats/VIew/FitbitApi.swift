//
//  FitbitApi.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 10/5/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class FitbitApi {
    
    static let sharedInstance: FitbitApi = FitbitApi()
    
    static let baseAPIURL = URL(string:"https://api.fitbit.com/1")
    
    func authorize(with token: String) {
        let sessionConfiguration = URLSessionConfiguration.default
        var headers = sessionConfiguration.httpAdditionalHeaders ?? [:]
        headers["Authorization"] = "Bearer \(token)"
        sessionConfiguration.httpAdditionalHeaders = headers
        session = URLSession(configuration: sessionConfiguration)
    }
    
    var session: URLSession?
}
