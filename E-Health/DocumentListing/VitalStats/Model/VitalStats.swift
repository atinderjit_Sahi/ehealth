//
//  VItalModel.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/29/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class VitalStats {
    
    var id: Int?
    var cognitoUserId: Int?
    var minorId: String?
    var blood_type_id: Int?
    var baselineTemperature: String?
    var restingHeartRate: String?
    var pulse: String?
    var bloodPressure: String?
    var respiratoryRate: String?
    var a1c: String?
    var mornignSugar: String?
    var cholestrolScore: String?
    var ldl: String?
    var hdl: String?
    var triglycerides: String?
    var oxygenSaturation: String?
    var ph: String?
    var co2Level: String?
    
    init() {
        
    }
        
    init(value: [String: Any]) {
        if let vital_details = value["vital_detail"] as? [String: Any] {
            let vitalInfo = vital_details
            self.id = vitalInfo["id"] as? Int
            self.cognitoUserId = vitalInfo["cognito_id"] as? Int
            self.minorId = vitalInfo["minor_id"] as? String
            self.blood_type_id = vitalInfo["blood_type_id"] as? Int
            self.baselineTemperature = vitalInfo["baseline_temperature"] as? String
            self.restingHeartRate = vitalInfo["resting_heart_rate"] as? String
            self.pulse = vitalInfo["pulse"] as? String
            self.bloodPressure = vitalInfo["blood_pressure"] as? String
            self.respiratoryRate = vitalInfo["respiratory_rate"] as? String
            self.a1c = vitalInfo["A1C"] as? String
            self.mornignSugar = vitalInfo["morning_sugar"] as? String
            self.cholestrolScore = vitalInfo["cholestrol"] as? String
            self.ldl = vitalInfo["LDL"] as? String
            self.hdl = vitalInfo["HDL"] as? String
            self.triglycerides = vitalInfo["triglycerides"] as? String
            self.oxygenSaturation = vitalInfo["oxygen_saturation"] as? String
            self.ph = vitalInfo["PH"] as? String
            self.co2Level = vitalInfo["CO2_level"] as? String
        }
        
    }
}
