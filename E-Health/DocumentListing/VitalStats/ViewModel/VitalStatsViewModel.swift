//
//  VitalStatsViewModel.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/29/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class VitalStatsViewModel: NSObject {
    
    static func addVitalStatsRecord(isUploadDoc: Bool, params: [String: Any], completion: @escaping (_ response: String?, _ error: String?) -> Void) {
        
        let url = Constant.URL.addUpdateVitalRecord
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
//                            let insuranceRecord = VitalStats(value: data)
                            completion(result["message"] as? String, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
        
    }
    
    static func getVitalStatsDetail(params: [String: Any], completion: @escaping (_ response: VitalStats?, _ error: String?) -> Void) {
        
        let url = Constant.URL.getVitalDetailById
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [String: AnyObject] {
                            let insuranceRecord = VitalStats(value: data)
                            completion(insuranceRecord, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func getRestingHeartRateFromFitibit(url: String, completion: @escaping (_ response: VitalStats?, _ error: String?) -> Void) {
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [String: AnyObject] {
                            let insuranceRecord = VitalStats(value: data)
                            completion(insuranceRecord, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
        
        
    
    }
}
