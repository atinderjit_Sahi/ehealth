//
//  DocumentListingModel.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/1/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class DocumentListingModel {

    var id: Int?
    var userId: String?
    var minorId: Int?
    var name: String?
    var fileType: Int?
    var documentTypeId: Int?
    var documentTypeName: String?
    var filePath: String?
    var updated_at: String?
    var createdAt: String?
    var typeId: Int?
    var allergyType: String?
    var description: String?
    var severity : String?
    var epipenUsed : String?
   var medical_original_date: String?
    var medical_categorization : String?
   var vaccine_for : String?
    var vaccine_purpose : String?
    var vaccine_date : String?
    var additional_notes : String?
    var selected : String?
    
    init() {}
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.userId = value["cognito_id"] as? String
        self.minorId = value["minor_id"] as? Int
        self.name = value["name"] as? String
        self.fileType = value["file_type"] as? Int
        self.documentTypeId = value["document_type_id"] as? Int
        self.documentTypeName = value["document_type_name"] as? String
        self.filePath = value["file_path"] as? String
        self.updated_at = value["updated_at"] as? String
        self.createdAt = value["created_at"] as? String
        self.typeId = value["type_id"] as? Int
        self.allergyType = value["allergy_type_name"] as? String
        self.description = value["description"] as? String
        
        self.severity = value["severity"] as? String
        self.epipenUsed = value["use_epipen"] as? String
        
        self.medical_original_date = value["medical_original_date"] as? String
        self.medical_categorization = value["medical_categorization"] as? String
        
        self.vaccine_for = value["vaccine_for"] as? String
        self.vaccine_purpose = value["vaccine_purpose"] as? String
        self.vaccine_date = value["vaccine_date"] as? String
        self.additional_notes = value["additional_notes"] as? String
        self.selected = "no"
    }
    
}
