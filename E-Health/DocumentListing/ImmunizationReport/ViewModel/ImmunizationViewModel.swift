//
//  ImmunizationViewModel.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 8/6/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class ImmunizationViewModel: NSObject {
    
    static func addImunizationRecord(params: [String: Any], completion: @escaping (_ response: String?, _ error: String?) -> Void) {
        
        let url = Constant.URL.uploadDocument
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            completion(result["message"] as? String, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func getImunizationRecordDetail(recordId: Int, completion: @escaping (_ response: InsuranceRecord?, _ error: String?) -> Void) {
        
        let url = Constant.URL.getImmunozationRecordDetail + String(describing: recordId)
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [String: AnyObject] {
                            let insuranceRecord = InsuranceRecord(value: data)
                            completion(insuranceRecord, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func deleteImunizationRecord(parameters: [String: Any], completion: @escaping (_ response: Bool?, _ error: String?) -> Void) {
        
        let url = Constant.URL.deleteImmunizationRecord
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        completion(true , result["message"] as? String)
                    } else {
                        completion(false , result["message"] as? String)
                    }
                } else {
                    completion(false , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    
    static func getImunizationList(recordId: Int, completion: @escaping (_ response: InsuranceRecord?, _ error: String?) -> Void) {
        
        let url = Constant.URL.getInsuranceRecord + String(describing: recordId)
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [String: AnyObject] {
                            let insuranceRecord = InsuranceRecord(value: data)
                            completion(insuranceRecord, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
}
