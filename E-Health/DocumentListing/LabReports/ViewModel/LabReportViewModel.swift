//
//  LabReportViewModel.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 8/12/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class LabReportViewModel: NSObject {

    static func getLabReportDetail(reportId: Int, completion: @escaping (_ response: DocumentListingModel?, _ error: String?) -> Void) {
        
        let url = Constant.URL.getLabReportDetailById + String(describing: reportId)
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [String: AnyObject] {
                            let insuranceRecord = DocumentListingModel(value: data)
                            completion(insuranceRecord, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func deleteLabReport(parameters: [String: Any], completion: @escaping (_ response: Bool?, _ error: String?) -> Void) {
        
        let url = Constant.URL.deleteLabReport
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        completion(true , result["message"] as? String)
                    } else {
                        completion(false , result["message"] as? String)
                    }
                } else {
                    completion(false , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
}
