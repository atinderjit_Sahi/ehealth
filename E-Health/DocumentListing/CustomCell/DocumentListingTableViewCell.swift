//
//  DocumentListingTableViewCell.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 6/30/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

protocol DocumentListingProtocol {
    func viewReport(sender: UIButton)
}

class DocumentListingTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var reportTitleLabel: UILabel!
    @IBOutlet weak var scanUploadLabel: UILabel!
    @IBOutlet weak var dateAddedLabel: UILabel!
    @IBOutlet weak var viewReportButton: UIButton!
    
    //MARK - Properties
    var documentType = String()
    var delegate: DocumentListingProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /*for family: String in UIFont.familyNames {
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family) {
                print("== \(names)")
            }
        }*/
        
        var customButtonTitle = NSMutableAttributedString()
        
        if documentType == "1" {
            customButtonTitle = NSMutableAttributedString(string: viewReportButton.titleLabel?.text ?? "View Details", attributes: [
                NSAttributedString.Key.font : UIFont(name: "Montserrat-SemiBold", size: 15.0) ?? UIFont.boldSystemFont(ofSize: 15.0),
                NSAttributedString.Key.foregroundColor : UIColor(red: 31/255, green: 181/255, blue: 245/255, alpha: 1.0),
                NSAttributedString.Key.underlineStyle : 1]
            )
        } else {
            customButtonTitle = NSMutableAttributedString(string: viewReportButton.titleLabel?.text ?? "View Report", attributes: [
                NSAttributedString.Key.font : UIFont(name: "Montserrat-SemiBold", size: 15.0) ?? UIFont.boldSystemFont(ofSize: 15.0),
                NSAttributedString.Key.foregroundColor : UIColor(red: 31/255, green: 181/255, blue: 245/255, alpha: 1.0),
                NSAttributedString.Key.underlineStyle : 1]
            )
        }
        viewReportButton.setAttributedTitle(customButtonTitle, for: .normal)
        containerView.dropShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func viewReportButtonTapped(_ sender: UIButton) {
        delegate?.viewReport(sender: sender)
    }
    
}
