//
//  InsuranceDocumentCollectionViewCell.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/22/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class InsuranceDocumentCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var labelInsuranceDocument: UILabel!
    @IBOutlet weak var imageViewInsuranceDoc: UIImageView!
}
