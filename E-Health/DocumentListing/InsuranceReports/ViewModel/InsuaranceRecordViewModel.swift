//
//  InsuaranceRecordViewModel.swift
//  
//
//  Created by Jaspreet Kaur on 7/24/20.
//

import UIKit

class InsuaranceRecordViewModel: NSObject {

//    static func addInsuranceRecord(params: [String: Any]?, insuranceFrontImage: UIImage?, insuranceBackImage: UIImage?, completion: @escaping (_ response: String?, _ error: String?) -> Void) {
//
//        let url = Constant.URL.addInsuranceRecord
//        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
//
//        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params as [String : AnyObject]?, method: .post, headers: headers) { (response, error) in
//            print(response)
//            print(error)
//        }
//    }
    
    static func addInsuranceRecord(isUploadDoc: Bool, params: [String: Any], insuranceFrontImage: UIImage?, insuranceBackImage: UIImage?, pdfArray: [Any]?, completion: @escaping (_ response: String?, _ error: String?) -> Void) {

        let url = Constant.URL.addInsuranceRecord
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]

        ApiService.shared.triggerMultipartRequest(isUploadDoc: isUploadDoc, url, parameters: params as [String : AnyObject], headers: headers, insuranceFrontImage: insuranceFrontImage, insuranceBackImage: insuranceBackImage, pdfUrls: pdfArray as? [Data], compBlock: { (response, success) in
            guard let responseDict = response as? Dictionary<String, Any>  else {
                completion(nil, "Something went wrong!")
                return
            }
            if let code = responseDict["code"] as? NSNumber {
                print(responseDict)
                if code.stringValue == "200"{
                    completion(responseDict["message"] as? String, nil)
                } else {
                    completion(nil, responseDict["message"] as? String)
                }
            }

        }) { (message, error) in
            completion(message as String, message as String)
            print(error)
        }
    }
    
    static func getInsuranceRecordListing(parameters: [String: Any], completion: @escaping (_ response: [InsuranceRecord]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.getInsuranceListing
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            var documentListingArray = [InsuranceRecord]()
                            for item in data {
                                documentListingArray.append(InsuranceRecord(value: item))
                            }
                            completion(documentListingArray, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func getInsuranceRecordDetail(recordId: Int, completion: @escaping (_ response: InsuranceRecord?, _ error: String?) -> Void) {
        
        let url = Constant.URL.getInsuranceRecord + String(describing: recordId)
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        print(result)
                        if let data = result["data"] as? [String: AnyObject] {
                            let insuranceRecord = InsuranceRecord(value: data)
//                            var documentListingArray = [InsuranceRecord]()
//                            for item in data {
//                                documentListingArray.append(InsuranceRecord(value: item))
//                            }
                            completion(insuranceRecord, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func deleteInsuranceRecord(parameters: [String: Any], completion: @escaping (_ response: Bool?, _ error: String?) -> Void) {
        
        let url = Constant.URL.deleteInsuranceRecord
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        completion(true , result["message"] as? String)
                    } else {
                        completion(false , result["message"] as? String)
                    }
                } else {
                    completion(false , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
}
