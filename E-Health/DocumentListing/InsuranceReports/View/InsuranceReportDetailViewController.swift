//
//  InsuranceReportDetailViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/22/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

class InsuranceReportDetailViewController: UIViewController {

    // MARK: - Properties
    var insuranceImagesArray = [[String: String]]()
    var selectedCareTaker: CaretakerModel?
    var selectedInsuranceRecord: InsuranceRecord?
    
    // MARK: - Outlets
    @IBOutlet weak var labelInsuranceDocument: UILabel!
    @IBOutlet weak var labelPayeeName: UILabel!
    @IBOutlet weak var labelMemberServices: UILabel!
    @IBOutlet weak var labelInsuranceType: UILabel!
    @IBOutlet weak var labelMemberId: UILabel!
    @IBOutlet weak var labelGroupId: UILabel!
    @IBOutlet weak var labelContactType: UILabel!
    @IBOutlet weak var labelPlanCodes: UILabel!
    @IBOutlet weak var labelDocumentName: UILabel!
    @IBOutlet weak var labelPrimaryMemberName: UILabel!
    @IBOutlet weak var labelPrimaryMemberDOB: UILabel!
    @IBOutlet weak var labelRelationship: UILabel!
    @IBOutlet weak var insuranceImageCollectionView: UICollectionView!
    @IBOutlet weak var txtViewNotes: UITextView!
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for family: String in UIFont.familyNames {
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family) {
                print("== \(names)")
            }
        }
        let isDarkMode = self.checkIfDarkMode()
        
        if isDarkMode {
            getInsuranceRecordDetailApi(recordId: selectedInsuranceRecord?.id ?? 0, textColor: .white)
        } else {
            getInsuranceRecordDetailApi(recordId: selectedInsuranceRecord?.id ?? 0, textColor: .black)
        }
    }
    
    // MARK: - UIButton Actions
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension InsuranceReportDetailViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width - 50, height: collectionView.frame.size.height)
    }
    
}

extension InsuranceReportDetailViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return insuranceImagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: InsuranceDocumentCollectionViewCell.className, for: indexPath) as? InsuranceDocumentCollectionViewCell else {
            fatalError()
        }
        
        let dict = insuranceImagesArray[indexPath.row]
        let imageString = dict["imageString"] ?? ""
        let imageUrl = URL(string: imageString)
        cell.labelInsuranceDocument.text = dict["title"]
        cell.imageViewInsuranceDoc.kf.indicatorType = .activity
        cell.imageViewInsuranceDoc.kf.setImage(with: imageUrl)
        return cell
    }
    
}

extension InsuranceReportDetailViewController {
    
    func getInsuranceRecordDetailApi(recordId: Int, textColor: UIColor) {
        
        Utility.shared.startLoader()
        InsuaranceRecordViewModel.getInsuranceRecordDetail(recordId: recordId) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                if response != nil {
                    self.labelInsuranceDocument.setString(strings: ["Insurance Document : ", (response?.insuranceDocument ?? "")], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
                    self.labelPayeeName.setString(strings: ["Payee Name : ", (response?.payerName ?? "")], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
                    self.labelMemberServices.setString(strings: ["Member Services : ", (response?.memberServices ?? "")], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
                    self.labelInsuranceType.setString(strings: ["Insurance Type : ", (response?.insuranceTypes ?? "")], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
                    self.labelMemberId.setString(strings: ["Member ID : ", (response?.memberId ?? "")], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
                    self.labelGroupId.setString(strings: ["Group ID : ", (response?.groupId ?? "")], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
//                    self.labelContactType.setString(strings: ["Contact Type : ", (response?.contactType ?? "")], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
//                    self.labelPlanCodes.setString(strings: ["Plan Codes : ", (response?.planCodes ?? "")], fontSize: [16,15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
                    self.labelDocumentName.setString(strings: ["Document Name : ", (response?.documentName ?? "")], fontSize: [16, 15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
                    
                    self.labelPrimaryMemberName.setString(strings: ["Primary Member Name : ", (response?.primaryMemberName ?? "")], fontSize: [16, 15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
                    self.labelPrimaryMemberDOB.setString(strings: ["Primary Member DOB : ", (response?.primaryMemberDOB ?? "")], fontSize: [16, 15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
                    self.labelRelationship.setString(strings: ["Relationship : ", (response?.relationship ?? "")], fontSize: [16, 15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left)
                    if response?.additional_notes != nil && response?.additional_notes != "" {
                        self.txtViewNotes.setString(strings: ["Additional Notes : ", (response?.additional_notes ?? "")], fontSize: [16, 15], color: [textColor, Color.labelGreyColor], fonts: ["Montserrat-SemiBold", "Poppins-Regular"], alignment: .left) }
                    else {
                        self.txtViewNotes.isHidden = true
                    }
                    
                    let imagesDict = [["title": "View Front", "imageString": response?.viewFront ?? ""], ["title": "View Back", "imageString": response?.viewBack ?? ""]]
                    self.insuranceImagesArray = imagesDict
                    self.insuranceImageCollectionView.reloadData()
                } else {
                    Utility.shared.showToast(message: error ?? "Something went wrong!")
                }                
            }
        }
    }
}
