//
//  AddInsuranceRecordViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/22/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import DropDown
import AWSMobileClient

class AddInsuranceRecordViewController: UIViewController {

    // MARK: - Properties
    var fileType: String?
    var documentType: String?
    var selectedCareTaker: CaretakerModel?
    var insuranceDocuments = ["Insurance card", "HSA card", "Prescription Card"]
    var insuranceTypes = ["Health maintenance organizations (HMOs)", "Preferred provider organizations (PPOs)", "Exclusive provider organizations (EPOs)", "Point-of-service (POS) plans", "High-deductible health plans (HDHPs)"]
    var insuranceDocId: String?
    var insuranceTypeId: String?
    
    var imagePicker: ImagePicker!
    var isFrontImageSelected: Int?
    var selectedRelationship = Relation()
    
    var relations = [Relation]()
    var datePicker = UIDatePicker()
    
    // MARK: - Outlets
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var textFieldInsuranceDocument: ExtendedTextField!
    @IBOutlet weak var textFieldPayerName: ExtendedTextField!
    @IBOutlet weak var textFieldMemberServices: ExtendedTextField!
    @IBOutlet weak var textFieldInsuranceTypes: UITextField!
    @IBOutlet weak var textFieldMemberId: ExtendedTextField!
    @IBOutlet weak var textFieldGroupId: UITextField!
    @IBOutlet weak var textFieldContactType: ExtendedTextField!
    @IBOutlet weak var textFieldPlanCodes: ExtendedTextField!
    @IBOutlet weak var textFieldDocumentName: ExtendedTextField!
    @IBOutlet weak var textFieldPrimaryMemberName: CustomTextField!
    @IBOutlet weak var textFieldPrimaryMemberDOB: CustomTextField!
    @IBOutlet weak var textFieldRelationship: CustomTextField!
    @IBOutlet weak var imageViewFrontDoc: UIImageView!
    @IBOutlet weak var imageViewBackDoc: UIImageView!
    
    
    @IBOutlet weak var txtViewNotes: UITextView!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldPrimaryMemberDOB.inputView = datePicker
//        labelUsername.text = Constant.userProfile?.name
        labelUsername.text = getName(name: Constant.userProfile?.name ?? "" , middleName: Constant.userProfile?.middleName)
        imagePicker = ImagePicker(presentationController: self, delegate: self)
        tapestureOnImageView()
        textFieldSetUp()
        relations = Constant.masterData?.relationship ?? [Relation]()
    }
    
    // MARK: - UIButton Actions
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func tapestureOnImageView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(_:)))

        imageViewFrontDoc.tag = 1
        imageViewFrontDoc.isUserInteractionEnabled = true
        imageViewFrontDoc.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(_:)))

        imageViewBackDoc.isUserInteractionEnabled = true
        imageViewBackDoc.tag = 2
        imageViewBackDoc.addGestureRecognizer(tapGestureRecognizer1)
    }
    
    @objc func imageViewTapped(_ sender: UITapGestureRecognizer){
        guard let imageView = sender.view as? UIImageView else {
            return
        }
        print("You tap image number: \(imageView.tag)")
        isFrontImageSelected = imageView.tag
        imagePicker.present(from: view)
    }
    
    func getName(name: String, middleName: String?) -> String {
        var senderFullName = String()
        let name = name
        let middleName = middleName
        if middleName != "" && middleName != nil {
            let myStringArr = name.components(separatedBy: " ")
            let firstName = (myStringArr[0])
            var lastName = ""
            if myStringArr.count == 2 {
                lastName = (myStringArr[1])
            }
            senderFullName = firstName + " " + (middleName ?? "") + " " + lastName
        } else {
            senderFullName = name
        }
        return senderFullName
    }
    
    func textFieldSetUp() {
        textFieldInsuranceDocument.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        textFieldPayerName.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        textFieldMemberServices.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        textFieldInsuranceTypes.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        textFieldMemberId.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        textFieldGroupId.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
//        textFieldContactType.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
//        textFieldPlanCodes.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        textFieldDocumentName.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        textFieldPrimaryMemberName.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        textFieldPrimaryMemberDOB.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        textFieldRelationship.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        
        textFieldInsuranceDocument.attributedPlaceholder = NSAttributedString(string: "Insurance Document*", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldPayerName.attributedPlaceholder = NSAttributedString(string: "Insurance Carrier Name*", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldMemberServices.attributedPlaceholder = NSAttributedString(string: "Member Services(1800#)", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldInsuranceTypes.attributedPlaceholder = NSAttributedString(string: "Insurance Type*", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldMemberId.attributedPlaceholder = NSAttributedString(string: "Member Id*", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldGroupId.attributedPlaceholder = NSAttributedString(string: "Group ID*", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        /*textFieldContactType.attributedPlaceholder = NSAttributedString(string: "Contact Type*", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldPlanCodes.attributedPlaceholder = NSAttributedString(string: "Plan Codes*", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])*/
        textFieldDocumentName.attributedPlaceholder = NSAttributedString(string: "Document Name*", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldPrimaryMemberName.attributedPlaceholder = NSAttributedString(string: "Primary Member Name", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldPrimaryMemberDOB.attributedPlaceholder = NSAttributedString(string: "Primary Member DOB", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        textFieldRelationship.attributedPlaceholder = NSAttributedString(string: "Relationship", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
        
//        textFieldRelationship.setRightImage(rightImage: UIImage(named: "dropDown")!)
//        textFieldInsuranceDocument.setRightImage(rightImage: UIImage(named: "dropDown")!)
//        textFieldInsuranceTypes.setRightImage(rightImage: UIImage(named: "dropDown")!)
        
        txtViewNotes.text = " Notes (Optional)" //Placeholder
        txtViewNotes.textColor = Color.textFieldPlaceholderColor
        txtViewNotes.autocorrectionType = UITextAutocorrectionType.no
        txtViewNotes.addBorder(borderColor : Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        
        
    }
    
    @IBAction func saveRecordsButtonAction(_ sender: Any) {
        
        var parameters = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": String(describing: selectedCareTaker?.minor_id ?? 0)] as [String : Any]
            } else {
                parameters = ["cognito_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": "0"] as [String : Any]
            }
        } else {
            parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": "0"] as [String : Any]
        }
        
        parameters["document_name"] = textFieldDocumentName.text ?? ""
        parameters["insurance_document"] = textFieldInsuranceDocument.text ?? ""
        parameters["payer_name"] = textFieldPayerName.text ?? ""
        parameters["member_services"] = textFieldMemberServices.text ?? ""
        parameters["insurance_types"] = textFieldInsuranceTypes.text ?? ""
        parameters["member_id"] = textFieldMemberId.text ?? ""
        parameters["group_id"] = textFieldGroupId.text ?? ""
//        parameters["contract_type"] = textFieldContactType.text ?? ""
//        parameters["plan_codes"] = textFieldPlanCodes.text ?? ""
        parameters["upload_type"] = "upload"
        parameters["primary_member_name"] = textFieldPrimaryMemberName.text ?? ""
        parameters["primary_member_dob"] = textFieldPrimaryMemberDOB.text ?? ""
        parameters["relationship"] = textFieldRelationship.text ?? ""
        parameters["additional_notes"] = txtViewNotes.text
        
        let insuranceRecord = InsuranceRecord(value: parameters)
        let frontImageData = imageViewFrontDoc.image?.pngData()
        let backImageData = imageViewBackDoc.image?.pngData()
        let result = Validations.shared.validateAddInsuranceRecord(insuranceRecord: insuranceRecord, frontImage: frontImageData, backImage: backImageData)
        
        if result.0 == false {
            Utility.shared.showToast(message: result.1)
            return
        } else {
            Utility.shared.startLoader()
            addInsuranceRecordApi(params: parameters, frontImage: imageViewFrontDoc.image, backImage: imageViewBackDoc.image)
        }
    }
    
    @IBAction func insuranceDocumentDropDown(_ sender: Any) {
        if Constant.masterData?.insuranceDocuments.count != 0 {
            guard let dataSource = Constant.masterData?.insuranceDocuments.map({$0.name}) else {
                return
            }
            initdropDown(textField: textFieldInsuranceDocument, dataSource: dataSource as! [String])
        }
    }
    
    @IBAction func insuranceTypeDropDown(_ sender: Any) {
        if Constant.masterData?.insuranceTypes.count != 0 {
            guard let dataSource = Constant.masterData?.insuranceTypes.map({$0.name}) else {
                return
            }
            initdropDown(textField: textFieldInsuranceTypes, dataSource: dataSource as? [String] ?? [""])
        }
    }
    
    @IBAction func relationshipDropDown(_ sender: Any) {
        if Constant.masterData?.relationship.count != 0 {
            guard let dataSource = Constant.masterData?.relationship.map({$0.name}) else {
                return
            }
            initdropDown(textField: textFieldRelationship, dataSource: dataSource as? [String] ?? [""])
        }
    }
}

extension AddInsuranceRecordViewController {
    
    func addInsuranceRecordApi(params: [String: Any], frontImage: UIImage?, backImage: UIImage?) {
        InsuaranceRecordViewModel.addInsuranceRecord(isUploadDoc: false, params: params, insuranceFrontImage: frontImage, insuranceBackImage: backImage, pdfArray: nil) { (message, error) in
            Utility.shared.stopLoader()
            if error == nil {
                Utility.shared.showToast(message: message ?? "Insurance record added successfully")
                self.resetTextField()
                self.popToListingController()
            }
        }
    }
    
    func popToListingController() {
        guard let viewControllers = navigationController?.viewControllers else { return }
        // Check if disease registry controller is already into the navigation stack. It will be in the stack incase user is saving follow up and disease registry forms
        for documentListingVC in viewControllers {
            if let vc = documentListingVC as? DocumentListingViewController {
                vc.fileType = self.fileType
                vc.documentType = self.documentType
                if self.selectedCareTaker != nil {
                    vc.selectedCareTaker = self.selectedCareTaker
                }
                navigationController?.popToViewController(vc, animated: true)
                return
            }
        }
    }
    
    func resetTextField() {
        textFieldInsuranceDocument.text = ""
        textFieldPayerName.text = ""
        textFieldMemberServices.text = ""
        textFieldInsuranceTypes.text = ""
        textFieldMemberId.text = ""
        textFieldGroupId.text = ""
//        textFieldContactType.text = ""
//        textFieldPlanCodes.text = ""
        textFieldDocumentName.text = ""
        textFieldPrimaryMemberName.text = ""
        textFieldPrimaryMemberDOB.text = ""
        textFieldRelationship.text = ""
        imageViewFrontDoc.image = UIImage(named: "global_add_image")
        imageViewBackDoc.image = UIImage(named: "global_add_image")
    }
    
}

extension AddInsuranceRecordViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //All Text Field Maximum Limitation
        let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 32, newCharacter: string)
        return result
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == textFieldInsuranceDocument {
            return false
        } else if textField == textFieldInsuranceTypes {
            return false
        } else if textField == textFieldRelationship {
            return false
        } else if textField == textFieldPrimaryMemberDOB {
            self.showDatePicker()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textFieldInsuranceDocument || textField == textFieldPayerName {
            if textFieldPayerName.text != nil && textFieldInsuranceDocument.text != nil {
                textFieldDocumentName.text = textFieldInsuranceDocument.text! + "_" + textFieldPayerName.text!
            }
        }
    }
    
    func initdropDown (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                textField.text = item
                if self.textFieldPayerName.text != nil && self.textFieldInsuranceDocument.text != nil {
                    self.textFieldDocumentName.text = self.textFieldInsuranceDocument.text! + "_" + self.textFieldPayerName.text!
                }
                if textField == self.textFieldInsuranceDocument {
                    self.insuranceDocId = String(describing: index)
                } else if textField == textFieldInsuranceTypes {
                    self.insuranceTypeId = String(describing: index)
                } else {
                    self.selectedRelationship = relations[index]
                    self.textFieldRelationship.text = self.selectedRelationship.name
                }
                dropDown.hide()
            }
            dropDown.show()
        }
    }
}

//MARK:- Image Picker Delegate
extension AddInsuranceRecordViewController: ImagePickerDelegate {
    
    func didSelect(image: UIImage?) {
        if let selectedImage = image {
            self.setProfileImage(image: selectedImage)
        }
    }
    
    func setProfileImage(image: UIImage) {
        if isFrontImageSelected == 1 {
            imageViewFrontDoc.image = image
        } else {
            imageViewBackDoc.image = image
        }
    }
    
}
//MARK:- Date Picker Methods
extension AddInsuranceRecordViewController {
    func showDatePicker(){
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat //"yyyy-MM-dd"//"dd MMM, yyyy"
        var date = Date()
        if textFieldPrimaryMemberDOB.text != "" {
            date = formater.date(from: textFieldPrimaryMemberDOB.text!) ?? Date()
        }
        
        
        let screenWidth = UIScreen.main.bounds.width
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -110, to: Date())
        datePicker.maximumDate = Date() //Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datePicker.setDate(date, animated: true)
        
        if #available(iOS 14.0, *)
        {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()

//            datePicker.datePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        
        textFieldPrimaryMemberDOB.inputAccessoryView = toolbar
        textFieldPrimaryMemberDOB.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = Constant.dateFormat //"yyyy-MM-dd" //"dd MMM, yyyy"
        textFieldPrimaryMemberDOB.text = formatter.string(from: datePicker.date)
//        let age = self.calculateAge()
//        if age < 18 {
//            emailTextField.isHidden = true
//        } else {
//            emailTextField.isHidden = false
//        }
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}


extension AddInsuranceRecordViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtViewNotes.text == " Notes (Optional)"  {
           txtViewNotes.text = ""
           txtViewNotes.textColor = Color.textFieldPlaceholderColor
       }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtViewNotes.text.isEmpty {
            txtViewNotes.text = " Notes (Optional)"
            txtViewNotes.textColor = Color.textFieldPlaceholderColor
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count // for Swift use count(newText)
    
            return numberOfChars <= 200;
      
    }
}
