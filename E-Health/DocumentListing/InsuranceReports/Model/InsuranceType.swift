//
//  InsuranceType.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/24/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class InsuranceType {

    var id : Int?
    var name : String?
    
    init() {
    }
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.name = value["name"] as? String
    }
}
