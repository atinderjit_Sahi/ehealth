//
//  InsuranceRecord.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/24/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class InsuranceRecord {

    var id: Int?
    var cognitoUserId: Int?
    var documentName: String?
    var minorId: String?
    var insuranceDocument: String?
    var payerName: String?
    var memberServices: String?
    var insuranceTypes: String?
    var memberId: String?
    var groupId: String?
    var contactType: String?
    var planCodes: String?
    var viewFront: String?
    var viewBack: String?
    var uploadType: String?
    var createdAt: String?
    var primaryMemberName: String?
    var primaryMemberDOB: String?
    var relationship: String?
    var additional_notes : String?
    
    init() {}
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.cognitoUserId = value["cognito_id"] as? Int
        self.documentName = value["document_name"] as? String
        self.minorId = value["minor_id"] as? String
        self.insuranceDocument = value["insurance_document"] as? String
        self.payerName = value["payer_name"] as? String
        self.memberServices = value["member_services"] as? String
        self.insuranceTypes = value["insurance_types"] as? String
        self.memberId = value["member_id"] as? String
        self.groupId = value["group_id"] as? String
        self.contactType = value["contract_type"] as? String
        self.planCodes = value["plan_codes"] as? String
        self.viewFront = value["view_front"] as? String
        self.viewBack = value["view_back"] as? String
        self.uploadType = value["upload_type"] as? String
        self.createdAt = value["created_at"] as? String
        self.primaryMemberName = value["primary_member_name"] as? String
        self.primaryMemberDOB = value["primary_member_dob"] as? String
        self.relationship = value["relationship"] as? String
        self.additional_notes = value["additional_notes"] as? String
    }
}
