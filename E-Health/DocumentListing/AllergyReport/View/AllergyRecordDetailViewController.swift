//
//  AllergyRecordDetailViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 8/10/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class AllergyRecordDetailViewController: UIViewController {

    //MARK: - Properties
    var allergyRecordId: Int?
    
    //MARK: - Outlets
    @IBOutlet weak var labelAllergyTypeName: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var lblSeverity: UILabel!
    @IBOutlet weak var lblEpipen: UILabel!
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        getAllergyRecordDetailApi(recordId: allergyRecordId ?? 0)
    }

}
extension AllergyRecordDetailViewController {
    
    func getAllergyRecordDetailApi(recordId: Int) {
        Utility.shared.startLoader()
        
        AllergyViewModel.getAllergyRecordDetail(recordId: recordId) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                if response != nil {
                    self.labelAllergyTypeName.text = response?.allergyType
                    self.descriptionTextView.text = response?.description
                } else {
                    Utility.shared.showToast(message: error ?? "Something went wrong!")
                }
            }
        }
    }
}
