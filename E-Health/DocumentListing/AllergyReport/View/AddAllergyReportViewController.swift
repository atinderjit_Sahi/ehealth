//
//  AddAllergyReportViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 8/5/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import DropDown
import AWSMobileClient

class AddAllergyReportViewController: UIViewController {
    
    // MARK: - Properties
    var allergyType: String?
    var documentType: String?
    var fileType: String?
    var selectedCareTaker: CaretakerModel?
    var selectedDocumentType = DocumentListingModel()
    
    //MARK: - Outlets
    @IBOutlet weak var textFieldAllergyType: UITextField!
    @IBOutlet weak var textViewAllergyDescription: UITextView!
    @IBOutlet weak var buttonSubmit: UIButton!
    
    @IBOutlet weak var StackViewSeverity: UIStackView!
    @IBOutlet weak var StackViewEpipen: UIStackView!
    var severity : String = ""
    var use_epipen : String = ""
    
    @IBOutlet weak var btncritical: UIButton!
    @IBOutlet weak var btnModerate: UIButton!
    @IBOutlet weak var btnMil: UIButton!
    
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        textFieldSetUp()
        setUpTextView()
    }

    func textFieldSetUp() {
        textFieldAllergyType.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        
        textFieldAllergyType.attributedPlaceholder = NSAttributedString(string: "Select Type of Allergy*", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        
//        textFieldAllergyType.setRightImage(rightImage: UIImage(named: "dropDown")!)
    }
    
    func setUpTextView() {
        textViewAllergyDescription.text = " Add Description*" //Placeholder
        textViewAllergyDescription.textColor = Color.textFieldPlaceholderColor
        textViewAllergyDescription.autocorrectionType = UITextAutocorrectionType.no
        
        textViewAllergyDescription.dropShadowOnTextView()
    }
    
    @IBAction func buttonSubmitTapped(_ sender: Any) {
        
        view.endEditing(true)
        if textFieldAllergyType.text == "" {
            Utility.shared.showToast(message: "Please select allergy type")
            return
        }
        else if textViewAllergyDescription.text == "" || textViewAllergyDescription.text == " Add Description" {
            Utility.shared.showToast(message: "Please enter allergy description")
            return
        } else if textViewAllergyDescription.text.count < 3 {
            Utility.shared.showToast(message: "Description should be minimum 3 characters")
            return
        } else if textViewAllergyDescription.text.count > 128 {
            Utility.shared.showToast(message: "Description should be maximum 128 characters")
            return
        }
        
        else if severity == "critical" {
            if use_epipen == "" {
            Utility.shared.showToast(message: "Please select epipen option")
            return
            }
        }
        var parameters = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": String(describing: selectedCareTaker?.minor_id ?? 0)] as [String : Any]
            } else {
                parameters = ["cognito_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": "0"] as [String : Any]
            }
        } else {
            parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": "0"] as [String : Any]
        }
        
        parameters["allergy_type_name"] = textFieldAllergyType.text ?? ""
        parameters["description"] = textViewAllergyDescription.text ?? ""
        if severity != "" {
            parameters["severity"] = severity
            parameters["use_epipen"] = use_epipen
        }
        
//        parameters["id"] = "0"
        
        callAddAllergyReportApi(parameters: parameters)
    }
    
    @IBAction func actionSeverity(_ sender: UIButton) {
        
        if sender.tag == 101 {
            StackViewEpipen.isHidden = false
            severity = "critical"
            btncritical.isSelected = true
            btnModerate.isSelected = false
            btnMil.isSelected = false
            
        }
        else if sender.tag == 102 {
            StackViewEpipen.isHidden = true
            severity = "moderate"
            btncritical.isSelected = false
            btnModerate.isSelected = true
            btnMil.isSelected = false
        }
        else if sender.tag == 103 {
            StackViewEpipen.isHidden = true
            severity = "mild"
            btncritical.isSelected = false
            btnModerate.isSelected = false
            btnMil.isSelected = true
        }
        
    }
    
    @IBAction func actionEpipen(_ sender: UIButton) {
        
        if sender.tag == 201 {
            use_epipen = "Yes"
            btnYes.isSelected = true
            btnNo.isSelected = false
            
        }
        else if sender.tag == 202 {
            use_epipen = "No"
            btnYes.isSelected = false
            btnNo.isSelected = true
        }
       
        
    }
    
    @IBAction func buttonBackTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func allergyTypeDropDown(_ sender: Any) {
        if Constant.masterData?.allergyTypes.count != 0 {
            let dataSource = Constant.masterData?.allergyTypes.map({$0.name})
            self.initdropDown(textField: textFieldAllergyType, dataSource: dataSource as! [String])
            return
        } else {
            Utility.shared.showToast(message: "No allergy type available!")
        }
    }
    
}

extension AddAllergyReportViewController {
    
    func callAddAllergyReportApi(parameters: [String: Any]) {
        
        buttonSubmit.isUserInteractionEnabled = false
        Utility.shared.startLoader()
        
        AllergyViewModel.addAllergyRecord(params: parameters) { (message, error) in
            Utility.shared.stopLoader()
            self.buttonSubmit.isUserInteractionEnabled = true
            if error == nil {
                Utility.shared.showToast(message: message ?? "")
                self.popToDocumentListingController()
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func popToDocumentListingController() {
        guard let viewControllers = navigationController?.viewControllers else { return }
        
        // Check if disease registry controller is already into the navigation stack. It will be in the stack incase user is saving follow up and disease registry forms
        for documentListingVC in viewControllers {
            if let vc = documentListingVC as? DocumentListingViewController {
                vc.fileType = self.fileType
                vc.documentType = self.documentType
                if self.selectedCareTaker != nil {
                    vc.selectedCareTaker = self.selectedCareTaker
                }
                navigationController?.popToViewController(vc, animated: true)
                return
            }
        }
    }
    
}
//MARK:- Drop Down
extension AddAllergyReportViewController {
    func initdropDown (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.textFieldAllergyType.text  = item
//                self.documentType = String(describing: (Constant.masterData?.allergyTypes[index].id ?? 0))
                dropDown.hide()
            }
            dropDown.show()
        }
    }
}
extension AddAllergyReportViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == textFieldAllergyType {
//            if Constant.masterData?.allergyTypes.count != 0 {
//                let dataSource = Constant.masterData?.allergyTypes.map({$0.name})
//                self.initdropDown(textField: textField, dataSource: dataSource as! [String])
//                return false
//            } else {
//                Utility.shared.showToast(message: "No allergy type available!")
//            }
            return false
        }
        return true
    }
}

extension AddAllergyReportViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textViewAllergyDescription.textColor == Color.textFieldPlaceholderColor {
            textViewAllergyDescription.text = ""
            textViewAllergyDescription.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textViewAllergyDescription.text.isEmpty {
            textViewAllergyDescription.text = " Add Description"
            textViewAllergyDescription.textColor = Color.textFieldPlaceholderColor
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count // for Swift use count(newText)
        return numberOfChars <= 128;
    }
}

