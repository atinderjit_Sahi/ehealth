//
//  DocumentListingViewModel.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/1/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class DocumentListingViewModel: NSObject {

    static func getDocumentsTypeList(userID: String, completion: @escaping (_ response: [DocumentListingModel]?, _ error: String?) -> Void) {
        let url = Constant.URL.getCaretakersByUserID + userID
        
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                if let data = result["data"] as? [String: AnyObject] {
                    if let careTakers = data["care_taker"] as? [[String: AnyObject]] {
                        var caretakerList = [DocumentListingModel]()
                        for item in careTakers {
                            caretakerList.append(DocumentListingModel(value: item))
                        }
                        completion(caretakerList, nil)
                    } else {
                       completion(nil, result["message"] as? String)
                    }
                }
                else {
                   completion(nil, result["message"] as? String)
                }
                
            } else {
                completion(nil, error?.localizedDescription)
            }
        }
    }
    
    static func uploadDocuments(params: [String: AnyObject], documentImage: [Any], pdfData: [Any]?, completion: @escaping (_ response: String?, _ error: String?) -> Void) {
        let url = Constant.URL.uploadDocument
        //_ = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        print(url)
        ApiService.shared.uploadDocuments(url, parameters: params, profileImage: documentImage, pdfUrlArray: pdfData as? [Data], compBlock: { (response, success) in
            
            
             guard let responseDict =   response as? Dictionary<String, Any>  else {
                           completion(nil, "Something went wrong!")
                           return
                       }
                       if let code = responseDict["code"] as? NSNumber {
                           print(responseDict)
                           if code.stringValue == "200"{
                               completion(responseDict["message"] as? String, nil)
                           } else {
                               completion(nil, responseDict["message"] as? String)
                           }
                       }
        }) { (message, error) in
                        completion(nil, message as String)

        }
    }
        
       
    static func searchDocument(parameters: [String: Any], completion: @escaping (_ response: [DocumentListingModel]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.searchDocuments
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            print(data)
                            var documentListingArray = [DocumentListingModel]()
                            for item in data {
                                documentListingArray.append(DocumentListingModel(value: item))
                            }
                            completion(documentListingArray, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func searchInsuranceDocument(parameters: [String: Any], completion: @escaping (_ response: [InsuranceRecord]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.searchDocuments
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        print(result)
                        if let data = result["data"] as? [[String: AnyObject]] {
                            var documentListingArray = [InsuranceRecord]()
                            for item in data {
                                documentListingArray.append(InsuranceRecord(value: item))
                            }
                            completion(documentListingArray, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func searchDocumentsByDate(parameters: [String: Any], completion: @escaping (_ response: [DocumentListingModel]?, _ insuranceRecord: [InsuranceRecord]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.searchDocumentByDate
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            var documentListingArray = [DocumentListingModel]()
                            for item in data {
                                documentListingArray.append(DocumentListingModel(value: item))
                            }
                            var insuranceRecordArray = [InsuranceRecord]()
                            for item in data {
                                insuranceRecordArray.append(InsuranceRecord(value: item))
                            }
                            completion(documentListingArray, insuranceRecordArray, nil)
                        } else {
                            completion(nil, nil, result["message"] as? String)
                        }
                    } else {
                        completion(nil, nil, result["message"] as? String)
                    }
                } else {
                    completion(nil, nil, result["message"] as? String)
                }
            } else {
                completion(nil, nil, error?.localizedDescription)
            }
        }
    }
    
}
