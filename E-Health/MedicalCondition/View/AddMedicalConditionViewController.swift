//
//  AddMedicalConditionViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/17/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import DropDown
import AWSMobileClient

class AddMedicalConditionViewController: UIViewController {

    @IBOutlet weak var textViewDescription: UITextView!
    @IBOutlet weak var textFieldMedicalConditionType: UITextField!
    @IBOutlet weak var buttonCurrent: UIButton!
    @IBOutlet weak var buttonPast: UIButton!
    
    //MARK: - Properties
    var selectedCareTaker: CaretakerModel?
    var conditionStatus: String?
    var typeId: Int?
    var arrayButtons:[UIButton] = []
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrayButtons.append(buttonCurrent)
        arrayButtons.append(buttonPast)
        setUpTextView()
        textFieldSetUp()
    }
    
    func textFieldSetUp() {
        textFieldMedicalConditionType.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        textFieldMedicalConditionType.attributedPlaceholder = NSAttributedString(string: "Medical Condition*", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
    }
    
    func setUpTextView() {
        textViewDescription.text = " Add Description*" //Placeholder
        textViewDescription.textColor = Color.lightTextFieldPlaceholderColor
        textViewDescription.autocorrectionType = UITextAutocorrectionType.no
        
        textViewDescription.dropShadowOnTextView()
    }
    
    // MARK: - UIButton Actions
    @IBAction func buttonBackTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonRadioTapped(_ sender: UIButton) {
        for button in arrayButtons {
            button.isSelected = false
        }
        if let index = arrayButtons.index(where: { $0 == sender }) {
            if index == 0 {
                conditionStatus = "current"
            } else {
                conditionStatus = "past"
            }
            arrayButtons[index].isSelected = true
        }
    }
    
    @IBAction func buttonSubmitTapped(_ sender: Any) {
        view.endEditing(true)
        if textFieldMedicalConditionType.text?.trimmingCharacters(in: .whitespaces) == "" {
            Utility.shared.showToast(message: "Please enter medical condition")
            return
        } else if textViewDescription.text.trimmingCharacters(in: .whitespaces) == "" || textViewDescription.text == " Add Description*" {
            Utility.shared.showToast(message: "Please enter description")
            return
        } else if textViewDescription.text.count < 3 {
            Utility.shared.showToast(message: "Description should be minimum 3 characters")
            return
        } else if textViewDescription.text.count > 128 {
            Utility.shared.showToast(message: "Description should be maximum 128 characters")
            return
        } else if conditionStatus == nil {
            Utility.shared.showToast(message: "Please select one amongst past or current")
            return
        }
        addMedicalConditionApi()
    }
}
extension AddMedicalConditionViewController {
    
    func addMedicalConditionApi() {
        
        var params = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "type_id": 0, "type_name": textFieldMedicalConditionType.text ?? "", "description": textViewDescription.text ?? "", "condition_status": conditionStatus ?? ""] as [String : Any]
            } else {
                params = ["cognito_user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "type_id": 0, "type_name": textFieldMedicalConditionType.text ?? "", "description": textViewDescription.text ?? "", "condition_status": conditionStatus ?? ""] as [String : Any]
            }
        } else {
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "type_id": 0, "type_name": textFieldMedicalConditionType.text ?? "", "description": textViewDescription.text ?? "", "condition_status": conditionStatus ?? ""] as [String : Any]
        }
        
        //current / past
        Utility.shared.startLoader()
        MedicalConditionViewModel.addMedicalCondition(params: params) { (message, error) in
            Utility.shared.stopLoader()
            if error == nil {
                Utility.shared.showToast(message: message ?? "Medical Condition added successfully")
                self.resetTextField()
                self.popToListingController()
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func resetTextField() {
        textFieldMedicalConditionType.text = ""
        textViewDescription.text = ""
    }
    
    func popToListingController() {
        guard let viewControllers = navigationController?.viewControllers else { return }
        
        // Check if disease registry controller is already into the navigation stack. It will be in the stack incase user is saving follow up and disease registry forms
        for documentListingVC in viewControllers {
            if let vc = documentListingVC as? MedicalConditionDetailListViewController {
                if self.selectedCareTaker != nil {
                    vc.selectedCareTaker = self.selectedCareTaker
                }
                navigationController?.popToViewController(vc, animated: true)
                return
            }
        }
    }
    
}
extension AddMedicalConditionViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textViewDescription.textColor == Color.lightTextFieldPlaceholderColor {
            textViewDescription.text = ""
            textViewDescription.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textViewDescription.text.isEmpty {
            textViewDescription.text = " Add Description*"
            textViewDescription.textColor = Color.lightTextFieldPlaceholderColor
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count // for Swift use count(newText)
        return numberOfChars <= 128;
    }
}
extension AddMedicalConditionViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldMedicalConditionType {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 30, newCharacter: string)
            if !result {
                return result
            }
        }
        return true
    }
}
