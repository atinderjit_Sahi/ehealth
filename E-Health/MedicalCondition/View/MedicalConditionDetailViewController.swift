//
//  MedicationDetailViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/17/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class MedicalConditionDetailViewController: UIViewController {

    //MARK: - Properties
    var medicalCondition: MedicalCondition?
    
    //MARK: - Outlets
    @IBOutlet weak var labelMedicalCondition: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var buttonPast: UIButton!
    @IBOutlet weak var buttonCurrent: UIButton!
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        labelMedicalCondition.text = medicalCondition?.typeName
        descriptionTextView.text = medicalCondition?.medicalDescription
        if medicalCondition?.conditionStatus == "current" {
            self.buttonCurrent.isSelected = true
        } else {
            self.buttonPast.isSelected = true
        }
        //getMedicalConditionDetailApi(recordId: medicalConditionId ?? 0)
//        textFieldSetUp()
//        setUpTextView()
    }
    
    func textFieldSetUp() {
        labelMedicalCondition.borderedLabel(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        //textFieldMedicalConditionType.attributedPlaceholder = NSAttributedString(string: "Medical Condition*", attributes: [NSAttributedString.Key.foregroundColor: Color.lightTextFieldPlaceholderColor])
    }
    
    func setUpTextView() {
        //descriptionTextView.text = " Add Description*" //Placeholder
        //descriptionTextView.textColor = Color.lightTextFieldPlaceholderColor
        //descriptionTextView.autocorrectionType = UITextAutocorrectionType.no
        
        descriptionTextView.dropShadowOnTextView()
    }
    
    @IBAction func buttonBackTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
extension MedicalConditionDetailViewController {
    
    func getMedicalConditionDetailApi(recordId: Int) {
        Utility.shared.startLoader()
        
        MedicalConditionViewModel.getMedicalConditionDetail(id: recordId) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                if response != nil {
                    self.labelMedicalCondition.text = response?.typeName
                    self.descriptionTextView.text = response?.medicalDescription
                } else {
                    Utility.shared.showToast(message: error ?? "Something went wrong!")
                }
            }
        }
    }
}
