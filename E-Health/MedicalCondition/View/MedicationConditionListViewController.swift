//
//  MedicationListViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/17/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class MedicationConditionListViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var medicalConditionListTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK: - Properties
    var selectedCareTaker: CaretakerModel?
    
    // MARK: - Navigation
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // MARK: - UIButton Actions
    @IBAction func buttonBackTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonSyncTapped(_ sender: Any) {
        view.endEditing(true)
        
        guard let vc = UIStoryboard.init(name: StoryboardNames.medicalCondition, bundle: Bundle.main).instantiateViewController(withIdentifier: AddMedicalConditionViewController.className) as? AddMedicalConditionViewController else {return}
        
        vc.selectedCareTaker = selectedCareTaker
        navigationController?.pushViewController(vc, animated: true)
    }
}
//MARK: - Table View Delegate
extension MedicationConditionListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
//MARK: - Table View Data Source
extension MedicationConditionListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: MedicalConditionTableViewCell.className)! as? MedicalConditionTableViewCell else {
            fatalError()
        }
        
        cell.delegate = self
        return cell
    }
}
extension MedicationConditionListViewController: MedicalConditionMoreProtocol {
    func moreButtonTapped(sender: UIButton) {
        Utility.shared.showToast(message: "Coming Soon")
    }
}
