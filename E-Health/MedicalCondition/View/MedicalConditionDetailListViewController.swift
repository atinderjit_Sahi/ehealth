//
//  MedicalConditionDetailListViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/22/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

class MedicalConditionDetailListViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var listingTableView: UITableView!
    @IBOutlet weak var addDataButton: UIButton!
    @IBOutlet weak var addMedicalConditionButton: UIButton!
    
    //MARK: - Outlets
    var pageSize = 500
    var pageNumber = 1
    var searchText = String()
    var selectedCareTaker: CaretakerModel?
    var medicalConditionArray: [MedicalCondition]?
    var searchMedicalConditions: [MedicalCondition]?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listingTableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        tabBarController?.tabBar.isHidden = true
        if searchText == "" {
            getMedicalCondition(searchText: "")
        }
    }
    
    //MARK:- UIButton Actions
    @IBAction func addDataButtonAction(_ sender: Any) {
        moveToAddMedicalConditionVC()
    }
    
    @IBAction func addMedicalConditionButtonAction(_ sender: Any) {
        moveToAddMedicalConditionVC()
    }
    
    func moveToAddMedicalConditionVC() {
        view.endEditing(true)
        
        guard let vc = UIStoryboard.init(name: StoryboardNames.medicalCondition, bundle: Bundle.main).instantiateViewController(withIdentifier: MedicationConditionListViewController.className) as? MedicationConditionListViewController else {return}
        
        vc.selectedCareTaker = selectedCareTaker
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backButton(_ sender: Any) {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
}
extension MedicalConditionDetailListViewController {
    
    func getMedicalCondition(searchText: String) {
        
        var params = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "page": pageNumber, "size": pageSize] as [String : Any]
            } else {
                params = ["cognito_user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "page": pageNumber, "size": pageSize] as [String : Any]
            }
        } else {
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "page": pageNumber, "size": pageSize] as [String : Any]
        }
                
        Utility.shared.startLoader()
        MedicalConditionViewModel.getMedicalCondition(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.medicalConditionArray = response
                self.searchMedicalConditions = response
                self.listingTableView.reloadData()
            } else {
                if searchText != "" {
                    self.medicalConditionArray?.removeAll()
                    self.searchMedicalConditions?.removeAll()
                    self.listingTableView.reloadData()
                }
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
}
//MARK: - Table View Delegate
extension MedicalConditionDetailListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
//MARK: - Table View Data Source
extension MedicalConditionDetailListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchMedicalConditions == nil {
            addDataButton.isHidden = false
        } else {
            addDataButton.isHidden = true
        }
        return searchMedicalConditions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: MedicalConditionDetailTableViewCell.className)! as? MedicalConditionDetailTableViewCell else {
            fatalError()
        }
        
        cell.delegate = self
        cell.medicalCondition = searchMedicalConditions?[indexPath.row]
        return cell
    }
    
    /*func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            Utility.shared.showCustomAlertWithCompletion("Delete Record", message: "Are you sure you want to delete this record?", withButtonTypes: ["No", "Yes"], withAlertCompletionBlock: { (alertAction, buttonTitle) in
                
                if buttonTitle == "Yes" {
                    self.deleteMedicalCondition(indexpath: indexPath)
                }
            })
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    func deleteMedicalCondition(indexpath: IndexPath) {
        
    }*/
    
}
extension MedicalConditionDetailListViewController : MedicalConditionProtocol {
    
    func getSelectedIndexPath(sender: UIButton?, tableView: UITableView?) -> IndexPath? {
        let point = sender?.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView?.indexPathForRow(at: point!)
        return indexPath!
    }
    
    func getConditiondetails(sender: UIButton) {
        
        guard let indexPath = getSelectedIndexPath(sender: sender, tableView: listingTableView) else {
            return
        }
        
        let medicalCondition = searchMedicalConditions?[indexPath.row]
        
        guard let vc = UIStoryboard.init(name: StoryboardNames.medicalCondition, bundle: Bundle.main).instantiateViewController(withIdentifier: MedicalConditionDetailViewController.className) as? MedicalConditionDetailViewController else {return}
        
        vc.medicalCondition = medicalCondition
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
//MARK:- Search Bar Delegates
extension MedicalConditionDetailListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let trimmedString = searchText.trimmingCharacters(in: .whitespaces)
        self.searchText = trimmedString
        if trimmedString != "" {
            searchMedicalConditions = searchMedicalConditions?.filter({ medication -> Bool in
                if searchText.isEmpty {
                    return true
                }
                return medication.typeName?.lowercased().contains(searchText.lowercased()) ?? false
            })
        } else {
            searchBar.endEditing(true)
            searchMedicalConditions = medicalConditionArray
        }
        listingTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
