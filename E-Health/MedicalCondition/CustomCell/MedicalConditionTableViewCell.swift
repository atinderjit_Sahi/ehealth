//
//  MedicalConditionTableViewCell.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/21/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
protocol MedicalConditionMoreProtocol {
    func moreButtonTapped(sender: UIButton)
}
class MedicalConditionTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var medicalConditionName: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    var delegate: MedicalConditionMoreProtocol?
    
    //MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.dropShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonMoreTapped(_ sender: UIButton) {
        delegate?.moreButtonTapped(sender: sender)
    }
    
}
