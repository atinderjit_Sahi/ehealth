//
//  MedicalConditionDetailTableViewCell.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/21/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
protocol MedicalConditionProtocol {
    func getConditiondetails(sender: UIButton)
}
class MedicalConditionDetailTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var medicalConditionName: UILabel!
    @IBOutlet weak var labelConditionStatus: UILabel!
    @IBOutlet weak var labelAddedOn: UILabel!
    @IBOutlet weak var viewDetailsButton: UIButton!
    
    var delegate: MedicalConditionProtocol?
    
    //MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.dropShadow()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var medicalCondition: MedicalCondition? {
        didSet {
            medicalConditionName.text = medicalCondition?.typeName
            labelConditionStatus.text = medicalCondition?.conditionStatus
            labelAddedOn.text = medicalCondition?.createdAt?.convertToDateString(currentDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", requiredDateFormat: Constant.dateFormat)
        }
    }
    
    @IBAction func buttonViewTapped(_ sender: UIButton) {
        delegate?.getConditiondetails(sender: sender)
    }
}
