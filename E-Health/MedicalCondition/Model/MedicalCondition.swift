//
//  MedicalCondition.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/17/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class MedicalCondition: NSObject {
    
    var id: Int?
    var cognitoId: String?
    var minorId: Int?
    var typeName: String?
    var typeId: Int?
    var medicalDescription: String?
    var conditionStatus: String?
    var createdAt: String?
    
    init(value: [String: Any]) {
        id = value["id"] as? Int
        cognitoId = value["cognito_user_id"] as? String
        minorId = value["minor_id"] as? Int
        typeName = value["type_name"] as? String
        typeId = value["type_id"] as? Int
        medicalDescription = value["description"] as? String
        conditionStatus = value["condition_status"] as? String
        createdAt = value["created_at"] as? String
    }
}
