//
//  NewDrugInfoViewController.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/27/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class NewDrugInfoViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblTitle: UILabel!
    var medication: Medication?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "TabletName", bundle: nil), forCellReuseIdentifier: TabletNameTableViewCell.className)
        tableView.register(UINib(nibName: "PharmaNameCell", bundle: nil), forCellReuseIdentifier: PharmaNameTableViewCell.className)
        tableView.register(UINib(nibName: "TabletInfoCell", bundle: nil), forCellReuseIdentifier: TabletInfoTableViewCell.className)
        tableView.register(UINib(nibName: "TabletFrequencyCell", bundle: nil), forCellReuseIdentifier: TabletFrequencyTableViewCell.className)
        
        if medication != nil {
            displayMedicationDetails()
            self.lblTitle.text = medication?.drug_name ?? "N.A."
        }  
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        medicationDrugs = Medication()
        typesOfMedicines = [StandardDosage]()
        typesOfFrequencies = [StandardDosage]()
        standardDosage = [StandardDosage]()
        StrengthDosage = [dosage_strength]()
    }
    
    
    
    @IBAction func actionEditMedication(_ sender: UIButton) {
        
        let vc = UIStoryboard.init(name: StoryboardNames.Medication, bundle: Bundle.main).instantiateViewController(withIdentifier: AddNewDrugViewController.className) as? AddNewDrugViewController
       // vc?.medicationDrugs = medication
        medicationDrugs = medication
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    
}

extension NewDrugInfoViewController {
    func displayMedicationDetails() {
        
    }
}

extension NewDrugInfoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TabletNameTableViewCell.className, for: indexPath) as? TabletNameTableViewCell else {
                fatalError()
            }
            cell.item = medication
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PharmaNameTableViewCell.className, for: indexPath) as? PharmaNameTableViewCell else {
                fatalError()
            }
            cell.pharmaNameCell.text = medication?.pharmacy_name ?? "N.A."
            return cell
            
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TabletInfoTableViewCell.className, for: indexPath) as? TabletInfoTableViewCell else {
                fatalError()
            }
            cell.item = medication
            return cell
            
        case 3:
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TabletFrequencyTableViewCell.className, for: indexPath) as? TabletFrequencyTableViewCell else {
            fatalError()
        }
        cell.item = medication
        return cell
            
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TabletNameTableViewCell.className, for: indexPath) as? TabletNameTableViewCell else {
                fatalError()
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 206 //UITableView.automaticDimension
    }
}


