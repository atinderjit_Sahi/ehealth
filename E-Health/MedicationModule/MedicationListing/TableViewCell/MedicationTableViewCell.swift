//
//  MedicationTableViewCell.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/23/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

protocol MedicationListProtocol {
    func moreDetails(sender: UIButton)
}

class MedicationTableViewCell: UITableViewCell {

    @IBOutlet weak var medicationNameLabel: UILabel!
    @IBOutlet weak var frequencyLabel: UILabel!
    @IBOutlet weak var tabletsQuantityLabel: UILabel!
    @IBOutlet weak var usageLabel: UILabel!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var moreDetailsButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    @IBAction func moreDetailsButtonAction(_ sender: UIButton) {
        delegate?.moreDetails(sender: sender)
    }
    
    var delegate: MedicationListProtocol?
    
    var item: Medication? = Medication() {
        didSet {
            medicationNameLabel.text = item?.drug_name ?? ""
            frequencyLabel.text = (item?.frequency ?? "")
            tabletsQuantityLabel.text = (item?.tablet ?? "")
            
            if item?.usage != "" && item?.usage != nil {
            usageLabel.text = item?.usage ?? ""
            }
            else {
                usageLabel.text = "N.A."
            }
            if item?.days != nil {
                daysLabel.text = "\(item?.days ?? 0) Days"
            } else {
                daysLabel.text = "No Limit"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let customButtonTitle = NSMutableAttributedString(string: moreDetailsButton.titleLabel?.text ?? "More Details", attributes: [
            NSAttributedString.Key.font : UIFont(name: "Montserrat-SemiBold", size: 13.0) ?? UIFont.boldSystemFont(ofSize: 13.0),
            NSAttributedString.Key.foregroundColor : UIColor(red: 31/255, green: 181/255, blue: 245/255, alpha: 1.0),
            NSAttributedString.Key.underlineStyle : 1]
        )
        moreDetailsButton.setAttributedTitle(customButtonTitle, for: .normal)
        containerView.dropShadow()
        
        medicationNameLabel.text = "Nucynta Oral Tablet 75 mg"
        frequencyLabel.text = "4 Hours"
        tabletsQuantityLabel.text = "2 Tablets/Day"
        usageLabel.text = "By Oral route"
        daysLabel.text = "10 Days"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
