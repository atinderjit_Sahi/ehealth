//
//  Medication.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/23/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class Medication {

    var Id : Int?
    var drug_name: String?
    var usage: String?
    var pharmacy_name: String?
    var side_effects: String?
    var standard_dosage: String?
    var interactions: String?
    var frequency: String?
    var tablet: String?
    var to_date: String?
    var from_date: String?
    var rx_number: String?
    var prescription: String?
    var refills: String?
    var days: Int?
    var additionalNotes: String?
    var strength : String?
    
    init() {
        
    }
    
    init(value: [String: AnyObject]) {
        self.Id = value["id"] as? Int
        self.drug_name = value["drug_name"] as? String
        self.usage = value["usage"] as? String
        self.pharmacy_name = value["pharmacy_name"] as? String
        self.side_effects = value["side_effects"] as? String
        self.standard_dosage = value["standard_dosage"] as? String
        self.interactions = value["interactions"] as? String
        self.frequency = value["frequency"] as? String
        self.tablet = value["tablet"] as? String
        self.to_date = value["to_date"] as? String
        self.from_date = value["from_date"] as? String
        self.rx_number = value["rx_number"] as? String
        self.prescription = value["prescription"] as? String
        self.refills = value["refills"] as? String
        self.days = value["days"] as? Int
        self.additionalNotes = value["additional_notes"] as? String //strength
        self.strength = value["strength"] as? String
    }

}
