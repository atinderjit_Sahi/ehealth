//
//  MedicationListViewModel.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/23/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class MedicationListViewModel: NSObject {

    static func deleteDrug(parameters: [String: Any], completion: @escaping (_ response: String?, _ error: String?) -> Void) {

        let url = Constant.URL.deleteDrugRecord
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            
                if let result = result as? [String: AnyObject] {
                    
                    if let statusCode = result["code"] as? Int {
                        if statusCode == 200 {
                            if let message = result["message"] as? String {
                                completion(message , nil)
                            } else {
                                completion(nil , result["message"] as? String)
                            }
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , error?.localizedDescription)
                }
            }
        }
        

       
    
    static func getMedicationListing(parameters: [String: Any], completion: @escaping (_ response: [Medication]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.searchDocuments
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            print(result)
                            var medicationListingArray = [Medication]()
                            for item in data {
                                medicationListingArray.append(Medication(value: item))
                            }
                            completion(medicationListingArray, nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func searchDocumentsByDate(parameters: [String: Any], completion: @escaping (_ response: [Medication]?, _ error: String?) -> Void) {
        
        let url = Constant.URL.searchDocumentByDate
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [[String: AnyObject]] {
                            var medicationListingArray = [Medication]()
                            for item in data {
                                medicationListingArray.append(Medication(value: item))
                            }
                            completion(medicationListingArray, nil)
                        } else {
                            completion(nil, result["message"] as? String)
                        }
                    } else {
                        completion(nil, result["message"] as? String)
                    }
                } else {
                    completion(nil, result["message"] as? String)
                }
            } else {
                completion(nil, error?.localizedDescription)
            }
        }
    }
   
}
