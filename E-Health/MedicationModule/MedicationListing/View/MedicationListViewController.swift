//
//  MedicationListViewController.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/23/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

class MedicationListViewController: UIViewController {
    
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var resultCountLabel: UILabel!
    @IBOutlet weak var addDataButton: UIButton!
    @IBOutlet weak var textFieldToDate: UITextField!
    @IBOutlet weak var textFieldFromDate: UITextField!
    @IBOutlet weak var searchDone: UIButton!
    @IBOutlet weak var searchClear: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    
    @IBOutlet weak var fromToDateView: UIView!
    @IBOutlet weak var doneClearButtonView: UIView!
    
    var medicationList: [Medication]?
    var searchedMedicationList: [Medication]?
    
    var searchText = String()
    var pageSize = 500
    var pageNumber = 1
    var documentType: String?
    var fileType: String? = "5"
    var isDataLoading = false
    var titleText: String?
    var selectedCareTaker: CaretakerModel?
    
    var datePicker = UIDatePicker()
    var activeTextField = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addDataButton.isHidden = true
        resultCountLabel.text = "No Results"
        listTableView.tableFooterView = UIView()
        searchBar.setSearchBarUI()
//        customizeTextFields()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        resultCountLabel.text = ""
        searchBar.text = ""
        medicationDrugs = Medication()
        self.getMedicationList()
    }
    
    func customizeTextFields() {
        textFieldFromDate.setLeftImage(leftImage: UIImage(named: "calendar")!)
        textFieldToDate.setLeftImage(leftImage: UIImage(named: "calendar")!)
        textFieldFromDate.attributedPlaceholder = NSAttributedString(string: "From", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        textFieldToDate.attributedPlaceholder = NSAttributedString(string: "To", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
    }
    
    @IBAction func filterButton(_ sender: UIButton) {
        view.endEditing(true)
        self.searchBar.text = ""
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            print(sender.isSelected)
            hideShowSearchView(isHidden: false)
        } else {
            print(sender.isSelected)
            hideShowSearchView(isHidden: true)
            emptySearchByDateFields()
//            callSearchApi()
        }
    }
    
    @IBAction func searchDoneButton(_ sender: Any) {
        view.endEditing(true)
        if textFieldFromDate.text == "" {
            Utility.shared.showToast(message: "Please select from date")
            return
        } else if textFieldToDate.text == "" {
            Utility.shared.showToast(message: "Please select to date")
            return
        }
        searchMedicationByDate(toDate: textFieldToDate.text ?? "", fromDate: textFieldFromDate.text ?? "")
    }
    
    @IBAction func searchClearButton(_ sender: Any) {
        view.endEditing(true)
        hideShowSearchView(isHidden: true)
        filterButton.isSelected = false
        emptySearchByDateFields()
        getMedicationList()
    }
    
    func hideShowSearchView(isHidden: Bool) {
        DispatchQueue.main.async {
            self.fromToDateView.isHidden = isHidden
            self.doneClearButtonView.isHidden = isHidden
        }
    }
    
    func emptySearchByDateFields() {
        textFieldToDate.text = ""
        textFieldFromDate.text = ""
    }
    
    @IBAction func addDataButtonAction(_ sender: Any) {
//        let vc = UIStoryboard.init(name: StoryboardNames.Medication, bundle: Bundle.main).instantiateViewController(withIdentifier: SearchMedicationViewController.className) as? SearchMedicationViewController
//        vc?.selectedCareTaker = self.selectedCareTaker
//        navigationController?.pushViewController(vc!, animated: true)
        
        let vc = UIStoryboard.init(name: StoryboardNames.Medication, bundle: Bundle.main).instantiateViewController(withIdentifier: AddNewDrugViewController.className) as? AddNewDrugViewController
        vc?.selectedCareTaker = self.selectedCareTaker
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
// atinder       let vc = UIStoryboard.init(name: StoryboardNames.Medication, bundle: Bundle.main).instantiateViewController(withIdentifier: SearchMedicationViewController.className) as? SearchMedicationViewController
//        vc?.selectedCareTaker = self.selectedCareTaker
//        navigationController?.pushViewController(vc!, animated: true)
        
        let vc = UIStoryboard.init(name: StoryboardNames.Medication, bundle: Bundle.main).instantiateViewController(withIdentifier: AddNewDrugViewController.className) as? AddNewDrugViewController
        vc?.selectedCareTaker = self.selectedCareTaker
        navigationController?.pushViewController(vc!, animated: true)
    }
}

//MARK:- API METHODS
extension MedicationListViewController {
    func getMedicationList() {
        
        view.endEditing(true)
        var params = [String: Any]()
        
        fileType = "1"
        documentType = "5"
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": "", "page": pageNumber, "size": pageSize] as [String : Any]
            } else {
                params = ["cognito_user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": "", "page": pageNumber, "size": pageSize] as [String : Any]
            }
            
        } else {
            
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": "", "page": pageNumber, "size": pageSize] as [String : Any]
        }
        
        if documentType == "6" {
            params["document_type"] = "0"
        }
        
        Utility.shared.startLoader()
        MedicationListViewModel.getMedicationListing(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.isDataLoading = false
                self.medicationList = response
                self.searchedMedicationList = response
                self.listTableView.reloadData()
                self.resultCountLabel.text = "\(String(describing: self.searchedMedicationList!.count)) Results"
            } else {
                self.resultCountLabel.text = "No Results"
                if error != "No data found." {
                    Utility.shared.showToast(message: error ?? "Something went wrong")
                }
            }
        }
    }
    
    func searchMedicationByDate(toDate: String, fromDate: String) {
        view.endEditing(true)
        var params = [String: Any]()
        
        fileType = "1"
        documentType = "5"
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": "", "page": pageNumber, "size": pageSize] as [String : Any]
            } else {
                params = ["cognito_user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": "", "page": pageNumber, "size": pageSize] as [String : Any]
            }
        } else {
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": "", "page": pageNumber, "size": pageSize] as [String : Any]
        }
        
        if documentType == "6" {
            params["document_type"] = "0"
        }
        
        Utility.shared.startLoader()
        MedicationListViewModel.searchDocumentsByDate(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.isDataLoading = false
                self.medicationList = response
                self.searchedMedicationList = response
                self.listTableView.reloadData()
                self.resultCountLabel.text = "\(String(describing: self.searchedMedicationList!.count)) Results"
            } else {
                self.resultCountLabel.text = "No Results"
                if error != "No data found." {
                    Utility.shared.showToast(message: error ?? "Something went wrong")
                }
            }
        }
    }
    
    func deleteDrugRecord(drug: Medication, indexPath: IndexPath) {
        var parameters = [String: Any]()
        if selectedCareTaker != nil {
            if selectedCareTaker?.minor_id != nil {
                parameters = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0] as [String : Any]
            } else {
                parameters = ["cognito_user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0] as [String : Any]
            }
        } else {
            parameters = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0] as [String : Any]
        }
        
        parameters["id"] = drug.Id ?? ""
        print(parameters)
        Utility.shared.startLoader()
        MedicationListViewModel.deleteDrug(parameters: parameters) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.view.makeToast(response!)
                if let index = self.medicationList?.firstIndex(where: {$0.Id == drug.Id}) {
                    self.medicationList?.remove(at: index)
                }
                self.searchedMedicationList?.remove(at: indexPath.row)
                self.resultCountLabel.text = "\(String(describing: self.searchedMedicationList!.count)) Results"
                self.listTableView.reloadData()
                Utility.shared.showToast(message: error ?? "Drugs Record Deleted Successfully")
            } else {
                self.view.makeToast(error ?? "Something went wrong!")
            }
        }
    }
}

//MARK:- TABLE VIEW DELEGATES AND CELL DELEGATES
extension MedicationListViewController: UITableViewDelegate, UITableViewDataSource, MedicationListProtocol {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let numOfSections = showNoRecordFound(dataArray: searchedMedicationList ?? [], tableview: tableView)
        return numOfSections
    }
    
    func showNoRecordFound(dataArray: [Medication], tableview: UITableView) -> Int {
        var noOfSections: Int = 0

        if !dataArray.isEmpty {
            tableview.separatorStyle = .singleLine
            noOfSections            = 1
            addDataButton.isHidden = true
        } else {
            addDataButton.isHidden = false
        }
        return noOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedMedicationList?.count ?? 0
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MedicationTableViewCell.className, for: indexPath) as? MedicationTableViewCell else {
            fatalError()
        }
        cell.delegate = self
        cell.item = self.searchedMedicationList?[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let drug = searchedMedicationList![indexPath.row]
        
        if editingStyle == .delete {
            Utility.shared.showCustomAlertWithCompletion("Delete Record", message: "Are you sure you want to delete this record?", withButtonTypes: ["No", "Yes"], withAlertCompletionBlock: { (alertAction, buttonTitle) in
                
                if buttonTitle == "Yes" {
                    self.deleteDrugRecord(drug: drug, indexPath: indexPath)
                }
            })
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 206 //UITableView.automaticDimension
    }
    
    func moreDetails(sender: UIButton) {
        view.endEditing(true)
        let indexpath = getSelectedIndexPath(sender: sender, tableView: listTableView)
        if indexpath != nil && searchedMedicationList != nil {
            let medication = searchedMedicationList![indexpath!.row]
            
            guard let vc = UIStoryboard.init(name: StoryboardNames.Medication, bundle: Bundle.main).instantiateViewController(withIdentifier: NewDrugInfoViewController.className) as? NewDrugInfoViewController else {return}
            vc.medication = medication
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getSelectedIndexPath(sender: UIButton?, tableView: UITableView?) -> IndexPath? {
        let point = sender?.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView?.indexPathForRow(at: point!)
        return indexPath!
    }
}

extension MedicationListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchedMedicationList = medicationList?.filter({ medication -> Bool in
            if searchText.isEmpty {
                return true
            }
            return medication.drug_name?.lowercased().contains(searchText.lowercased()) ?? false
        })
        if searchedMedicationList != nil {
            self.resultCountLabel.text = "\(String(describing: (searchedMedicationList?.count)!)) Results"
        }
        listTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
