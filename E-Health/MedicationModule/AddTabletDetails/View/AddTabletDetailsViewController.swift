//
//  AddTabletDetailsViewController.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/24/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class AddTabletDetailsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "TabletName", bundle: nil), forCellReuseIdentifier: TabletNameTableViewCell.className)
        tableView.register(UINib(nibName: "PharmaNameCell", bundle: nil), forCellReuseIdentifier: PharmaNameTableViewCell.className)
        tableView.register(UINib(nibName: "TabletInfoCell", bundle: nil), forCellReuseIdentifier: TabletInfoTableViewCell.className)
        tableView.register(UINib(nibName: "TableConsumptionDetailCell", bundle: nil), forCellReuseIdentifier: TabletConsumtionDetailTableViewCell.className)
        tableView.register(UINib(nibName: "AddButtonCell", bundle: nil), forCellReuseIdentifier: AddButtonTableViewCell.className)
        // Do any additional setup after loading the view.
    }

    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AddTabletDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TabletNameTableViewCell.className, for: indexPath) as? TabletNameTableViewCell else {
                fatalError()
            }
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PharmaNameTableViewCell.className, for: indexPath) as? PharmaNameTableViewCell else {
                fatalError()
            }
            return cell
            
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TabletInfoTableViewCell.className, for: indexPath) as? TabletInfoTableViewCell else {
                fatalError()
            }
            return cell
            
        case 3:
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TabletConsumtionDetailTableViewCell.className, for: indexPath) as? TabletConsumtionDetailTableViewCell else {
            fatalError()
        }
//        cell.delegate = self
        return cell
            
         case 4:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: AddButtonTableViewCell.className, for: indexPath) as? AddButtonTableViewCell else {
                    fatalError()
                }
                cell.buttonTitle = "addDrugInListButton"
                cell.delegate = self
                return cell
            
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TabletNameTableViewCell.className, for: indexPath) as? TabletNameTableViewCell else {
                fatalError()
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 206 //UITableView.automaticDimension
    }
    
}


extension AddTabletDetailsViewController: AddButtonProtocol {
    func addTablet() {
        let indexPath = IndexPath(row: 3, section: 0)
        if let firstCell = tableView.cellForRow(at: indexPath) {
            if !validateTextFields(cell: firstCell) {
                return
            }
        }
        
        self.view.makeToast("Coming Soon")
//        let vc = UIStoryboard.init(name: StoryboardNames.Medication, bundle: Bundle.main).instantiateViewController(withIdentifier: NewDrugInfoViewController.className) as? NewDrugInfoViewController
//        navigationController?.pushViewController(vc!, animated: true)
    }
}

extension AddTabletDetailsViewController {
    func validateTextFields(cell: UITableViewCell) -> Bool {
            let objValidate   = Validations()
                        
            if let consumptionCell = cell as? TabletConsumtionDetailTableViewCell {
                //FREQUENCY FIELD
                var (status,message) = objValidate.validateField(consumptionCell.frequencyTextField, sizeLimit: false)
                if !status{
                    switch message {
                    case "Empty field":
                        self.view.makeToast("Please enter Frequency")
                        break
                        
                    default:
                        break
                    }
                    return false
                }
                let frequencyValue = Int(consumptionCell.frequencyTextField.text!)!
                if frequencyValue < 1 {
                    self.view.makeToast("Frequency must be atleast 1 Tablet.")
                    return false
                }
                if frequencyValue > 50 {
                    self.view.makeToast("Frequency must be maximum 50 Tablets.")
                    return false
                }
                
                
                //TABLET TEXT FIELD
                (status,message) = objValidate.validateField(consumptionCell.tabletTextField, sizeLimit: false)
                if !status{
                    switch message {
                    case "Empty field":
                        self.view.makeToast("Please enter Tablets")
                        break
                        
                    default:
                        break
                    }
                    return false
                }
                let tabletValue = Int(consumptionCell.tabletTextField.text!)!
                if tabletValue < 1 {
                    self.view.makeToast("Tablets must be atleast 1.")
                    return false
                }
                if tabletValue > 50 {
                    self.view.makeToast("Tablets must be maximum 50.")
                    return false
                }
                
                
                //TO DATE TEXT FIELD
                (status,message) = objValidate.validateField(consumptionCell.toDateTextField, sizeLimit: false)
                if !status{
                    switch message {
                    case "Empty field":
                        self.view.makeToast("Please enter To date")
                        break
                        
                    default:
                        break
                    }
                    return false
                }
                
                
                //FROM DATE TEXT FIELD
                (status,message) = objValidate.validateField(consumptionCell.fromDateTextField, sizeLimit: false)
                if !status{
                    switch message {
                    case "Empty field":
                        self.view.makeToast("Please enter From date")
                        break
                        
                    default:
                        break
                    }
                    return false
                }
                
                //RX NUMBER TEXT FIELD
                (status,message) = objValidate.validateField(consumptionCell.rxNumberTextField, sizeLimit: false)
                if !status{
                    switch message {
                    case "Empty field":
                        self.view.makeToast("Please enter Rx Number")
                        break
                        
                    default:
                        break
                    }
                    return false
                }
    //            let rxValue = Int(consumptionCell.rxNumberTextField.text!)!
    //            if rxValue < 1 {
    //                self.view.makeToast("Rx Number must be atleast 1.")
    //                return false
    //            }
    //            if rxValue > 50 {
    //                self.view.makeToast("Rx Number must be maximum 50.")
    //                return false
    //            }
                
                if !consumptionCell.prescriptionNoButton.isSelected && !consumptionCell.prescriptionYesButton.isSelected {
                    self.view.makeToast("Please select Prescription option")
                    return false
                }
                if !consumptionCell.refillsNoButton.isSelected && !consumptionCell.refillsYesButton.isSelected {
                    self.view.makeToast("Please select Refills option")
                    return false
                }
                
                return true
            }
            
            self.view.makeToast("Something went wrong!")
            return false
        }
}
