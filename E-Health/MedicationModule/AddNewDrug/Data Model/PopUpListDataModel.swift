// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
struct Welcome: Codable {
    let data: DataClass?
    let message, technicalMessage: String?
    let customCode, code: Int?

    enum CodingKeys: String, CodingKey {
        case data, message
        case technicalMessage = "technical_message"
        case customCode = "custom_code"
        case code
    }
}

// MARK: - DataClass
struct DataClass: Codable {
    let typesOfMedicines, typesOfFrequencies, standardDosage: [StandardDosage]?
    let dosageStrength : [dosage_strength]?
    enum CodingKeys: String, CodingKey {
        case typesOfMedicines = "types_of_medicines"
        case typesOfFrequencies = "types_of_frequencies"
        case standardDosage = "standard_dosage"
        case dosageStrength = "dosage_strength"
    }
}

// MARK: - StandardDosage
struct StandardDosage: Codable {
    let id: Int?
    let dosage: String?
    let createdAt, updatedAt: String?
    let frequencyName, typeOfMedicine: String?

    enum CodingKeys: String, CodingKey {
        case id, dosage
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case frequencyName = "frequency_name"
        case typeOfMedicine = "type_of_medicine"
    }
}

struct dosage_strength: Codable {
    let id: Int?
    let name: String?
    let created_at, updated_at: String?
}
