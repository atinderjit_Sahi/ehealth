//
//  AddNewDrugViewController.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/27/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

var typesOfMedicines = [StandardDosage]()
var typesOfFrequencies = [StandardDosage]()
var standardDosage = [StandardDosage]()
var StrengthDosage = [dosage_strength]()
var medicationDrugs: Medication?


class AddNewDrugViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    var selectedCareTaker: CaretakerModel?
    var tablet = Medication()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "NewDrugFields", bundle: nil), forCellReuseIdentifier: NewDrugFieldsTableViewCell.className)
        tableView.register(UINib(nibName: "TableConsumptionDetailCell", bundle: nil), forCellReuseIdentifier: TabletConsumtionDetailTableViewCell.className)
        tableView.register(UINib(nibName: "AddButtonCell", bundle: nil), forCellReuseIdentifier: AddButtonTableViewCell.className)
        // Do any additional setup after loading the view.
        
        if medicationDrugs != nil  && medicationDrugs?.drug_name != nil && medicationDrugs?.drug_name != "" {
            labelTitle.text = "Update Medication"
           // tablet = medicationDrugs ?? Medication()
            tableView.reloadData()
        }
        
        AddNewDrugViewModel.addPopUps { (response, error) in
            
            print(response)
            typesOfMedicines = response?.data?.typesOfMedicines ?? [StandardDosage]()
            typesOfFrequencies = response?.data?.typesOfFrequencies ?? [StandardDosage]()
            standardDosage = response?.data?.standardDosage ?? [StandardDosage]()
            StrengthDosage = response?.data?.dosageStrength ?? [dosage_strength]()
            
        }
        }
      
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setData() {
        
    }
}

extension AddNewDrugViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NewDrugFieldsTableViewCell.className, for: indexPath) as? NewDrugFieldsTableViewCell else {
                fatalError()
            }
           // cell.textFieldSetUp(localMedication: medicationDrugs ?? Medication())
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TabletConsumtionDetailTableViewCell.className, for: indexPath) as? TabletConsumtionDetailTableViewCell else {
                fatalError()
            }
         //   cell.textFieldSetUp(localMedication: medicationDrugs ?? Medication())
            return cell
            
         case 2:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: AddButtonTableViewCell.className, for: indexPath) as? AddButtonTableViewCell else {
                    fatalError()
                }
                cell.buttonTitle = "addDrugInListButton"
                cell.delegate = self
            
            cell.setButtonTitle (localMedication : medicationDrugs ?? Medication())
                return cell
            
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TabletNameTableViewCell.className, for: indexPath) as? TabletNameTableViewCell else {
                fatalError()
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 206 //UITableView.automaticDimension
    }
}

//MARK:- ADD TABLET BUTTON ACTION
extension AddNewDrugViewController: AddButtonProtocol {
    func addTablet() {
        view.endEditing(true)
        var indexPath = IndexPath(row: 0, section: 0)
        let subviews = tableView.subviews
        for cell in subviews {
            if cell.isKind(of: NewDrugFieldsTableViewCell.self) {
                if !validateTextFields(cell: cell as! UITableViewCell) {
                    return
                }
            }
        }
        
        indexPath.row = 1
        if let secondCell = tableView.cellForRow(at: indexPath) {
            if !validateTextFields(cell: secondCell) {
                return
            }
        }
        
        self.addNewDrugAPI()
//        self.view.makeToast("Coming Soon")
    }
}

//MARK:- API METHODS
extension AddNewDrugViewController {
    func addNewDrugAPI() {
        
        var parameters = [String: Any]()
        if selectedCareTaker != nil {
            if selectedCareTaker?.minor_id != nil {
                parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0] as [String : Any]
            } else {
                parameters = ["cognito_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0] as [String : Any]
            }
        } else {
            parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": 0] as [String : Any]
        }
        
        if medicationDrugs == nil {
        
        parameters["drug_name"] = self.tablet.drug_name ?? ""
        parameters["usage"] = self.tablet.usage ?? ""
        parameters["pharmacy_name"] = self.tablet.pharmacy_name ?? ""
        parameters["side_effects"] = self.tablet.side_effects ?? ""
        parameters["standard_dosage"] = self.tablet.standard_dosage ?? ""
        parameters["interactions"] = self.tablet.interactions ?? ""
        parameters["frequency"] = self.tablet.frequency ?? ""
        parameters["tablet"] = self.tablet.tablet ?? ""
        parameters["to_date"] = self.tablet.to_date ?? ""
        parameters["from_date"] = self.tablet.from_date ?? ""
        parameters["rx_number"] = self.tablet.rx_number ?? ""
        parameters["prescription"] = self.tablet.prescription ?? ""
        parameters["refills"] = self.tablet.refills ?? ""
        parameters["additional_notes"] = self.tablet.additionalNotes ?? ""
            parameters["strength"] = self.tablet.strength ?? ""
        }
        else{
            
            parameters["drug_id"] = medicationDrugs?.Id
            parameters["drug_name"] = self.tablet.drug_name ?? ""
            parameters["usage"] = self.tablet.usage ?? ""
            parameters["pharmacy_name"] = self.tablet.pharmacy_name ?? ""
            parameters["side_effects"] = self.tablet.side_effects ?? ""
            parameters["standard_dosage"] = self.tablet.standard_dosage ?? ""
            parameters["interactions"] = self.tablet.interactions ?? ""
            parameters["frequency"] = self.tablet.frequency ?? ""
            parameters["tablet"] = self.tablet.tablet ?? ""
            parameters["to_date"] = self.tablet.to_date ?? ""
            parameters["from_date"] = self.tablet.from_date ?? ""
            parameters["rx_number"] = self.tablet.rx_number ?? ""
            parameters["prescription"] = self.tablet.prescription ?? ""
            parameters["refills"] = self.tablet.refills ?? ""
            parameters["additional_notes"] = self.tablet.additionalNotes ?? ""
            parameters["strength"] = self.tablet.strength ?? ""

            
        }
        
        Utility.shared.startLoader()
        AddNewDrugViewModel.addNewDrug(parameters: parameters) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.view.makeToast(response!)
                self.tablet = Medication()
                self.tableView.reloadData()
                if self.navigationController != nil {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: MedicationListViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
            }
            else {
                self.view.makeToast(error ?? "Something went wrong!")
            }
        }
    }
}

//MARK:- VALIDATIONS
extension AddNewDrugViewController {
    func validateTextFields(cell: UITableViewCell) -> Bool {
        let objValidate = Validations()
        
        if let tabletCell = cell as? NewDrugFieldsTableViewCell {
            //DRUG NAME field Validation
            var (status,message) = objValidate.validateField(tabletCell.drugNameTextField, sizeLimit: true, maxLimit: 32, minLimit: 3)
            if !status{
                switch message {
                case "Empty field":
                    self.view.makeToast("Please enter Drug Name")
                    break
                    
                case "Short length text":
                    self.view.makeToast("Drug name must be atleast 3 characters")
                    break
                    
                case "Long length text":
                    self.view.makeToast("Drug name must be maximum 32 characters")
                    break
                    
                default:
                    break
                }
                return false
            }
            self.tablet.drug_name = tabletCell.drugNameTextField.text
            
            if tabletCell.standardDosageTextField.text != "Dosage" {
                self.tablet.standard_dosage = tabletCell.standardDosageTextField.text }
            if tabletCell.standardDosageTextField.text != "Strength" {
                self.tablet.strength = tabletCell.txtfieldStrength.text } 

            
            //TABLET USAGE FIELD
            if tabletCell.usageTextField.text != "" && tabletCell.usageTextField.text != nil {
            (status,message) = objValidate.validateField(tabletCell.usageTextField, sizeLimit: true, maxLimit: 32, minLimit: 3)
            if !status{
                switch message {
              
                case "Short length text":
                    self.view.makeToast("Tablet Usage must be atleast 3 characters")
                    break
                    
                case "Long length text":
                    self.view.makeToast("Tablet Usage must be maximum 32 characters")
                    break
                    
                default:
                    break
                }
                return false
            }
            }
            self.tablet.usage = tabletCell.usageTextField.text
            
            //PHARMACY NAME FIELD
            /*(status,message) = objValidate.validateField(tabletCell.pharmacyNameTextField, sizeLimit: true, maxLimit: 32, minLimit: 3)
            if !status{
                switch message {
                case "Empty field":
                    self.view.makeToast("Please enter Pharmacy Name")
                    break
                    
                case "Short length text":
                    self.view.makeToast("Pharmacy Name must be atleast 3 characters")
                    break
                    
                case "Long length text":
                    self.view.makeToast("Pharmacy Name must be maximum 32 characters")
                    break
                    
                default:
                    break
                }
                return false
            }*/
            
            //SIDE EFFECTS FIELD
            /*(status,message) = objValidate.validateField(tabletCell.sideEffectsTextField, sizeLimit: true, maxLimit: 32, minLimit: 3)
            if !status{
                switch message {
                case "Empty field":
                    self.view.makeToast("Please enter Side Effects of Tablet")
                    break
                    
                case "Short length text":
                    self.view.makeToast("Side Effects must be atleast 3 characters")
                    break
                    
                case "Long length text":
                    self.view.makeToast("Side Effects must be maximum 32 characters")
                    break
                    
                default:
                    break
                }
                return false
            }*/
            self.tablet.side_effects = tabletCell.sideEffectsTextField.text
            
            //STANDARD DOSAGE FIELD
            /*(status,message) = objValidate.validateField(tabletCell.standardDosageTextField, sizeLimit: true, maxLimit: 2, minLimit: 1)
            if !status{
                switch message {
                case "Empty field":
                    self.view.makeToast("Please enter Standard Dosage")
                    break
                    
                default:
                    break
                }
                return false
            }
            let standardDosage = Int(tabletCell.standardDosageTextField.text!)!
            if standardDosage < 1 {
                self.view.makeToast("Standard Dosage must be atleast 1 Tablet.")
                return false
            }
            if standardDosage > 50 {
                self.view.makeToast("Standard Dosage must be maximum 50 Tablets.")
                return false
            }*/
            
            //INTERACTIONS FIELD
            /*(status,message) = objValidate.validateField(tabletCell.interfactionsTextField, sizeLimit: true, maxLimit: 32, minLimit: 3)
            if !status{
                switch message {
                case "Empty field":
                    self.view.makeToast("Please enter Interfactions")
                    break
                    
                case "Short length text":
                    self.view.makeToast("Interactions must be atleast 3 characters")
                    break
                    
                case "Long length text":
                    self.view.makeToast("Interactions must be maximum 32 characters")
                    break
                    
                default:
                    break
                }
                return false
            }*/
            self.tablet.interactions = tabletCell.interfactionsTextField.text
            return true
        }
        
        if let consumptionCell = cell as? TabletConsumtionDetailTableViewCell {
            //FREQUENCY FIELD
            var (status,message) = objValidate.validateField(consumptionCell.frequencyTextField, sizeLimit: false)
            if !status{
                switch message {
                case "Empty field":
                    self.view.makeToast("Please enter Frequency")
                    break
                    
                default:
                    break
                }
                return false
            }
//            let frequencyValue = Int(consumptionCell.frequencyTextField.text!)!
//            if frequencyValue < 1 {
//                self.view.makeToast("Frequency must be atleast 1 Tablet.")
//                return false
//            }
//            if frequencyValue > 50 {
//                self.view.makeToast("Frequency must be maximum 50 Tablets.")
//                return false
//            }
            self.tablet.frequency = consumptionCell.frequencyTextField.text
            
            //TABLET TEXT FIELD
            (status,message) = objValidate.validateField(consumptionCell.tabletTextField, sizeLimit: false)
            if !status{
                switch message {
                case "Empty field":
                    self.view.makeToast("Please enter type of medicine")
                    break
                    
                default:
                    break
                }
                return false
            }
//            let tabletValue = Int(consumptionCell.tabletTextField.text!)!
//            if tabletValue < 1 {
//                self.view.makeToast("Tablets must be atleast 1.")
//                return false
//            }
//            if tabletValue > 50 {
//                self.view.makeToast("Tablets must be maximum 50.")
//                return false
//            }
            self.tablet.tablet = consumptionCell.tabletTextField.text
        
            //FROM DATE TEXT FIELD
        /*    (status,message) = objValidate.validateField(consumptionCell.fromDateTextField, sizeLimit: false)
            if !status{
                switch message {
                case "Empty field":
                    self.view.makeToast("Please enter From date")
                    break
                    
                default:
                    break
                }
                return false
            } */
            self.tablet.from_date = consumptionCell.fromDateTextField.text
            
            //TO DATE TEXT FIELD
         /*   (status,message) = objValidate.validateField(consumptionCell.toDateTextField, sizeLimit: false)
            if !status{
                switch message {
                case "Empty field":
                    self.view.makeToast("Please enter To date")
                    break
                    
                default:
                    break
                }
                return false
            } */
            
            self.tablet.to_date = consumptionCell.toDateTextField.text
            
            if let toDate = self.tablet.to_date?.convertToDate() {
                if let fromDate = self.tablet.from_date?.convertToDate() {
                    if fromDate > toDate {
                        self.view.makeToast("From Date cannot be greater than To Date")
                        return false
                    }
                }
            }
            
            /*if let toDate = self.tablet.to_date?.convertToDate() {
                if let fromDate = self.tablet.from_date?.convertToDate() {
                    if fromDa > fromDate {
                        self.view.makeToast("To Date cannot be greater than From Date")
                        return false
                    }
                }
            }
            
            //RX NUMBER TEXT FIELD
            (status,message) = objValidate.validateField(consumptionCell.rxNumberTextField, sizeLimit: true, maxLimit: 12, minLimit: 12)
            if !status{
                switch message {
                case "Empty field":
                    self.view.makeToast("Please enter Rx Number")
                    break
                    
                case "Short length text":
                    self.view.makeToast("Rx Number should be of 12 characters")
                    break
                    
                case "Long length text":
                    self.view.makeToast("Rx Number should be of 12 characters")
                    break
                    
                default:
                    break
                }
                return false
            }*/
            self.tablet.rx_number = consumptionCell.rxNumberTextField.text
//            let rxValue = Int(consumptionCell.rxNumberTextField.text!)!
//            if rxValue < 1 {
//                self.view.makeToast("Rx Number must be atleast 1.")
//                return false
//            }
//            if rxValue > 50 {
//                self.view.makeToast("Rx Number must be maximum 50.")
//                return false
//            }
            
            /*if !consumptionCell.prescriptionNoButton.isSelected && !consumptionCell.prescriptionYesButton.isSelected {
                self.view.makeToast("Please select Prescription option")
                return false
            }
            if !consumptionCell.refillsNoButton.isSelected && !consumptionCell.refillsYesButton.isSelected {
                self.view.makeToast("Please select Refills option")
                return false
            }*/
            
            if consumptionCell.refillsYesButton.isSelected {
                self.tablet.refills = "Yes"
            }
           
            
            if consumptionCell.refillsNoButton.isSelected {
                self.tablet.refills = "No"
            }
            
            
            if consumptionCell.prescriptionYesButton.isSelected {
                self.tablet.prescription = "Yes"
            }
           
            
            if consumptionCell.prescriptionNoButton.isSelected {
                self.tablet.prescription = "No"
            }
            
            self.tablet.pharmacy_name = consumptionCell.pharmacyNameTextField.text

            if consumptionCell.textViewAdditionalNotes.text != " Additional Notes" {
                self.tablet.additionalNotes = consumptionCell.textViewAdditionalNotes.text}
            return true
        }
        
        self.view.makeToast("Something went wrong!")
        return false
    }
}
