//
//  AddNewDrugViewModel.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/28/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class AddNewDrugViewModel: NSObject {
    
    static func addNewDrug(parameters: [String: Any], completion: @escaping (_ response: String?, _ error: String?) -> Void) {

        let url = Constant.URL.addNewDrug
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        print(url)
        print(parameters)
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                print(result)
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let message = result["message"] as? String {
                            completion(message , nil)
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }

   
    }
    
    static func addPopUps(completion: @escaping (_ response: Welcome?, _ error: String?) -> Void) {

        let url = Constant.URL.popUpAddMedications
        _ = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result1, error) in
            if let result = result1 as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let message = result["message"] as? String {
                            print(result)
                            
                            do
                            {
                                let jsonData = try JSONSerialization.data(withJSONObject: result1, options: .prettyPrinted)
                                let model = try JSONDecoder().decode(Welcome.self, from: jsonData)
                                //self.delegate.getData(list: model.data!)
                                completion(model , nil)

                            }
                            catch
                            {
                                completion(nil , result["message"] as? String)

                            }
                            
                            
                            
                        } else {
                            completion(nil , result["message"] as? String)
                        }
                    } else {
                        completion(nil , result["message"] as? String)
                    }
                } else {
                    completion(nil , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }

   
    }
    
    
}
