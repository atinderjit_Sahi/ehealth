//
//  AddButtonTableViewCell.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/24/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

protocol AddButtonProtocol {
    func addTablet()
}

class AddButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var addButton: UIButton!
    var delegate: AddButtonProtocol?
    
    var buttonTitle: String? = String() {
        didSet {
//            addButton.setTitle(addDrugInListButton, for: .normal)
            addButton.setImage(UIImage(named: "addDrugInListButton"), for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setButtonTitle (localMedication : Medication) {
        
        if localMedication.Id != nil && localMedication.Id != 0 {
            addButton.setImage(UIImage(named: "bb"), for: .normal)
           // addButton.setTitle("Update Drug In List", for: .normal)
          //  addButton.setTitleColor(.white, for: .normal)
        }
       
    }
    
    @IBAction func addButtonAction(_ sender: UIButton) {
        delegate?.addTablet()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
