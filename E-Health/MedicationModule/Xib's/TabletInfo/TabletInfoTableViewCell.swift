//
//  TabletInfoTableViewCell.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/24/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class TabletInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sideEffectsTextView: UITextView!
    @IBOutlet weak var dosageTextView: UITextView!
    @IBOutlet weak var otherInfoTextView: UITextView!
    @IBOutlet weak var interactionsTextView: UITextView!
    
    var item: Medication? = Medication() {
        didSet {
            if let sideEffect = item?.side_effects?.components(separatedBy: ",") {
                for effect in sideEffect {
                    sideEffectsTextView.text += effect + "\n"
                }
            } else {
                sideEffectsTextView.text = "No Side Effects"
            }
            
            if let dosage = item?.standard_dosage {
                dosageTextView.text = dosage
            } else {
                dosageTextView.text = "N.A."
            }
            
            if let interactions = item?.interactions?.components(separatedBy: ",") {
                for effect in interactions {
                    interactionsTextView.text += effect + "\n"
                }
            } else {
                interactionsTextView.text = "N.A."
            }
            
            if let otherInfo = item?.additionalNotes {
                otherInfoTextView.text = otherInfo
            } else {
                otherInfoTextView.text = "No information"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
