//
//  TabletConsumtionDetailTableViewCell.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/24/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import DropDown

class TabletConsumtionDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var frequencyTextField: UITextField!
    @IBOutlet weak var tabletTextField: UITextField!
    @IBOutlet weak var toDateTextField: UITextField!
    @IBOutlet weak var fromDateTextField: UITextField!
    @IBOutlet weak var rxNumberTextField: UITextField!
    @IBOutlet weak var prescriptionYesButton: UIButton!
    @IBOutlet weak var prescriptionNoButton: UIButton!
    @IBOutlet weak var refillsYesButton: UIButton!
    @IBOutlet weak var refillsNoButton: UIButton!
    @IBOutlet weak var textViewAdditionalNotes: UITextView!
    @IBOutlet weak var pharmacyNameTextField: UITextField!

    var datePicker = UIDatePicker()
    var selectedTextField = UITextField()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        frequencyTextField.setBorderColor()
        tabletTextField.setBorderColor()
        toDateTextField.setBorderColor()
        fromDateTextField.setBorderColor()
        rxNumberTextField.setBorderColor()
        pharmacyNameTextField.setBorderColor()

//        frequencyTextField.text = ""
//        tabletTextField.text = ""
//        toDateTextField.text = ""
//        fromDateTextField.text = ""
//        rxNumberTextField.text = ""
//        pharmacyNameTextField.text = ""

        prescriptionYesButton.isSelected = false
        prescriptionNoButton.isSelected = false
        refillsYesButton.isSelected = false
        refillsNoButton.isSelected = false
        
        frequencyTextField.setLeftImage(leftImage: UIImage(named: "clock")!)
        tabletTextField.setLeftImage(leftImage: UIImage(named: "drug")!)
        toDateTextField.setLeftImage(leftImage: UIImage(named: "calenderMed")!)
        fromDateTextField.setLeftImage(leftImage: UIImage(named: "calenderMed")!)
       // textFieldSetUp()
        textViewSetUp()
        textFieldSetUp(localMedication : medicationDrugs ?? Medication())
    }
    
    //BUTTON ACTIONS
    @IBAction func prescriptionYesButtonAction(_ sender: Any) {
        prescriptionYesButton.isSelected = true
        prescriptionNoButton.isSelected = false
    }
    @IBAction func prescriptionNoButtonAction(_ sender: Any) {
        prescriptionYesButton.isSelected = false
        prescriptionNoButton.isSelected = true
    }
    @IBAction func refillsNoButtonAction(_ sender: Any) {
        refillsYesButton.isSelected = false
        refillsNoButton.isSelected = true
    }
    @IBAction func refillsYesButtonAction(_ sender: Any) {
        refillsYesButton.isSelected = true
        refillsNoButton.isSelected = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textViewSetUp() {
        textViewAdditionalNotes.text = " Additional Notes" //Placeholder
        textViewAdditionalNotes.textColor = Color.textFieldPlaceholderColor
        textViewAdditionalNotes.autocorrectionType = UITextAutocorrectionType.no
        pharmacyNameTextField.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        textViewAdditionalNotes.addBorder(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
    }

    func textFieldSetUp(localMedication : Medication) {
        frequencyTextField.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        tabletTextField.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        toDateTextField.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        fromDateTextField.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        rxNumberTextField.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        
        frequencyTextField.attributedPlaceholder = NSAttributedString(string: "Enter Frequency*", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        tabletTextField.attributedPlaceholder = NSAttributedString(string: "Type of Medicine*", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        toDateTextField.attributedPlaceholder = NSAttributedString(string: "To", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        fromDateTextField.attributedPlaceholder = NSAttributedString(string: "From", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        pharmacyNameTextField.attributedPlaceholder = NSAttributedString(string: "Pharmacy Name", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        
        
        if localMedication.frequency != "" && localMedication.frequency != "N.A." && localMedication.frequency != nil  {
            frequencyTextField.text = localMedication.frequency
        }
        
        if localMedication.tablet != "" && localMedication.tablet != "N.A." && localMedication.tablet != nil {
            tabletTextField.text = localMedication.tablet
        }
        
        if localMedication.to_date != "" && localMedication.to_date != "N.A." && localMedication.to_date != nil {
            toDateTextField.text = localMedication.to_date
        }
        
        if localMedication.from_date != "" && localMedication.from_date != "N.A." && localMedication.from_date != nil {
            fromDateTextField.text = localMedication.from_date
        }
        if localMedication.rx_number != "" && localMedication.rx_number != "N.A." && localMedication.rx_number != nil {
            rxNumberTextField.text = localMedication.rx_number
        }
        
        if localMedication.additionalNotes != nil && localMedication.additionalNotes != "N.A."  {
            textViewAdditionalNotes.text = localMedication.additionalNotes
        }
        else {
            textViewAdditionalNotes.text = " Additional Notes"
        }
        
        if localMedication.pharmacy_name != "" && localMedication.pharmacy_name != "N.A." && localMedication.pharmacy_name != nil  {
        pharmacyNameTextField.text = localMedication.pharmacy_name
        }
        
        if localMedication.prescription != "" && localMedication.prescription != "N.A." && localMedication.prescription != nil {
            
            if localMedication.prescription ?? "a" == "Yes" {
                prescriptionYesButton.isSelected = true
            }
            else{
                prescriptionNoButton.isSelected = true
            }
        }
        
        if localMedication.refills != "" && localMedication.refills != "N.A." && localMedication.refills != nil {
            if localMedication.refills ?? "a" == "Yes" {
                refillsYesButton.isSelected = true
            }
            else{
                refillsNoButton.isSelected = true
            }        }
    }
    
    
    
    func initdropDown (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                textField.text = item
               //self.selectedBloodType = Constant.masterData?.bloodtypes[index]
                dropDown.hide()
            }
            dropDown.show()
        }
    }
    
}

extension TabletConsumtionDetailTableViewCell: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textViewAdditionalNotes.text == " Additional Notes" {
            textViewAdditionalNotes.text = ""
            textViewAdditionalNotes.textColor = Color.textFieldPlaceholderColor
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textViewAdditionalNotes.text.isEmpty {
            textViewAdditionalNotes.text = " Additional Notes"
            textViewAdditionalNotes.textColor = Color.textFieldPlaceholderColor
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count // for Swift use count(newText)
        return numberOfChars <= 128;
    }
}




extension TabletConsumtionDetailTableViewCell: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == toDateTextField {
            self.showDatePicker(textField: textField)
        }
        else if textField == fromDateTextField {
            self.showDatePicker(textField: textField)
        }
     else if textField == frequencyTextField {
            var data = [String]()
            for dataMed in typesOfFrequencies {
                if let dosage = dataMed.frequencyName {
                data.append( "\(dosage)")
                }
            }
            if typesOfMedicines.count == 0 {
                Utility.shared.showToast(message: "No data available!")
                return false
            }
            self.initdropDown(textField: textField, dataSource: data)
            return false
        }
        
        else if textField == tabletTextField {
               var data = [String]()
               for dataMed in typesOfMedicines {
                   if let dosage = dataMed.typeOfMedicine {
                   data.append( "\(dosage)")
                   }
               }
               if typesOfMedicines.count == 0 {
                   Utility.shared.showToast(message: "No data available!")
                   return false
               }
               self.initdropDown(textField: textField, dataSource: data)
               return false
           }
        
        return true
    }
    

    
  
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == frequencyTextField || textField == tabletTextField  {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 2, newCharacter: string)
            return result
        }
        if textField == rxNumberTextField {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 12, newCharacter: string)
            return result
        }
        
        if textField == pharmacyNameTextField {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 32, newCharacter: string)
            return result
        }
        return true
    }
}

extension TabletConsumtionDetailTableViewCell {
    func showDatePicker(textField: UITextField){
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat
         //"dd MMM, yyyy"
        var date = Date()
        if textField.text != "" {
            date = formater.date(from: textField.text!) ?? Date()
        }
        
        let screenWidth = UIScreen.main.bounds.width
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        //datePicker.minimumDate = Date()  //Calendar.current.date(byAdding: .year, value: -110, to: Date())
//        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 10, to: Date())
        datePicker.setDate(date, animated: true)
        
        if #available(iOS 14.0, *)
        {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()

//            datePicker.datePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        textField.inputAccessoryView = toolbar
        textField.inputView = datePicker
        selectedTextField = textField
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = Constant.dateFormat
        selectedTextField.text = formatter.string(from: datePicker.date)
        self.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.endEditing(true)
    }
}
