//
//  TabletFrequencyTableViewCell.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/27/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class TabletFrequencyTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var tabletCountLabel: UILabel!
    @IBOutlet weak var numberOfDaysLabel: UILabel!
    
    var item: Medication? = Medication() {
        didSet {
            hoursLabel.text = "\(item?.frequency ?? "0")"
            tabletCountLabel.text = "\(item?.tablet ?? "0")"
            if item?.days != nil {
                numberOfDaysLabel.text = "\(item?.days ?? 0) Days"
            } else {
                numberOfDaysLabel.text = "No Limit"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.containerView.dropShadow()
        print("atinderjit")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
