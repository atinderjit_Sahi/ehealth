//
//  NewDrugFieldsTableViewCell.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/27/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import DropDown


class NewDrugFieldsTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var drugNameTextField: UITextField!
    @IBOutlet weak var usageTextField: UITextField!
    @IBOutlet weak var pharmacyNameTextField: UITextField!
    @IBOutlet weak var sideEffectsTextField: UITextField!
    @IBOutlet weak var standardDosageTextField: UITextField!
    @IBOutlet weak var interfactionsTextField: UITextField!
    @IBOutlet weak var txtfieldStrength: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        drugNameTextField.setBorderColor()
        usageTextField.setBorderColor()
      //  pharmacyNameTextField.setBorderColor()
        sideEffectsTextField.setBorderColor()
        standardDosageTextField.setBorderColor()
        interfactionsTextField.setBorderColor()
       txtfieldStrength.setBorderColor()
    /*    drugNameTextField.text = ""
        usageTextField.text = ""
      //  pharmacyNameTextField.text = ""
        sideEffectsTextField.text = ""
        standardDosageTextField.text = ""
        interfactionsTextField.text = "" */
        
        textFieldSetUp(localMedication: medicationDrugs ?? Medication())
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textFieldSetUp(localMedication : Medication) {
            drugNameTextField.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
            usageTextField.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
           // pharmacyNameTextField.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        sideEffectsTextField.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        standardDosageTextField.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
        interfactionsTextField.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
         txtfieldStrength.borderedTextField(borderColor: Color.textFieldBorderColor, borderWidth: 0.5, cornerRadius: 6)
            
        txtfieldStrength.attributedPlaceholder = NSAttributedString(string: "Strength", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        drugNameTextField.attributedPlaceholder = NSAttributedString(string: "Drug Name*", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        usageTextField.attributedPlaceholder = NSAttributedString(string: "Usage Instructions", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
          //  pharmacyNameTextField.attributedPlaceholder = NSAttributedString(string: "Pharmacy Name", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        sideEffectsTextField.attributedPlaceholder = NSAttributedString(string: "Side Effect", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        standardDosageTextField.attributedPlaceholder = NSAttributedString(string: "Dosage", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        interfactionsTextField.attributedPlaceholder = NSAttributedString(string: "Interactions", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        
        if localMedication.drug_name != "" && localMedication.drug_name != "N.A." && localMedication.drug_name != nil  {
        drugNameTextField.text = localMedication.drug_name
        }
        
        if localMedication.usage != "" && localMedication.usage != "N.A." && localMedication.usage != nil {
            usageTextField.text = localMedication.usage
        }
//        if localMedication.pharmacy_name != "" && localMedication.pharmacy_name != "N.A." {
//        pharmacyNameTextField.text = localMedication.pharmacy_name
//        }
        if localMedication.side_effects != "" && localMedication.side_effects != "N.A." && localMedication.side_effects != nil {
        sideEffectsTextField.text = localMedication.side_effects
        }
        
        if localMedication.standard_dosage != "" && localMedication.standard_dosage != "N.A." && localMedication.standard_dosage != nil {

        standardDosageTextField.text = localMedication.standard_dosage
        }
        
        if localMedication.strength != "" && localMedication.strength != "N.A." && localMedication.strength != nil {

        txtfieldStrength.text = localMedication.strength
        }
        
        if localMedication.interactions != "" && localMedication.interactions != "N.A." && localMedication.interactions != nil  {

        interfactionsTextField.text = localMedication.interactions
        }
   
    //        textFieldInsuranceDocument.setRightImage(rightImage: UIImage(named: "dropDown")!)
    //        textFieldInsuranceTypes.setRightImage(rightImage: UIImage(named: "dropDown")!)
        }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == drugNameTextField || textField == usageTextField || textField == sideEffectsTextField || textField == interfactionsTextField  {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 32, newCharacter: string)
            return result
        }
        if textField == standardDosageTextField {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 2, newCharacter: string)
            return result
        }
        return true
    }
    
    
    @IBAction func actionStrengthDropdown(_ sender: UIButton) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == standardDosageTextField {
            var data = [String]()
            
            data.insert(contentsOf: ["Dosage"], at: 0)
            for (_, value) in standardDosage.enumerated() {

                    if let dosage = value.dosage {
                    data.append( "\(dosage)")
            }
            }
            print(data)

            if typesOfMedicines.count == 0 {
                Utility.shared.showToast(message: "No data available!")
                return false
            }
            self.initdropDown(textField: textField, dataSource: data)
            return false
        }
        
        else if textField == txtfieldStrength {
            
            var data = [String]()
            
            data.insert(contentsOf: ["Strength"], at: 0)
            for (_, value) in StrengthDosage.enumerated() {

                    if let dosage = value.name {
                    data.append( "\(dosage)")
            }
            }
            print(data)

            if StrengthDosage.count == 0 {
                Utility.shared.showToast(message: "No data available!")
                return false
            }
            self.initdropDown(textField: textField, dataSource: data)
            return false
        }
        return true
    }
    
    
    func initdropDown (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                textField.text = item
               //self.selectedBloodType = Constant.masterData?.bloodtypes[index]
                dropDown.hide()
            }
            dropDown.show()
        }
    }
}
