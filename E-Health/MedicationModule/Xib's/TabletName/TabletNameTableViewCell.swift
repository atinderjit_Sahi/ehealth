//
//  TabletNameTableViewCell.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/24/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class TabletNameTableViewCell: UITableViewCell {

    @IBOutlet weak var tabletNameLabel: UILabel!
    @IBOutlet weak var usageLabel: UILabel!
    @IBOutlet weak var rxNumber: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    var item: Medication? = Medication() {
        didSet {
            tabletNameLabel.text = item?.drug_name
            usageLabel.text = item?.usage
            rxNumber.text = "RX : " + (item?.rx_number ?? "N.A")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.dropShadow()
        
        tabletNameLabel.text = "Nucynta Oral Tablet 75 mg"
        usageLabel.text = "By oral route"
        rxNumber.text = "RX : 23-JUNE-2019"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
