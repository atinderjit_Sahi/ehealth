//
//  SearchMedicationViewController.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/23/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class SearchMedicationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var selectedCareTaker: CaretakerModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.tableFooterView = UIView()
        searchBar.setSearchBarUI()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addNewTabletButtonAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: StoryboardNames.Medication, bundle: Bundle.main).instantiateViewController(withIdentifier: AddNewDrugViewController.className) as? AddNewDrugViewController
        vc?.selectedCareTaker = self.selectedCareTaker
        navigationController?.pushViewController(vc!, animated: true)
    }
}

extension SearchMedicationViewController: UITableViewDataSource, UITableViewDelegate, SearchMedicationProtocol, searchMedicationDelegate {
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: AddMedicationButtonTableViewCell.className, for: indexPath) as? AddMedicationButtonTableViewCell else {
                fatalError()
            }
            cell.delegate = self
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchMedicationTableViewCell.className, for: indexPath) as? SearchMedicationTableViewCell else {
            fatalError()
        }
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 206 //UITableView.automaticDimension
    }
    
    func addTablet(sender: UIButton) {
        let vc = UIStoryboard.init(name: StoryboardNames.Medication, bundle: Bundle.main).instantiateViewController(withIdentifier: AddTabletViewController.className) as? AddTabletViewController
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    func addSelectedMedicaton(sender: UIButton) {
        let vc = UIStoryboard.init(name: StoryboardNames.Medication, bundle: Bundle.main).instantiateViewController(withIdentifier: AddNewDrugViewController.className) as? AddNewDrugViewController
        vc?.selectedCareTaker = self.selectedCareTaker
        navigationController?.pushViewController(vc!, animated: true)
//        self.view.makeToast("Coming Soon")
    }
}

extension SearchMedicationViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        Utility.shared.showToast(message: "Coming Soon")
        searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        Utility.shared.showToast(message: "Coming Soon")
    }
}
