//
//  SearchMedicationTableViewCell.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/23/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

protocol SearchMedicationProtocol {
    func addTablet(sender: UIButton)
}

class SearchMedicationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var medicationName: UILabel!
    @IBOutlet weak var doseLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var delegate: SearchMedicationProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        containerView.dropShadow()
        medicationName.text = "Nucynta Oral Tablet"
        doseLabel.text = "75 mg"
    }    
    
    @IBAction func addMedicationButton(_ sender: UIButton) {
        delegate?.addTablet(sender: sender)
        
//        if addButton.isSelected == false {
//            addButton.isSelected = true
//        } else {
//            addButton.isSelected = false
//        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
