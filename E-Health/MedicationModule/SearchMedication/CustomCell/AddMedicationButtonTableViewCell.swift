//
//  AddMedicationButtonTableViewCell.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/23/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

protocol searchMedicationDelegate {
    func addSelectedMedicaton(sender: UIButton)
}

class AddMedicationButtonTableViewCell: UITableViewCell {

    var delegate: searchMedicationDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func addNewMedicationButtonAction(_ sender: UIButton) {
        delegate?.addSelectedMedicaton(sender: sender)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
