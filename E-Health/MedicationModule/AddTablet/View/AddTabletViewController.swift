//
//  AddTabletViewController.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/24/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class AddTabletViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "TabletName", bundle: nil), forCellReuseIdentifier: TabletNameTableViewCell.className)
        tableView.register(UINib(nibName: "PharmaNameCell", bundle: nil), forCellReuseIdentifier: PharmaNameTableViewCell.className)
        tableView.register(UINib(nibName: "TabletInfoCell", bundle: nil), forCellReuseIdentifier: TabletInfoTableViewCell.className)
        tableView.register(UINib(nibName: "AddButtonCell", bundle: nil), forCellReuseIdentifier: AddButtonTableViewCell.className)
    }
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension AddTabletViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TabletNameTableViewCell.className, for: indexPath) as? TabletNameTableViewCell else {
                fatalError()
            }
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PharmaNameTableViewCell.className, for: indexPath) as? PharmaNameTableViewCell else {
                fatalError()
            }
            return cell
            
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TabletInfoTableViewCell.className, for: indexPath) as? TabletInfoTableViewCell else {
                fatalError()
            }
            cell.item = Medication()
            return cell
            
        case 3:
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AddButtonTableViewCell.className, for: indexPath) as? AddButtonTableViewCell else {
            fatalError()
        }
        cell.delegate = self
        return cell
            
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TabletNameTableViewCell.className, for: indexPath) as? TabletNameTableViewCell else {
                fatalError()
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 206 //UITableView.automaticDimension
    }
}


extension AddTabletViewController: AddButtonProtocol {
    func addTablet() {
        let vc = UIStoryboard.init(name: StoryboardNames.Medication, bundle: Bundle.main).instantiateViewController(withIdentifier: AddTabletDetailsViewController.className) as? AddTabletDetailsViewController
        navigationController?.pushViewController(vc!, animated: true)
    }    
}
