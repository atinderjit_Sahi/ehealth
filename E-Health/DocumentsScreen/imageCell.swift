//
//  imageCell.swift
//  SeasiaBaseProject
//
//  Created by Manoj Kashyap on 7/4/18.
//  Copyright © 2018 Shashank Kumar. All rights reserved.
//

import Foundation
import UIKit
//import Kingfisher

protocol ImageCellView {
    var iconImageString: UIImage? {get set}
}

class imageCell: UICollectionViewCell, ImageCellView {
    
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var selectedIconImage: UIImageView!
    @IBOutlet weak var errorView: UIView!
    static var identifier = "SelectedImageCell"
    var iconImageString: UIImage? {
        didSet {
            if let iconImageString = iconImageString {
                selectedIconImage.image =  iconImageString
            }
        }
    }
//        override func awakeFromNib() {
//        super.awakeFromNib()
//        //        menuIconImage.layer.cornerRadius = userImage.frame.height / 2
//        //        menuIconImage.layer.masksToBounds = true
//    }
    
}
