//
//  AddDocumentViewController.swift
//  E-Health
//
//  Created by Prabjot Kaur on 6/29/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import DropDown
import OpalImagePicker
import AWSMobileClient
import MobileCoreServices
import PDFKit

protocol MedicalReportAdded {
    func reloadMedicalReportList()
}

class AddDocumentViewController: UIViewController {
    
    //MARK: Properties
    
    var datePicker = UIDatePicker()
    var selectedTextField = UITextField()
    
    @IBOutlet weak var lblTitle: UILabel!
    var pdfUrlArray : [Data]?
    var displayItemArray = [Any]()
    let MaxSelecetdImages = 1
    var selectedImages = [UIImage]()
    var documentType : String = ""
    var fileType: String?
    var isLabReport = Int()
    var selectedCareTaker: CaretakerModel?
    var allDocumentTypeList = [DocumentListingModel]()
    var selectedDocumentType = DocumentListingModel()
    
    var labReportTypeArray = [DocumentListingModel]()
    var searchLapReportTypeArray = [DocumentListingModel]()
//    var selectedLapReportTypeArray = [DocumentListingModel]()
//    var selectedLabReportType = DocumentListingModel()
    var delegate : MedicalReportAdded?
    var localDocTyp : String = ""
    
    //MARK: - Outlets
    @IBOutlet weak var labelUploadDocument: UILabel!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var imagesCollectionViewContainer: UIView!
    @IBOutlet weak var enterRecordButton: UIButton!
    @IBOutlet weak var scanUploadBarcode: UIButton!
    @IBOutlet weak var scanUploadBarcodeView: UIView!
    @IBOutlet weak var enterRecordView: UIView!
    @IBOutlet weak var textFieldSelectDocumentType: UITextField!
    @IBOutlet weak var viewSelectDocumentType: UIView!
    @IBOutlet weak var textFieldFileName: UITextField!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionViewContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var saveButton: UIButton!
    
    //Search List
    @IBOutlet var searchLabReportTypeView: UIView!
    @IBOutlet weak var labReportTypesListTableView: UITableView!
    @IBOutlet weak var LabReportsTypesView: UIView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var saveLabTypesButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    @IBOutlet weak var txtfieldDateVac: UITextField!
    @IBOutlet weak var txtViewPurpose: UITextView!
    @IBOutlet weak var txtViewVaccineFor: UITextView!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var topUploadViewVac: NSLayoutConstraint!
    
    @IBOutlet weak var heightBGView: NSLayoutConstraint!
    @IBOutlet weak var heightDocTyp: NSLayoutConstraint!
    @IBOutlet weak var txtViewNotes: UITextView!
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBg.isHidden = true
        heightBGView.constant = 0
        heightDocTyp.constant = 0
        topUploadViewVac.constant = 10
      //  topUploadViewVac.constant = 20
        localDocTyp = documentType
        if Constant.masterData != nil {
            labReportTypeArray = Constant.masterData!.labReports
            searchLapReportTypeArray = labReportTypeArray
            labReportTypesListTableView.reloadData()
        }
        
        if documentType == "7" || documentType == "8" {
            viewSelectDocumentType.isHidden = false
        } else {
            viewSelectDocumentType.isHidden = true
        }
//        textFieldSelectDocumentType.setUnderLine()
//        textFieldFileName.setUnderLine()
        textFieldSetUp()
        
        //Hide Tab Bar
        tabBarController?.tabBar.isHidden = true
        scanUploadBarcode.customizeButton(titleTopInset: 80)
        enterRecordButton.customizeButton(titleTopInset: 80)
        scanUploadBarcodeView.dropShadow(cornerRadius: 5, shadowOpacity: 0.25)
        enterRecordView.dropShadow(cornerRadius: 5, shadowOpacity: 0.25)
        
        if documentType == "6"  {
            lblTitle.text = "Add Immunization/Vaccine"
            txtViewVaccineFor.text = " Vaccine for"
            txtViewPurpose.text = " Purpose" //Placeholder
            topUploadViewVac.constant = 140
            enterRecordView.isHidden = true
            viewBg.isHidden = false
          //  topUploadViewVac.constant = 225
            heightBGView.constant = 170
            txtfieldDateVac.setBorderColor()
            textViewSetUp()
            txtfieldDateVac.attributedPlaceholder = NSAttributedString(string: "Date", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
            
        }
        
        else if documentType == "7" {
            heightDocTyp.constant = 48
            topUploadViewVac.constant = 80
            enterRecordView.isHidden = true
            lblTitle.text = "Add Medical Record"
            heightBGView.constant = 120
            txtViewVaccineFor.isHidden = true
            viewBg.isHidden = false
            txtfieldDateVac.setBorderColor()
            txtfieldDateVac.attributedPlaceholder = NSAttributedString(string: "Orignal Date", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
            txtViewPurpose.text = " Categorization"
            textViewSetUp()
         }
        
        else if documentType == "8"  {
           // lblTitle.text = "Add Immunization/Vaccine"
            heightDocTyp.constant = 48
            topUploadViewVac.constant = -40
            heightBGView.constant = 0
            enterRecordView.isHidden = true
            textViewSetUp()
            
        }
        
        else {
            enterRecordView.isHidden = false
            
            if documentType == "3" {
                heightDocTyp.constant = 0
                topUploadViewVac.constant = -50
                heightBGView.constant = 0
                textViewSetUp()
            }
        }
        
        searchLabReportTypeViewUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        manageImagesCollectionViewAppearance()
    }
    
   
    
    @IBAction func actionDateSelect(_ sender: UIButton) {
    }
    
 
    
    func textViewSetUp() {
        txtViewPurpose.textColor = Color.textFieldPlaceholderColor
        txtViewPurpose.autocorrectionType = UITextAutocorrectionType.no
        txtViewPurpose.addBorder(borderColor: UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0), borderWidth: 0.8, cornerRadius: 4)
       //Placeholder
        txtViewVaccineFor.textColor = Color.textFieldPlaceholderColor
        txtViewVaccineFor.autocorrectionType = UITextAutocorrectionType.no
        txtViewVaccineFor.addBorder(borderColor: UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0), borderWidth: 0.8, cornerRadius: 4)
        
        txtViewNotes.text = " Notes (Optional)" //Placeholder
        txtViewNotes.textColor = Color.textFieldPlaceholderColor
        txtViewNotes.autocorrectionType = UITextAutocorrectionType.no
        txtViewNotes.addBorder(borderColor: UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0), borderWidth: 0.8, cornerRadius: 4)
    }
    
    func textFieldSetUp() {
        textFieldSelectDocumentType.borderedTextField(borderColor: UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0), borderWidth: 0.8, cornerRadius: 4)
        if documentType == "4" {
            textFieldSelectDocumentType.attributedPlaceholder = NSAttributedString(string: "Select Lab Report Type*", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        } else {
            textFieldSelectDocumentType.attributedPlaceholder = NSAttributedString(string: "Select Document Type*", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        }
        
//        textFieldSelectDocumentType.setRightImage(rightImage: UIImage(named: "dropDown")!)
        
        textFieldFileName.borderedTextField(borderColor: UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0), borderWidth: 0.8, cornerRadius: 4)
        
        textFieldFileName.attributedPlaceholder = NSAttributedString(string: "Enter File Name*", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
    }
    
    func searchLabReportTypeViewUI() {
        searchLabReportTypeView.frame = self.view.frame
        searchLabReportTypeView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        searchBar.delegate = self
        
        labReportTypesListTableView.tableFooterView = UIView()
        searchBar.showsScopeBar = false
        searchBar.placeholder = "Search Lab Report Type"
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func saveTypesButtonAction(_ sender: Any) {
        searchBar.endEditing(true)
        hideAlertWithBounceEffect(myView: searchLabReportTypeView)
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        
        view.endEditing(true)
        if textFieldSelectDocumentType.text == "" {
            if documentType == "7" {
                Utility.shared.showToast(message: "Please select document type")
                return
            } else if isLabReport == 1 {
                Utility.shared.showToast(message: "Please select lab report type")
                return
            }
        }
        if displayItemArray.count == 0 {
            Utility.shared.showToast(message: "Please select file to upload")
            return
        } else if textFieldFileName.text == "" {
            Utility.shared.showToast(message: "Please enter file name")
            return
        } else if textFieldFileName.text!.count < 3 {
            Utility.shared.showToast(message: "File name should be minimum 3 characters")
            return
        } else if textFieldFileName.text!.count > 50 {
            Utility.shared.showToast(message: "File name should be maximum 50 characters")
            return
        }
        
        
        
        if documentType == "3" {
            var parameters = [String: Any]()
            if selectedCareTaker != nil {
                
                if selectedCareTaker?.minor_id != nil {
                    parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": String(describing: selectedCareTaker?.minor_id ?? 0)] as [String : Any]
                } else {
                    parameters = ["cognito_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": "0"] as [String : Any]
                }
                
            } else {
                parameters = ["cognito_id": AWSMobileClient.default().username ?? "", "minor_id": "0"] as [String : Any]
            }
                        
            parameters["document_name"] = textFieldFileName.text ?? ""
            parameters["insurance_document"] = ""
            parameters["payer_name"] = ""
            parameters["member_services"] = ""
            parameters["insurance_types"] = ""
            parameters["member_id"] = ""
            parameters["group_id"] = ""
            parameters["contract_type"] = ""
            parameters["plan_codes"] = ""
            parameters["upload_type"] = "scan"
            if txtViewNotes.text != " Notes (Optional)" {
                parameters["additional_notes"] = txtViewNotes.text

            }
            else {
                parameters["additional_notes"] = ""

            }
            

            if pdfUrlArray == nil {
                addInsuranceRecordApi(params: parameters, frontImage: displayItemArray.first as? UIImage, backImage: nil, pdfUrlArray: nil)
            } else {
                addInsuranceRecordApi(params: parameters, frontImage: nil, backImage: nil, pdfUrlArray: pdfUrlArray)
            }
            
        } else {
            uploadDocument()
        }
    }
    
    @IBAction func enterRecordButtonAction(_ sender: Any) {
        view.endEditing(true)
        
        switch documentType {
        case "4":
            self.view.makeToast("Coming Soon")
        case "6":
            self.view.makeToast("Coming Soon")
        case "3":
            goToAddInsuranceRecordView()
        case "8":
            self.view.makeToast("Coming Soon")
        case "5":
            self.view.makeToast("Coming Soon")
        default:
            self.view.makeToast("Coming Soon")
        }
    }
    
    @IBAction func documentTypeDropDown(_ sender: Any) {
        if isLabReport == 1 {
            showAlertWithBounceEffect(myView: searchLabReportTypeView, onWindow: true)
            return
        }
        if Constant.masterData?.documentTypes.count != 0 {
            let dataSource = Constant.masterData?.documentTypes.map({$0.name})
            if dataSource != nil {
            self.initdropDown(textField: textFieldSelectDocumentType, dataSource: dataSource as! [String])
            return
            }
        } else {
            Utility.shared.showToast(message: "No document type available!")
        }
    }
    
    func goToAddInsuranceRecordView() {
        
        DispatchQueue.main.async {

        guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: AddInsuranceRecordViewController.className) as? AddInsuranceRecordViewController else {return}
            vc.documentType = self.documentType ?? "0"
            vc.fileType = self.fileType ?? "0"
            if self.selectedCareTaker != nil {
                vc.selectedCareTaker = self.selectedCareTaker!
        }
            self.navigationController?.pushViewController(vc, animated: true) }
    }
    
    @IBAction func uploadDocumentButtonAction(_ sender: Any) {
        self.presentActionSheet()
    }
    
    func addInsuranceRecordApi(params: [String: Any], frontImage: UIImage?, backImage: UIImage?, pdfUrlArray: [Any]?) {
        
        Utility.shared.startLoader()
        InsuaranceRecordViewModel.addInsuranceRecord(isUploadDoc: true, params: params, insuranceFrontImage: frontImage, insuranceBackImage: nil, pdfArray: pdfUrlArray) { (message, error) in
            Utility.shared.stopLoader()
            self.saveButton.isUserInteractionEnabled = true
            if error != nil {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            } else {
                self.textFieldFileName.text = ""
                self.textFieldSelectDocumentType.text = ""
                self.displayItemArray.removeAll()
                self.pdfUrlArray?.removeAll()
                self.manageImagesCollectionViewAppearance()
                Utility.shared.showToast(message: message ?? "Document uploaded successfully")
                self.popToDocumentListingController()
            }
        }
    }
    
    func uploadDocument() {
        
        saveButton.isUserInteractionEnabled = false
        var params = [String: Any]()
        
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                
                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": String(describing: selectedCareTaker?.minor_id ?? 0), "name": textFieldFileName.text ?? "", "file_type": fileType ?? "0", "document_type": documentType ?? "0", "is_lab_report": "0"] as [String : Any]
                
            } else {
                
                params = ["cognito_user_id": String(describing: selectedCareTaker?.caretakerSub ?? "0"), "minor_id": "0", "name": textFieldFileName.text ?? "", "file_type": fileType ?? "0", "document_type": documentType ?? "0", "is_lab_report": "0"] as [String : Any]
            }
            
        } else {
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": "0", "name": textFieldFileName.text ?? "", "file_type": fileType ?? "0", "document_type": documentType ?? "0", "is_lab_report": "0"] as [String : Any]
        }
        
        if isLabReport == 1 {
            params["is_lab_report"] = "1"
        }
        
        if  (lblTitle.text == "Add Immunization/Vaccine")  {
            
            params["vaccine_date"] = txtfieldDateVac.text
            
            if txtViewPurpose.text != " Vaccine for" {
            params["vaccine_purpose"] = txtViewPurpose.text
            }
            if txtViewVaccineFor.text != " Purpose" {
            params["vaccine_for"] = txtViewVaccineFor.text
            }
            if txtViewNotes.text != " Notes (Optional)" {
                params["additional_notes"] = txtViewNotes.text
            }
            
        }
        
        if (lblTitle.text == "Add Medical Record") {
            params["medical_original_date"] = txtfieldDateVac.text
         if txtViewPurpose.text != " Categorization" {
            params["medical_categorization"] = txtViewPurpose.text }
            if txtViewNotes.text != " Notes (Optional)" {
                params["additional_notes"] = txtViewNotes.text

            }
            
        }
        
        if localDocTyp == "8"{
            if txtViewNotes.text != " Notes (Optional)" {
                params["additional_notes"] = txtViewNotes.text

            }
        }
        
        
        print(params)
        print(displayItemArray)
        Utility.shared.startLoader()
        DocumentListingViewModel.uploadDocuments(params: params as [String : AnyObject], documentImage: displayItemArray, pdfData: pdfUrlArray) { (response, error) in
            
            Utility.shared.stopLoader()
            self.saveButton.isUserInteractionEnabled = true
            if error != nil {
                if error == "Response status code was unacceptable: 405." {
                    Utility.shared.showToast(message: "This name is already taken. Please choose a different name")
                } else {
                    Utility.shared.showToast(message: error!)
                }
            } else {
                self.textFieldFileName.text = ""
                self.textFieldSelectDocumentType.text = ""
                self.displayItemArray.removeAll()
                self.pdfUrlArray?.removeAll()
                self.manageImagesCollectionViewAppearance()
                Utility.shared.showToast(message: response ?? "Document file uploaded successfully")
                self.popToDocumentListingController()
            }
        }
    }
    
    func popToDocumentListingController() {
        guard let viewControllers = navigationController?.viewControllers else { return }
        
        // Check if disease registry controller is already into the navigation stack. It will be in the stack incase user is saving follow up and disease registry forms
        for documentListingVC in viewControllers {
            if let vc = documentListingVC as? DocumentListingViewController {
//                vc.fileType = self.fileType
//                vc.documentType = self.documentType
//                if self.selectedCareTaker != nil {
//                    vc.selectedCareTaker = self.selectedCareTaker
//                }
                navigationController?.popToViewController(vc, animated: true)
                return
            }
        }
        if fileType == "2" {
            self.delegate?.reloadMedicalReportList()
        }
    }
    
    func manageImagesCollectionViewAppearance() {
        DispatchQueue.main.async {
            if self.displayItemArray.count == 0 {
                if self.localDocTyp == "7" {
                    self.topUploadViewVac.constant = 80 }
                else if self.localDocTyp == "6" {
                    self.topUploadViewVac.constant = 140
                }
                else if self.localDocTyp == "3" {
                    self.topUploadViewVac.constant = -50
                }
                else {
                    self.topUploadViewVac.constant = 0
                }
                self.textFieldFileName.isHidden = true
                self.labelUploadDocument.isHidden = true
                self.imagesCollectionViewContainer.isHidden = true
             
            } else {
               
                if self.localDocTyp == "6" {
                    self.topUploadViewVac.constant = 185
                }
                else if self.localDocTyp == "7" {
                    self.topUploadViewVac.constant = 120
                }
                else {
                    self.topUploadViewVac.constant = 0
                }
                self.textFieldFileName.isHidden = false
                self.labelUploadDocument.isHidden = false
                // collectionViewHeight.constant = 140
                // self.collectionViewContainerHeight.constant = 184
                self.imagesCollectionViewContainer.isHidden = false
                self.imagesCollectionView.reloadData()
            }
        }
    }

}

//MARK:- Collection View Delegates
extension AddDocumentViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    /// - Returns: Returns the `UICollectionViewCell`
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = imageCell.identifier
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? imageCell else {
            fatalError()
        }
        
//        let imageData1 = UIImageJPEGRepresentation(selectedImages[indexPath.item], CGFloat(1))!
//        let imgData: NSData = NSData(data: imageData1)
//        let imageSize: Int = (imgData.length/1024)/1024
        
//        if imageSize > 2 {
//            cell.errorView.isHidden = false
//        }else{
            cell.errorView.isHidden = true
//        }
        let item = displayItemArray[indexPath.item]
        if let img = item as? UIImage {
            DispatchQueue.main.async {
                cell.selectedIconImage.image = img
            }
        } else {
            if let img = pdfThumbnail(data: item as! Data) {
                DispatchQueue.main.async {
                    cell.selectedIconImage.image = img
                }
            }
        }
        
        cell.crossBtn.addTarget(self, action: #selector(self.deleteImage), for: .touchUpInside)
        cell.crossBtn.tag = indexPath.item
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return displayItemArray.count
//        return selectedImages.count
    }
    
    func pdfThumbnail(data: Data) -> UIImage? {
        if #available(iOS 11.0, *) {
            let page = PDFDocument(data: data)?.page(at: 0)
            let pageSize = page?.bounds(for: .mediaBox)
            let pdfScale = 240 / pageSize!.width
            let scale = UIScreen.main.scale * pdfScale
            let screenSize = CGSize(width: (pageSize!.width) * scale, height: pageSize!.height * scale)
            return page?.thumbnail(of: screenSize, for: .mediaBox)
        } else {
            return UIImage()
        }
    }
    
    @objc func deleteImage(sender : UIButton){
        let index = sender.tag
        let item = displayItemArray[index]
        if let img = item as? UIImage {
            if let indexInImgArray = selectedImages.firstIndex(of: img) {
                self.removeImage(index: indexInImgArray)
            }
        }
        else {
            if let pdfFile = item as? Data {
                if let indexOfPdf = pdfUrlArray?.firstIndex(of: pdfFile) {
                    self.removePDFFile(index: indexOfPdf)
                }
            }
        }
      
        
        self.textFieldFileName.text = ""
        self.textFieldFileName.text = ""
        displayItemArray.remove(at: index)
        self.manageImagesCollectionViewAppearance()
//        self.imagesCollectionView.reloadData()
    }
    
    func removePDFFile(index: Int) {
        pdfUrlArray?.remove(at: index)
    }
    
    func removeImage(index: Int) {
        if self.selectedImages.count > index {
//            if overSizeImagesArray.count != 0{
//                self.deleteImageFromOversizeArray(image: self.selectedImages[index])
//            }
            self.selectedImages.remove(at: index)
        }
        
//        if selectedImages.count == 0 {
//            imageCollectionView.isHidden = true
//        } else {
//            imageCollectionView.isHidden = false
//        }
    }
    
//    func deleteImageFromOversizeArray(image: UIImage) {
//        for i in 0...overSizeImagesArray.count-1 {
//            let oversizeImage = overSizeImagesArray[i]
//            let oversizeImageData = UIImagePNGRepresentation(oversizeImage)
//            let imageData = UIImagePNGRepresentation(image)
//            if imageData == oversizeImageData {
//                overSizeImagesArray.remove(at: i)
//                break
//            }
//        }
//    }
    
}

extension AddDocumentViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3, height: collectionView.frame.height)
    }
    
}

//MARK:- Add Photos Method
extension AddDocumentViewController: UINavigationControllerDelegate {
    func presentActionSheet() {
        
        let actionsheet = UIAlertController(title: "Select Option", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionsheet.addAction(UIAlertAction(title: "Select PDF", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            self.openDocumentViewer()
        }))
        
        actionsheet.addAction(UIAlertAction(title: "Take Photo", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            self.openCamera()
        }))
        
        actionsheet.addAction(UIAlertAction(title: "Select Photo", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            self.openImagePicker()
        }))
        
        actionsheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (action) -> Void in
            
        }))
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let popoverController = actionsheet.popoverPresentationController {
                popoverController.sourceView = self.view //to set the source of your alert
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
            }
        }
        self.present(actionsheet, animated: true, completion: nil)
    }
    
    func openDocumentViewer() {
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .fullScreen
        self.present(importMenu, animated: true, completion: nil)
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .fullScreen
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openImagePicker() {
//        if(self.displayItemArray.count == 0){
//            //            self.imageCollectionView.isHidden = false
//            let imagePicker = OpalImagePickerController()
//            imagePicker.imagePickerDelegate = self
//            imagePicker.maximumSelectionsAllowed = MaxSelecetdImages
//            imagePicker.modalPresentationStyle = .fullScreen
//            present(imagePicker, animated: true, completion: nil)
//        }
//        else if(self.displayItemArray.count < MaxSelecetdImages){
            //            self.imageCollectionView.isHidden = false
            let imagePicker = OpalImagePickerController()
            imagePicker.imagePickerDelegate = self
            imagePicker.maximumSelectionsAllowed = (MaxSelecetdImages - self.displayItemArray.count)
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker, animated: true, completion: nil)
//        }
    }
}

//MARK: - Pdf Picker
extension AddDocumentViewController: UIDocumentMenuDelegate, UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let fileURL = urls.first else {
            return
        }
        
        do {
            let fileData = try Data(contentsOf: fileURL)
            if pdfUrlArray == nil {
                pdfUrlArray = [Data]()
            }
            pdfUrlArray = [fileData]
            displayItemArray = [fileData]
            if displayItemArray.count != 0{
                self.imagesCollectionView.isHidden = false
                manageImagesCollectionViewAppearance()
            }
            self.imagesCollectionView.reloadData()
        } catch _ {
        }
    }
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK:- Opal Image Picker Delegates
extension AddDocumentViewController: OpalImagePickerControllerDelegate{
    
    func imagePickerNumberOfExternalItems(_ picker: OpalImagePickerController) -> Int {
        return 1
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
        //Cancel action?
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        //Save Images, update UI
        pdfUrlArray = nil
//        for image in images  {
            self.displayItemArray = images
//        }

        if displayItemArray.count != 0 {
            self.imagesCollectionView.isHidden = false
            manageImagesCollectionViewAppearance()
        }
        self.imagesCollectionView.reloadData()
        //Dismiss Controller
        presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerTitleForExternalItems(_ picker: OpalImagePickerController) -> String {
        return NSLocalizedString("External", comment: "External (title for UISegmentedControl)")
    }
    
    func imagePicker(_ picker: OpalImagePickerController, imageURLforExternalItemAtIndex index: Int) -> URL? {
        return URL(string: "https://placeimg.com/500/500/nature")
    }
}

//MARK:- UIImagePickerDelegate For Camera
extension AddDocumentViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            print(pickedImage)
            
            self.pdfUrlArray = nil
            self.displayItemArray = [pickedImage]
            self.imagesCollectionView.isHidden = false
            self.imagesCollectionView.reloadData()
            self.manageImagesCollectionViewAppearance()
        }
//        let image = info[.] as! UIImage
//
////        checkSelectedImageSize(images: [image])
//
////        if(self.selectedImages.count == 0){
////            self.selectedImages.append(image)
////        }
////        else{
////            //            for  image in images  {
////            //                self.selectedImages.append(image)
////            //            }
////            self.selectedImages.append(image)
////        }
//
//        self.selectedImages.append(image)
//        displayItemArray.append(image)
//        self.imageCollectionView.reloadData()
        
        dismiss(animated:true, completion: nil)
    }
}

//MARK:- Drop Down
extension AddDocumentViewController {
    func initdropDown (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.textFieldSelectDocumentType.text  = item
                self.textFieldSelectDocumentType.textColor = Color.textFieldPlaceholderColor
                self.documentType = String(describing: (Constant.masterData?.documentTypes[index].id ?? 0))
                self.selectedDocumentType = (Constant.masterData?.documentTypes[index])!
                dropDown.hide()
            }
            dropDown.show()
        }
    }
}

extension AddDocumentViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtfieldDateVac {
            self.showDatePicker(textField: textField)
        }
        if textField == textFieldSelectDocumentType {
//            if isLabReport == 1 {
//                showAlertWithBounceEffect(myView: searchLabReportTypeView, onWindow: true)
//                return false
//            }
//            if Constant.masterData?.documentTypes.count != 0 {
//                let dataSource = Constant.masterData?.documentTypes.map({$0.name})
//                self.initdropDown(textField: textField, dataSource: dataSource as! [String])
//                return false
//            } else {
//                Utility.shared.showToast(message: "No document type available!")
//            }
            return false
        }
        return true
    }
}

//MARK:- Search Bar Delegates
extension AddDocumentViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchLapReportTypeArray = labReportTypeArray.filter({ country -> Bool in
            if searchText.isEmpty {
//                isCountrySearchedEmpty = true
                return true
            }
//            isCountrySearchedEmpty = false
            return country.name?.lowercased().contains(searchText.lowercased()) ?? false
        })
        labReportTypesListTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
}


//MARK:- Search Table view delegates
extension AddDocumentViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections = 1
        numOfSections = showNoRecordFound(dataArray: searchLapReportTypeArray, tableview: tableView)
        return numOfSections
    }
    
    func showNoRecordFound(dataArray: [DocumentListingModel], tableview: UITableView) -> Int {
        var noOfSections: Int = 0
        noDataLabel.isHidden = true
        if !dataArray.isEmpty {
            tableview.separatorStyle = .singleLine
            noOfSections            = 1
            tableview.isScrollEnabled = true
//            tableview.backgroundView = nil
//            self.saveButton.isHidden = false
        }
        else {
//            noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: searchSpecialityView.bounds.size.width, height: searchSpecialityView.bounds.size.height))
            tableview.isScrollEnabled = false
            noDataLabel.numberOfLines = 0
            if searchBar.text == "" {
                noDataLabel.text   = "You have to type in something, you are looking for."
            } else {
                noDataLabel.text   = "No Records Found"
            }
            
            noDataLabel.font = UIFont(name: "Montserrat", size: 17)
            noDataLabel.textColor     = UIColor.darkGray
            noDataLabel.textAlignment = .center
//            tableview.backgroundView  = noDataLabel
            noDataLabel.isHidden = false
//            searchSpecialityView.addSubview(noDataLabel)
            tableview.separatorStyle  = .none
//            self.saveButton.isHidden = true
        }
        return noOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchLapReportTypeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCellTableViewCell") as? CountryCellTableViewCell else {
            return UITableViewCell()
        }
        
        cell.countryLabel.text = self.searchLapReportTypeArray[indexPath.row].name
        /*if selectedLabReportType.name == self.searchLapReportTypeArray[indexPath.row].name {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }*/
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerView = UIView()
        headerView = searchBar
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view.endEditing(true)
        let selectedLabReportType = searchLapReportTypeArray[indexPath.row]
        documentType = String(describing: selectedLabReportType.id ?? 0)
        textFieldSelectDocumentType.text = selectedLabReportType.name
        hideAlertWithBounceEffect(myView: searchLabReportTypeView)
    }
}

extension AddDocumentViewController {
    func showAlertWithBounceEffect(myView : UIView,onWindow:Bool) {
        let window = UIApplication.shared.keyWindow!
        myView.isHidden = false
        if onWindow == true {
            window.addSubview(myView)
        }  else {
            self.view.addSubview(myView)
            self.view.bringSubviewToFront(myView)
        }
        myView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            myView.transform = .identity
        }, completion: {(finished: Bool) -> Void in
        })
    }
    
    func hideAlertWithBounceEffect(myView : UIView) {
        myView.transform = .identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            myView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: {(finished: Bool) -> Void in
            myView.isHidden = true
        })
    }
}
extension AddDocumentViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtViewPurpose {
        if txtViewPurpose.text == " Purpose" {
            txtViewPurpose.text = ""
            txtViewPurpose.textColor = Color.textFieldPlaceholderColor
        }
            if txtViewPurpose.text == " Categorization" {
                txtViewPurpose.text = ""
                txtViewPurpose.textColor = Color.textFieldPlaceholderColor
            }
            
        }
        
        else if textView == txtViewNotes {
            if txtViewNotes.text == " Notes (Optional)"  {
                txtViewNotes.text = ""
                txtViewNotes.textColor = Color.textFieldPlaceholderColor
            }
        }
        
        
        else  if textView == txtViewVaccineFor {
        
        if txtViewVaccineFor.text == " Vaccine for"  {
            txtViewVaccineFor.text = ""
            txtViewVaccineFor.textColor = Color.textFieldPlaceholderColor
        }
    
            
        }
    }
    
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if documentType == "7" {
        if txtViewPurpose.text.isEmpty {
            txtViewPurpose.text = " Categorization"
            txtViewPurpose.textColor = Color.textFieldPlaceholderColor
        }
          
            
    }
        else {
            if txtViewPurpose.text.isEmpty {
                txtViewPurpose.text = " Purpose"
                txtViewPurpose.textColor = Color.textFieldPlaceholderColor
            }
        }
        
        if txtViewVaccineFor.text.isEmpty {
            txtViewVaccineFor.text = " Vaccine for"
            txtViewVaccineFor.textColor = Color.textFieldPlaceholderColor
        }
        
        if txtViewNotes.text.isEmpty {
            txtViewNotes.text = " Notes (Optional)"
            txtViewNotes.textColor = Color.textFieldPlaceholderColor
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count // for Swift use count(newText)
        
        if textView == txtViewNotes {
            return numberOfChars <= 200;}
        else {
            return numberOfChars <= 128
        }
    }
}



extension AddDocumentViewController {
    func showDatePicker(textField: UITextField){
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat
         //"dd MMM, yyyy"
        var date = Date()
        if textField.text != "" {
            date = formater.date(from: textField.text!) ?? Date()
        }
        
        let screenWidth = UIScreen.main.bounds.width
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
       // datePicker.minimumDate = Date()  //Calendar.current.date(byAdding: .year, value: -110, to: Date())
//        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 10, to: Date())
        datePicker.setDate(date, animated: true)
        
        if #available(iOS 14.0, *)
        {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()

//            datePicker.datePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        textField.inputAccessoryView = toolbar
        textField.inputView = datePicker
        selectedTextField = textField
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = Constant.dateFormat
        selectedTextField.text = formatter.string(from: datePicker.date)
        selectedTextField.textColor = Color.textFieldPlaceholderColor
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}
