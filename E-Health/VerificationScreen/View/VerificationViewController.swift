//
//  VerificationViewController.swift
//  E-Health
//
//  Created by Abhishek Chugh on 5/13/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import SVPinView
import AWSMobileClient
import IQKeyboardManagerSwift

class VerificationViewController: UIViewController {

    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var txtOTPView: SVPinView!
    @IBOutlet weak var backButton: UIButton!
    
    var dispatchGroup = DispatchGroup()
    var signupEmail = String()
    var signinPassword = String()
    var isFromLogin = false
    var isFromForgotPassword = false
    var federatedId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        let tapView = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnView(_:)))
        self.view.addGestureRecognizer(tapView)
    }
    
    @objc func handleTapOnView(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
    func setupUI() {
        if !isFromLogin {
            backButton.isHidden = true
        }
        
        txtOTPView.style = .underline
        txtOTPView.borderLineThickness = 2
        /*txtOTPView.didFinishCallback = { [weak self] pin in
            self?.verifyCode(code: pin)
        }*/
        
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func verifyButtonAction(_ sender: Any) {
        if validateVerificationPin() {
            let code = txtOTPView.getPin().trimmingCharacters(in: .whitespacesAndNewlines)
            verifyCode(code: code)
        }
    }
    
    func verifyCode(code: String) {
        view.endEditing(true)
        if isFromLogin {
            AWSAuth.shared.confirmUserSignIn(code: code, controller: self) { (success) -> (Void) in
                if success {
                    DispatchQueue.main.async {
                        self.txtOTPView.clearPin()
                    }
                    self.getUserProfile()
                }
            }
        } else {
            AWSAuth.shared.confirmNewUserAfterSignUp(emailID: self.signupEmail, code: code, controller: self) { (message, success) in
                if success {
                    if self.federatedId != "" {
                        self.signInFederatedUser()
                    } else {
                        self.callVerifyEmail(message: message)
                    }
                } else {
                    Utility.shared.showToast(message: message)
                }
            }
        }
    }
    
    func signInFederatedUser() {
        AWSAuth.shared.signInWithAWS(isSocial: true, emailID: signupEmail, password: "Mind@1234", controller: self) { (state, error) -> (Void) in
            Constant.isSocialLogin = true
            Constant.loggedInProfile = .consumer
            self.getUserProfile()
//            self.goToDashboardScreen()
        }
    }
    
    func callVerifyEmail(message: String) {
        let params = ["email": signupEmail]
        RegisterViewModel.verifyEmail(parameters: params) { (msg, success) in
            if success {
                self.goToAuthenticationScreen()
                Utility.shared.showAlert(message: message, controller: self)
            } else {
                Utility.shared.showToast(message: msg ?? "Something went wrong")
            }
        }
    }
    
    func getUserProfile() {
        if AWSAuth.shared.userAWSStatus() == .signedIn {
            self.dispatchGroup.enter()
            AWSAuth.shared.getUserAWSAttributes { (data, error) -> (Void) in
                self.dispatchGroup.leave()
                guard error == nil else {
                    return
                }
                Constant.userProfile = ProfileDetail(value: data)
                
                UserDefaults.standard.set(Constant.userProfile?.isNewUser, forKey: "isNewUser")
                UserDefaults.standard.removeObject(forKey: UserDefaultKey.userProfile)
                let userDetail = NSKeyedArchiver.archivedData(withRootObject: data as Any)
                UserDefaults.standard.set(userDetail, forKey: UserDefaultKey.userProfile)
                
                if Constant.userProfile?.family_name == "consumer" {
                    Constant.loggedInProfile =  .consumer
                    
                    //API TO POST USER's COGNITO SUB ID TO DATABASE
                    RegisterViewModel.postCognitoUserID(userID: AWSMobileClient.default().username ?? "") { result in
                        if Constant.userProfile?.isNewUser == "0" {
                            self.goToDashboardScreen()
                        } else {
                            self.goToConsumerTutorialScreen()
//                            self.goToWidgetsScreen()
                        }
                    }
                } else if Constant.userProfile?.family_name == "provider"{
                    Constant.loggedInProfile =  .provider
                    self.saveProviderProfileToDB()
                } else {
                    Constant.loggedInProfile = .none
                }
            }
        }
    }
    
    func goToConsumerTutorialScreen() {
        DispatchQueue.main.async {
            guard let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: ConsumerTutorialViewController.className) as? ConsumerTutorialViewController else {
                return
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func goToWidgetsScreen() {
        DispatchQueue.main.async {
            guard let vc = UIStoryboard.init(name: StoryboardNames.TabBar, bundle: Bundle.main).instantiateViewController(withIdentifier: SetWidgetViewController.className) as? SetWidgetViewController else {
                return
            }
            self.navigationController?.pushViewController(vc, animated: true)
            
            
            
                        
            
        }
    }
    
    func goToDashboardScreen() {
        DispatchQueue.main.async {
            guard let vc = UIStoryboard.init(name: StoryboardNames.TabBar, bundle: Bundle.main).instantiateViewController(withIdentifier: TabBarViewController.className) as? TabBarViewController else {
                return
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func goToAuthenticationScreen() {
        DispatchQueue.main.async {
            guard let authenticationVC = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: AuthenticationViewController.className) as? AuthenticationViewController else {
                return
            }
            authenticationVC.isComingFromSignup = true
            self.navigationController?.pushViewController(authenticationVC, animated: true)
        }
    }
    
    func saveProviderProfileToDB() {
        var params = [String: AnyObject]()
        
        //Provider Full Name
        var providerFullName = String()
        let name = Constant.userProfile?.name
        let middleName = Constant.userProfile?.middleName
        if middleName != "" || middleName != nil {
            let myStringArr = name?.components(separatedBy: " ")
            let firstName = (myStringArr?[0] ?? "")
            var lastName = ""
            if myStringArr?.count == 2 {
                lastName = (myStringArr?[1] ?? "")
            }
            providerFullName = firstName + " " + (middleName ?? "") + " " + lastName
        } else {
            providerFullName = name ?? ""
        }
        
        let providerType = Constant.userProfile?.providerType
        //IF PROVIDER TYPE NOT DOCTOR/HOSPITAL, HIDE FIELDS
        if providerType != "1" && providerType != "2" {
        }
            //SHOW FIELDS
        else {
            //For SPECIALIST FIELD
            if let specialityString = Constant.userProfile?.specialistIn {
                var specialityNamesArray = ""
                let specialitiesArray = specialityString.components(separatedBy: ";")
                if specialitiesArray.count != 0 {
                    for speciality in specialitiesArray {
                        let specialityDetailArray = speciality.components(separatedBy: "-")
                        specialityNamesArray.append((specialityDetailArray.last ?? "") + ",")
                    }
                    specialityNamesArray.removeLast()
                }
                params["speciality"] = specialityNamesArray as AnyObject
            }
            
            //FOR PRACTICE NAME FIELD
            if let practiceNameString = Constant.userProfile?.praticeName {
                var practiceNamesArray = ""
                let praticeArray = practiceNameString.components(separatedBy: ";")
                if praticeArray.count != 0 {
                    for practice in praticeArray {
                        let practiceDetailArray = practice.components(separatedBy: "-")
                        practiceNamesArray.append((practiceDetailArray.last ?? "") + ",")
                    }
                    practiceNamesArray.removeLast()
                }
                params["practice_name"] = practiceNamesArray as AnyObject
            }
        }
        
        params["cognito_id"] = Constant.userProfile?.sub as AnyObject
        params["provider_name"] = providerFullName as AnyObject
        //        params["practice_name"] = Constant.userProfile?.praticeName as AnyObject
        //        params["speciality"] = Constant.userProfile?.specialistIn as AnyObject
        params["email"] = Constant.userProfile?.email as AnyObject
        params["provider_type"] = Constant.userProfile?.providerType as AnyObject
        
        HomeViewModel.saveProvideProfileToDB(params: params) { (result, error) in
            print(result)
            if Constant.userProfile?.isNewUser == "0" {
                self.goToDashboardScreen()
            } else {
                self.goToWidgetsScreen()
            }
        }
    }
    
    @IBAction func resendButtonAction(_ sender: Any) {
        
        txtOTPView.clearPin()
        
        if isFromLogin {
            AWSAuth.shared.signInWithAWS(isSocial: false, emailID: signupEmail, password: signinPassword, controller: self) { (state, error) -> (Void) in
                if state == .smsMFA {
                    print("sms sent")
                } else if state == .signedIn {
                    print("signed in")
                }
            }
            
        } else {
            AWSAuth.shared.resendConfirmationCode(emailID: signupEmail, controller: self) { (result) in
                print(result)
            }
        }
    }
    
    func validateVerificationPin() -> Bool {
        let verificationPin = txtOTPView.getPin().trimmingCharacters(in: .whitespacesAndNewlines)
        if verificationPin == "" || verificationPin.count < 6 {
            Utility.shared.showToast(message: "Please enter a valid code")
            return false
        }
        return true
    }
}

/*extension VerificationViewController {
    
    func callVerifyForgotPasswordCode(parameters: [String: Any]) {
        ForgotPasswordViewModel.confirmOtpAndChangePassword(parameters: parameters) { (success, message) in
            if success! {
                self.goToResetPasswordScreen()
            } else {
                Utility.shared.showToast(message: message ?? "Something went wrong")
            }
        }
    }
    
    func goToResetPasswordScreen() {
        DispatchQueue.main.async {
            let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: ResetPasswordViewController.className) as? ResetPasswordViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
}*/
