//
//  VerificationViewModel.swift
//  E-Health
//
//  Created by Prabjot Kaur on 6/3/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class VerificationViewModel: NSObject {
    
    static func getAuthToken(completionHandler: @escaping(_ result: Bool) -> Void) {
        let url = Constant.URL.getAuthToken
        
        let params = ["email": "api-user@ehealth-briefcase.com" as AnyObject, "password": "y4Jhtexd" as AnyObject]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params, method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                let token = result["access_token"] as? String ?? ""
                Constant.HeaderConstant.authorization = "Bearer " + token
              //  Constant.HeaderConstant.accessToken = token
                completionHandler(true)
//                RegisterViewModel.postCognitoUserID()
            } else {
                print(error?.localizedDescription ?? "")
                completionHandler(false)
            }
        }
    }

}
