//
//  WebViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/2/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    

    @IBOutlet weak var txtviewFor: UITextView!
    @IBOutlet weak var txtviewPurpose: UITextView!
    @IBOutlet weak var txtfieldDate: UITextField!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var heightWebView: NSLayoutConstraint!
    @IBOutlet weak var txtViewNotes: UITextView!
    
    var strDate : String = ""
    var strPurpose : String = ""
    var strFor: String = ""
    var docType : String = ""
    var strNotes : String = ""
    
    //MARK: - Outlets
    var webView = WKWebView()
    
    //MARK: - Properties
    var webUrlString: String?
        
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        heightWebView.constant = 0
        tabBarController?.tabBar.isHidden = true
        webView.navigationDelegate = self
   
        
        if docType == "6" {

            heightWebView.constant = 186
            txtfieldDate.text = strDate
            txtviewPurpose.text = strPurpose
            txtviewFor.text = strFor
          
            if strPurpose == "" {
                txtviewPurpose.text = "No Data Available"
            }
            if strFor == "" {
                txtviewFor.text = "No Data Available"
            }
            if strDate == "" {
                txtfieldDate.text = "No Data Available"
            }
            if strNotes == "" {
                txtViewNotes.isHidden = true
            }
            else {
                txtViewNotes.text = strNotes
            }
            if strNotes == "" {
                txtViewNotes.isHidden = true
                            if #available(iOS 11.0, *) {
                
                                let window = UIApplication.shared.keyWindow
                                let topPadding = window?.safeAreaInsets.top
                
                                webView.frame = CGRect(x: 0, y: topPadding! + 250, width: view.frame.size.width, height: view.frame.size.height - (topPadding! + 250))
                            } else {
                                // Fallback on earlier versions
                                webView.frame = CGRect(x: 0, y: 230, width: view.frame.size.width, height: view.frame.size.height - 230)
                            }
            }
            else {
                txtViewNotes.text = strNotes
                
                if #available(iOS 11.0, *) {
                    
                    let window = UIApplication.shared.keyWindow
                    let topPadding = window?.safeAreaInsets.top
                    
                    webView.frame = CGRect(x: 0, y: topPadding! + 330, width: view.frame.size.width, height: view.frame.size.height - (topPadding! + 200))
                } else {
                    // Fallback on earlier versions
                    webView.frame = CGRect(x: 0, y: 300, width: view.frame.size.width, height: view.frame.size.height - 170)
                }
            }
            
        }
        
        else if docType == "7" {
        
            heightWebView.constant = 124
            txtfieldDate.text = strDate
            txtviewPurpose.text = strPurpose
            txtviewFor.isHidden = true
            //txtviewFor.text = strFor
            
            if strPurpose == "" {
                txtviewPurpose.text = "No Data Available"
            }
            if strDate == "" {
                txtfieldDate.text = "No Data Available"
            }
            
            if strNotes == "" {
                txtViewNotes.isHidden = true
                if #available(iOS 11.0, *) {
                    
                    let window = UIApplication.shared.keyWindow
                    let topPadding = window?.safeAreaInsets.top
                    
                    webView.frame = CGRect(x: 0, y: topPadding! + 200, width: view.frame.size.width, height: view.frame.size.height - (topPadding! + 200))
                } else {
                    // Fallback on earlier versions
                    webView.frame = CGRect(x: 0, y: 170, width: view.frame.size.width, height: view.frame.size.height - 170)
                }
            }
            else {
                txtViewNotes.text = strNotes
                if #available(iOS 11.0, *) {
                    
                    let window = UIApplication.shared.keyWindow
                    let topPadding = window?.safeAreaInsets.top
                    
                    webView.frame = CGRect(x: 0, y: topPadding! + 265, width: view.frame.size.width, height: view.frame.size.height - (topPadding! + 200))
                } else {
                    // Fallback on earlier versions
                    webView.frame = CGRect(x: 0, y: 230, width: view.frame.size.width, height: view.frame.size.height - 170)
                }
            }
        }
        else{
            viewBG.isHidden = true
            heightWebView.constant = 0
        
            
            if strNotes == "" {
                txtViewNotes.isHidden = true
                
                if #available(iOS 11.0, *) {
                    
                    let window = UIApplication.shared.keyWindow
                    let topPadding = window?.safeAreaInsets.top
                    
                    webView.frame = CGRect(x: 0, y: topPadding! + 44, width: view.frame.size.width, height: view.frame.size.height - (topPadding! + 44))
                } else {
                    // Fallback on earlier versions
                    webView.frame = CGRect(x: 0, y: 64, width: view.frame.size.width, height: view.frame.size.height - 64)
                }
            }
            else {
                txtViewNotes.text = strNotes
                if #available(iOS 11.0, *) {
                    
                    let window = UIApplication.shared.keyWindow
                    let topPadding = window?.safeAreaInsets.top
                    
                    webView.frame = CGRect(x: 0, y: topPadding! + 140, width: view.frame.size.width, height: view.frame.size.height - (topPadding! + 200))
                } else {
                    // Fallback on earlier versions
                    webView.frame = CGRect(x: 0, y: 150, width: view.frame.size.width, height: view.frame.size.height - 170)
                }
            }
        }
        
        
        txtviewPurpose.textColor = Color.textFieldPlaceholderColor
        txtviewPurpose.addBorder(borderColor: UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0), borderWidth: 0.8, cornerRadius: 4)
        txtviewFor.textColor = Color.textFieldPlaceholderColor
        txtviewFor.addBorder(borderColor: UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0), borderWidth: 0.8, cornerRadius: 4)
        txtfieldDate.borderedTextField(borderColor: UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0), borderWidth: 0.8, cornerRadius: 4)
        txtfieldDate.textColor = Color.textFieldPlaceholderColor
        webView.isOpaque = true
        txtViewNotes.textColor = Color.textFieldPlaceholderColor
        txtViewNotes.addBorder(borderColor: UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0), borderWidth: 0.8, cornerRadius: 4)
        
        guard let url = URL(string: webUrlString ?? "") else {
            return
        }
        let request = URLRequest(url: url)
        
        if UIApplication.shared.canOpenURL(url) {
            UIPasteboard.general.string = request.description
            webView.load(request)
        } else {
            UIPasteboard.general.string = request.description
            Utility.shared.showToast(message: request.description)
            return
        }
        Utility.shared.startLoader()
        view.addSubview(webView)
    }
    
    //MARK: - UIButton Actions
    @IBAction func backButtonAction(_ sender: Any) {
//        tabBarController?.tabBar.isHidden = false
        navigationController?.popViewController(animated: true)
    }
}
extension WebViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Utility.shared.stopLoader()
        print(navigation)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!) {
        Utility.shared.stopLoader()
        print(navigation)
    }
    
}
