//
//  InternetReachability.swift
//  RFIDReminderApp
//
//  Created by Ritesh Raj on 1/16/19.
//  Copyright © 2019 Ritesh Raj. All rights reserved.
//

import UIKit

/**
 InternetReachability implements Reachability framework to determine InternetConnection status.
 */

class InternetReachability: NSObject {
    
    class func startTracking(){
        _ = InternetReachability.sharedInstance
    }
    
    static let sharedInstance = InternetReachability()
    let reachability = Reachability()!
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            
        }
    }
    
    @objc func reachabilityChanged(note: NSNotification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            InternetConnection.isConnected = true
            
        }else {
            InternetConnection.isConnected = false
        }
    }
}

/**
 InternetConnection returns the current internet connection status. isConnected property of InternetConnection returns true if internet is connected otherwise it returns false.
 */

struct InternetConnection {
    static var isConnected = true
}
