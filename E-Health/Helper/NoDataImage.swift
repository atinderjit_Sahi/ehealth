//
//  NoDataImage.swift
//  QuickPick
//
//  Created by Aman Gangurde on 07/04/18.
//  Copyright © 2018 WEBWING. All rights reserved.
//

import UIKit

let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height

public class DeviceInfo {
    public class var isIpad:Bool {
        if #available(iOS 8.0, * ) {
            return UIScreen.main.traitCollection.userInterfaceIdiom == .pad
        } else {
            return UIDevice.current.userInterfaceIdiom == .pad
        }
    }
    public class var isIphone:Bool {
        if #available(iOS 8.0, *) {
            return UIScreen.main.traitCollection.userInterfaceIdiom == .phone
        } else {
            return UIDevice.current.userInterfaceIdiom == .phone
        }
    }
}

class NoDataImage: NSObject{
    
    private var noDataIndicator:UILabel!
    
    let WIDTH:CGFloat = DeviceInfo.isIpad ? 160:120
    let HEIGHT:CGFloat = DeviceInfo.isIpad ? 140:100
    
    //MARK: SET NO DATA IMAGE
    func set(view:UIView, alertMsg:String)
    {
        for v in view.subviews
        {
            if v == noDataIndicator
            {
                noDataIndicator.removeFromSuperview()
            }
        }
        
        //        let img = UIImage.init(named: "sadcloud")
        //        let h = (img?.size.height)! * 0.6
        //        let w = (img?.size.width)!  * 0.6
        noDataIndicator = UILabel()
//                noDataIndicator.frame = CGRect.init(x: view.frame.width/2 - (w/2), y: view.frame.height/2 - (h/2), width: w, height: h)
        //        noDataIndicator.image = img
        noDataIndicator.text = alertMsg
        noDataIndicator.textAlignment = .center
        noDataIndicator.font = UIFont.boldSystemFont(ofSize: DeviceInfo.isIpad ? 22:18)
        noDataIndicator.textColor = UIColor.darkGray
        noDataIndicator.numberOfLines = 0
        view.addSubview(noDataIndicator)
        noDataIndicator.center.x = 10//view.frame.width/2 //view.center.x
        noDataIndicator.center.y = view.frame.height/2 - 50//view.center.y
        noDataIndicator.frame.size.width = SCREEN_WIDTH - 20
        noDataIndicator.frame.size.height = 60
//        noDataIndicator.enableAutolayout()
//        noDataIndicator.fixWidth(SCREEN_WIDTH - 20)
//        noDataIndicator.fixHeight(60)
//        noDataIndicator.centerX()
//        noDataIndicator.centerY()
    }
    
    //MARK: REMOVE NO DATA IMAGE
    func remove(view:UIView)
    {
        for v in view.subviews
        {
            if v == noDataIndicator
            {
                noDataIndicator.removeFromSuperview()
            }
            
            if v.isKind(of: UIImageView.self)
            {
                v.removeFromSuperview()
            }
            
            if v.isKind(of: UILabel.self)
            {
                v.removeFromSuperview()
            }
        }
    }
}
