//
//  CustomTextField.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/30/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
@IBDesignable
class CustomTextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    @IBInspectable var placeholderColor: UIColor = Color.lightTextFieldPlaceholderColor {
        didSet {
            attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
        }
    }
}
