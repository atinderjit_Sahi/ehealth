//
//  Utility.swift
//  E-Health
//
//  Created by Akash Dhiman on 18/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import NVActivityIndicatorView

class Utility: NSObject {
    
    let projectName = "E-Health"
    
    // MARK: - Shared Instance
    static let shared: Utility = {
        let instance = Utility()
        return instance
    }()
    
    func showToast(message: String) {
        DispatchQueue.main.async {
            let topViewController = UIApplication.shared.topControllerInHierarchy()
            topViewController?.view.makeToast(message)
        }
    }
    
    func showAlert(message: String, controller: UIViewController) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: self.projectName, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    func showLogoutAlert(message: String) {
        DispatchQueue.main.async {
            let topViewController = UIApplication.shared.topControllerInHierarchy()
            
            let alert = UIAlertController(title: self.projectName, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
            topViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func showAlertWithCompletion(message : String, withAlertCompletionBlock:@escaping (_ alertAction:UIAlertAction) -> (Void)) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: self.projectName, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
            let topViewController = UIApplication.shared.topControllerInHierarchy()
            topViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func showCustomAlertWithCompletion(_ title: String?, message:String?, withButtonTypes:[String], withAlertCompletionBlock:@escaping (_ alertAction:UIAlertAction,_ buttonTitle_string:String) -> (Void)){
        let alertController = UIAlertController( title: title ?? projectName, message: message, preferredStyle: UIAlertController.Style.alert)
        for buttonTitle:String in withButtonTypes {
            let alertAction:UIAlertAction
            alertAction = UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: { (action:UIAlertAction) in
                withAlertCompletionBlock(action, buttonTitle)
            })
            alertController.addAction(alertAction)
        }
        let topViewController = UIApplication.shared.topControllerInHierarchy()
        topViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func limitTextFieldCharacters(range: NSRange, oldString: NSString, limit: Int?, newCharacter: String) -> Bool {
        let maxLength = limit ?? 32
        let newString: NSString =
            oldString.replacingCharacters(in: range, with: newCharacter as String) as NSString
        return newString.length <= maxLength
    }
    
    /*func limitContactNumberTextFieldCharacters(range: NSRange, oldString: NSString, minLimit: Int?, maxLimit: Int?, newCharacter: String) -> Bool {
        let minLength = minLimit ?? 9
        let maxLength = maxLimit ?? 12
        let newString: NSString =
            oldString.replacingCharacters(in: range, with: newCharacter as String) as NSString
        return newString.length <= maxLength && newString.length >= minLength
    }*/
    
    func validateEmail(email_string:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email_string)
    }
    
    func loginValidation(emailTextField: UITextField, passwordtextfield: UITextField) -> (Bool?,String?) {
        var error = ""
        
        if String.trim(emailTextField.text).isEmpty {
            error = "Please enter your Email Id."
        }
        
        else if !(CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: emailTextField.text!))){
            if (validateEmail(email_string: emailTextField.text!) == false) {
                error = "Please enter a valid email address."
            } else if String.trim(passwordtextfield.text).isEmpty {
                error = "Please enter the password."
            }
        }
        
        if error.isEmpty{
            return (true, nil)
        } else {
            return (false, error)
        }
    }
    
    func forgotPasswordValidation(emailTextField: UITextField) -> (Bool?,String?) {
        var error = ""
        
        if String.trim(emailTextField.text).isEmpty {
            error = "Please enter your Email Id."
        }
        
        else if !(CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: emailTextField.text!))){
            if (validateEmail(email_string: emailTextField.text!) == false) {
                error = "Please enter a valid email address."
            }
        }
        
        if error.isEmpty{
            return (true, nil)
        } else {
            return (false, error)
        }
    }
    
    func otpAndChangePasswordValidation(newPassword: UITextField, confirmPassword: UITextField, verificationCode: UITextField) -> (Bool?,String?) {
        var error = ""
        
        if String.trim(newPassword.text).isEmpty {
            error = "Please enter your New Password"
        } else if !(newPassword.text?.isValidPassword())! {
            error = "Password should contain 1 uppercase, 1 lowercase, 1 special character and min length as 8"
        } else if String.trim(confirmPassword.text).isEmpty || newPassword.text != confirmPassword.text {
            error = "Password and Confirm Password should be same"
        } else if String.trim(verificationCode.text).isEmpty {
            error = "Please enter Verification Code"
        }
        
        if error.isEmpty{
            return (true, nil)
        } else {
            return (false, error)
        }
    }
    
    func resetPasswordValidation(oldPasswordTextField: UITextField, newPasswordTextfield: UITextField, confirmPasswordTextfield: UITextField) -> (Bool?,String?) {
        var error = ""
        
        if String.trim(oldPasswordTextField.text).isEmpty {
            error = "Please enter old Password."
        } else {
            if String.trim(newPasswordTextfield.text).isEmpty {
                error = "Please enter new Password."
            } else {
                if !(newPasswordTextfield.text?.isValidPassword())! {
                    error = "Password should contain 1 uppercase, 1 lowercase, 1 special character and min length as 8"
                } else {
                    if String.trim(confirmPasswordTextfield.text).isEmpty {
                        error = "Please enter new Password."
                    }else if newPasswordTextfield.text != confirmPasswordTextfield.text {
                        error = "Password does not match."
                    }else{
                         return (true, nil)
                    }
                }
            }
        }
        
        if error.isEmpty{
            return (true, nil)
        } else {
            return (false, error)
        }
    }
    
    func initDropDown(textField : UITextField, array : [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = array
        DispatchQueue.main.async {
            
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                textField.text  = item
                dropDown.hide()
            }
            dropDown.show()
        }
    }
    
    func startLoader() {
        DispatchQueue.main.async {
            let activityData = ActivityData(type: .lineScale)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        }
    }
    
    func stopLoader() {
        DispatchQueue.main.async {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
    
    func saveImageToDocumentDirectory(image: UIImage) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileName = "image.jpg"
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        if let data = image.jpegData(compressionQuality:  1.0),
          !FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                // writes the image data to disk
                try data.write(to: fileURL)
                
                print("file saved")
            } catch {
                print("error saving file:", error)
            }
        }
    }
    
    func fetchImageFromDocumentDirectory() -> UIImage? {
        let fileName = "image.jpg"
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            if let image    = UIImage(contentsOfFile: imageURL.path) {
                return image
            }
        }
        return nil
    }
    
    func clearDocumentDirectory() {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileName = "image.jpg"
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(at: fileURL)
                print("file removed")
            } catch {
                print("error removing file:", error)
            }
        }
    }
}
