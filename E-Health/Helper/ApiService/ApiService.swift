//
//  ApiService.swift
//  QuickRide_Customer
//
//  Created by Akash Dhiman on 3/18/19.
//  Copyright © 2019 Akash Dhiman. All rights reserved.
//

import UIKit
import Alamofire

class ApiService: NSObject {
    typealias completionBlock = (AnyObject,Bool) -> Void
    typealias failureBlock = (NSString,Bool) -> Void
    
    static let shared = ApiService()
    
    func triggerRequest(controller: UIViewController?,
                        url: String,
                        parameters: [String: AnyObject]?,
                        method: HTTPMethod,
                        encoding: ParameterEncoding = JSONEncoding.default,completion: @escaping (_ response: AnyObject?, _ error: Error?) -> Void) {
        
        if !InternetConnection.isConnected {
            Utility.shared.stopLoader()
            Utility.shared.showToast(message: "No internet connection!")
            return
        }
//        Utility.shared.startLoader()
        let serviceHeaders : HTTPHeaders = ["APP-KEY": Constant.HeaderConstant.appKey,
                                            "Authorization": Constant.HeaderConstant.authorization]
//        if headers != nil {
//            serviceHeaders = headers as! HTTPHeaders
//        } else {
//            serviceHeaders = ["APP-KEY": Constant.HeaderConstant.appKey]
//        }
        print(serviceHeaders)
        print(url)
        print(parameters)
        AF.request(url, method: method, parameters: parameters, encoding: encoding, headers: serviceHeaders).responseJSON { (response) in

            DispatchQueue.main.async {
                //let dataString = NSString(data: response.data!, encoding:String.Encoding.utf8.rawValue)

                if let error = response.error {
                    completion(nil, error)
                }
                else if let JSON = response.value as? NSDictionary {

                    if let status = JSON["statusCode"] as? Int {
                        if status == 200 {
                            completion(JSON, nil)
                            print(JSON)
                        } else {
                            if let message = JSON["Message"] as? String {
                                let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : message])
                                completion(nil, error)
                            }
                            else {
                                print(JSON)
                            }
                        }
                    } else {
                        completion(JSON, nil)
                    }
                }
                else if let JSON = response.value as? NSArray {
                    completion(JSON, nil)
                }
                else {
                    print(response)
                }
            }
        }
    }
    
//    func uploadProfileImage(_ url: String, parameters: Dictionary<String,AnyObject>?, profileImage:UIImage, pdfUrlArray: [Data]?, compBlock : @escaping completionBlock, failure : @escaping failureBlock) {
//        var serviceHeaders : HTTPHeaders = ["APP-KEY": Constant.HeaderConstant.appKey]
////        if headers != nil {
////            serviceHeaders = headers!
////        } else {
////            serviceHeaders = ["APP-KEY": Constant.HeaderConstant.appKey]
////        }
//        AF.upload(multipartFormData: { multipartFormData in
//
//            for (key, value) in parameters! {
//                if let data = value.data(using: String.Encoding.utf8.rawValue) {
//                    multipartFormData.append(data, withName: key)
//                }
//            }
//                //New method for reducing image size
//                let compressedImage = profileImage //.compressedData()
//                let jpegImage = compressedImage.jpegData(compressionQuality: CGFloat(0.8))!
//
//                multipartFormData.append(jpegImage, withName: "profile_image", fileName: "image.jpg", mimeType: "image/jpeg")
//
//        },to: url,method:HTTPMethod.post,
//          headers:serviceHeaders, encodingCompletion: { encodingResult in
//            switch encodingResult {
//            case .success(let upload, _, _):
//                upload.validate().responseJSON { response in
//                        switch response.result {
//                        case .success(let value):
//                            compBlock(value as AnyObject,true)
//                        case .failure(let responseError):
//                            failure(responseError.localizedDescription as NSString,false)
//                        }
//                }
//            case .failure(let encodingError):
//                let errorDesc = (encodingError as NSError).localizedDescription
//                failure(errorDesc as NSString,false)
//            }
//        })
//    }
    
    func uploadProfileImage(_ url: String, parameters: Dictionary<String,AnyObject>?, profileImage:UIImage?, pdfUrlArray: [Data]?, compBlock : @escaping completionBlock, failure : @escaping failureBlock) {
        let serviceHeaders : HTTPHeaders = ["APP-KEY": Constant.HeaderConstant.appKey,
                                            "Authorization": Constant.HeaderConstant.authorization]
        
        AF.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters! {
                if let data = value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue).rawValue) {
                    multipartFormData.append(data, withName: key)
                }
            }
            
            if profileImage != nil {
                //New method for reducing image size
                let compressedImage = profileImage //.compressedData()
                let jpegImage = compressedImage!.jpegData(compressionQuality: CGFloat(0.5))!
                multipartFormData.append(jpegImage, withName: "profile_image", fileName: "image.jpg", mimeType: "image/jpeg")
            }
        }, to: url, method: .post, headers: serviceHeaders).response{ response in

            if((response.error == nil)){
                do{
                    if let jsonData = response.data{
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                        let status = parsedData["code"] as? NSInteger ?? 0

                        if (status == 200){
                            compBlock(parsedData as AnyObject, true)
                        } else {
                            compBlock(parsedData as AnyObject, false)
                        }
                    }
                }catch{
                    print("error message")
                }
            }else{
                if((response.error != nil)){
                    failure(response.error!.localizedDescription as NSString, false)
                    print(response.error!.localizedDescription)
                }else{
                    failure("Something went wrong", false)
                }
            }
        }
    }
    
    func uploadDocuments(_ url: String, parameters: Dictionary<String,AnyObject>?, profileImage:[Any], pdfUrlArray: [Data]?, compBlock : @escaping completionBlock, failure : @escaping failureBlock){
        let serviceHeaders : HTTPHeaders = ["APP-KEY": Constant.HeaderConstant.appKey,
                                            "Authorization": Constant.HeaderConstant.authorization]
        
        AF.upload(multipartFormData: { multipartFormData in
            
            for (key, value) in parameters! {
                if let data = value.data(using: String.Encoding.utf8.rawValue) {
                    multipartFormData.append(data, withName: key)
                }
            }
            //New method for reducing image size
            
            if pdfUrlArray != nil {
                for pdfData in pdfUrlArray! {
                    multipartFormData.append(pdfData, withName: "document_file", fileName: "application.pdf", mimeType: "application/pdf")
                }
            } else {
                let images = profileImage as! [UIImage]
                for image in images {
                    let compressedImage = image //.compressedData()
                    let jpegImage = compressedImage.jpegData(compressionQuality: CGFloat(0.5))!
                    
                    multipartFormData.append(jpegImage, withName: "document_file", fileName: "image.jpg", mimeType: "image/jpeg")
                }
            }
            
        }, to: url, method: .post, headers: serviceHeaders).response{ response in
            
            if((response.error == nil)){
                do{
                    if let jsonData = response.data{
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                        let status = parsedData["code"] as? NSInteger ?? 0

                        if (status == 200){
                            compBlock(parsedData as AnyObject, true)
                        } else {
                            compBlock(parsedData as AnyObject, false)
                        }
                    }
                }catch{
                    print("error message")
                }
            }else{
                if((response.error != nil)){
                    failure(response.error!.localizedDescription as NSString, false)
                    print(response.error!.localizedDescription)
                }else{
                    failure("Something went wrong", false)
                }
            }
        }
    }
    
//    func uploadDocuments(_ url: String, parameters: Dictionary<String,AnyObject>?, headers: [String: String]? = nil, profileImage:[Any], pdfUrlArray: [Data]?, compBlock : @escaping completionBlock, failure : @escaping failureBlock)
//    {
//        var serviceHeaders = [String: String]()
//        if headers != nil {
//            serviceHeaders = headers!
//        } else {
//            serviceHeaders = ["APP-KEY": Constant.HeaderConstant.appKey]
//        }
//        AF.upload(multipartFormData: { multipartFormData in
//
//            for (key, value) in parameters! {
//                if let data = value.data(using: String.Encoding.utf8.rawValue) {
//                    multipartFormData.append(data, withName: key)
//                }
//            }
//            //New method for reducing image size
//
//            if pdfUrlArray != nil {
//                for pdfData in pdfUrlArray! {
//                    multipartFormData.append(pdfData, withName: "document_file", fileName: "application.pdf", mimeType: "application/pdf")
//                }
//            } else {
//                let images = profileImage as! [UIImage]
//                for image in images {
//                    let compressedImage = image //.compressedData()
//                    let jpegImage = compressedImage.jpegData(compressionQuality: CGFloat(0.8))!
//
//                    multipartFormData.append(jpegImage, withName: "document_file", fileName: "image.jpg", mimeType: "image/jpeg")
//                }
//            }
//
//        },to: url,method:HTTPMethod.post,
//          headers:serviceHeaders, encodingCompletion: { encodingResult in
//            switch encodingResult {
//            case .success(let upload, _, _):
//                upload.validate().responseJSON { response in
//                        switch response.result {
//                        case .success(let value):
//                            compBlock(value as AnyObject,true)
//                        case .failure(let responseError):
////                            compBlock(responseError.localizedDescription as NSString,false)
//                            failure(responseError.localizedDescription as NSString,false)
//                        }
//                }
//            case .failure(let encodingError):
//                let errorDesc = (encodingError as NSError).localizedDescription
//                failure(errorDesc as NSString,false)
//            }
//        })
//    }
    
//    func triggerMultipartRequest(isUploadDoc: Bool, _ url: String, parameters: [String: AnyObject]?, headers: [String: String]? = nil, insuranceFrontImage: UIImage?, insuranceBackImage: UIImage?, pdfUrls: [Data]?, compBlock : @escaping completionBlock, failure : @escaping failureBlock) {
//
//        let serviceHeaders : HTTPHeaders = ["APP-KEY": Constant.HeaderConstant.appKey]
////        if headers != nil {
////            serviceHeaders = headers!
////        } else {
////            serviceHeaders = ["APP-KEY": Constant.HeaderConstant.appKey]
////        }
//
//        AF.upload(multipartFormData: { multipartFormData in
//            //New method for reducing image size
//
//            for (key, value) in parameters! {
//                if let data = value.data(using: String.Encoding.utf8.rawValue) {
//                    multipartFormData.append(data, withName: key)
//                }
//            }
//
//            if pdfUrls != nil {
//                for pdfData in pdfUrls! {
//                    multipartFormData.append(pdfData, withName: "view_front", fileName: "application.pdf", mimeType: "application/pdf")
//                }
//            } else {
//                if insuranceFrontImage != nil {
//                    let compressedImage = insuranceFrontImage!.jpegData(compressionQuality: CGFloat(1.0))!
//
//                    multipartFormData.append(compressedImage, withName: "view_front", fileName: "image.jpg", mimeType: "image/jpeg")
//                }
//            }
//
//            if !isUploadDoc {
//                if insuranceBackImage != nil {
//                    let compressedImage1 = insuranceBackImage!.jpegData(compressionQuality: CGFloat(1.0))!
//
//                    multipartFormData.append(compressedImage1, withName: "view_back", fileName: "imageBack.jpg", mimeType: "image/jpeg")
//                }
//            } else {
//                multipartFormData.append(Data(), withName: "view_back", fileName: "imageBack.jpg", mimeType: "image/jpeg")
//            }
//
//        },to: url,method:HTTPMethod.post,
//          headers:serviceHeaders, encodingCompletion: { encodingResult in
//            switch encodingResult {
//            case .success(let upload, _, _):
//                upload.validate().responseJSON { response in
//                        switch response.result {
//                        case .success(let value):
//                            compBlock(value as AnyObject,true)
//                        case .failure(let responseError):
//                            failure(responseError.localizedDescription as NSString,false)
//                        }
//                }
//            case .failure(let encodingError):
//                let errorDesc = (encodingError as NSError).localizedDescription
//                failure(errorDesc as NSString,false)
//            }
//        })
//    }
    
    func triggerMultipartRequest(isUploadDoc: Bool, _ url: String, parameters: [String: AnyObject]?, headers: [String: String]? = nil, insuranceFrontImage: UIImage?, insuranceBackImage: UIImage?, pdfUrls: [Data]?, compBlock : @escaping completionBlock, failure : @escaping failureBlock) {
        
        let serviceHeaders : HTTPHeaders = ["APP-KEY": Constant.HeaderConstant.appKey,
                                            "Authorization": Constant.HeaderConstant.authorization]
        
        AF.upload(multipartFormData: { multipartFormData in
            //New method for reducing image size
            
            for (key, value) in parameters! {
                if let data = value.data(using: String.Encoding.utf8.rawValue) {
                    multipartFormData.append(data, withName: key)
                }
            }
            
            if pdfUrls != nil {
                for pdfData in pdfUrls! {
                    multipartFormData.append(pdfData, withName: "view_front", fileName: "application.pdf", mimeType: "application/pdf")
                }
            } else {
                if insuranceFrontImage != nil {
                    let compressedImage = insuranceFrontImage!.jpegData(compressionQuality: CGFloat(0.5))!
                    
                    multipartFormData.append(compressedImage, withName: "view_front", fileName: "image.jpg", mimeType: "image/jpeg")
                }
            }
            
            if !isUploadDoc {
                if insuranceBackImage != nil {
                    let compressedImage1 = insuranceBackImage!.jpegData(compressionQuality: CGFloat(0.5))!

                    multipartFormData.append(compressedImage1, withName: "view_back", fileName: "imageBack.jpg", mimeType: "image/jpeg")
                }
            } else {
                multipartFormData.append(Data(), withName: "view_back", fileName: "imageBack.jpg", mimeType: "image/jpeg")
            }
            
        }, to: url, method: .post, headers: serviceHeaders).response{ response in
            
            if((response.error == nil)){
                do{
                    if let jsonData = response.data{
                        let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! Dictionary<String, AnyObject>
                        let status = parsedData["code"] as? NSInteger ?? 0

                        if (status == 200){
                            compBlock(parsedData as AnyObject, true)
                        } else {
                            compBlock(parsedData as AnyObject, false)
                        }
                    }
                }catch{
                    failure("Something went wrong", false)
                }
            }else{
                if((response.error != nil)){
                    failure(response.error!.localizedDescription as NSString, false)
                    print(response.error!.localizedDescription)
                }else{
                    failure("Something went wrong", false)
                }
            }
        }
    }
}
