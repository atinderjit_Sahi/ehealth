//
//  Validations.swift
//  E-Health
//
//  Created by Abhishek Chugh on 5/18/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class Validations: NSObject {

    static let shared = Validations()
    
    func validateField(_ txtField: ExtendedTextField)-> (Bool, NSString)
    {
        if txtField.text!.isEmpty{
            return (false,"Empty field")
        }
        if (txtField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0 {
            txtField.text = ""
            return (false,"Empty field")
        }
        if txtField.sizeLimit{
            if txtField.minLength > (txtField.text?.count)!{
                return (false,"Short length text")
            }
            if txtField.maxLength < (txtField.text?.count)!{
                return (false,"Long length text")
            }
        }
        
        return (true,"")
    }
    
    func validateField(_ txtField: UITextField, sizeLimit: Bool, maxLimit: Int? = 100, minLimit: Int? = 0)-> (Bool, NSString)
    {
        if txtField.text!.isEmpty{
            return (false,"Empty field")
        }
        if (txtField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0 {
            txtField.text = ""
            return (false,"Empty field")
        }
        if sizeLimit{
            if minLimit! > (txtField.text?.count)!{
                return (false,"Short length text")
            }
            if maxLimit! < (txtField.text?.count)!{
                return (false,"Long length text")
            }
        }
        
        return (true,"")
    }
    
    func validateTextViewField(_ txtField: UITextView)-> (Bool, NSString)
    {
        if txtField.text!.isEmpty || txtField.text == "Description"{
            return (false,"Empty field")
        }
        if (txtField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0 || txtField.text == "Description"{
            txtField.text = ""
            return (false,"Empty field")
        }
        return (true,"")
    }
    
    
    func validateEmailField(_ txtField: ExtendedTextField)-> (Bool, NSString)
    {
        if txtField.text!.isEmpty{
            return (false,"Empty field")
        }
        if (txtField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0 {
            txtField.text = ""
            return (false,"Empty field")
        }
        
        if txtField.sizeLimit{
            if txtField.minLength > (txtField.text?.count)!{
                return (false,"Short length text")
            }
            if txtField.maxLength < (txtField.text?.count)!{
                return (false,"Long length text")
            }
        }
        
        let result = Utility.shared.validateEmail(email_string: txtField.text!)
        
//        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
//
//        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
//
//        let result = emailTest.evaluate(with: txtField.text)
//
        if result{
            return(true,"")
        }
        else{
            return(false, "Invalid Email format")
        }
    }
    
    func validatePasswordField(_ txtField: ExtendedTextField)-> (Bool, NSString)
    {
        if txtField.text!.isEmpty{
            return (false,"Empty field")
        }
        if (txtField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0 {
            txtField.text = ""
            return (false,"Empty field")
        }
        
        if txtField.sizeLimit{
            if txtField.minLength > (txtField.text?.count)!{
                return (false,"Short length text")
            }
            if txtField.maxLength < (txtField.text?.count)!{
                return (false,"Long length text")
            }
        }
        
        let passwordRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&#]{8,16}"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        let result = passwordTest.evaluate(with: txtField.text)
        
        if result{
            return(true,"")
        }
        else {
            return(false, "Invalid password format")
        }
    }
    
    func validateAddInsuranceRecord(insuranceRecord: InsuranceRecord, frontImage: Data?, backImage: Data?) -> (Bool, String) {
        
        let noImageData = UIImage(named: "global_add_image")?.pngData()
        
        if insuranceRecord.insuranceDocument!.isEmpty {
            return (false,"Please select insurance document")
        } else if insuranceRecord.payerName!.isEmpty {
            return (false,"Please enter payer name")
        } else if insuranceRecord.payerName!.count < 3 {
            return (false,"Payer name must be at least 3 characters")
        } /*else if insuranceRecord.memberServices!.isEmpty {
            return (false,"Please enter member services")
        } else if insuranceRecord.memberServices!.count < 3 {
            return (false,"Member Services must be at least 3 characters")
        }*/ else if insuranceRecord.insuranceTypes!.isEmpty {
            return (false,"Please select insurance type")
        } else if insuranceRecord.memberId!.isEmpty {
            return (false,"Please enter member id")
        } else if insuranceRecord.memberId!.count < 3 {
            return (false,"Member Id must be al least 3 characters")
        }  else if insuranceRecord.groupId!.isEmpty {
            return (false,"Please enter group id")
        } else if insuranceRecord.groupId!.count < 3 {
            return (false, "Group Id must be at least 3 characters")
        } /*else if insuranceRecord.contactType!.isEmpty {
            return (false,"Please enter contact type")
        } else if insuranceRecord.contactType!.count < 3 {
            return (false, "Contact type must be at least 3 characters")
        } else if insuranceRecord.planCodes!.isEmpty {
            return (false,"Please enter plan code")
        } else if insuranceRecord.planCodes!.count < 3 {
            return (false, "Plan code must be at least 3 characters")
        }*/ else if insuranceRecord.documentName!.isEmpty {
            return (false,"Please enter document name")
        } else if insuranceRecord.documentName!.count < 3 {
            return (false, "Document name must be at least 3 characters")
        } else if frontImage == nil || frontImage == noImageData {
            return (false,"Please select insurance front view")
        } else if backImage == nil || backImage == noImageData {
            return (false,"Please select insurance back view")
        } else if insuranceRecord.uploadType!.isEmpty {
            return (false,"Please enter upload type")
        } else {
            return(true,"")
        }
    }
        
    func validateAddProvider(provider: AddProvider) -> (Bool, String) {
        if provider.typeId!.isEmpty {
            return (false,"Please select provider category")
        }
        else if provider.doctorType!.isEmpty && provider.typeId! == "1" {
            return (false,"Please select type")
        }
        /*else if provider.practiceName == "0" && provider.typeId! == "1" {
            return (false,"Please select practice name")
        }*/
        else if provider.speciality == "0" && provider.typeId! == "1" {
            return (false,"Please select speciality")
        } else if provider.providerName!.isEmpty {
            return (false,"Please enter provider name")
        }
        /*else if provider.email!.isEmpty {
            return (false,"Please enter email")
        }*/
        else if !provider.email!.isValidEmail && !provider.email!.isEmpty {
            return (false,"Please enter a valid Email address")
        }
        else if provider.country!.isEmpty || provider.country! == "0" {
            return (false,"Please select country")
        } else if provider.officeContact!.isEmpty{
            return (false,"Please enter contact number")
        }
//        else if provider.address1!.isEmpty {
//            return (false,"Please enter address 1")
//        } else if provider.address2!.isEmpty {
//            return (false,"Please enter address 2")
//        }  else if provider.city!.isEmpty {
//            return (false,"Please enter city")
//        } else if provider.state!.isEmpty {
//            return (false, "Please select state")
//        }
        else if !provider.fax!.isEmpty {
            if provider.fax!.count < 10 {
                return (false, "Fax should be of 10 digits")
            }
        }
        if provider.userIdofCustomer!.count != 0 {
            if provider.userIdofCustomer!.count < 2 {
                return (false, "Patient Portal User Id must be atleast 2 characters")
            }
        }
        if provider.passwordOfCustomer!.count != 0 {
            if provider.passwordOfCustomer!.count < 4 {
                return (false, "Patient Portal Password must be atleast 4 characters")
            }
        }
//        else {
            return(true,"")
//        }
    }
}
extension String
{
    var isValidEmail: Bool
    {
       let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegEx).evaluate(with: self)
    }
}
