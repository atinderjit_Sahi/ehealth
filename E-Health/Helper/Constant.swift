//
//  Constant.swift
//  E-Health
//
//  Created by Abhishek Chugh on 5/15/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

enum Profile {
    case consumer
    case careGiver
    case provider
    case none
}

class Constant: NSObject {
    
    static let noInternet = "No Internet Connection"
    static var loggedInProfile = Profile.none
    
    static var masterData: MasterData?
    static var userProfile: ProfileDetail?
    static var selectedCareTaker: CaretakerModel?
    
    static var dateFormat = "MM/dd/yyyy"
    static var isSocialLogin = Bool()
    
    struct AWS {
        static let userPoolName = ""
        static let SNSPlatformApplicationArn = "arn:aws:cognito-idp:us-east-1:351947363766:userpool/us-east-1_9SijlOEHl"
        static var endPointID = ""
        static let clientSecret = "17eag4be3lufpgonjfhkvv73mnp8jh25q9ps9ufgrccmea4gh0c3"
        static let clientId = "fvlbp4cefk1g63l35fvocnu3o"
        static let poolId = "us-east-1_9SijlOEHl"
        static let identityPoolId = "us-east-1:14d6c834-9da4-4f0a-a553-d8e97cec657b"
    }
    
    struct Token {
        static var deviceToken = ""
    }

    struct URL {
//        static let baseURL = "http://np.seasiafinishingschool.com:7089/"    //Live
//        static let baseURL = "http://34.207.233.26/"  // Client's Server
        static let baseURL = "http://3.236.116.101/"  // Client's Server
        
//        static let imageUrl = "http://3.236.116.101"
//        static let baseURL = "http://np.seasiafinishingschool.com:7092/"    //Development
        static let deleteUser = Constant.URL.baseURL + "api/v2/delete-cognito-user"
        /*Team New api to delete cognito user from DB and cognito pool
         
        URL  :     /api/v2/delete-cognito-user
         
        Method - >post
        Authorization required
         
        Params ->
        cognito_id:                        // this param is also required to delete user from DB and check user existance in cognito pool
        access_token: <string>      //  this param is required to delete user from cognito pool

        Success response:

        {
            "data": "2da2db27-666e-47fe-a9f6-76e4c68492c2",
            "message": "User record deleted successfully",
            "technical_message": "",
            "custom_code": 2,
            "code": 200
        }*/
        static let enableMFA = Constant.URL.baseURL + "api/v2/mfa-options"
        static let getCountriesList = Constant.URL.baseURL + "api/countriesData/231"
        static let getCountriesListAPI = Constant.URL.baseURL + "api/countries"
        //        static let addCognitoUserID = Constant.URL.baseURL + "api/addCognitoUserID"
        static let addCognitoUserID = Constant.URL.baseURL + "api/cognito-users"
//        static let getMasterData = Constant.URL.baseURL + "api/v2/masterData"
        static let getMasterData = Constant.URL.baseURL + "api/v2/master-data-new"
        static let getDevicesList = Constant.URL.baseURL + "api/devices"
        static let saveVitals = Constant.URL.baseURL + "api/vital"
        static let getCaretakersList = Constant.URL.baseURL + "api/relationship-caretaker"
        static let getLoginToken = Constant.URL.baseURL + "auth/login"
        static let addCaretaker = Constant.URL.baseURL + "api/add-caretaker"
        static let deleteCaretaker = Constant.URL.baseURL + "api/delete-caretaker/"
        static let getVitalsByCognitoID = Constant.URL.baseURL + "api/vitalByCognitoID/"
        static let getPendingCaretakersByID = Constant.URL.baseURL + "api/pending-caretakers/"
        //        static let getCaretakersByUserID = Constant.URL.baseURL + "api/caretaker/"
        //        static let acceptRejectCaretaker = Constant.URL.baseURL + "api/accepted-or-rejected/"
        static let acceptRejectCaretaker = Constant.URL.baseURL + "api/caretaker-request-state/"
        static let getAuthToken = Constant.URL.baseURL + "api/auth/login"
        static let getProfileDetails = Constant.URL.baseURL + "api/cognito-users/"
        static let updateCaretakerProfile = Constant.URL.baseURL + "api/update-cognito-user"
        static let autocompleteSpecialist = Constant.URL.baseURL + "api/auto-complete-speciality/"
        static let autocompletePracticeName = Constant.URL.baseURL + "api/auto-complete-med-practice/"
        //V2 API's
        static let saveProviderProfile = Constant.URL.baseURL + "api/v2/providers"
        static let addCaretakerV2 = Constant.URL.baseURL + "api/v4/add-caretaker"
        static let getVitalsForMinor = Constant.URL.baseURL + "api/v2/vitalByMinorID/"
        static let saveVitalsV2 = Constant.URL.baseURL + "api/v2/vital"
        static let updateProfilePhoto = Constant.URL.baseURL + "api/v2/profile-photos"
        //        static let searchDocuments = Constant.URL.baseURL + "api/v2/search-documents"
        static let uploadDocument = Constant.URL.baseURL + "api/v2/documents-upload"
        static let getCaretakersByUserID = Constant.URL.baseURL + "api/v4/caretaker/"
        static let forgotPassword = Constant.URL.baseURL + "api/v2/forgot-password"
        static let confirmOtpAndChangePassword = Constant.URL.baseURL + "api/v2/confirm-otp"
        
        static let searchDocuments = Constant.URL.baseURL + "api/v4/search-documents"
        static let addInsuranceRecord = Constant.URL.baseURL + "api/v4/add-insurance-record1"
        static let getInsuranceListing = Constant.URL.baseURL + "api/v4/get-insurance-documents-listing"
        static let getInsuranceRecord = Constant.URL.baseURL + "api/v4/get-insurance-document/"
        static let deleteInsuranceRecord = Constant.URL.baseURL + "api/v4/delete-insurance-document"
        
        static let getDrugByID = Constant.URL.baseURL + "api/v4/get-drug-record/"
        static let addNewDrug = Constant.URL.baseURL + "api/v4/drug-add1"
        static let deleteDrugRecord = Constant.URL.baseURL + "api/v4/delete-drug-record"
        static let popUpAddMedications = Constant.URL.baseURL + "api/v4/drug-type-listing"
        
        static let addUpdateVitalRecord = Constant.URL.baseURL + "api/v4/add-vital-detail"
        static let getVitalDetailById = Constant.URL.baseURL + "api/v4/get-vital-detail"
        
        static let getImmunozationRecordDetail = Constant.URL.baseURL + "api/v4/get-immunization-report/"
        static let deleteImmunizationRecord = Constant.URL.baseURL + "api/v4/delete-immunization-report"
        
        static let addUpdateAllergyRecord = Constant.URL.baseURL + "api/v4/add-allergies-record"
        static let getAllergyRecordDetailById = Constant.URL.baseURL + "api/v4/get-allergies-record/"
        static let deleteAllergyRecord = Constant.URL.baseURL + "api/v4/delete-allergies-record"
        //Lab Report
        static let deleteLabReport = Constant.URL.baseURL + "api/v4/delete-lab-report"
        static let getLabReportDetailById = Constant.URL.baseURL + "api/v4/get-lab-report/"
        //Social Login
        static let getFederatedUserByEmail = Constant.URL.baseURL + "api/v2/get-user-with-federated-id/"
        static let createFederatedUser = Constant.URL.baseURL + "api/v2/create-federated-user"
        //Verify Email
        static let verifyEmail = Constant.URL.baseURL + "api/v2/verify-email-attr"
        //SearchDocument By Date
        static let searchDocumentByDate = Constant.URL.baseURL + "api/v4/search-documents-bydate"
        //Provider Apis
        static let getProviderListBySpeciality = Constant.URL.baseURL + "api/v2/getProviderListBySpeciality"
        static let addProvider = Constant.URL.baseURL + "api/v4/addProvider"
        static let updateProvider = Constant.URL.baseURL + "api/v4/updateProvider"
        static let getGlobalProviderList = Constant.URL.baseURL + "api/v4/getProviderList"
        static let deleteProvider = Constant.URL.baseURL + "api/v2/removeUserProvider"
        static let getMyProviders = Constant.URL.baseURL + "api/v2/getUserProviders"
        static let getShareDataMyProvider = Constant.URL.baseURL + "api/v5/my-provider-list"
        static let sendPdfDetail = Constant.URL.baseURL + "api/v5/send-pdf-details"
        static let assignProvider = Constant.URL.baseURL + "api/v2/assignProvider"
        static let getProviderDetail = Constant.URL.baseURL + "api/v4/editProvider"
        static let getProviderListSearch = Constant.URL.baseURL + "api/v2/getProvider"
        

        //Medical Condition
        static let addMedicalCondition = Constant.URL.baseURL + "api/v2/save-medical-conditions"
        static let getMedicalConditions = Constant.URL.baseURL + "api/v2/get-medical-conditions"
        static let getMedicalConditionDetail = Constant.URL.baseURL + "api/v2/get-medical-conditions/"
        //
        static let deleteMedicalSafe = Constant.URL.baseURL + "api/v2/delete-medical-safe"
        static let getReportDetailCustomer = Constant.URL.baseURL + "api/v5/get-provider-report-details"
        static let getReportDetailProvider = Constant.URL.baseURL + "api/v5/get-customer-report-details/"
        

    }
    
    struct ServiceType {
        static var get = "GET"
        static var post = "POST"
    }
    
    struct SeriveConstant {
        static var status = "status"
        static var data = "data"
        static var message = "message"
        static var otp = "otp"
        static var confirmation = "confirmation"
        static var userData = "userData"
    }
    
    struct HeaderConstant {
        static var appKey = "ABCDEFGHK"
        static var token = ""
        static var authorization = ""//"Bearer " + token
        static var accessToken = ""
    }
    
    struct NotificationObserverName {
        static var selectedFitnessDevices = "selectedFitnessDevices"
        static var selectedMedicalDevices = "selectedMedicalDevices"
        static var selectedConsumerProfileImage = "selectedConsumerProfileImage"
    }

}

struct Color {
    static var labelGreyColor = UIColor(red: 131/255, green: 131/255, blue: 131/255, alpha: 1.0)
    static var textFieldBorderColor = UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0)
    static var textFieldPlaceholderColor = UIColor(red: 83/255, green: 83/255, blue: 83/255, alpha: 1.0)
    static var lightTextFieldPlaceholderColor = UIColor(red: 133/255, green: 133/255, blue: 133/255, alpha: 1.0)
}

struct UserDefaultKey {
    static let appFirstLaunch = "screenFirstLaunch"
    static let userProfile = "userProfile"
}

var isInternetActive : Bool!

struct StoryboardNames {
    static let ProviderStoryboard = "ProviderStoryboard"
    static let Profile = "Profile"
    static let Main = "Main"
    static let Caregiver = "CareGiver"
    static let Report = "Report"
    static let TabBar = "TabBar"
    static let Medication = "Medication"
    static let medicalCondition = "MedicalCondition"
}

struct StoryBoardIDs {
    static let RegisterProfileViewController = "RegisterProfileViewController"
    static let ProviderSignupViewController = "ProviderSignupViewController"
    static let ResetPasswordViewController = "ResetPasswordViewController"
    static let ProviderProfileViewController = "ProviderProfileViewController"
    static let MyProfileViewController = "MyProfileViewController"
//    static let AddDocumentViewController = "AddDocumentViewController"
}

extension NSObject {
    class var className: String {
        return String(describing: self)
    }
}
