//
//  Extensions.swift
//  SocialNetworkingApp
//
//  Created by Arshdeep Singh on 11/15/19.
//  Copyright © 2019 Gurwinder Singh. All rights reserved.
//

import UIKit

class Extensions: NSObject {

}

extension UISearchBar {
    func setSearchBarUI() {
        let textFieldInsideSearchBar = self.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.layer.borderWidth = 1.0
        textFieldInsideSearchBar?.layer.borderColor = UIColor.darkGray.cgColor
        textFieldInsideSearchBar?.layer.cornerRadius = 4
        textFieldInsideSearchBar?.backgroundColor = UIColor.white
        textFieldInsideSearchBar?.frame.size.height = 50
        textFieldInsideSearchBar?.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1.0)
        textFieldInsideSearchBar?.leftView?.tintColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1.0)
    }
}

extension UIView {
    
    func gradienteBackground(colors: GradientColors, orientation: GradientOrientation) {
        let gradient = CAGradientLayer()
        self.removeLayer(layerName: "Gradiente")
        gradient.colors = [colors.initColor.cgColor, colors.endColor.cgColor]
        gradient.startPoint = orientation.points().startPoint
        gradient.endPoint = orientation.points().endPoint
        gradient.frame = self.layer.bounds
        gradient.name = "Gradiente"
        
        self.layer.insertSublayer(gradient, at: 0)
    }

    func removeLayer(layerName: String) {
        for item in self.layer.sublayers ?? [] where item.name == layerName {
            item.removeFromSuperlayer()
        }
    }
    
    func dropShadow(cornerRadius: CGFloat = 10, shadowOpacity: CGFloat = 0.4) {
      //    self.layer.cornerRadius = cornerRadius
      //    let shadowPath2 = UIBezierPath(rect: self.bounds)
      //    self.layer.masksToBounds = false
      //    self.layer.shadowColor = UIColor.black.cgColor
      //    self.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(3.0))
      //    self.layer.shadowOpacity = 0.5
      //    self.layer.shadowPath = shadowPath2.cgPath
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowOpacity = Float(shadowOpacity)//0.4
        self.layer.shadowOffset = CGSize(width: 1, height: 3)//CGSize.zero
        self.layer.shadowRadius = 3
        self.layer.shadowColor = UIColor.gray.cgColor
    }
}

extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: size.height - lineWidth, width: size.width, height: lineWidth))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

extension UITextField {
    func setBorderColor(color: UIColor = UIColor(red: 156/255, green: 156/255, blue: 156/255, alpha: 1.0)) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 0.8
        self.layer.cornerRadius = 4
    }
    
    func setUnderLine() {
        DispatchQueue.main.async {
            let border = CALayer()
            let width = CGFloat(0.5)
            border.borderColor = UIColor.lightGray.cgColor
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
            
            border.borderWidth = width
            self.layer.addSublayer(border)
            self.layer.masksToBounds = true
        }
    }
    
    func setRightImage(rightImage: UIImage) {
        
        self.rightViewMode = .always
        let rightImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 28))
        rightImageView.image = rightImage

        let rightView = UIView()
        rightView.frame = CGRect(x: 0, y: 0, width: 30+3, height: 28)
        rightView.addSubview(rightImageView)

        self.rightView?.frame = rightView.frame

        rightImageView.backgroundColor = .clear
        rightImageView.contentMode = .center
        self.rightView = rightView
    }
    
    func setLeftImage(leftImage: UIImage) {
        self.leftViewMode = .always
        let leftImageView = UIImageView(frame: CGRect(x: 3, y: 0, width: 30, height: 20))
        leftImageView.image = leftImage
        
        let leftView = UIView()
        leftView.frame = CGRect(x: 3, y: 0, width: 30, height: 20)
        leftView.addSubview(leftImageView)
        
        self.leftView?.frame = leftView.frame
        
        leftImageView.backgroundColor = .clear
        leftImageView.contentMode = .scaleAspectFit
        self.leftView = leftView
    }
    
    func borderedTextField(borderColor: UIColor, borderWidth: CGFloat, cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
    
}

extension UITextView {
    
    func dropShadowOnTextView() {
        self.layer.cornerRadius = 6
        self.clipsToBounds = false
        self.layer.shadowOpacity=0.4
        self.layer.shadowRadius = 3
        self.layer.shadowOffset = CGSize(width: 1, height: 3)
        self.layer.shadowColor = UIColor.gray.cgColor
    }
    
    func addBorder(borderColor: UIColor, borderWidth: CGFloat, cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
    
}

extension UIColor {
    
    func setColor(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: alpha)
    }
    
}

extension String {
    static func trim(_ string: String?) -> String {
        return string?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? ""
    }
    
    func isValidPassword() -> Bool? {
        let regularExpression = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}"
        let passwordValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)
        return passwordValidation.evaluate(with: self)
    }
    
    func convertToDateString(currentDateFormat: String, requiredDateFormat: String) -> String {
        let formater = DateFormatter()
        formater.dateFormat = currentDateFormat
        let date = formater.date(from: self) ?? Date()
        formater.dateFormat = requiredDateFormat
        let stringDate = formater.string(from: date)
        return stringDate
    }
    
    func convertToDate() -> Date? {
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat
         //"dd MMM, yyyy"
        if let date = formater.date(from: self) {
            return date
        }
        return nil
    }
}

extension NSObject {
    @objc public func topControllerInHierarchy() -> UIViewController? {
        let controller = UIApplication.shared.keyWindow?.rootViewController
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
            
    }

    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

//@IBDesignable
//class CardView: UIView {
//    
//    @IBInspectable override var cornerRadius: CGFloat {
//        get {
//            return layer.cornerRadius
//        }
//        set {
//            layer.cornerRadius = newValue
//            layer.shadowRadius = newValue
//            layer.masksToBounds = false
//        }
//    }
//    
//    @IBInspectable override var shadowOpacity: Float {
//        get {
//            return layer.shadowOpacity
//        }
//        set {
//            layer.shadowOpacity = newValue
//            layer.shadowColor = UIColor.gray.cgColor
//        }
//    }
//    
//    @IBInspectable override var shadowOffset: CGSize {
//        get {
//            return layer.shadowOffset
//        }
//        set {
//            layer.shadowOffset = newValue
//            layer.shadowColor = UIColor.gray.cgColor
//            layer.masksToBounds = false
//        }
//    }
//    
//}
@IBDesignable
class TextField: UITextField {
    @IBInspectable var insetX: CGFloat = 0
    @IBInspectable var insetY: CGFloat = 0
    
    // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
    
    // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
}
extension String {
    func localizedString() ->String {
        return NSLocalizedString(self, comment: "")
    }
}

extension UIButton {
    
    func customizeButton(titleTopInset: CGFloat) {
        let imageSize: CGSize = self.imageView!.image!.size
        self.titleLabel?.textAlignment = .center
        self.titleEdgeInsets = UIEdgeInsets(top: titleTopInset , left: -imageSize.width, bottom: 0.0, right: 0.0);
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10.0, bottom: 0.0, right: -(self.titleLabel?.frame.size.width)!)
        self.backgroundColor = .clear
    }
    
    func customizeVitalsButton(titleTopInset: CGFloat) {
        let imageSize: CGSize = self.imageView!.image!.size
        self.titleEdgeInsets = UIEdgeInsets(top: titleTopInset , left: -imageSize.width, bottom: 0.0, right: 0.0);
        let labelString = NSString(string: self.titleLabel!.text!)
        let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: self.titleLabel!.font])
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0.0, bottom: 0.0, right: -titleSize.width)
        self.backgroundColor = .clear
        self.titleLabel?.baselineAdjustment = .alignCenters
    }
    
}

class ButtonWithImage: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 35), bottom: 5, right: 5)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)!)
        }
    }
}

extension UITableView {
    func getSelectedIndexPath(sender: UIButton?) -> IndexPath? {
        let point = sender?.convert(CGPoint.zero, to: self)
        let indexPath = self.indexPathForRow(at: point!)
        return indexPath!
    }
}

extension UILabel {
    
    func underlinedLabel() {
        guard let text = self.text else {return}
        let textRange = NSMakeRange(0, text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
        // Add other attributes if needed
        self.attributedText = attributedText
    }
    
    func setString(strings:[String], fontSize:[CGFloat], color:[UIColor], fonts:[String], alignment:NSTextAlignment) /*-> NSMutableAttributedString*/ {
        
        //Get All Strings
        
        let attrString = NSMutableAttributedString()
        
        for i in 0..<strings.count{
            let substring: NSString = strings[i] as NSString
            
            // Set Paragraph style for text.
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = alignment
            
            let color = color[i]
            let font:UIFont? = UIFont(name: fonts[i] as String, size: fontSize[i] as CGFloat)
            
            // Make attributed string by adding custom font, custom color and paragraph style in it.
            let attrString1 = NSMutableAttributedString(string: substring as String)
            attrString1.addAttribute(NSAttributedString.Key.font, value: font!, range: NSMakeRange(0, attrString1.length))
            attrString1.addAttribute(NSAttributedString.Key.foregroundColor, value: color ,range: NSMakeRange(0, attrString1.length))
            attrString1.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle ,range: NSMakeRange(0, attrString1.length))
            attrString.append(attrString1)
        }
        
        self.attributedText = attrString
//        return attrString
    }
    
    func borderedLabel(borderColor: UIColor, borderWidth: CGFloat, cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
}
extension UIViewController {
    func checkIfDarkMode() -> Bool {
        if #available(iOS 12.0, *) {
            if traitCollection.userInterfaceStyle == .dark {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
}
extension Date {
    func dateString(withFormat format: String = "yyyy-MM-dd") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

extension UITextView {
    func setString(strings:[String], fontSize:[CGFloat], color:[UIColor], fonts:[String], alignment:NSTextAlignment) /*-> NSMutableAttributedString*/ {
        
        //Get All Strings
        
        let attrString = NSMutableAttributedString()
        
        for i in 0..<strings.count{
            let substring: NSString = strings[i] as NSString
            
            // Set Paragraph style for text.
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = alignment
            
            let color = color[i]
            let font:UIFont? = UIFont(name: fonts[i] as String, size: fontSize[i] as CGFloat)
            
            // Make attributed string by adding custom font, custom color and paragraph style in it.
            let attrString1 = NSMutableAttributedString(string: substring as String)
            attrString1.addAttribute(NSAttributedString.Key.font, value: font!, range: NSMakeRange(0, attrString1.length))
            attrString1.addAttribute(NSAttributedString.Key.foregroundColor, value: color ,range: NSMakeRange(0, attrString1.length))
            attrString1.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle ,range: NSMakeRange(0, attrString1.length))
            attrString.append(attrString1)
        }
        
        self.attributedText = attrString
//        return attrString
    }
}
