//
//  WidgetViewModel.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/16/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

class WidgetViewModel: NSObject {
    
    static let shared = WidgetViewModel()
    
    func updateWidgets(userData: [String: String], completionHandler: @escaping(Bool) -> (Void)) {
        
        if !InternetConnection.isConnected {
            Utility.shared.stopLoader()
            Utility.shared.showToast(message: "No internet connection!")
            completionHandler(false)
            return
        }
        Utility.shared.startLoader()
        AWSMobileClient.default().updateUserAttributes(attributeMap: userData) { (result, error) in
            Utility.shared.stopLoader()
            if let result = result {
                
                let userData = UserDefaults.standard.object(forKey: UserDefaultKey.userProfile) as? NSData
                
                if let userData = userData {
                    let userDetails = NSKeyedUnarchiver.unarchiveObject(with: userData as Data)
                    var userDetailDict = userDetails as? [String: Any] ?? [:]
                    
                    userDetailDict["custom:new_user"] = "0"
                    print(userDetailDict)
                    UserDefaults.standard.removeObject(forKey: UserDefaultKey.userProfile)
                    
                    let userDetail = NSKeyedArchiver.archivedData(withRootObject: userDetailDict as Any)
                    UserDefaults.standard.set(userDetail, forKey: UserDefaultKey.userProfile)
                }
                
                UserDefaults.standard.set("0", forKey: "isNewUser")
                Utility.shared.showToast(message: "Widgets updated successfully")
                completionHandler(true)
            } else if let error = error {
                print("\(error.localizedDescription)")
                Utility.shared.showToast(message: "Error occurred while updating widgets")
//                                    Utility.shared.showToast(message: self.handleErrors(error: error))
                completionHandler(false)
            }
        }
    }
    
    
    func updateUserStatus(userData: [String: String], completionHandler: @escaping(Bool) -> (Void)) {
        
        if !InternetConnection.isConnected {
            Utility.shared.stopLoader()
            Utility.shared.showToast(message: "No internet connection!")
            completionHandler(false)
            return
        }
        Utility.shared.startLoader()
        AWSMobileClient.default().updateUserAttributes(attributeMap: userData) { (result, error) in
            Utility.shared.stopLoader()
            if let result = result {
                
                let userData = UserDefaults.standard.object(forKey: UserDefaultKey.userProfile) as? NSData
                
                if let userData = userData {
                    let userDetails = NSKeyedUnarchiver.unarchiveObject(with: userData as Data)
                    var userDetailDict = userDetails as? [String: Any] ?? [:]
                    
                    userDetailDict["custom:new_user"] = "0"
                    print(userDetailDict)
                    UserDefaults.standard.removeObject(forKey: UserDefaultKey.userProfile)
                    
                    let userDetail = NSKeyedArchiver.archivedData(withRootObject: userDetailDict as Any)
                    UserDefaults.standard.set(userDetail, forKey: UserDefaultKey.userProfile)
                }
                
                UserDefaults.standard.set("0", forKey: "isNewUser")
                completionHandler(true)
            } else if let error = error {
                print("\(error.localizedDescription)")
                Utility.shared.showToast(message: "Error occurred")
//                                    Utility.shared.showToast(message: self.handleErrors(error: error))
                completionHandler(false)
            }
        }
    }
    
}
