//
//  ForgotPasswordViewModel.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 8/5/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class ForgotPasswordViewModel: NSObject {

    static func forgotPassword(parameters: [String: Any], completion: @escaping (_ response: Bool?, _ error: String?) -> Void) {
        
        let url = Constant.URL.forgotPassword
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey]
        
        
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        completion(true , result["message"] as? String)
                    } else {
                        completion(false , result["message"] as? String)
                    }
                } else {
                    completion(false , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
    static func confirmOtpAndChangePassword(parameters: [String: Any], completion: @escaping (_ response: Bool?, _ error: String?) -> Void) {
        
        let url = Constant.URL.confirmOtpAndChangePassword
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: parameters as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        completion(true , result["message"] as? String)
                    } else {
                        completion(false , result["message"] as? String)
                    }
                } else {
                    completion(false , result["message"] as? String)
                }
            } else {
                completion(nil , error?.localizedDescription)
            }
        }
    }
    
}
