//
//  OtpAndChangePasswordViewController.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 8/6/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class OtpAndChangePasswordViewController: UIViewController {

    //MARK: - Properties
    var registeredEmail: String?
    
    //MARK: - Outlets
    @IBOutlet weak var textFieldNewPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldConfirmNewPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldVerificationCode: SkyFloatingLabelTextField!

    @IBOutlet weak var newPassDisplayToggleButton: UIButton!
    @IBOutlet weak var confirmPassDisplayToggleButton: UIButton!
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    //MARK: - UIButton Actions
    @IBAction func newPasswordDisplayToggleButtonAction(_ sender: Any) {
        if textFieldNewPassword.isSecureTextEntry {
            textFieldNewPassword.isSecureTextEntry = false
            newPassDisplayToggleButton.setImage(UIImage(named: "unhide_password"), for: .normal)
        } else {
            textFieldNewPassword.isSecureTextEntry = true
            newPassDisplayToggleButton.setImage(UIImage(named: "hide_password"), for: .normal)
        }
    }
    @IBAction func confirmPasswordDisplayToggleButtonAction(_ sender: Any) {
        if textFieldConfirmNewPassword.isSecureTextEntry {
            textFieldConfirmNewPassword.isSecureTextEntry = false
            confirmPassDisplayToggleButton.setImage(UIImage(named: "unhide_password"), for: .normal)
        } else {
            textFieldConfirmNewPassword.isSecureTextEntry = true
            confirmPassDisplayToggleButton.setImage(UIImage(named: "hide_password"), for: .normal)
        }
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        let (status, errorMessage) = Utility.shared.otpAndChangePasswordValidation(newPassword: textFieldNewPassword, confirmPassword: textFieldConfirmNewPassword, verificationCode: textFieldVerificationCode)
        if status == false{
            Utility.shared.showToast(message: errorMessage ?? "")
        } else {
            AWSAuth.shared.confirmAWSForgotPassword(emailID: registeredEmail ?? "", newPassword: textFieldNewPassword.text ?? "", confirmationCode: textFieldVerificationCode.text ?? "") { (success, message) -> (Void) in
                if success {
                    DispatchQueue.main.async {
                        self.textFieldNewPassword.text = ""
                        self.textFieldConfirmNewPassword.text = ""
                        self.textFieldVerificationCode.text = ""
                        
                        guard let controller = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: AuthenticationViewController.className) as? AuthenticationViewController else { return }
                        AWSAuth.shared.signOutAWS()
                        let navigationController = UINavigationController(rootViewController: controller)
                        navigationController.navigationBar.isHidden = true
                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                        appdelegate.window!.rootViewController = navigationController
                        Utility.shared.showAlert(message: message, controller: controller)
                    }
                } else {
                    Utility.shared.showToast(message: message)
                }
            }
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
