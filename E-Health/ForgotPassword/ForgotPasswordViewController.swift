//
//  ForgotPasswordViewController.swift
//  E-Health
//
//  Created by Akash Dhiman on 21/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

class ForgotPasswordViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var emailTextField: ExtendedTextField!
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*if Constant.userProfile?.isEmailVerified == "false" {
            Utility.shared.showToast(message: "Please verify your email")
        }*/
        //Get access token
        //Code to verify email
        /*dAWSMobileClient.default().getTokens { (tokens, error) in
            print(tokens)
            print(error)
            //Verify Email
            o {
                let requestDict = ["AccessToken": tokens?.accessToken?.tokenString ?? "", "AttributeName": "email", "Code": "633869"]
                let request = try AWSCognitoIdentityProviderVerifyUserAttributeRequest(dictionary: requestDict, error: ())
                AWSCognitoIdentityProvider.default().verifyUserAttribute(request) { (response, error) in
                    print(response)
                    print(error)
                }
            } catch {
            }*/
            //Get verification code on email
            /*do {
                let requestDict = ["AccessToken": tokens?.accessToken?.tokenString ?? "", "AttributeName": "email"]
                let request = try AWSCognitoIdentityProviderGetUserAttributeVerificationCodeRequest(dictionary: requestDict, error: ())
                AWSCognitoIdentityProvider.default().getUserAttributeVerificationCode(request) { (response, error) in
                    print(response)
                    print(error)
                }
            } catch {
            }
        }*/
        
    }
    
    //MARK: - UIButton Actions
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendButtonAction(_ sender: Any) {
        let (status, errorMessage) = Utility.shared.forgotPasswordValidation(emailTextField: emailTextField)
        if status == false{
            Utility.shared.showToast(message: errorMessage ?? "")
        } else {
            AWSAuth.shared.forgotAWSPassword(emailID: emailTextField.text ?? "", controller: self) { (success, message) -> (Void) in
                if success {
                    DispatchQueue.main.async {
                        let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: OtpAndChangePasswordViewController.className) as? OtpAndChangePasswordViewController
                        
                        vc?.registeredEmail = self.emailTextField.text ?? ""
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                } else {
                    Utility.shared.showToast(message: message)
                }
            }
        }
    }
}

//MARK: - Text field Delegate
extension ForgotPasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard range.location == 0 else {
            return true
        }
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
        return newString.rangeOfCharacter(from: CharacterSet.whitespacesAndNewlines).location != 0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

extension ForgotPasswordViewController {
    
    func callForgotPasswordApi(parameters: [String: Any]) {
        ForgotPasswordViewModel.forgotPassword(parameters: parameters) { (success, message) in
            if success! {
                DispatchQueue.main.async {
                    let vc = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: VerificationViewController.className) as? VerificationViewController
                    vc?.isFromForgotPassword = true
                    vc?.signupEmail = self.emailTextField.text ?? ""
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            } else {
                Utility.shared.showToast(message: message ?? "Something went wrong")
            }
        }
    }
    
}
