//
//  PhoneNumberVerificationViewController.swift
//  E-Health
//
//  Created by Abhishek Chugh on 5/13/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import FlagPhoneNumber

class PhoneNumberVerificationViewController: UIViewController {
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var phoneNumTextField: FPNTextField!
    
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    func setupUI() {
        phoneNumTextField.hasPhoneNumberExample = false // true by default
        phoneNumTextField.placeholder = "Mobile Number"
        phoneNumTextField.displayMode = .list
        listController.setup(repository: phoneNumTextField.countryRepository)
        phoneNumTextField.delegate = self
        phoneNumTextField.textColor = UIColor(red: 19/255, green: 160/255, blue: 221/255, alpha: 1.0)
        listController.didSelect = { [weak self] country in
            self?.phoneNumTextField.setFlag(countryCode: country.code)
            
        }
        
        submitButton.layer.cornerRadius = submitButton.frame.height/2
        
        submitButton.gradienteBackground(colors: (initColor: UIColor(rgb: 0x20B6F7), endColor: UIColor(rgb: 0x0A90CB)), orientation: .leftRight)
    }

    @IBAction func countryCodeButtonAction(_ sender: Any) {
    }
    @IBAction func submitButtonAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "VerificationViewController") as? VerificationViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension PhoneNumberVerificationViewController: FPNTextFieldDelegate {
    
    /// The place to present/push the listController if you choosen displayMode = .list
    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)
        listController.title = "Countries"
        self.present(navigationViewController, animated: true, completion: nil)
    }
    
    /// Lets you know when a country is selected
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code) // Output "France", "+33", "FR"
//        phoneNumTextField.set
    }
    
    /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            // Do something...
//            textField.getFormattedPhoneNumber(format: .E164),           // Output "+33600000001"
//            textField.getFormattedPhoneNumber(format: .International),  // Output "+33 6 00 00 00 01"
//            textField.getFormattedPhoneNumber(format: .National),       // Output "06 00 00 00 01"
//            textField.getFormattedPhoneNumber(format: .RFC3966),        // Output "tel:+33-6-00-00-00-01"
//            textField.getRawPhoneNumber()                               // Output "600000001"
        } else {
            // Do something...
        }
    }
}
