//
//  SelectDocumentVC.swift
//  E-Health
//
//  Created by Atinder Kaur on 2/3/21.
//  Copyright © 2021 AbhiShek Chugh. All rights reserved.
//

import UIKit

class SelectDocumentVC: UIViewController {
    
    @IBOutlet weak var txtfieldStartDate: UITextField!
    @IBOutlet weak var txtfieldEndDate: UITextField!
    @IBOutlet weak var tblViewOptions: UITableView!
    @IBOutlet weak var btnOthers: UIButton!
    @IBOutlet weak var btnMedicalSafe: UIButton!
    
    var datePicker = UIDatePicker()
    var selectedTextField = UITextField()
    var arrData = [["name":"Allergies", "id": "4", "selected": "no"], ["name":"Lab Reports", "id": "8", "selected": "no"],["name":"Insurance", "id": "3", "selected": "no"], ["name":"Medications", "id": "5", "selected": "no"], ["name":"Immunizations/Vaccines", "id": "6", "selected": "no"]]
    var selectedIds = [Int]()
    var typeId : String = "1"
    var documentListing = [DocumentListingModel]()
    var widget_Types: [WidgetModel] = []
    var OptionsSelected : String? = ""
    var selectedCareTaker: CaretakerModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtfieldStartDate.setLeftImage(leftImage: UIImage(named: "calenderMed")!)
        txtfieldEndDate.setLeftImage(leftImage: UIImage(named: "calenderMed")!)
        tblViewOptions.register(UINib(nibName: "ShareDataTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tblViewOptions.tableFooterView = UIView()
        OptionsSelected = "others"
        
        btnOthers.isSelected = true
        btnMedicalSafe.isSelected = false
        getOthersData()
        
        if Constant.masterData != nil {
        if Constant.masterData?.documentTypes.count != 0 {
                documentListing = Constant.masterData!.documentTypes
        } } else{
            getMasterData()
        }
    }
    
    func getMasterData() {
        RegisterViewModel.getMasterData() { (countries, error) in
            if error == nil{
                Constant.masterData = countries
                if Constant.masterData != nil {
                    if Constant.masterData?.documentTypes.count != 0 {
                        self.documentListing = Constant.masterData!.documentTypes
                    }
                }
            }
        }
    }
    
    @IBAction func actionSafe(_ sender: UIButton) {
        selectedIds.removeAll()
        
        if sender.tag == 101 {
            arrData.indices.forEach{ arrData[$0]["selected"] = "no" }
            tblViewOptions.reloadData()
            typeId = "2"
            btnOthers.isSelected = false
            btnMedicalSafe.isSelected = true
            getMedicalSafe()
        } else {
            documentListing.indices.forEach{ documentListing[$0].selected = "no" }
            tblViewOptions.reloadData()
            typeId = "1"
            btnOthers.isSelected = true
            btnMedicalSafe.isSelected = false
            getOthersData()
        }
    }
    
    func getMedicalSafe() {
        OptionsSelected = "medicalSafe"
        if documentListing.count != 0 {
            tblViewOptions.reloadData()
        } else {
            Utility.shared.showToast(message: "No document type available!")
        }
    }
    
    func getOthersData() {
        OptionsSelected = "others"
        tblViewOptions.reloadData()
    }
    
    @IBAction func actionProceed(_ sender: UIButton) {
        if self.txtfieldStartDate.text == "" {
            Utility.shared.showToast(message: "Please select from date")
            return
        }
        if self.txtfieldEndDate.text == "" {
            Utility.shared.showToast(message: "Please select to date")
            return
        }
        if selectedIds.count == 0 {
            Utility.shared.showToast(message: "Please select categories")
            return
        }
        if let toDate = self.txtfieldEndDate.text?.convertToDate() {
            if let fromDate = self.txtfieldStartDate.text?.convertToDate() {
                if fromDate > toDate {
                    self.view.makeToast("From Date cannot be greater than To Date")
                    return
                }
            }
        }
        
        let selectDoc = UIStoryboard.init(name: StoryboardNames.TabBar, bundle: Bundle.main).instantiateViewController(withIdentifier: "ShareDataProListVC") as? ShareDataProListVC
        
        if selectedCareTaker != nil {
            selectDoc?.selectedCareTaker = self.selectedCareTaker
        }
        selectDoc?.selectedIds = self.selectedIds
        selectDoc?.type_Ids = self.typeId
        selectDoc?.fromDate = self.txtfieldStartDate.text ?? ""
        selectDoc?.toDate = self.txtfieldEndDate.text ?? ""
        
        if selectDoc != nil {
            self.navigationController?.pushViewController(selectDoc!, animated: true)
            return
        }
    }

    @IBAction func actionBack(_ sender: Any) {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
}
extension SelectDocumentVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  OptionsSelected == "others" {
            return arrData.count
        } else {
            return documentListing.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell( withIdentifier: "cell", for: indexPath) as? ShareDataTableViewCell else {
            fatalError()
        }
        
        cell.btnCheck.tag = indexPath.row
        cell.delegate = self
        
        if  OptionsSelected == "others" {
            
            let dic = arrData[indexPath.row]
            cell.lblName.text = dic["name"]
            if dic["selected"] == "no" {
                cell.btnCheck.setImage(UIImage(named: "uncheck"), for: .normal)
            }
            else {
                
                cell.btnCheck.setImage(UIImage(named: "check"), for: .normal)
            }
        }
        else{
            cell.lblName.text = documentListing[indexPath.row].name
            if documentListing[indexPath.row].selected == "no" {
                cell.btnCheck.setImage(UIImage(named: "uncheck"), for: .normal)
            }
            else {
                cell.btnCheck.setImage(UIImage(named: "check"), for: .normal)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
}


extension SelectDocumentVC: ShareDataProtocol {
    func selectUnselectCat(sender: UIButton) {
        
        //
        if  OptionsSelected == "others" {
            if arrData[sender.tag]["selected"] == "no" {
                guard let selectedValue = Int(arrData[sender.tag]["id"] ?? "0") else { return  }
                selectedIds.append(selectedValue)
                print(selectedIds)
                
                arrData[sender.tag]["selected"] = "yes"
            }
            else {
                guard let selectedValue = Int(arrData[sender.tag]["id"] ?? "0") else { return  }
                selectedIds.removeAll { value in
                    return value == selectedValue
                }
                print(selectedIds)
                arrData[sender.tag]["selected"] = "no"
            }
            
        }
        else{
            
            
            if documentListing[sender.tag].selected == "no" {
                let selectedValue = documentListing[sender.tag].id ?? 0
                selectedIds.append(selectedValue)
                print(selectedIds)
                documentListing[sender.tag].selected = "yes"
            }
            else {
                let selectedValue = documentListing[sender.tag].id ?? 0
                selectedIds.removeAll { value in
                    return value == selectedValue
                }
                print(selectedIds)
                documentListing[sender.tag].selected = "no"
            }
        }
        
        
        tblViewOptions.reloadData()
    }
    
    
}

extension SelectDocumentVC {
    func showDatePicker(textField: UITextField){
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat
         //"dd MMM, yyyy"
        var date = Date()
        if textField.text != "" {
            date = formater.date(from: textField.text!) ?? Date()
        }
        
        let screenWidth = UIScreen.main.bounds.width
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
       // datePicker.minimumDate = Date()  //Calendar.current.date(byAdding: .year, value: -110, to: Date())
//        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 10, to: Date())
        datePicker.setDate(date, animated: true)
        
        if #available(iOS 14.0, *)
        {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()

//            datePicker.datePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        textField.inputAccessoryView = toolbar
        textField.inputView = datePicker
        selectedTextField = textField
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = Constant.dateFormat
        selectedTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}


extension SelectDocumentVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtfieldEndDate  || textField == txtfieldStartDate {
            self.showDatePicker(textField: textField)
        }

        return true
    }
}
extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
}
