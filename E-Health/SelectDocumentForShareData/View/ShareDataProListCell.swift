//
//  ShareDataProListCell.swift
//  E-Health
//
//  Created by Atinder Kaur on 2/11/21.
//  Copyright © 2021 AbhiShek Chugh. All rights reserved.
//

import UIKit

class ShareDataProListCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRelation: UILabel!
    @IBOutlet weak var lblAddedOn: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
