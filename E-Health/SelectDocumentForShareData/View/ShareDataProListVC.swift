//
//  ShareDataProListVC.swift
//  E-Health
//
//  Created by Atinder Kaur on 2/11/21.
//  Copyright © 2021 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient


class ShareDataProListVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    var myProvidersArray : [AddProvider] = []
    var selectedIds = [Int]()
    var type_Ids : String = ""
    var selectedCareTaker: CaretakerModel?
    var fromDate : String = ""
    var toDate : String = ""
    var selectedProviderIds = [Int]()
    var minorId : String = ""
    var senderName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.tableFooterView = UIView()
        getMyProviderList()
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getMyProviderList() {
        var params = [String: Any]()
        if selectedCareTaker != nil {
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0] as [String : Any]
                minorId = String(selectedCareTaker?.minor_id ?? 0)
                senderName = selectedCareTaker?.name ?? ""
            } else {
                params = ["cognito_user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0] as [String : Any]
                minorId = "0"
                senderName = Constant.userProfile?.name ?? ""
            }
        } else {
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0] as [String : Any]
            minorId = "0"
            senderName = Constant.userProfile?.name ?? ""
        }
        
        Utility.shared.startLoader()
        AddProviderViewModel.shareDatMyProviders(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                
                self.myProvidersArray = response!
                self.tblView.reloadData()
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    @IBAction func actionSaveData(_ sender: UIButton) {
        //******
        if selectedProviderIds.count == 0 {
            Utility.shared.showToast(message: "Please select provider")
            return
        }
        
        var parameters = [String: Any]()
        parameters["cognito_user_id"] = AWSMobileClient.default().username ?? ""
        parameters["document_type"] = selectedIds
        parameters["file_type"] = type_Ids
        parameters["from_date"] = fromDate
        parameters["minor_id"] = minorId
        parameters["page"] = "1"
        parameters["search_string"] = ""
        parameters["size"] = "500"
        parameters["to_date"] = toDate
        parameters["provider_id"] = selectedProviderIds
        parameters["sender_name"] = senderName
        
        Utility.shared.startLoader()
        AddProviderViewModel.sendPdfDetail(params: parameters) { (message, error) in
            Utility.shared.stopLoader()
            if error == nil {
                Utility.shared.showToast(message: message ?? "Provider updated successfully")
                self.navigationController?.popToRootViewController(animated: true)
               
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
        //***********
    }
    
    @IBAction func actionUntickTick(_ sender: UIButton) {
        
        if myProvidersArray[sender.tag].selected == "no" {
            let selectedValue = myProvidersArray[sender.tag].id ?? 0
            selectedProviderIds.append(selectedValue)
            print(selectedIds)
            myProvidersArray[sender.tag].selected = "yes"
               }
        else {
            let selectedValue = myProvidersArray[sender.tag].id ?? 0
            selectedProviderIds.removeAll { value in
               return value == selectedValue
             }
             print(selectedIds)
             myProvidersArray[sender.tag].selected = "no"
               }
        
        tblView.reloadData()
    }
    
}


extension ShareDataProListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if myProvidersArray.count > 0 {
         return myProvidersArray.count
            }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell( withIdentifier: "ShareDataProListCell", for: indexPath) as? ShareDataProListCell else {
            fatalError()
        }
        if myProvidersArray.count > 0 {
            
        cell.btnCheck.tag = indexPath.row
        cell.lblName.text = self.myProvidersArray[indexPath.row].providerName
        cell.lblRelation.text = "Category : " + (self.myProvidersArray[indexPath.row].type ?? "N.A")
        let date = self.myProvidersArray[indexPath.row].dateAdded?.convertToDateString(currentDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", requiredDateFormat: Constant.dateFormat)
        cell.lblAddedOn.text = "Added " + (date ?? "")
            
            if self.myProvidersArray[indexPath.row].selected == "no" {
               cell.btnCheck.setImage(UIImage(named: "uncheck"), for: .normal)
        }
        else {
            cell.btnCheck.setImage(UIImage(named: "check"), for: .normal)
        }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }
}
