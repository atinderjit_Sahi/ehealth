//
//  ShareDataTableViewCell.swift
//  E-Health
//
//  Created by Atinder Kaur on 2/3/21.
//  Copyright © 2021 AbhiShek Chugh. All rights reserved.
//

import UIKit

protocol ShareDataProtocol {
    func selectUnselectCat(sender: UIButton)
}


class ShareDataTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    var delegate: ShareDataProtocol?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBAction func actionCheckUncheck(_ sender: UIButton) {
        delegate?.selectUnselectCat(sender: sender)

    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
