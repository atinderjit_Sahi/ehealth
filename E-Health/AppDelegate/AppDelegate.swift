//
//  AppDelegate.swift
//  E-Health
//
//  Created by Abhishek Chugh on 5/11/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient
import IQKeyboardManagerSwift
import DropDown
import UserNotifications
import AWSSNS
import FBSDKCoreKit
import GoogleSignIn
import LoginWithAmazon

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let SNSPlatformApplicationArn = "arn:aws:sns:us-east-1:351947363766:app/APNS_SANDBOX/EHealth_iOS"
    var cognitoConfig:CognitoConfig?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        DropDown.startListeningToKeyboard()
        InternetReachability.startTracking()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        Constant.loggedInProfile = .none
        
//        VerificationViewModel.getAuthToken()
//        Thread.sleep(forTimeInterval: 4)
//        getMasterData()
        application.registerForRemoteNotifications()
        
//        Messaging.messaging().delegate = self
        
        //Google Signin Setup
        GIDSignIn.sharedInstance().clientID = "789308974382-iese1itha0vlsv0a1ecq4mm356g8bh96.apps.googleusercontent.com"
//        GIDSignIn.sharedInstance()?.serverClientID = "168129560419-umdu8nnrb7arqo3eiagnugmkemcj6hdv.apps.googleusercontent.com"
        
        /// Setup AWS Cognito credentials
        let credentialsProvider = AWSCognitoCredentialsProvider(
            regionType: AWSRegionType.USEast1, identityPoolId: Constant.AWS.identityPoolId)
        
        let defaultServiceConfiguration = AWSServiceConfiguration(
            region: AWSRegionType.USEast1, credentialsProvider: credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = defaultServiceConfiguration
        
        registerForPushNotifications(application: application)
//        registerForPushNotifications()
        // Initialize AWSMobileClient singleton
        AWSDDLog.add(AWSDDTTYLogger.sharedInstance)
        AWSDDLog.sharedInstance.logLevel = .verbose
        
        AWSMobileClient.default().initialize { (userState, error) in
            if let userState = userState {
                print("UserState: \(userState.rawValue)")
            } else if let error = error {
                print("error: \(error.localizedDescription)")
            }
        }
        
        //When user open app for the first time
        if UserDefaults.standard.integer(forKey: UserDefaultKey.appFirstLaunch) == 1 {
            getUserProfile()
        } else {
            UserDefaults.standard.set(1, forKey: UserDefaultKey.appFirstLaunch)
            AWSAuth.shared.signOutAWS()
            goToGetStartedScreen()
        }
        
        //FaceBook
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }
    
    func getUserProfile() {
//                if AWSAuth.shared.userAWSStatus() == .signedIn {
//                    AWSAuth.shared.getUserAWSAttributes { (data, error) -> (Void) in
//                        guard error == nil else {
//                            return
//                        }
//                        Constant.userProfile = ProfileDetail(value: data)
//
//                        UserDefaults.standard.set(Constant.userProfile?.isNewUser, forKey: "isNewUser")
//                        UserDefaults.standard.removeObject(forKey: UserDefaultKey.userProfile)
//                        let userDetail = NSKeyedArchiver.archivedData(withRootObject: data as Any)
//                        UserDefaults.standard.set(userDetail, forKey: UserDefaultKey.userProfile)
        
        let userData = UserDefaults.standard.object(forKey: UserDefaultKey.userProfile) as? NSData
        UserDefaults.standard.object(forKey: UserDefaultKey.userProfile) as? NSData
        if let userData = userData {
            
            let userDetails = NSKeyedUnarchiver.unarchiveObject(with: userData as Data)
            let userDetailDict = userDetails as? [String: Any] ?? [:]
            
            Constant.userProfile = ProfileDetail(value: userDetailDict)
            
            if Constant.userProfile?.family_name == "consumer" {
                Constant.loggedInProfile =  .consumer
                let isSocial = UserDefaults.standard.value(forKey: "isSocialLogin") as? Int ?? 0
                Constant.isSocialLogin = isSocial == 1 ? true : false
                
                //API TO POST USER's COGNITO SUB ID TO DATABASE
                //                RegisterViewModel.postCognitoUserID(userID: AWSMobileClient.default().username ?? "") { result in
                if Constant.userProfile?.isNewUser == "0" {
                    self.goToDashboardScreen()
                } else if Constant.userProfile?.isNewUser == "1" {
                    self.goToWidgetsScreen()
                } else {
                    self.goToDashboardScreen()
                }
                //                }
            } else if Constant.userProfile?.family_name == "provider"{
                Constant.loggedInProfile =  .provider
                if Constant.userProfile?.isNewUser == "0" {
                    self.goToDashboardScreen()
                } else {
                    self.goToWidgetsScreen()
                }
            } else {
                Constant.loggedInProfile = .none
            }
        } else {
            goToAuthenticationScreen()
        }
    }
    
    func goToAuthenticationScreen() {
        DispatchQueue.main.async {
            let initialViewController = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: AuthenticationViewController.className)
            
            let nc = UINavigationController(rootViewController: initialViewController)
            nc.isNavigationBarHidden = true
            
            self.window?.rootViewController = nc
            self.window?.makeKeyAndVisible()
        }
    }
    
    func goToGetStartedScreen() {
        DispatchQueue.main.async {
            let initialViewController = UIStoryboard.init(name: StoryboardNames.Main, bundle: Bundle.main).instantiateViewController(withIdentifier: GetStartedViewController.className)
            
            let nc = UINavigationController(rootViewController: initialViewController)
            nc.isNavigationBarHidden = true
            
            self.window?.rootViewController = nc
            self.window?.makeKeyAndVisible()
        }
    }
    
    func goToWidgetsScreen() {
        DispatchQueue.main.async {
          
            let initialViewController = UIStoryboard.init(name: StoryboardNames.Profile, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardIDs.MyProfileViewController)
            
            let nc = UINavigationController(rootViewController: initialViewController)
            nc.isNavigationBarHidden = true
            
            self.window?.rootViewController = nc
            self.window?.makeKeyAndVisible()
        }
    }
    
    func goToDashboardScreen() {
        DispatchQueue.main.async {
            let initialViewController = UIStoryboard.init(name: "TabBar", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBarViewController")
            
            let nc = UINavigationController(rootViewController: initialViewController)
            nc.isNavigationBarHidden = true
            
            self.window?.rootViewController = nc
            self.window?.makeKeyAndVisible()
        }
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        if url.scheme == "fb3533343310023488" {
            return AWSMobileClient.default().interceptApplication(application, open: url, sourceApplication: sourceApplication,             annotation: annotation)
        } else if url.scheme == "com.googleusercontent.apps.789308974382-iese1itha0vlsv0a1ecq4mm356g8bh96" {
            GIDSignIn.sharedInstance()?.handle(url)
            return AWSMobileClient.default().interceptApplication(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
            //            GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
        } else if url.scheme == "amzn-com.seasia.eHealth" {
            return AWSMobileClient.default().interceptApplication(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        } else if url.scheme == "E-Health" {
            let notification = Notification(
                name: Notification.Name(rawValue: "SampleBitLaunchNotification"),
                object:nil,
                userInfo:[UIApplication.LaunchOptionsKey.url:url])
            NotificationCenter.default.post(notification)
            return true
        }
        return false
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.scheme == "e-health" {
            let notification = Notification(
                name: Notification.Name(rawValue: "SampleBitLaunchNotification"),
                object:nil,
                userInfo:[UIApplication.LaunchOptionsKey.url:url])
            NotificationCenter.default.post(notification)
            return true
        }
        return ApplicationDelegate.shared.application( app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
    }
    
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        let appvar = UIApplication.shared.delegate as! AppDelegate
//        let result = appvar.application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation]!
//        )
//        return result
//    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: Push Notification methods
    func registerForPushNotifications(application: UIApplication) {
        /// The notifications settings
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted) {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
                else {
                    //Do stuff if unsuccessful...
                }
            })
        } else {
            let settings = UIUserNotificationSettings(types: [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current()
            .requestAuthorization(options:[.alert, .sound, .badge]) {[weak self] granted, error in
                print("Permission granted: \(granted)")
                guard granted else { return }
                // Only get the notification settings if user has granted permissions
                self?.getNotificationSettings()
        }
    }

    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                // Register with Apple Push Notification service
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func getMasterData() {
        RegisterViewModel.getMasterData() { (masterData, error) in
            if error == nil{
                Constant.masterData = masterData
            }
        }
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    // MARK: Remote Notifications Lifecycle
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        UIPasteboard.general.string = token
        // Register the device token with Pinpoint as the endpoint for this user
        /// Create a platform endpoint. In this case,  the endpoint is a
        /// device endpoint ARN
        let request = AWSSNSCreatePlatformEndpointInput()
        request?.token = token
        request?.platformApplicationArn = SNSPlatformApplicationArn
        AWSSNS.default().createPlatformEndpoint(request!).continueWith(executor: AWSExecutor.mainThread(), block: { (task: AWSTask!) -> AnyObject? in
            if task.error != nil {
                print("Error: \(String(describing: task.error))")
            } else {
                let createEndpointResponse = task.result! as AWSSNSCreateEndpointResponse
                
                if let endpointArnForSNS = createEndpointResponse.endpointArn {
                    print("endpointArn: \(endpointArnForSNS)")
                    Constant.AWS.endPointID = endpointArnForSNS
                    UserDefaults.standard.set(endpointArnForSNS, forKey: "endpointArnForSNS")
                }
            }
            
            return nil
        })
    }
    
    // Called when a notification is delivered to a foreground app.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("User Info = ",notification.request.content.userInfo)
        completionHandler([.alert, .badge, .sound])
    }
    
    // Called to let your app know which action was selected by the user for a given notification.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("User Info = ",response.notification.request.content.userInfo)
        
        moveToCareTakerListScreen()
        completionHandler()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // if the app is in the foreground, create an alert modal with the contents
        if (application.applicationState == .active) {
            let alert = UIAlertController(title: "Notification Received", message: userInfo.description, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion:nil)
        }
    }
    
    func moveToCareTakerListScreen() {
        DispatchQueue.main.async {
            let viewController = UIStoryboard.init(name: StoryboardNames.Caregiver, bundle: Bundle.main).instantiateViewController(withIdentifier: CareTakerListViewController.className)
            let navigationController = UINavigationController(rootViewController: viewController)
            navigationController.isNavigationBarHidden = true
            
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }
    }
}
