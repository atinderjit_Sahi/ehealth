//
//  CaretakerTableViewCell.swift
//  E-Health
//
//  Created by Prabjot Kaur on 7/3/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import DropDown

class CaretakerTableViewCell: DropDownCell {

    ///Properties
    @IBOutlet weak var careTakerNameLabel: UILabel!
    @IBOutlet weak var careTakerRelationLabel: UILabel!
    @IBOutlet weak var careTakerRequestTimeStampLabel: UILabel!
    @IBOutlet weak var careTakerImageView: UIImageView!
    
    ///Reference of delegate
    var delegate : AllCareTakerTableViewCellDelegate?
    ///Reference of of item passed
    var item: CaretakerModel? = CaretakerModel() {
        didSet {
            guard item != nil else {
                return
            }
            
            self.careTakerNameLabel.text = item?.name
            self.careTakerRelationLabel.text = item?.relationship_name
            
            if let profileImage = item?.profile_image  {
                DispatchQueue.main.async {
                    if let url = URL(string: profileImage) {
                        self.careTakerImageView.kf.indicatorType = .activity
                        self.careTakerImageView.kf.setImage(with: url)
                        self.careTakerImageView.layer.cornerRadius = self.careTakerImageView.frame.height/2
                        self.careTakerImageView.clipsToBounds = true
                    }
                }
            }
            
            if item?.status == 2 {
                self.careTakerRequestTimeStampLabel.text = "REJECTED"
            } else {
                let date = item?.created_at?.convertToDateString(currentDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", requiredDateFormat: Constant.dateFormat)
                self.careTakerRequestTimeStampLabel.text = "Added on " + (date ?? "")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
