//
//  HomeViewController.swift
//  E-Health
//
//  Created by Akash Dhiman on 20/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import DropDown
import AWSMobileClient
import FBSDKLoginKit

class HomeViewController: UIViewController {

    var consumerTitleArray = ["Allergy Reports", "Immunization Reports", "Insurance Documents", "Lab Reports", "Medications", "Medical Safe", "Profile Details", "Dependents/Caregivers", "Vital Stats", "Device Management", "Provider List"]
    var imageArray = [UIImage(named: "home_allergies"), UIImage(named: "home_immunization"), UIImage(named: "home_insuranceDocument"), UIImage(named: "home_lab"), UIImage(named: "home_medication"), UIImage(named: "home_medicalDocument"), UIImage(named: "home_profileDetails"), UIImage(named: "home_caretakerList"), UIImage(named: "home_vitalStats"), UIImage(named: "homeDevice"), UIImage(named: "home_providerList")]
    
    var providerHomeImageArray = [UIImage(named: "home_patient"), UIImage(named: "home_report")]
    var providerHomeWidgets = ["Patient", "Report Requests"]
    
    var dispatchGroup = DispatchGroup()
    var isLoadingFirstTime = true
    var allCaretakersList = [CaretakerModel]()
    var selectedCaretaker: CaretakerModel?
    
    let fbLoginManager = LoginManager()
    
    @IBOutlet weak var labelHealthRecords: UILabel!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var myProfileButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var careTakerTextField: UITextField!
    @IBOutlet weak var caretakerTypeDropDown: UIButton!
    
    var selectedWidgetItems: [Int] = []
    
    //MARK:- View Hierarchy
    override func viewDidLoad() {
        super.viewDidLoad()
        
        careTakerTextField.isHidden = true
        caretakerTypeDropDown.isHidden = true
        labelHealthRecords.isHidden = true
//        careTakerTextField.text = "My Dashboard"
//        careTakerTextField.setRightImage(rightImage: UIImage(named: "dropDown")!)
        careTakerTextField.setUnderLine()
        Utility.shared.startLoader()
        Constant.selectedCareTaker = nil
        //Get Auth Token
       
        //Calling in getAuthToken Response - Need to change it later
        dispatchGroup.notify(queue: .main) { [unowned self] in
            Utility.shared.stopLoader()
            self.isLoadingFirstTime = false
        }
        
        mainCollectionView.register(UINib(nibName: "ProviderDashboardCell", bundle: nil), forCellWithReuseIdentifier: "ProviderDashboardCell")
        
        if self.navigationItem != nil {
            self.navigationItem.setHidesBackButton(true, animated: true) }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
        firstTimeSignup =  false
        if !isLoadingFirstTime {
            Utility.shared.startLoader()
            getUserProfile()
            dispatchGroup.notify(queue: .main) { [unowned self] in
                Utility.shared.stopLoader()
                self.isLoadingFirstTime = false
            }
        }
        VerificationViewModel.getAuthToken() { (result) in
            self.getMasterData()
            self.getUserProfile()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       // mainCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    func saveSocialLoginUserData() {
        if Constant.userProfile?.family_name == "consumer" {
            Constant.loggedInProfile =  .consumer
            self.setProfileImage()
            DispatchQueue.main.async {
                self.careTakerTextField.isHidden = false
                self.caretakerTypeDropDown.isHidden = false
                self.labelHealthRecords.isHidden = true
            }
            
            //API TO POST USER's COGNITO SUB ID TO DATABASE
            
            RegisterViewModel.postCognitoUserID(userID: Constant.userProfile?.socialCognitoId ?? "0") { result in
                if result {
                    //API TO GET CARETAKER's LIST
                    self.getAllCaretakersList()
                }
            }
        }
    }
    
    func getMasterData() {
        dispatchGroup.enter()
        RegisterViewModel.getMasterData() { (countries, error) in
            self.dispatchGroup.leave()
            if error == nil{
                Constant.masterData = countries
            }
        }
    }
    
    func getUserProfile() {
        if AWSAuth.shared.userAWSStatus() == .signedIn {
            self.dispatchGroup.enter()
            AWSAuth.shared.getUserAWSAttributes { (data, error) -> (Void) in
                self.dispatchGroup.leave()
                guard error == nil else {
                    return
                }
              
                
                Constant.userProfile = ProfileDetail(value: data)
                
                let widgetItemsArray = Constant.userProfile?.widgets?.components(separatedBy: ",")
                self.selectedWidgetItems.removeAll()
                if widgetItemsArray != nil {
                    for widgetItem in widgetItemsArray! {
                        let widget = Int(widgetItem)
                        self.selectedWidgetItems.append(widget ?? 0)
                    }
                    self.selectedWidgetItems = self.selectedWidgetItems.sorted()
                }
                
                if Constant.userProfile?.family_name == "consumer" {
                    Constant.loggedInProfile =  .consumer
                    
                    if self.selectedCaretaker == nil {
                        DispatchQueue.main.async {
                            
                            self.careTakerTextField.text = "Hello " + (Constant.userProfile?.name ?? "")
                            
                            if Constant.userProfile?.middleName != nil && Constant.userProfile?.middleName != "" {
                            
                                let mname = Constant.userProfile?.middleName ?? ""
                                
                                let str = Constant.userProfile?.name
                                let strArray = str?.components(separatedBy: " ")
                                
                                if let lname = strArray?[1] {
                                if let fname = strArray?[0] {
                                    self.careTakerTextField.text = "Hello " +  "\(String(describing: fname))" + " " + mname + " " + "\(String(describing: lname))"

                                }
                                }

                            }
                        }
                    }
                    
                    self.setProfileImage()
                    DispatchQueue.main.async {
                        self.careTakerTextField.isHidden = false
                        self.caretakerTypeDropDown.isHidden = false
                        self.labelHealthRecords.isHidden = true
                    }
                    
                    //API TO POST USER's COGNITO SUB ID TO DATABASE
                    let userId = AWSMobileClient.default().username ?? ""
                    RegisterViewModel.postCognitoUserID(userID: userId) { result in
                        if result {
                            //API TO GET CARETAKER's LIST
                            self.getAllCaretakersList()
                        }
                    }
                } else if Constant.userProfile?.family_name == "provider"{
                    Constant.loggedInProfile =  .provider
                    DispatchQueue.main.async {
                        self.careTakerTextField.isHidden = true
                        self.caretakerTypeDropDown.isHidden = true
                        self.labelHealthRecords.isHidden = false
                    }
                    self.setProfileImage()
                    self.saveProviderProfileToDB()
                } else {
                    Constant.loggedInProfile =  .none
                    DispatchQueue.main.async {
                       self.careTakerTextField.isHidden = true
                        self.caretakerTypeDropDown.isHidden = true
                        self.labelHealthRecords.isHidden = false
                    }
                }
                DispatchQueue.main.async {
                    self.mainCollectionView.reloadData()
                }
            }
        }
    }
    
    func setProfileImage() {
        //SET PROFILE IMAGE
        if let profileImage = Constant.userProfile?.userImageUrl {
            let imageURLString = Constant.URL.baseURL + "uploads/profile_pictures/" + (Constant.userProfile?.sub ?? "") + "/\(profileImage)"
            DispatchQueue.main.async {
                if let url = URL(string: imageURLString) {
                    self.profileImageView.kf.indicatorType = .activity
                    self.profileImageView.kf.setImage(with: url, placeholder: UIImage(named: "profile_no_image"))
                }
            }
        } else {
            DispatchQueue.main.async {
                self.profileImageView.image = UIImage(named: "profile_no_image")
            }
        }
    }
    
    func saveProviderProfileToDB() {
        var params = [String: AnyObject]()
        
        //Provider Full Name
        var providerFullName = String()
        let name = Constant.userProfile?.name
        let middleName = Constant.userProfile?.middleName
        if middleName != "" || middleName != nil {
            let myStringArr = name?.components(separatedBy: " ")
            let firstName = (myStringArr?[0] ?? "")
            var lastName = ""
            if myStringArr?.count == 2 {
                lastName = (myStringArr?[1] ?? "")
            }
            providerFullName = firstName + " " + (middleName ?? "") + " " + lastName
        } else {
            providerFullName = name ?? ""
        }
        
        let providerType = Constant.userProfile?.providerType
        //IF PROVIDER TYPE NOT DOCTOR/HOSPITAL, HIDE FIELDS
        if providerType != "1" && providerType != "2" {
        }
        //SHOW FIELDS
        else {
            //For SPECIALIST FIELD
            if let specialityString = Constant.userProfile?.specialistIn {
                var specialityNamesArray = ""
                let specialitiesArray = specialityString.components(separatedBy: ";")
                if specialitiesArray.count != 0 {
                    for speciality in specialitiesArray {
                        let specialityDetailArray = speciality.components(separatedBy: "-")
                        specialityNamesArray.append((specialityDetailArray.last ?? "") + ",")
                    }
                    specialityNamesArray.removeLast()
                }
                params["speciality"] = specialityNamesArray as AnyObject
            }
            
            //FOR PRACTICE NAME FIELD
            if let practiceNameString = Constant.userProfile?.praticeName {
                var practiceNamesArray = ""
                let praticeArray = practiceNameString.components(separatedBy: ";")
                if praticeArray.count != 0 {
                    for practice in praticeArray {
                        let practiceDetailArray = practice.components(separatedBy: "-")
                        practiceNamesArray.append((practiceDetailArray.last ?? "") + ",")
                    }
                    practiceNamesArray.removeLast()
                }
                params["practice_name"] = practiceNamesArray as AnyObject
            }
        }
        
        params["cognito_id"] = Constant.userProfile?.sub as AnyObject
        params["provider_name"] = providerFullName as AnyObject
        params["email"] = Constant.userProfile?.email as AnyObject
        params["provider_type"] = Constant.userProfile?.providerType as AnyObject
        params["doctor_type"] = Constant.userProfile?.doctorType as AnyObject
        
        HomeViewModel.saveProvideProfileToDB(params: params) { (result, error) in
            
        }
    }
    
    @IBAction func actionShareData(_ sender: UIButton) {
        
//        let shareAll = ["https://apps.apple.com/in/app/afterlight-photo-editor/id1293122457"] as [Any]
//        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
//        activityViewController.popoverPresentationController?.sourceView = self.view
//        self.present(activityViewController, animated: true, completion: nil)
        
       // tabBarController?.view.makeToast("Coming soon")
        tabBarController?.tabBar.isHidden = true
        let selectDoc = UIStoryboard.init(name: StoryboardNames.TabBar, bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectDocumentVC") as? SelectDocumentVC
        
        if selectedCaretaker != nil {
            selectDoc?.selectedCareTaker = self.selectedCaretaker
        }
        
        self.navigationController?.pushViewController(selectDoc!, animated: true)
        return
    }
    
    @IBAction func setBaseLineButtonAction(_ sender: Any) {
        //tabBarController?.view.makeToast("Coming soon")
        moveToVitalStatsScreen()
    }
    
    @IBAction func setWidgetButtonAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        guard let vc = UIStoryboard.init(name: StoryboardNames.TabBar, bundle: Bundle.main).instantiateViewController(withIdentifier: SetWidgetViewController.className) as? SetWidgetViewController else {
            return
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func logoutButtonAction(_ sender: Any) {
        
        Utility.shared.showCustomAlertWithCompletion("Logout", message: "Are you sure you want to Logout?", withButtonTypes: ["No", "Yes"], withAlertCompletionBlock: { (alertAction, buttonTitle) in
            
            if buttonTitle == "Yes" {
                self.fbLoginManager.logOut()
                if self.navigationController != nil {
                    self.navigationController!.viewControllers.removeAll()
                }
                guard let controller = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AuthenticationViewController") as? AuthenticationViewController else { return }
                AWSAuth.shared.signOutAWS()
                Constant.loggedInProfile = .none
                Constant.selectedCareTaker = nil
                Constant.isSocialLogin = false
                Constant.userProfile = nil
                UserDefaults.standard.removeObject(forKey: UserDefaultKey.userProfile)
              //  UserDefaults.standard.removeObject(forKey: "emerContactName")
             //   UserDefaults.standard.removeObject(forKey: "emerContactNumber")
                Utility.shared.clearDocumentDirectory()
                let navigationController = UINavigationController(rootViewController: controller)
                navigationController.navigationBar.isHidden = true
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.window!.rootViewController = navigationController
            }
        })
    }
    
    @IBAction func careTakerDropDown(_ sender: Any) {
        if allCaretakersList.count != 0 {
            var dataSource = self.allCaretakersList.map({$0.name})
            dataSource.insert("My Dashboard", at: 0)
            self.initdropDown(textField: careTakerTextField, dataSource: dataSource as! [String])
        } else {
            tabBarController?.view.makeToast("No Caretaker's available!")
        }
    }
    
    @IBAction func myProfileButtonAction(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        if Constant.userProfile?.family_name == "consumer" {
            let vc = UIStoryboard.init(name: StoryboardNames.Profile, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardIDs.MyProfileViewController) as? MyProfileViewController
            
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        else {
            let vc = UIStoryboard.init(name: StoryboardNames.ProviderStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardIDs.ProviderProfileViewController) as? ProviderProfileViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    func getAllCaretakersList() {
        self.dispatchGroup.enter()
        
        CaretakerViewModel.getAllCaretakersList(userID: AWSMobileClient.default().username ?? "") { (response, error) in
            self.dispatchGroup.leave()
            self.allCaretakersList.removeAll()
            if self.selectedCaretaker == nil {
                self.careTakerTextField.text = "Hello " + (Constant.userProfile?.name ?? "")
                
                if Constant.userProfile?.middleName != nil && Constant.userProfile?.middleName != "" {
                    
                    let mname = Constant.userProfile?.middleName ?? ""
                    let str = Constant.userProfile?.name
                    let strArray = str?.components(separatedBy: " ")
                    
                    if let lname = strArray?[1] {
                        if let fname = strArray?[0] {
                            self.careTakerTextField.text = "Hello " +  "\(String(describing: fname))" + " " + mname + " " + "\(String(describing: lname))"
                            
                        }
                    }
                }
                Constant.selectedCareTaker = nil
            }
            if response != nil {
                self.allCaretakersList = response!
            } else {
                //                Utility.shared.showToast(message: error ?? "")
            }
        }
    }
}

//MARK:- Drop Down
extension HomeViewController {
    func initdropDown (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        dropDown.cellHeight = 102.0
        dropDown.cellNib = UINib(nibName: "CaretakerTableViewCell", bundle: nil)
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        
           guard let cell = cell as? CaretakerTableViewCell else { return }
            if index == 0{
                cell.careTakerNameLabel.text = dataSource[index]
                cell.careTakerRelationLabel.text = ""
                cell.careTakerRequestTimeStampLabel.text = ""
                if let profileImage = Constant.userProfile?.userImageUrl {
                    let imageURLString = Constant.URL.baseURL + "uploads/profile_pictures/" + (Constant.userProfile?.sub ?? "") + "/\(profileImage)"
                    DispatchQueue.main.async {
                        if let url = URL(string: imageURLString) {
                            cell.careTakerImageView.kf.indicatorType = .activity
                            cell.careTakerImageView.kf.setImage(with: url, placeholder: UIImage(named: "profile_no_image"))
                            cell.careTakerImageView.layer.cornerRadius = cell.careTakerImageView.frame.height/2
                            cell.careTakerImageView.clipsToBounds = true
                        }
                    }
                }
                return
            }
            cell.careTakerNameLabel.text = self.allCaretakersList[index-1].name
            cell.careTakerRelationLabel.text = self.allCaretakersList[index-1].relationship_name
            let date = self.allCaretakersList[index-1].created_at?.convertToDateString(currentDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", requiredDateFormat: Constant.dateFormat)
            cell.careTakerRequestTimeStampLabel.text = "Added on " + (date ?? "")
            
            if let profileImage = self.allCaretakersList[index-1].profile_image  {
                DispatchQueue.main.async {
                    if let url = URL(string: profileImage) {
                        cell.careTakerImageView.kf.indicatorType = .activity
                        cell.careTakerImageView.kf.setImage(with: url, placeholder: UIImage(named: "profile_no_image"))
                        cell.careTakerImageView.layer.cornerRadius = cell.careTakerImageView.frame.height/2
                        cell.careTakerImageView.clipsToBounds = true
                    }
                }
            }
        }
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                if index == 0{
//                    let firstname = Constant.userProfile?.firstName ?? ""
//                    let middleName = Constant.userProfile?.firstName ?? ""
//                    let lastname = Constant.userProfile?.firstName ?? ""
//                    if Constant.userProfile?.middleName != nil {
                    self.careTakerTextField.text = "Hello " + (Constant.userProfile?.name ?? "")
                    
                    if Constant.userProfile?.middleName != nil && Constant.userProfile?.middleName != "" {
                    
                        let mname = Constant.userProfile?.middleName ?? ""
                        
                        let str = Constant.userProfile?.name
                        let strArray = str?.components(separatedBy: " ")
                        
                        if let lname = strArray?[1] {
                        if let fname = strArray?[0] {
                            self.careTakerTextField.text = "Hello " +  "\(String(describing: fname))" + " " + mname + " " + "\(String(describing: lname))"

                        }
                        }
                    }
                    
//                    } else {
//                        self.careTakerTextField.text = (firstname) + ( lastname)
//                    }
//                    self.careTakerTextField.text  = item
                    Constant.selectedCareTaker = nil
                    self.selectedCaretaker = nil
                    return
                }
                self.careTakerTextField.text  = item
                Constant.selectedCareTaker = self.allCaretakersList[index-1]
                self.selectedCaretaker = self.allCaretakersList[index-1]
                dropDown.hide()
            }
            dropDown.show()
        }
    }
}

extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if Constant.loggedInProfile == .consumer {
            return selectedWidgetItems.count
        }
        else if Constant.loggedInProfile == .provider {
            return self.selectedWidgetItems.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if Constant.loggedInProfile == .consumer {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
            
            switch selectedWidgetItems[indexPath.row] {
            case 1:
                cell.homeCollectionButton.setImage(UIImage(named: "home_profileDetails"), for: .normal)
                cell.consumerTitleLabel.text = "My Profile"
            case 2:
                cell.homeCollectionButton.setImage(UIImage(named: "home_providerList"), for: .normal)
                cell.consumerTitleLabel.text = "Providers"
            case 3:
                cell.homeCollectionButton.setImage(UIImage(named: "home_insuranceDocument"), for: .normal)
                cell.consumerTitleLabel.text = "Insurance"
            case 4:
                cell.homeCollectionButton.setImage(UIImage(named: "home_allergies"), for: .normal)
                cell.consumerTitleLabel.text = "Allergies"
            case 5:
                cell.homeCollectionButton.setImage(UIImage(named: "home_medication"), for: .normal)
                cell.consumerTitleLabel.text = "Medications"
            case 6:
                cell.homeCollectionButton.setImage(UIImage(named: "home_immunization"), for: .normal)
                cell.consumerTitleLabel.text = "Immunizaions/Vaccines"
            case 7:
                cell.homeCollectionButton.setImage(UIImage(named: "home_medicalDocument"), for: .normal)
                cell.consumerTitleLabel.text = "Medical Safe"
            case 8:
                cell.homeCollectionButton.setImage(UIImage(named: "home_lab"), for: .normal)
                cell.consumerTitleLabel.text = "Lab Reports"
            case 9:
                cell.homeCollectionButton.setImage(UIImage(named: "home_caretakerList"), for: .normal)
                cell.consumerTitleLabel.text = "Dependents/Caregivers"
//            case 10:
//                cell.homeCollectionButton.setImage(UIImage(named: "home_vitalStats"), for: .normal)
//                cell.consumerTitleLabel.text = "Vital Stats"
            /* case 10:
                cell.homeCollectionButton.setImage(UIImage(named: "homeDevice"), for: .normal)
                cell.consumerTitleLabel.text = "Device Management"
            case 996:
                cell.homeCollectionButton.setImage(UIImage(named: "home_medicalCondition"), for: .normal)
                cell.consumerTitleLabel.text = "Medical Conditions"*/
            default:
                cell.consumerTitleLabel.text = ""
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProviderDashboardCell", for: indexPath) as! ProviderDashboardCollectionViewCell
            
            switch selectedWidgetItems[indexPath.row] {
            case 1:
                cell.categoryIcon.image = UIImage(named: "home_patient")
                cell.categoryNameLabel.text = "Patient"
            case 2:
                cell.categoryIcon.image = UIImage(named: "home_report")
                cell.categoryNameLabel.text = "Report Requests"
            default:
                cell.categoryNameLabel.text = ""
            }
            cell.countLabel.text = "45"
            return cell
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if Constant.loggedInProfile == .consumer
//        {
//       return CGSize(width: collectionView.frame.size.width / 3, height: 160)
////            let noOfCellsInRow = 3  //number of column you want
////                   let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
////                   let totalSpace = flowLayout.sectionInset.left
////                       + flowLayout.sectionInset.right
////                       + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
////                   let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
////                   return CGSize(width: size, height: 160)
//
//        } else
//        {
//            return CGSize(width: collectionView.frame.size.width, height: 160)
//        }
//    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if Device.device() == .iphone6 || Device.device() == .iphoneX   {
            return UIEdgeInsets (top: 0, left: 10, bottom: 0, right: 10)
        }else{
            return UIEdgeInsets (top: 0, left: 5, bottom: 0, right: 15)
        }
    }
   
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 10
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if Constant.loggedInProfile == .consumer {
            
            switch selectedWidgetItems[indexPath.row] {
            case 1:
                moveToProfileScreen()
            case 2:
                moveToProviderListListScreen()
            case 3:
                moveToDocumentListScreen(fileType: "1", textTitle: "Insurance", documentType: "3")
            case 4:
                moveToDocumentListScreen(fileType: "1", textTitle: "Allergies", documentType: "4")
            case 5:
                moveToMedicationListScreen()
            case 6:
                moveToDocumentListScreen(fileType: "1", textTitle: "Immunization Report", documentType: "6")
            case 7:
                moveToDocumentListScreen(fileType: "2", textTitle: "Medical Safe", documentType: "7")
            case 8:
                moveToDocumentListScreen(fileType: "1", textTitle: "Lab Reports", documentType: "8")
            case 9:
                moveToCareTakerListScreen()
            case 10:
                moveToVitalStatsScreen()
            default:
                print("Default Case")
            }
            
            
        } else {
            switch selectedWidgetItems[indexPath.row] {
            case 1:
                print("Patients")
            case 2:
                print("Report Requests")
            default:
                print("Default Case")
            }
        }
    }
    
//    func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
//        guard let collectionView = mainCollectionView else { return false }
//        return !newBounds.size.equalTo(collectionView.bounds.size)
//    }
    
    func moveToDocumentListScreen(fileType: String, textTitle: String, documentType: String) {
        guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: DocumentListingViewController.className) as? DocumentListingViewController else {return}
        
        vc.documentType = documentType
        vc.fileType = fileType
        vc.titleText = textTitle
        
        if selectedCaretaker != nil {
            vc.selectedCareTaker = self.selectedCaretaker
        }
        tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(vc, animated: true)
        return
    }
    
    func moveToMedicationListScreen() {
        tabBarController?.tabBar.isHidden = true
        guard let vc = UIStoryboard.init(name: StoryboardNames.Medication, bundle: Bundle.main).instantiateViewController(withIdentifier: MedicationListViewController.className) as? MedicationListViewController else {return}
        if selectedCaretaker != nil {
            vc.selectedCareTaker = self.selectedCaretaker
        }
        navigationController?.pushViewController(vc, animated: true)
        return
    }
    
    func moveToVitalStatsScreen() {
        tabBarController?.tabBar.isHidden = true
//        let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: VitalStatsDetailViewController.className) as? VitalStatsDetailViewController
//        self.navigationController?.pushViewController(vc!, animated: true)
        
        let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: VitalStatsViewController.className) as? VitalStatsViewController
      // vc?.userVitalStats = selectedVitalStats
       // vc?.selectedCareTaker = self.selectedCareTaker
        navigationController?.pushViewController(vc!, animated: true)
        
        
        return
    }
    
    func moveToCareTakerListScreen() {
        if selectedCaretaker != nil {
            tabBarController?.view.makeToast("You are not authorized to view this list")
            return
        }
        tabBarController?.tabBar.isHidden = true
        let vc = UIStoryboard.init(name: "CareGiver", bundle: Bundle.main).instantiateViewController(withIdentifier: CareTakerListViewController.className) as? CareTakerListViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        return
    }
    
    func moveToProfileScreen() {
        tabBarController?.tabBar.isHidden = true
        let myProfileVC = UIStoryboard.init(name: StoryboardNames.Profile, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardIDs.MyProfileViewController) as? MyProfileViewController
        
        myProfileVC?.isCaretakerProfile = false
        removeProfileControllerFromStack()
        if selectedCaretaker != nil {
            myProfileVC?.isCaretakerProfile = true
            myProfileVC?.caretakerDetails = selectedCaretaker
            myProfileVC?.caretakerEmail = selectedCaretaker?.email_address ?? ""
            myProfileVC?.caretakerSub = selectedCaretaker?.caretakerSub ?? ""
            myProfileVC?.caretakerRealtion = selectedCaretaker?.relationship_name ?? ""
        }
        
        self.navigationController?.pushViewController(myProfileVC!, animated: true)
        return
    }
    
    func moveToMedicalConditionListScreen() {
        tabBarController?.tabBar.isHidden = true
        guard let vc = UIStoryboard.init(name: StoryboardNames.medicalCondition, bundle: Bundle.main).instantiateViewController(withIdentifier: MedicalConditionDetailListViewController.className) as? MedicalConditionDetailListViewController else {return}
        if selectedCaretaker != nil {
            vc.selectedCareTaker = self.selectedCaretaker
        }
        navigationController?.pushViewController(vc, animated: true)
        return
    }
    
    func moveToProviderListListScreen() {
        tabBarController?.tabBar.isHidden = true
        guard let vc = UIStoryboard.init(name: StoryboardNames.ProviderStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: ProviderListingViewController.className) as? ProviderListingViewController else {return}
        if selectedCaretaker != nil {
            vc.selectedCareTaker = self.selectedCaretaker
        }
        navigationController?.pushViewController(vc, animated: true)
        return
    }
    
    func removeProfileControllerFromStack() {
        if var viewControllers = self.navigationController?.viewControllers
        {
            for controller in viewControllers
            {
                if controller is MyProfileViewController
                {
                    if let index = viewControllers.firstIndex(of: controller) {
                        viewControllers.remove(at: index)
                        self.navigationController?.viewControllers = viewControllers
                    }
                }
            }
        }
    }
}

extension HomeViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        /*if allCaretakersList.count != 0 {
            var dataSource = self.allCaretakersList.map({$0.name})
            dataSource.insert("My Dashboard", at: 0)
            self.initdropDown(textField: textField, dataSource: dataSource as! [String])
            return false
        } else {
            tabBarController?.view.makeToast("No Caretaker's available!")
        }*/
        return false
    }
}
