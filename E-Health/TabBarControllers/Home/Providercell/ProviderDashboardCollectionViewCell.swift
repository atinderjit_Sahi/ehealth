//
//  ProviderDashboardCollectionViewCell.swift
//  E-Health
//
//  Created by Prabjot Kaur on 6/15/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class ProviderDashboardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var categoryIcon: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
}
