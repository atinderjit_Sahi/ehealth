//
//  TabBarViewController.swift
//  E-Health
//
//  Created by Akash Dhiman on 20/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    let kBarHeight : CGFloat = 62
    let KColor : UIColor = UIColor(red: 0.04, green: 0.566, blue: 0.796, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        selectedIndex = 2
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        tabBar.backgroundImage = UIImage(color: UIColor.white)
        if UIDevice.current.hasNotch {
            print("Notch")
        } else {
            print("No Notch")
        }
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        if Device.device() == .iphone6 || Device.device() == .iphone6Plus{
            tabBar.frame.size.height = kBarHeight
            tabBar.frame.origin.y = view.frame.height - kBarHeight
            tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: KColor, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height: tabBar.frame.height), lineWidth: 2.0)
        } else if Device.device() == .unknown {
            tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: KColor, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height: tabBar.frame.height - 30), lineWidth: 2.0)
        } else if Device.device() == .iPadPro {
            tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: KColor, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height: tabBar.frame.height - 15), lineWidth: 2.0)
        } else {
            tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: KColor, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height: tabBar.frame.height - 35), lineWidth: 2.0)
        }
        tabBar.selectedImageTintColor = KColor
    }
}

extension TabBarViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool{
        return true
    }

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
        
    }
}
