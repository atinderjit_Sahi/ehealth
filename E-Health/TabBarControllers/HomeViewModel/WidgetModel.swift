//
//  WidgetModel.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 7/16/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class WidgetModel {

    var id : Int?
    var name : String?
    var status : Int?
    var typeId : Int?
    var createdAt : String?
    var updatedAt : String?
    
    init() {
    }
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.name = value["name"] as? String
        self.status = value["status"] as? Int
        self.typeId = value["type_id"] as? Int
        self.createdAt = value["created_at"] as? String
        self.updatedAt = value["updated_at"] as? String
    }
}
