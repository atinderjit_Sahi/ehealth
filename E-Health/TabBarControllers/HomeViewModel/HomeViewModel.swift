//
//  HomeViewModel.swift
//  E-Health
//
//  Created by Prabjot Kaur on 6/2/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

class HomeViewModel: NSObject {
    static func saveProvideProfileToDB(params: [String: AnyObject], completion: @escaping (_ response: Bool, _ error: Error?) -> Void) {
        let url = Constant.URL.saveProviderProfile
        
        let headers = ["APP_KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params, method: .post) { (result, error) in
            if error != nil {
                print(error ?? "")
                completion(false, error)
            } else {
                print(result as Any)
                completion(true, nil)
            }
        }
    }
    
}
