//
//  MenuViewController.swift
//  E-Health
//
//  Created by Akash Dhiman on 28/05/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import MessageUI

class MenuViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var menuCollectionView: UICollectionView!
    
    //MARK: - Properties
    var imageArray = [UIImage(named: "menu_termsPolicy_new"),UIImage(named: "menu_aboutUs_new"), UIImage(named: "menu_help"), UIImage(named: "menu_feedback")]
    var titleArray = ["Terms and Policies","About Us","Help", "Feedback"]
    var menuUrlStringConsumerArray = ["http://3.236.116.101/consumer/terms-conditions", "http://3.236.116.101/consumer/about", "http://3.236.116.101/consumer/help", ""]
    
    var menuUrlStringProviderArray = ["", "http://3.236.116.101/provider/terms-conditions", "http://3.236.116.101/provider/about", "http://3.236.116.101/provider/help"]
    
    var providerImageArray = [UIImage(named: "provider_profile_details"),UIImage(named: "menu_termsPolicy_new"),UIImage(named: "menu_aboutUs_new"),UIImage(named: "menu_help")]
    var providerTitleArray = ["Profile Details","Terms and Policies","About Us", "Help"]
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        tabBarController?.tabBar.isHidden = false
    }
}

extension MenuViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if Constant.loggedInProfile == .consumer {
            return imageArray.count
        }
        else if Constant.loggedInProfile == .provider {
            return providerImageArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCollectionViewCell", for: indexPath) as! MenuCollectionViewCell
        if Constant.loggedInProfile == .consumer {
            cell.menuImageView.image = imageArray[indexPath.item]
            cell.menuTitleLabel.text = titleArray[indexPath.item]
        }
        else if Constant.loggedInProfile == .provider {
            cell.menuImageView.image = self.providerImageArray[indexPath.item]
            cell.menuTitleLabel.text = self.providerTitleArray[indexPath.item]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: (collectionView.frame.size.width / 2) - 1, height: (collectionView.frame.size.height / 4) - 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets (top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //For consumer menu selection
        if Constant.loggedInProfile == .consumer {
            let urlString = menuUrlStringConsumerArray[indexPath.row]
            if indexPath.row == 3 {
                openMailComposer()
            } else {
                guard let webVC = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: WebViewController.className) as? WebViewController else {return}
                
                webVC.webUrlString = urlString
                self.navigationController?.pushViewController(webVC, animated: true)
            }
        }
        //For Provider Menu selection
        else if Constant.loggedInProfile == .provider {
            let urlString = menuUrlStringProviderArray[indexPath.row]
            //Navigate to Profile Screen
            if indexPath.row == 0 {
                tabBarController?.tabBar.isHidden = true
                let vc = UIStoryboard.init(name: StoryboardNames.ProviderStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardIDs.ProviderProfileViewController) as? ProviderProfileViewController
                self.navigationController?.pushViewController(vc!, animated: true)
                return
            }
            else {
                guard let webVC = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: WebViewController.className) as? WebViewController else {return}
                
                webVC.webUrlString = urlString
                self.navigationController?.pushViewController(webVC, animated: true)
//               tabBarController?.view.makeToast("Coming soon")
            }
        }
        else{
            tabBarController?.view.makeToast("Coming soon")
        }
    }
    
    func removeProfileControllerFromStack() {
        if var viewControllers = self.navigationController?.viewControllers
        {
            for controller in viewControllers
            {
                if controller is MyProfileViewController
                {
                    if let index = viewControllers.index(of: controller) {
                        viewControllers.remove(at: index)
                        self.navigationController?.viewControllers = viewControllers
                    }
                }
            }
        }
    }
    
    func openMailComposer() {
        //info@eheathbriefcase.com
        let recipientEmail = "info@ehealthbriefcase.com"//"info@eheathbriefcase.com"
        let subject = "E-Health Feedback"

        // Show default mail composer
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([recipientEmail])
            mail.setSubject(subject)
            mail.setMessageBody("", isHTML: false)

            present(mail, animated: true)
        }
    }
}
extension MenuViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
