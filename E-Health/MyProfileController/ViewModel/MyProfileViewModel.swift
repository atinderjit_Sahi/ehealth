//
//  MyProfileViewModel.swift
//  E-Health
//
//  Created by Abhishek Chugh on 5/25/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class MyProfileViewModel: NSObject {

    static func getDevicesList(completion: @escaping (_ response: Devices, _ error: String?) -> Void) {
        
        let url = Constant.URL.getDevicesList
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                completion(Devices(value: result), nil)
            } else {
                completion(Devices(value: [:]) , error?.localizedDescription)
            }
        }
    }
    
    static func saveVitals(params: [String: AnyObject], url: String, completion: @escaping (_ response: VitalsModel?,_ error: String?) -> Void) {
        
        let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params, method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                if let data = result["data"] as? [String: AnyObject] {
                    completion(VitalsModel(value: data), nil)
                } else {
                    completion(nil, result["message"] as? String)
                }
            } else {
                completion(nil, error?.localizedDescription)
            }
        }
    }
    
    static func getVitalDetails(cognitoID: String, completion: @escaping (_ response: VitalsModel?, _ error: String?) -> Void) {
        let url = Constant.URL.getVitalsByCognitoID + cognitoID
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                if let data = result["data"] as? [String: AnyObject] {
//                    if let vitals = data["vital_detail"] as?  [String: AnyObject] {
                         print(result)
                        completion(VitalsModel(value: data), nil)
//                    } else {
//                        completion(nil, result["message"] as? String)
//                    }
                } else {
                    completion(nil, result["message"] as? String)
                }
            } else {
                completion(nil, error?.localizedDescription)
            }
        }
    }
    
    static func getProfileDetails(email: String, completion: @escaping (_ response: ProfileDetail?, _ error: String?) -> Void) {
        let url = Constant.URL.getProfileDetails + email
        let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
                        if let data = result["data"] as? [AnyObject] {
                            if data.count != 0 {
                                if let profile = data[0] as?  [String: AnyObject] {
                                    completion(ProfileDetail(value: profile), nil)
                                } else {
                                    completion(nil, result["message"] as? String)
                                }
                            } else {
                                completion(nil, result["message"] as? String)
                            }
                        } else {
                            completion(nil, result["message"] as? String)
                        }
                    } else {
                        completion(nil, result["message"] as? String)
                    }
                }
            } else {
                completion(nil, error?.localizedDescription)
            }
        }
    }
    
    //UPDATE CARETAKER PROFILE
    static func updateCaretakerProfile(params: [String: AnyObject], completion: @escaping (_ response: Bool,_ error: String?) -> Void) {
        let url = Constant.URL.updateCaretakerProfile
        let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params, method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                if let data = result["data"] as? [String: AnyObject] {
                    completion(true, nil)
                } else {
                    completion(false, result["message"] as? String)
                }
            } else {
                completion(false, error?.localizedDescription)
            }
        }
    }
    
    //GET VITALS FOR MINOR CARETAKER
    static func getVitalDetailsForMinor(minorID: String, completion: @escaping (_ response: VitalsModel?, _ error: String?) -> Void) {
        let url = Constant.URL.getVitalsForMinor + minorID
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: nil, method: .get) { (result, error) in
            if let result = result as? [String: AnyObject] {
                if let data = result["data"] as? [String: AnyObject] {
                    completion(VitalsModel(value: data), nil)
                } else {
                    completion(nil, result["message"] as? String)
                }
            } else {
                completion(nil, error?.localizedDescription)
            }
        }
    }
    
    static func uploadProfileImage(params: [String: AnyObject], profileImage: UIImage, completion: @escaping (_ response: String?, _ error: String?) -> Void) {
        let url = Constant.URL.updateProfilePhoto
        let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.uploadProfileImage(url, parameters: params, profileImage: profileImage, pdfUrlArray: nil, compBlock: { (response, success) in
            guard let responseDict        =   response as? Dictionary<String, Any>  else {
                completion(nil, "Something went wrong!")
                return
            }
            if let code = responseDict["code"] as? NSNumber {
                print(responseDict)
                if code.stringValue == "200"{
                }
                else  if code.stringValue == "301"{
                }
                else  if code.stringValue == "501"{
                }
                completion(responseDict["message"] as? String, nil)
            }
            else {
                completion(responseDict["message"] as? String, nil)
            }
        }) { (error, failure) in
            print(error)
            completion(nil, error as String)
        }
    }
    
    static func enableMFA(isEnabled: Bool, completion: @escaping (_ response: Bool?, _ error: String?) -> Void) {
        let params = ["access_token": Constant.HeaderConstant.accessToken, "mfa_option": isEnabled] as [String : Any]

        let url = Constant.URL.enableMFA
        let headers = ["APP-KEY": Constant.HeaderConstant.appKey, "Authorization": Constant.HeaderConstant.authorization]
        
        ApiService.shared.triggerRequest(controller: nil, url: url, parameters: params as [String : AnyObject], method: .post) { (result, error) in
            if let result = result as? [String: AnyObject] {
                if let statusCode = result["code"] as? Int {
                    if statusCode == 200 {
//                        if (result["data"] as? [String: AnyObject]) != nil {
//                            completion(true, nil)
//                        } else {
                            completion(true, result["message"] as? String)
//                        }
                    } else {
                        completion(false, result["message"] as? String)
                    }
                }
            } else {
                completion(false, error?.localizedDescription)
            }
        }
    }
}
