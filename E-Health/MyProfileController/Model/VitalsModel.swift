//
//  VitalsModel.swift
//  E-Health
//
//  Created by Prabjot Kaur on 6/2/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit

class VitalsModel {
    var weight : CGFloat?
    var height : CGFloat?
    var chest : CGFloat?
    var waist : CGFloat?
    var hips : CGFloat?
    var created_by : String?
    var user_id : String?
    var bmi : CGFloat?
    var id : Int?
    var height_unit: String?
    var weight_unit: String?
    var chest_unit: String?
    var hips_unit: String?
    var waist_unit: String?
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.weight = value["weight"] as? CGFloat
        self.height = value["height"] as? CGFloat
        self.chest = value["chest"] as? CGFloat
        self.waist = value["waist"] as? CGFloat
        self.hips = value["hips"] as? CGFloat
        self.created_by = value["created_by"] as? String
        self.user_id = value["user_id"] as? String
        self.bmi = value["bmi"] as? CGFloat
        self.id = value["id"] as? Int
        
        self.height_unit = value["height_unit"] as? String
        self.weight_unit = value["weight_unit"] as? String
        self.chest_unit = value["chest_unit"] as? String
        self.hips_unit = value["hips_unit"] as? String
        self.waist_unit = value["waist_unit"] as? String

    }
}
