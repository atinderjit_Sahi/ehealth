//
//  MyProfileModel.swift
//  E-Health
//
//  Created by Abhishek Chugh on 5/25/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import LoginWithAmazon
import GoogleSignIn

class Devices {
    var fitness: [Fitness] = []
    var medical: [Medical] = []
//    var ethnicity: [Ethnicity] = []
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        if let regions = value["fitness"] as? [[String : Any]] {
            for data in regions{
                let dataToAppend = Fitness(value: data)
                fitness.append(dataToAppend)
            }
        }
        
        if let races = value["medical"] as? [[String : Any]] {
            for data in races{
                let dataToAppend = Medical(value: data)
                medical.append(dataToAppend)
            }
        }        
    }
}

class Fitness {
    var id: Int?
    var name: String?
    var model: String?
    var category: String?
    var status: Int?
    var updated_at: String?
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.name = value["name"] as? String
        self.model = value["model"] as? String
        self.category = value["category"] as? String
        self.status = value["status"] as? Int
        self.updated_at = value["updated_at"] as? String
    }
}

class Medical {
    var id: Int?
    var name: String?
    var model: String?
    var category: String?
    var status: Int?
    var updated_at: String?
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.id = value["id"] as? Int
        self.name = value["name"] as? String
        self.model = value["model"] as? String
        self.category = value["category"] as? String
        self.status = value["status"] as? Int
        self.updated_at = value["updated_at"] as? String
    }
}

class ProfileDetail {
    var firstName: String?
    var lastName: String?
    var sub: String?
    var name: String?
    var middleName: String?
    var dob: String?
    var country: String?
    var primaryPhone: String?
    var family_name: String?
    var email: String?
    var region: String?
    var state: String?
    var city: String?
    var race: String?
    var ethnicity: String?
    var healthDevice: String?
    var fitnessDevice: String?
    var address1: String?
    var address2: String?
    var zip: String?
    var altPhone: String?
    var smsRecieve: String?
    var groupStudies: String?
    var mfaEnabled: String?
    var gender: String?
    var providerType: String?
    var doctorType: String?
    var praticeName: String?
    var specialistIn: String?
    var socialID: String?
    var userImageUrl: String?
    var widgets: String?
    var isNewUser: String?
    var socialCognitoId: String?
    var isEmailVerified: String?
    
    var emerName: String?
    var emernumber: String?
    var medicalId: String?
    
    init() {
        
    }
    
    init(value: [String: Any]) {
        self.name = value["name"] as? String
        self.sub = value["sub"] as? String
        self.dob = value["custom:dob"] as? String
        self.country = value["custom:country"] as? String
        self.primaryPhone = value["phone_number"] as? String
        self.family_name = value["family_name"] as? String
        self.email = value["email"] as? String
        self.region = value["custom:region"] as? String
        self.race = value["custom:race"] as? String
        self.ethnicity = value["custom:ethnicity"] as? String
        self.healthDevice = value["custom:health_dev"] as? String
        self.fitnessDevice = value["custom:fitness_dev"] as? String
        self.middleName = value["custom:mid_name"] as? String
        self.address1 = value["custom:address1"] as? String
        self.address2 = value["custom:address2"] as? String
        self.zip = value["custom:zip"] as? String
        self.altPhone = value["custom:alt_pn"] as? String
        self.smsRecieve = value["custom:sms_r"] as? String
        self.groupStudies = value["custom:grp_stdy"] as? String
        self.mfaEnabled = value["custom:mfa_enabled"] as? String
        self.state = value["custom:state"] as? String
        self.city = value["custom:city"] as? String
        self.gender = value["custom:gender"] as? String
        self.providerType = value["custom:provider_type"] as? String
        self.doctorType = value["custom:doctor_type"] as? String
        self.praticeName = value["custom:practice_name"] as? String
        self.specialistIn = value["custom:specialist_in"] as? String
        self.userImageUrl = value["custom:profile_pic"] as? String
        isEmailVerified = value["email_verified"] as? String
        widgets = value["custom:widget_items"] as? String
        isNewUser = value["custom:new_user"] as? String
        
        self.emerName = value["custom:emergency_name"] as? String
        self.emernumber = value["custom:emergency_no"] as? String
        self.medicalId = value["custom:medical_alert_id"] as? String
    }
    
    init(facebookLogin : [String: Any], cognitoId: String?) {
        self.family_name = "consumer"
        self.email = facebookLogin["email"] as? String
        self.firstName = facebookLogin["first_name"] as? String
        self.lastName = facebookLogin["last_name"] as? String
        self.name = "\(firstName ?? "") \(lastName ?? "")"
        socialID = facebookLogin["id"] as? String
        socialCognitoId = cognitoId
        
        if let imageUrl = facebookLogin["picture"] as? [String: Any],
            let data = imageUrl["data"] as? [String: Any],
            let url = data["url"] as? String{
            userImageUrl = url
        }
    }
    
    init(googleLogin: GIDGoogleUser, cognitoId: String?) {
        self.family_name = "consumer"
        self.email = googleLogin.profile.email
        let name = googleLogin.profile.name
        self.name = name
        let names = name?.components(separatedBy: " ")
        if names != nil {
            self.firstName = names?[0]
            if names!.count > 1 {
                self.lastName = names?[1]
            }
        }
        socialID = googleLogin.userID
        socialCognitoId = cognitoId
        
        if googleLogin.profile.hasImage {
            let imageUrl = googleLogin.profile.imageURL(withDimension: 120)
            userImageUrl = imageUrl?.absoluteString
        }
    }
    
    init(amazonLogin : AMZNUser?, cognitoId: String?) {
        self.family_name = "consumer"
        self.email = amazonLogin?.email
        let name = amazonLogin?.name
        self.name = name
        let names = name?.components(separatedBy: " ")
        if names != nil {
            self.firstName = names?[0]
            if names!.count > 1 {
                self.lastName = names?[1]
            }
        }
        self.name = name
        socialID = amazonLogin?.userID
        socialCognitoId = cognitoId

//        if let imageUrl = amazonLogin["picture"] as? [String: Any],
//            let data = imageUrl["data"] as? [String: Any],
//            let url = data["url"] as? String{
//            userImageUrl = url
//        }
    }
}
