//
//  MyProfileViewController.swift
//  E-Health
//
//  Created by Abhishek Chugh on 5/21/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import SwiftMultiSelect
import AWSMobileClient

protocol MyProfileVCDelegates {
    func selectedFitnessDevices(devices: [Fitness]?)
    func selectedMedicalDevices(devices: [Medical]?)
}

class MyProfileViewController: UIViewController {

    @IBOutlet weak var btnback: UIButton!
    @IBOutlet var tblView: UITableView!
    @IBOutlet weak var profileTabbar: ScrollPager!
    @IBOutlet var profileDetailScrollView: UIScrollView!
    @IBOutlet var devicesListView: UIView!
    @IBOutlet weak var deviceListHeaderView: UIView!
    @IBOutlet weak var devicesListTblView: UITableView!
    @IBOutlet var vitalScrollView: UIScrollView!
    @IBOutlet var medicalReportScrollView: UIScrollView!
    @IBOutlet weak var addButton: UIButton!
    
    var countriesListArray = [CountryList]()
    var selectedCountry = CountryList()
    var devicesList = Devices()
    var selectedDeviceType = String()
    var selectedFitnessDevices = [Fitness]()
    var selectedMedicalDevices = [Medical]()
    
    var isCaretakerProfile = false
    var caretakerDetails : CaretakerModel?
    var caretakerEmail = String()
    var caretakerSub = String()
    var caretakerRealtion = String()
    
    var delegate: MyProfileVCDelegates?
    var imagePicker: ImagePicker!
    lazy var profileDetailsView : ProfileDetailsView = {
        let profileView = ProfileDetailsView().loadNib() as! ProfileDetailsView
        return profileView
    }()
    
    lazy var medicalReportsView : MedicalReport = {
        let medicalView = MedicalReport().loadNib() as! MedicalReport
        return medicalView
    }()
    
    //MARK:- View Hierarchy
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        devicesListView.frame = self.view.frame
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        setupPaginationView()
        
        VerificationViewModel.getAuthToken() { (result) in
            let userId = AWSMobileClient.default().username ?? ""
            RegisterViewModel.postCognitoUserID(userID: userId) { result in
                if result {
                   
                }
            }
             }
        
        
        if Constant.userProfile?.isNewUser == "1" {
            btnback.isHidden = true
        }
        else{
            btnback.isHidden = false
        }
        
        
    }

    func setupPaginationView() {
        //First Tab - Profile Details
        //profileDetailsView?.loadNib() as! ProfileDetailsView
        profileDetailsView.isCaretakerProfile = self.isCaretakerProfile
        profileDetailsView.caretakerDetails = self.caretakerDetails
                
        let viewFrame = self.view.frame
        if isCaretakerProfile {
            
            if self.caretakerDetails?.care_taker_type_id == 1 {
                profileDetailsView.frame = CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: viewFrame.width, height: 1250)
            } else {
                profileDetailsView.frame = CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: viewFrame.width, height: profileDetailsView.frame.height - 300)
            }
            
        } else {
            profileDetailsView.frame = CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: viewFrame.width, height: profileDetailsView.frame.height - 300)
        }
        profileDetailsView.delegate = self
        
        profileDetailScrollView.addSubview(profileDetailsView)
        if self.isCaretakerProfile {
            if self.caretakerDetails?.care_taker_type_id == 1 { //self.caretakerDetails?.minor_id != nil {
                profileDetailScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1350)//1855)
            } else {
                profileDetailScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: profileDetailsView.frame.height)//1855)
            }
        } else {
            profileDetailScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: profileDetailsView.frame.height + 50)//1855)
        }
        
        //Second tab - Vital Stats
//        var padding: CGFloat = 0
//        if Device.device() == .iphone6{
//            padding = 115
//        }
        
        let vitalDetailsView = VitalStatusView().loadNib() as! VitalStatusView
        if self.isCaretakerProfile{
            vitalDetailsView.isCaretaker = self.isCaretakerProfile
            vitalDetailsView.userId = self.caretakerDetails?.caretakerSub ?? "" //self.caretakerSub
            vitalDetailsView.caretakerDetails = self.caretakerDetails
        }
        
        vitalDetailsView.frame = CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: viewFrame.width, height: 700)

        vitalScrollView.addSubview(vitalDetailsView)
        vitalScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 700)
        
        //Third Tab
        if self.isCaretakerProfile{
            medicalReportsView.isCaretakerProfile = self.isCaretakerProfile
            medicalReportsView.caretakerDetails = self.caretakerDetails
//            medicalReportsView.caretakerDetails = self.caretakerDetails
        }
        
        medicalReportsView.medicalDelegate = self
//        medicalReportsView.medicalReportTableView.delegate = self
//        medicalReportsView.medicalReportTableView.dataSource = self
        medicalReportsView.frame = CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: medicalReportScrollView.frame.size.width, height: medicalReportsView.frame.height)
        
        medicalReportScrollView.addSubview(medicalReportsView)
//        medicalReportScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 510)
        
        //Third Tab
        /*let thirdView = UILabel()
        thirdView.font = UIFont(name: "Montserrat-Medium", size: 16)
        if #available(iOS 13.0, *) {
            thirdView.textColor = UIColor.label
        } else {
            thirdView.textColor = UIColor.black
        }
        thirdView.backgroundColor = UIColor.white
        thirdView.text = "Coming soon"
        thirdView.textAlignment = .center*/
        
        
        profileTabbar.delegate = self
        profileTabbar.viewHeight = self.view.frame.height//profileDetailsView.frame.height
        profileTabbar.addSegmentsWithTitlesAndViews(segments: [
            ("Profile Details", profileDetailScrollView),
            ("Physical Stats", vitalScrollView),
            /*("Medical Reports", medicalReportScrollView),*/
            ])
       
        
    }
    
    @IBAction func hideDevicesList(_ sender: Any) {
        hideAlertWithBounceEffect(myView: devicesListView)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        if isCaretakerProfile {
            self.navigationController?.popViewController(animated: true)
            return
        }
        tabBarController?.tabBar.isHidden = false
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func buttonAddTapped(_ sender: Any) {
        guard let vc = UIStoryboard.init(name: StoryboardNames.Caregiver, bundle: Bundle.main).instantiateViewController(withIdentifier: AddDocumentViewController.className) as? AddDocumentViewController else {return}
        
        vc.fileType = "2"
        vc.documentType = "6"
        vc.delegate = self
        if caretakerDetails != nil {
            vc.selectedCareTaker = caretakerDetails!
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- Medical Report Added Delegate
extension MyProfileViewController: MedicalReportAdded {
    func reloadMedicalReportList() {
        self.medicalReportsView.callSearchApi()
    }    
}

//MARK:- Pop up view animation methods
extension MyProfileViewController {
    func showAlertWithBounceEffect(myView : UIView,onWindow:Bool) {
        let window = UIApplication.shared.keyWindow!
        myView.isHidden = false
        if onWindow == true {
            window.addSubview(myView)
        }  else {
            self.view.addSubview(myView)
            self.view.bringSubviewToFront(myView)
        }
        myView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            myView.transform = .identity
        }, completion: {(finished: Bool) -> Void in
        })
    }
    
    func hideAlertWithBounceEffect(myView : UIView) {
        myView.transform = .identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            myView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: {(finished: Bool) -> Void in
            myView.isHidden = true
        })
    }
}

//MARK:- Scroll Pager delegate
extension MyProfileViewController: ScrollPagerDelegate {
    func scrollPager(scrollPager: ScrollPager, changedIndex: Int) {
        print("scrollPager index changed: \(changedIndex)")
        if changedIndex == 2 {
            addButton.isHidden = false
        } else {
            addButton.isHidden = true
        }
    }
}

//MARK:- My Profile Tab Delegates
extension MyProfileViewController: ProfileDetailsTabDelegates {
    
    func setSelectedTab(index: Int) {
        self.profileTabbar.setSelectedIndex(index: 1, animated: true)
    }
    
    func changePassword() {
//        Utility.shared.showToast(message: "Coming Soon")
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: StoryBoardIDs.ResetPasswordViewController) as? ResetPasswordViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func setSelectedDevice(medical: [Medical]?, fitness: [Fitness]?) {
        self.selectedFitnessDevices = fitness ?? [Fitness]()
        self.selectedMedicalDevices = medical ?? [Medical]()
    }
    
    //MARK: Update profile methods
    func updateProfile(data: [String : String]) {
        if isCaretakerProfile {
            self.updateCaretakerProfile(data: data)
            return
        }
        var fitnessDevicesId = ""
        if selectedFitnessDevices.count != 0 {
            for device in selectedFitnessDevices {
                let deviceId = String(device.id ?? 0)
                fitnessDevicesId.append(deviceId + ",")
            }
            fitnessDevicesId.removeLast()
        }
        
        var medicalDeviceId = ""
        if selectedMedicalDevices.count != 0 {
            for device in selectedMedicalDevices {
                let deviceId = String(device.id ?? 0)
                medicalDeviceId.append(deviceId + ",")
            }
            medicalDeviceId.removeLast()
        }
                
        var profileData = data
        
        profileData["custom:fitness_dev"] = fitnessDevicesId
        profileData["custom:health_dev"] = medicalDeviceId
        print(profileData)
                
        AWSAuth.shared.updateUserAttributes(userData: profileData) { (result) in
            if result {
                Utility.shared.showToast(message: "Profile Updated successfully")
            }
        }
    }
    
    func enableMfa(isEnabled: Bool) {
        
    }
    
    func openDevicesList(listData: Devices, deviceType: String) {
        selectedDeviceType = deviceType
        devicesList = listData
        devicesListTblView.reloadData()
        showAlertWithBounceEffect(myView: devicesListView, onWindow: true)
    }
    
    func updateCaretakerProfile(data: [String : String]) {
        var fitnessDevicesId = ""
        if selectedFitnessDevices.count != 0 {
            for device in selectedFitnessDevices {
                let deviceId = String(device.id ?? 0)
                fitnessDevicesId.append(deviceId + ",")
            }
            fitnessDevicesId.removeLast()
        }
        
        var medicalDeviceId = ""
        if selectedMedicalDevices.count != 0 {
            for device in selectedMedicalDevices {
                let deviceId = String(device.id ?? 0)
                medicalDeviceId.append(deviceId + ",")
            }
            medicalDeviceId.removeLast()
        }
        
        print(medicalDeviceId)
        print(fitnessDevicesId)
        
        var profileData = data as [String: AnyObject]
        
        profileData["custom:fitness_dev"] = fitnessDevicesId as AnyObject
        profileData["custom:health_dev"] = medicalDeviceId as AnyObject
        profileData["caretakerId"] = caretakerSub as AnyObject
        profileData["userId"] = (AWSMobileClient.default().username ?? "") as AnyObject
        
        print(profileData)
        
        Utility.shared.startLoader()
        MyProfileViewModel.updateCaretakerProfile(params: profileData) { (result, error) in
            Utility.shared.stopLoader()
            if result {
                print(true)
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
    func updateMinorProfile(params: [String : AnyObject]) {
        var fitnessDevicesId = ""
        if selectedFitnessDevices.count != 0 {
            for device in selectedFitnessDevices {
                let deviceId = String(device.id ?? 0)
                fitnessDevicesId.append(deviceId + ",")
            }
            fitnessDevicesId.removeLast()
        }
        
        var medicalDeviceId = ""
        if selectedMedicalDevices.count != 0 {
            for device in selectedMedicalDevices {
                let deviceId = String(device.id ?? 0)
                medicalDeviceId.append(deviceId + ",")
            }
            medicalDeviceId.removeLast()
        }
        
        print(medicalDeviceId)
        print(fitnessDevicesId)
        
        var profileData = params as [String: AnyObject]
        
        profileData["fitness_device_ids"] = fitnessDevicesId as AnyObject
        profileData["health_device_ids"] = medicalDeviceId as AnyObject
        
        Utility.shared.startLoader()
        CaretakerViewModel.addCaretakerAPI(params: profileData) { (_ result, _ message, _ error)  in
            Utility.shared.stopLoader()
            if result {
                Utility.shared.showToast(message: message ?? "")
            }
            //Handling error
            else if error != nil {
                Utility.shared.showToast(message: error!)
            }
        }
    }
    
    func updateProfileImage(params: [String : AnyObject], image: UIImage) {
        //SAVE PROFILE IMAGE TO DOCUMENT DIRECTORY
        let noImageData = UIImage(named: "profile_no_image")?.pngData()
        let profileImage = image.pngData()
        
        if profileImage != noImageData {
            let imageParams = params
            Utility.shared.startLoader()
            MyProfileViewModel.uploadProfileImage(params: imageParams, profileImage: image) { (data, error) in
                Utility.shared.stopLoader()
                if error != nil {
                    Utility.shared.showToast(message: error!)
                } else {
                    Utility.shared.showToast(message: data!)
                }
            }
        }
    }
    
    func openImagePicker() {
        self.imagePicker.present(from: self.view)
    }
}

extension MyProfileViewController: MedicalReportTabDelegates {
    
    func addDocumentScreen(fileType: String, documentType: String) {
        guard let vc = UIStoryboard.init(name: StoryboardNames.Caregiver, bundle: Bundle.main).instantiateViewController(withIdentifier: AddDocumentViewController.className) as? AddDocumentViewController else {return}
        
        vc.fileType = fileType
        vc.documentType = documentType
        if caretakerDetails != nil {
            vc.selectedCareTaker = caretakerDetails!
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewMedicalReport(urlString: String) {
        guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: WebViewController.className) as? WebViewController else {return}
        vc.webUrlString = urlString
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func deleteMedicalReport(params: [String : Any]) {
        
    }
}

//MARK:- Table View Delegates
extension MyProfileViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedDeviceType == "medical" {
            return devicesList.medical.count
        }
        return devicesList.fitness.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCellTableViewCell") as? CountryCellTableViewCell else {
            return UITableViewCell()
        }
        
        if selectedDeviceType == "medical" {
            if selectedMedicalDevices.contains(where: { $0.id == devicesList.medical[indexPath.row].id }) {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
            cell.countryLabel.text = devicesList.medical[indexPath.row].name
        }
        
        else {
            if selectedFitnessDevices.contains(where: { $0.id == devicesList.fitness[indexPath.row].id }) {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
            cell.countryLabel.text = devicesList.fitness[indexPath.row].name
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var headerView = UIView()
        headerView = deviceListHeaderView
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var selectedCell: UITableViewCell?
        if let cell = tableView.cellForRow(at: indexPath) {
            selectedCell = cell
        }
        
        if selectedDeviceType == "medical" {
            let selectedDevice = devicesList.medical[indexPath.row]
            if selectedMedicalDevices.contains(where: { $0.id == selectedDevice.id }) {
                let index = selectedMedicalDevices.firstIndex(where: { $0.id == selectedDevice.id})
                if index != nil {
                    selectedMedicalDevices.remove(at: index!)
                    selectedCell?.accessoryType = .none
                }
            } else {
                selectedMedicalDevices.append(selectedDevice)
                selectedCell?.accessoryType = .checkmark
            }
            
            setSelectedMedicalDevices()
        } else {
            let selectedDevice = devicesList.fitness[indexPath.row]
            if selectedFitnessDevices.contains(where: { $0.id == selectedDevice.id }) {
                let index = selectedFitnessDevices.firstIndex(where: { $0.id == selectedDevice.id})
                if index != nil {
                    selectedFitnessDevices.remove(at: index!)
                    selectedCell?.accessoryType = .none
                }
            } else {
                selectedFitnessDevices.append(selectedDevice)
                selectedCell?.accessoryType = .checkmark
            }
            setSelectedFitnessDevices()
        }
    }
}

//MARK:- Image Picker Delegate
extension MyProfileViewController: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        if let selectedImage = image {
            self.setProfileImage(image: selectedImage)
        }
    }
    
    func setProfileImage(image: UIImage) {
        var params = [String: AnyObject]()
        if !isCaretakerProfile {
            params["email"] = Constant.userProfile?.email as AnyObject
            params["cognito_user_id"] = (AWSMobileClient.default().username ?? "") as AnyObject
            params["minor_id"] = "" as AnyObject
            params["user_type"] = "consumer" as AnyObject
        }
        else if isCaretakerProfile {
            //In case of Minor caretaker
            if self.caretakerDetails?.care_taker_type_id == 1 {
                params["email"] = "" as AnyObject //Constant.userProfile?.email as AnyObject
                params["cognito_user_id"] = "" as AnyObject //(AWSMobileClient.default().username ?? "") as AnyObject
                params["minor_id"] = (String(caretakerDetails?.minor_id ?? 0)) as AnyObject
                params["user_type"] = "" as AnyObject
            }
            //In case of Adult caretaker
            else {
                params["email"] = caretakerDetails?.email_address as AnyObject
                params["cognito_user_id"] = (String(caretakerDetails?.caretakerSub ?? "")) as AnyObject
                params["minor_id"] = "" as AnyObject
                params["user_type"] = "consumer" as AnyObject
            }
        }
        self.uploadProfileImage(params: params, image: image)
    }
    
    func uploadProfileImage(params: [String : AnyObject], image: UIImage) {
        let noImageData = UIImage(named: "profile_no_image")?.pngData()
        let profileImage = image.pngData()
        
        if profileImage != noImageData {
            let imageParams = params
            Utility.shared.startLoader()
            MyProfileViewModel.uploadProfileImage(params: imageParams, profileImage: image) { (data, error) in
                Utility.shared.stopLoader()
                if error != nil {
                    Utility.shared.showToast(message: error!)
                } else {
                    Utility.shared.showToast(message: data!)
                    self.profileDetailsView.profileImageView.image = image
                }
            }
        }
    }
}

//MARK:- Call Protocol methods to send selected devices to ProfileDetailsView
extension MyProfileViewController {
    func setSelectedFitnessDevices() {
//        var devicesID = [Int]()
//        for device in selectedFitnessDevices {
//            devicesID.append(device.id ?? 0)
//        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constant.NotificationObserverName.selectedFitnessDevices), object: selectedFitnessDevices, userInfo: nil)
        
//        delegate?.selectedFitnessDevices(devices: selectedFitnessDevices)
        
    }
    
    func setSelectedMedicalDevices() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constant.NotificationObserverName.selectedMedicalDevices), object: selectedMedicalDevices, userInfo: nil)
        
//        delegate?.selectedMedicalDevices(devices: selectedMedicalDevices)
    }
}

extension UIView {
    /** Loads instance from nib with the same name. */
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
}
