//
//  ProfileDetailsXib.swift
//  E-Health
//
//  Created by Abhishek Chugh on 5/21/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import DropDown
import Kingfisher
import AWSMobileClient
import IQKeyboardManagerSwift

protocol ProfileDetailsTabDelegates: class {
    func openDevicesList(listData: Devices, deviceType: String)
    func updateProfile(data: [String: String])
    func setSelectedDevice(medical: [Medical]?, fitness: [Fitness]?)
    func changePassword()
    func setSelectedTab(index: Int)
    func updateMinorProfile(params: [String: AnyObject])
    func openImagePicker()
    func updateProfileImage(params: [String: AnyObject], image: UIImage)
}

class ProfileDetailsView: UIView {
        
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var firstNameTextField: ExtendedTextField!
    @IBOutlet weak var middleNameTextField: ExtendedTextField!
    @IBOutlet weak var lastNameTextField: ExtendedTextField!
    @IBOutlet weak var emailTextField: ExtendedTextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var address1TextField: ExtendedTextField!
    @IBOutlet weak var address2TextFiedl: ExtendedTextField!
    @IBOutlet weak var regionTextField: ExtendedTextField!
    @IBOutlet weak var raceTextField: ExtendedTextField!
    @IBOutlet weak var ethnicityTextField: ExtendedTextField!
    @IBOutlet weak var zipTextField: ExtendedTextField!
    @IBOutlet weak var primaryContactContainerView: UIView!
    @IBOutlet weak var primaryContactTextField: ExtendedTextField!
    @IBOutlet weak var alternateContactContainerView: UIView!
    @IBOutlet weak var alternateNumberTextField: ExtendedTextField!
    @IBOutlet weak var alternateContactCode: UITextField!
    @IBOutlet weak var stateTextField: ExtendedTextField!
    @IBOutlet weak var cityTextField: ExtendedTextField!
    
    @IBOutlet weak var smsRecieveBtn: UIButton!
    @IBOutlet weak var groupStudiesBtn: UIButton!
    @IBOutlet weak var mfaEnabledBtn: UIButton!
    
    @IBOutlet weak var medicalDeviceButton: UIButton!
    @IBOutlet weak var fitnessDeviceButton: UIButton!
    @IBOutlet weak var selectDeviceView: UIView!
    
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var selectModelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var devicesCollectionView: UICollectionView!
    @IBOutlet weak var devicesCollectionViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var dobTextField: UITextField!
    @IBOutlet weak var changePasswordButton: UIButton!
    
    @IBOutlet weak var firstnameTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var relationshipFieldView: UIView!
    @IBOutlet weak var caretakerTypeFieldView: UIView!
    @IBOutlet weak var raceTypeFieldView: UIView!
    @IBOutlet weak var ethnicityTypeFieldView: UIView!
    
    @IBOutlet weak var relationshipTextField: ExtendedTextField!
    @IBOutlet weak var caretakerTypeTextfield: ExtendedTextField!
    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var femaleRadioButton: UIButton!
    @IBOutlet weak var otherRadioButton: UIButton!
    @IBOutlet weak var changePasswordButtonHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var selectModelButton: UIButton!
    @IBOutlet weak var collectionViewSeparatorLabel: UILabel!
    
    @IBOutlet weak var alternateContactCallButton: UIButton!
    @IBOutlet weak var primaryContactCallButton: UIButton!
    @IBOutlet weak var viewOtpAuthentication: UIView!
    @IBOutlet weak var genderStackView: UIStackView!
    
    @IBOutlet weak var txtfieldContactName: ExtendedTextField!
    @IBOutlet weak var txtfieldContactNumber: ExtendedTextField!
    
    @IBOutlet weak var txtfieldMedicalId: ExtendedTextField!
    
    var countriesArray = MasterData()
    var countriesListArray = [CountryList]()
    var countryCode : String = ""
    @IBOutlet weak var buttonCallPrimaryContact: UIButton!
    
    @IBOutlet weak var mainScrollView: UIStackView!
    
    
    var oldMiddleName = String()
    
    var selectedCountry = CountryList()
    var selectedRace = Race()
    var selectedEthnicity = Ethnicity()
    var selectedCaretakerType = CaretakerType()
    var selectedRelationship = Relation()
    var devicesArray = Devices()
    
    let dispatchGroup = DispatchGroup()
    
    var delegate: ProfileDetailsTabDelegates?
    var myProfileVC : MyProfileViewController?
    var selectedMedicalDevice = [Medical]()
    var selectedFitnessDevice = [Fitness]()
    
    var datePicker = UIDatePicker()
    var selectedDeviceNames = [String]()
    
    var isSmsRecieve = false
    var isGroupStudies = false
    var isMfaEnabled = false
    
    var isScreenLoaded = false
    
    var isCaretakerProfile = false
//    var caretakerEmail: String?
//    var caretakerRelation: String?
    var caretakerDetails: CaretakerModel?
    var imagePicker: ImagePicker!
    
    //MARK:- Class Methods
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        IQKeyboardManager.shared.enableAutoToolbar = true
        myProfileVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "MyProfileViewController") as? MyProfileViewController
        myProfileVC?.delegate = self
        
        if !isCaretakerProfile {
            if Constant.isSocialLogin {
                viewOtpAuthentication.isHidden = true
                changePasswordButton.isHidden = true
            } else {
                changePasswordButton.isHidden = false
                viewOtpAuthentication.isHidden = false
            }
            
            caretakerTypeFieldView.isHidden = true
            relationshipFieldView.isHidden = true
            changePasswordButtonHeightConstraint.constant = 30
        } else {
            changePasswordButton.isHidden = true
            caretakerTypeFieldView.isHidden = false
            relationshipFieldView.isHidden = false
            changePasswordButtonHeightConstraint.constant = 0
            viewOtpAuthentication.isHidden = true
        }
        
        setupUI()
        setupTextFieldsProperties()
        
        if !isScreenLoaded {
            isScreenLoaded = true
            addNotificationObservers()
            
            Utility.shared.startLoader()
            
            self.countriesArray = Constant.masterData ?? MasterData() //countries
            self.countriesListArray = self.countriesArray.countries
            getDevicesList()
            
            dispatchGroup.notify(queue: .main) { [unowned self] in
                if self.isCaretakerProfile {
                    if self.caretakerDetails?.care_taker_type_id == 1 { //self.caretakerDetails?.minor_id != nil {
                        Utility.shared.stopLoader()
                        self.setupFieldsForMinorCaretaker()
                        return
                    }
                    self.getCaretakerProfileAttributes()
                } else {
                    self.getUserAttributesFromAWS()
                }
            }
        }
        
        devicesCollectionView.register(UINib(nibName: "DevicesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DevicesCollectionViewCell")
        devicesCollectionView.delegate = self
        devicesCollectionView.dataSource = self
        getAccessTokens()
        
       
    }
    
    func getAccessTokens() {
        AWSAuth.shared.getAccessToken { (result, message) -> (Void) in
            print(result)
            Constant.HeaderConstant.accessToken = result.accessToken?.tokenString ?? ""
        }
    }
    
    @IBAction func actionEmergencyCall(_ sender: UIButton) {
        
        var phoneNumber = String()
        if txtfieldContactNumber.text == "" {
            self.makeToast("Contact number format is not correct!")
            return
        } else {
            phoneNumber = txtfieldContactNumber.text ?? ""
        }
        
        if let url = URL(string: "tel://\(String(describing: phoneNumber))"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
        else {
            self.makeToast("Something went wrong!")
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func buttonCallPrimaryContactTapped(_ sender: Any) {
//        var phoneNumber = String()
//        if buttonCallPrimaryContact.titleLabel?.text == "" {
//            self.makeToast("Contact number format is not correct!")
//            return
//        } else {
//            phoneNumber = buttonCallPrimaryContact.titleLabel?.text ?? ""
//        }
//
//        if let url = URL(string: "tel://\(String(describing: phoneNumber))"), UIApplication.shared.canOpenURL(url) {
//            UIApplication.shared.open(url)
//        }
//        else {
//            self.makeToast("Something went wrong!")
//        }
    }
    
    @IBAction func primaryContactCallButtonAction(_ sender: Any) {
        var phoneNumber = String()
        if primaryContactTextField.text == "" {
            self.makeToast("Contact number format is not correct!")
            return
        } else {
            phoneNumber = primaryContactTextField.text ?? ""
        }

        if let url = URL(string: "tel://\(String(describing: phoneNumber))"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
        else {
            self.makeToast("Something went wrong!")
        }
    }
    @IBAction func alternateContactCallButtonAction(_ sender: Any) {
        var phoneNumber = String()
        if alternateNumberTextField.text != "" && alternateNumberTextField.text != nil {
            phoneNumber = alternateContactCode.text! + alternateNumberTextField.text!
        } else {
            return
        }
        if let url = URL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
        else {
//            self.makeToast("Contact number is format is not correct!")
        }
    }
    
    @IBAction func maleRadioButtonAction(_ sender: Any) {
        self.maleRadioButton.isSelected = true
        self.femaleRadioButton.isSelected = false
        self.otherRadioButton.isSelected = false
    }
    @IBAction func femaleRadioButtonAction(_ sender: Any) {
        self.maleRadioButton.isSelected = false
        self.femaleRadioButton.isSelected = true
        self.otherRadioButton.isSelected = false
    }
    
    @IBAction func otherRadioButtonAction(_ sender: Any) {
        self.maleRadioButton.isSelected = false
        self.femaleRadioButton.isSelected = false
        self.otherRadioButton.isSelected = true
    }
    
    func addNotificationObservers() {
        //Notification Observer for selected fitness devices
        NotificationCenter.default.addObserver(self, selector: #selector(self.getSelectedFitnessDevices), name: NSNotification.Name(rawValue: Constant.NotificationObserverName.selectedFitnessDevices), object: nil)
        
        //For selected Medical devices
        NotificationCenter.default.addObserver(self, selector: #selector(self.getSelectedMedicalDevices), name: NSNotification.Name(rawValue: Constant.NotificationObserverName.selectedMedicalDevices), object: nil)
        
        //For selected Profile Image
        NotificationCenter.default.addObserver(self, selector: #selector(self.setProfileImage), name: NSNotification.Name(rawValue: Constant.NotificationObserverName.selectedConsumerProfileImage), object: nil)
    }
    
    func calculateAge() -> Int {
        //GET AGE OF CARETAKER FROM DATE OF BIRTH
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat //"yyyy-MM-dd" //"dd MMM, yyyy"
        var birthday = Date()
        if dobTextField.text != "" {
            birthday = formater.date(from: dobTextField.text!) ?? Date()
        }
        let calendar = Calendar.current

        let ageComponents = calendar.dateComponents([.year], from: birthday, to: Date())
        let age = ageComponents.year!
        return age
    }
    
    //MARK:- UI Methods
    func setupUI() {
        
        
//        if isCaretakerProfile {
//            addImageButton.isHidden = true
//        }
        
//        firstNameTextField.setUnderLine()
//        middleNameTextField.setUnderLine()
//        lastNameTextField.setUnderLine()
//        emailTextField.setUnderLine()
        countryTextField.setUnderLine()
//        address1TextField.setUnderLine()
//        address2TextFiedl.setUnderLine()
//        regionTextField.setUnderLine()
//        raceTextField.setUnderLine()
//        ethnicityTextField.setUnderLine()
//        zipTextField.setUnderLine()
//        primaryContactTextField.setUnderLine()
//        primaryContactCode.setUnderLine()
        alternateContactCode.setUnderLine()
//        alternateNumberTextField.setUnderLine()
//        raceTextField.setRightImage(rightImage: UIImage(named: "dropDown")!)
//        ethnicityTextField.setRightImage(rightImage: UIImage(named: "dropDown")!)
        dobTextField.setUnderLine()
        
        let changePasswordButtonTitle = NSMutableAttributedString(string: changePasswordButton.titleLabel?.text ?? "Change Password", attributes: [
            NSAttributedString.Key.font : UIFont(name: "Montserrat-Bold", size: 15.0) ?? UIFont.boldSystemFont(ofSize: 15.0),
            NSAttributedString.Key.foregroundColor : UIColor(red: 10/255, green: 144/255, blue: 203/255, alpha: 1.0),
            NSAttributedString.Key.underlineStyle : 1]
        )
        changePasswordButton.setAttributedTitle(changePasswordButtonTitle, for: .normal)
        
        DispatchQueue.main.async {
            self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
            self.profileImageView.layer.masksToBounds = false
            self.profileImageView.clipsToBounds = true
        }
    }
    
    func setupTextFieldsProperties() {
        firstNameTextField.minLength = 2
        middleNameTextField.minLength = 2
        lastNameTextField.minLength = 2
        emailTextField.minLength = 6
        zipTextField.minLength = 5
        primaryContactTextField.minLength = 9
        alternateNumberTextField.minLength = 9
        
        firstNameTextField.maxLength = 15
        middleNameTextField.maxLength = 15
        lastNameTextField.maxLength = 15
        zipTextField.maxLength = 8
        primaryContactTextField.maxLength = 15
        alternateNumberTextField.maxLength = 12
        txtfieldContactName.maxLength = 15
        txtfieldContactNumber.maxLength = 15
        txtfieldContactName.minLength = 2
        txtfieldContactNumber.minLength = 8
        txtfieldMedicalId.maxLength = 8
        
        
        firstNameTextField.sizeLimit = true
        middleNameTextField.sizeLimit = true
        lastNameTextField.sizeLimit = true
        zipTextField.sizeLimit = true
        primaryContactTextField.sizeLimit = true
        alternateNumberTextField.sizeLimit = true
        txtfieldContactName.sizeLimit = true
        txtfieldContactNumber.sizeLimit = true
        txtfieldMedicalId.sizeLimit = true
        
    }
    
    //MARK:- Show profile data methods
    func setupFieldsForMinorCaretaker() {
        caretakerTypeTextfield.text = self.getCaretakerTypeFromID(id: self.caretakerDetails?.care_taker_type_id ?? 0) //"Senior"
        relationshipTextField.text = self.caretakerDetails?.relationship_name ?? ""
        self.getRelationFromName(name: self.caretakerDetails?.relationship_name ?? "")
        
        if let profileImage = caretakerDetails?.profile_image {
            DispatchQueue.main.async {
                if let url = URL(string: profileImage) {
                    self.profileImageView.kf.indicatorType = .activity
                    self.profileImageView.kf.setImage(with: url)
                }
            }
        }
        
        let name = self.caretakerDetails?.name
        let myStringArr = name?.components(separatedBy: " ")
        let firstName = (myStringArr?[0] ?? "")
        var middleName = ""
        var lastName = ""
        if myStringArr?.count == 2 {
            lastName = (myStringArr?[1] ?? "")
        }
        else if myStringArr?.count == 3 {
            middleName = (myStringArr?[1] ?? "")
            lastName = (myStringArr?[2] ?? "")
        }
        
        firstNameTextField.text = firstName//self.caretakerDetails?.name
        middleNameTextField.text = middleName
        lastNameTextField.text = lastName
        dobTextField.text = self.caretakerDetails?.dob //(self.caretakerDetails?.dob)?.convertToDateString(currentDateFormat: "yyyy-MM-dd", requiredDateFormat: Constant.dateFormat)
        
        //Medical Devices
        let medicalDevice = caretakerDetails?.healthDevice ?? ""
        let medicalDevIDArray = medicalDevice.components(separatedBy: ",")
        self.getMedicalDeviceFromID(idArray: medicalDevIDArray)
        
        //Fitness Devices
        let fitnessDevice = caretakerDetails?.fitnessDevice ?? ""
        let fitnessDevIDArray = fitnessDevice.components(separatedBy: ",")
        self.getFitnessDeviceFromID(idArray: fitnessDevIDArray)
        if fitnessDevIDArray.count != 0 || medicalDevIDArray.count != 0 {
            self.frame.size.height += 100
        }
        
        //Set Device UI
        self.setDevicesCollectionView()
        self.setDevicesButtonImage()
        self.delegate?.setSelectedDevice(medical: self.selectedMedicalDevice, fitness: self.selectedFitnessDevice)
        
        emailTextField.isHidden = true
        countryTextField.isHidden = true
        address1TextField.isHidden = true
        address2TextFiedl.isHidden = true
        regionTextField.isHidden = true
        raceTypeFieldView.isHidden = true
        ethnicityTypeFieldView.isHidden = true
//        raceTextField.isHidden = true
//        ethnicityTextField.isHidden = true
        zipTextField.isHidden = true
        primaryContactContainerView.isHidden = true
//        alternateContactCode.isHidden = true
        alternateContactContainerView.isHidden = true
        stateTextField.isHidden = true
        cityTextField.isHidden = true
        
        /*emailTextField.text = "N/A"
        countryTextField.text = "N/A"
        address1TextField.text = "N/A"
        address2TextFiedl.text = "N/A"
        regionTextField.text = "N/A"
        raceTextField.text = "N/A"
        ethnicityTextField.text = "N/A"
        zipTextField.text = "N/A"
        primaryContactTextField.text = "N/A"
//        primaryContactCode.text = "N/A"
        alternateContactCode.text = "N/A"
        alternateNumberTextField.text = "N/A"
        stateTextField.text = "N/A"
        cityTextField.text = "N/A"
        
        emailTextField.isUserInteractionEnabled = false
        countryTextField.isUserInteractionEnabled = false
        address1TextField.isUserInteractionEnabled = false
        address2TextFiedl.isUserInteractionEnabled = false
        regionTextField.isUserInteractionEnabled = false
        raceTextField.isUserInteractionEnabled = false
        ethnicityTextField.isUserInteractionEnabled = false
        zipTextField.isUserInteractionEnabled = false
        primaryContactTextField.isUserInteractionEnabled = false
//        primaryContactCode.isUserInteractionEnabled = false
        alternateContactCode.isUserInteractionEnabled = false
        alternateNumberTextField.isUserInteractionEnabled = false
        stateTextField.isUserInteractionEnabled = false
        cityTextField.isUserInteractionEnabled = false*/
        
        maleRadioButton.isUserInteractionEnabled = false
        femaleRadioButton.isUserInteractionEnabled = false
        otherRadioButton.isUserInteractionEnabled = false
        
        smsRecieveBtn.isUserInteractionEnabled = false
        groupStudiesBtn.isUserInteractionEnabled = false
    }
    
    func setPrefilledForCaretaker(profile: ProfileDetail) {
        caretakerTypeTextfield.text = self.getCaretakerTypeFromID(id: self.caretakerDetails?.care_taker_type_id ?? 0) //"Senior"
        relationshipTextField.text = self.caretakerDetails?.relationship_name ?? ""
        self.getRelationFromName(name: self.caretakerDetails?.relationship_name ?? "")
        
        //SET PROFILE IMAGE
        if let profileImage = profile.userImageUrl {
            let imageURLString = Constant.URL.baseURL + "uploads/profile_pictures/" + (profile.sub ?? "") + "/\(profileImage)"
            DispatchQueue.main.async {
                if let url = URL(string: imageURLString) {
                    self.profileImageView.kf.indicatorType = .activity
                    self.profileImageView.kf.setImage(with: url)
                }
            }
        }
        
        //First Name and Last Name
        let name = profile.name
        let myStringArr = name?.components(separatedBy: " ")
        if let firstName = myStringArr?[0] {
            self.firstNameTextField.text = firstName
        }
        if let lastName = myStringArr?[1] {
            self.lastNameTextField.text = lastName
        }
        
        self.middleNameTextField.text = profile.middleName ?? ""
        
        //Set gender radio button
        if let gender = profile.gender {
            if gender == "Male" {
                self.maleRadioButton.isSelected = true
                self.femaleRadioButton.isSelected = false
            } else {
                self.maleRadioButton.isSelected = false
                self.femaleRadioButton.isSelected = true
            }
        }
        
        //Country name from code
         countryCode = profile.country ?? "0"
        self.getSelectedCountryFromCode(code: Int(countryCode)!)
        
        //Race Name From code
        let raceCode = profile.race ?? "0"
        self.getRaceFromCode(code: Int(raceCode)!)
        
        //Ethnicity name from Code
        let ethnicityCode = profile.ethnicity ?? "0"
        self.getEthnicityFromCode(code: Int(ethnicityCode)!)
        
        //Medical Devices
        let medicalDevice = profile.healthDevice ?? ""
        let medicalDevIDArray = medicalDevice.components(separatedBy: ",")
        self.getMedicalDeviceFromID(idArray: medicalDevIDArray)
        
        //Fitness Devices
        let fitnessDevice = profile.fitnessDevice ?? ""
        let fitnessDevIDArray = fitnessDevice.components(separatedBy: ",")
        self.getFitnessDeviceFromID(idArray: fitnessDevIDArray)
                
        self.emailTextField.text = profile.email ?? ""
        self.countryTextField.text = self.selectedCountry.name ?? ""
//        self.primaryContactTextField.text = profile.primaryPhone ?? ""
        self.primaryContactTextField.placeholder = ""
        let changePasswordButtonTitle = NSMutableAttributedString(string: profile.primaryPhone ?? "", attributes: [
            NSAttributedString.Key.font : UIFont(name: "Montserrat-Regular", size: 14.0) ?? UIFont.boldSystemFont(ofSize: 15.0),
            NSAttributedString.Key.foregroundColor : UIColor(red: 10/255, green: 144/255, blue: 203/255, alpha: 1.0),
            NSAttributedString.Key.underlineStyle : 1]
        )
      //  self.buttonCallPrimaryContact.setAttributedTitle(changePasswordButtonTitle, for: .normal)
        
        if profile.primaryPhone == "" || profile.primaryPhone == nil {
            self.primaryContactCallButton.isHidden = true
           // self.buttonCallPrimaryContact.isHidden = true
        }
        
        self.alternateContactCode.text = "+" + (self.selectedCountry.phonecode ?? "1")
        self.address1TextField.text = profile.address1 ?? ""
        self.address2TextFiedl.text = profile.address2 ?? ""
        self.regionTextField.text = profile.region ?? ""
        self.raceTextField.text = self.selectedRace.name ?? ""
        self.ethnicityTextField.text = self.selectedEthnicity.name ?? ""
        self.zipTextField.text = profile.zip ?? ""
        self.alternateNumberTextField.text = profile.altPhone ?? ""
        self.txtfieldContactName.text = profile.emerName ?? ""
        self.txtfieldContactNumber.text = profile.emernumber ?? ""//"+" + (self.selectedCountry.phonecode ?? "1") + (profile.emernumber ?? "")
        self.txtfieldMedicalId.text =  profile.medicalId ?? ""
        
        UserDefaults.standard.set(txtfieldContactName.text, forKey: "emerContactName")
        UserDefaults.standard.set(txtfieldContactNumber.text, forKey: "emerContactNumber")
        
        
        
        if self.alternateNumberTextField.text == "" {
            self.alternateContactCallButton.isHidden = true
        }
        self.dobTextField.text = profile.dob ?? ""
        self.stateTextField.text = profile.state ?? ""
        self.cityTextField.text = profile.city ?? ""
        
        //SMS recieve Switch
        if profile.smsRecieve == "true" {
            self.isSmsRecieve = true
        } else {
            self.isSmsRecieve = false
        }
        self.setImageforSmsRecieveButton()
        
        //Group Studies Switch
        if profile.groupStudies == "true" {
            self.isGroupStudies = true
        } else {
            self.isGroupStudies = false
        }
        self.setImageforGroupStudiesButton()
        
        //MFA Enabled
        if profile.mfaEnabled == "true" {
            self.mfaEnabledBtn.isSelected = true
        } else {
            self.mfaEnabledBtn.isSelected = false
        }
        
        //Set Device UI
        self.setDevicesCollectionView()
        self.setDevicesButtonImage()
        self.delegate?.setSelectedDevice(medical: self.selectedMedicalDevice, fitness: self.selectedFitnessDevice)
    }
    
    //SET PROFILE DATA FOR LOGGED IN USER (CONSUMER)
    func setScreenPrefilled(userData: [String: Any]) {
        DispatchQueue.main.async {
            //FETCH PROFILE IMAGE
            
            self.countryCode = userData["custom:country"] as? String ?? "0"
            self.getCountriesList()
            
            if let profileImage = userData["custom:profile_pic"] as? String { //Utility.shared.fetchImageFromDocumentDirectory() {
                let imageURLString = Constant.URL.baseURL + "uploads/profile_pictures/" + (AWSMobileClient.default().username!) + "/\(profileImage)"
                
                DispatchQueue.main.async {
                    if let url = URL(string: imageURLString) {
                        self.profileImageView.kf.indicatorType = .activity
                        self.profileImageView.kf.setImage(with: url, placeholder: UIImage(named: "profile_no_image"))
                    }
                }
            }
            
            //Separate first and last name
            let name = userData["name"] as? String ?? ""
            self.oldMiddleName = userData["custom:mid_name"] as? String ?? ""
            let myStringArr = name.components(separatedBy: " ")
            
            //NAme fiedls
            self.firstNameTextField.text = myStringArr[0]
            if myStringArr.count != 2 {
                self.lastNameTextField.text = ""
            } else {
                self.lastNameTextField.text = myStringArr[1]
            }
            
            self.middleNameTextField.text = userData["custom:mid_name"] as? String ?? ""
            
            //Set gender radio button
            if let gender = userData["custom:gender"] as? String{
                if gender == "Male" {
                    self.maleRadioButton.isSelected = true
                    self.femaleRadioButton.isSelected = false
                    self.otherRadioButton.isSelected = false
                } else if gender == "Female" {
                    self.maleRadioButton.isSelected = false
                    self.femaleRadioButton.isSelected = true
                    self.otherRadioButton.isSelected = false
                } else {
                    self.maleRadioButton.isSelected = false
                    self.femaleRadioButton.isSelected = false
                    self.otherRadioButton.isSelected = true
                }
            }
           
            //Country name from code
            self.getSelectedCountryFromCode(code: Int(self.countryCode)!)
            
            //Race Name From code
            let raceCode = userData["custom:race"] as? String ?? "0"
            self.getRaceFromCode(code: Int(raceCode)!)
            
            //Ethnicity name from Code
            let ethnicityCode = userData["custom:ethnicity"] as? String ?? "0"
            self.getEthnicityFromCode(code: Int(ethnicityCode)!)
            
            //Medical Devices
            let medicalDevice = userData["custom:health_dev"] as? String ?? ""
            let medicalDevIDArray = medicalDevice.components(separatedBy: ",")
            self.getMedicalDeviceFromID(idArray: medicalDevIDArray)
            
            //Fitness Devices
            let fitnessDevice = userData["custom:fitness_dev"] as? String ?? ""
            let fitnessDevIDArray = fitnessDevice.components(separatedBy: ",")
            self.getFitnessDeviceFromID(idArray: fitnessDevIDArray)
            
            self.emailTextField.text = userData["email"] as? String ?? ""
            self.countryTextField.text = self.selectedCountry.name ?? "" 
            self.primaryContactTextField.text = userData["phone_number"] as? String ?? ""
//            self.buttonCallPrimaryContact.setTitle(userData["phone_number"] as? String ?? "", for: .normal)
            
            let changePasswordButtonTitle = NSMutableAttributedString(string: userData["phone_number"] as? String ?? "", attributes: [
                NSAttributedString.Key.font : UIFont(name: "Montserrat-Regular", size: 14.0) ?? UIFont.boldSystemFont(ofSize: 15.0),
                NSAttributedString.Key.foregroundColor : UIColor(red: 10/255, green: 144/255, blue: 203/255, alpha: 1.0),
                NSAttributedString.Key.underlineStyle : 1]
            )
          //  self.buttonCallPrimaryContact.setAttributedTitle(changePasswordButtonTitle, for: .normal)
            
            self.alternateContactCode.text = "+" + (self.selectedCountry.phonecode ?? "1")
            self.address1TextField.text = userData["custom:address1"] as? String ?? ""
            self.address2TextFiedl.text = userData["custom:address2"] as? String ?? ""
            self.regionTextField.text = userData["custom:region"] as? String ?? ""
            self.raceTextField.text = self.selectedRace.name ?? ""
            self.ethnicityTextField.text = self.selectedEthnicity.name ?? ""
            self.zipTextField.text = userData["custom:zip"] as? String ?? ""
            self.alternateNumberTextField.text = userData["custom:alt_pn"] as? String ?? ""
            self.dobTextField.text = userData["custom:dob"] as? String ?? ""
            self.stateTextField.text = userData["custom:state"] as? String ?? ""
            self.cityTextField.text = userData["custom:city"] as? String ?? ""
            
            self.txtfieldContactName.text = userData["custom:emergency_name"] as? String ?? ""
            self.txtfieldContactNumber.text = userData["custom:emergency_no"] as? String ?? "" //"+" + (self.selectedCountry.phonecode ?? "1") + (userData["custom:emergency_no"] as? String ?? "")
            self.txtfieldMedicalId.text = userData["custom:medical_alert_id"] as? String ?? ""
            
            UserDefaults.standard.set(self.txtfieldContactName.text, forKey: "emerContactName")
            UserDefaults.standard.set(self.txtfieldContactNumber.text, forKey: "emerContactNumber")
            
            
            //Profile Picture
            let profilePicture = userData["custom:profile_pic"] as? String
            
            if profilePicture != nil {
                if let profileImageURL = URL(string: profilePicture!){
                    self.profileImageView.kf.indicatorType = .activity
                    self.profileImageView.kf.setImage(with: profileImageURL, placeholder: UIImage(named: "profile_no_image"))
                }
            }
            
            //SMS RECIEVE
            if userData["custom:sms_r"] as? String == "true" {
                self.isSmsRecieve = true
            } else {
                self.isSmsRecieve = false
            }
            self.setImageforSmsRecieveButton()
            
            //GROUP STUDIES
            if userData["custom:grp_stdy"] as? String == "true" {
                self.isGroupStudies = true
            } else {
                self.isGroupStudies = false
            }
            
            //MFA Enabled
            if userData["custom:mfa_enabled"] as? String == "true" {
                self.mfaEnabledBtn.isSelected = true
            } else {
                self.mfaEnabledBtn.isSelected = false
            }
            
          //  self.alternateContactCallButton.isHidden = true
           // self.primaryContactCallButton.isHidden = true
          //  self.buttonCallPrimaryContact.isHidden = true
            
            self.setImageforGroupStudiesButton()
            
            self.setDevicesCollectionView()
            self.setDevicesButtonImage()
            self.delegate?.setSelectedDevice(medical: self.selectedMedicalDevice, fitness: self.selectedFitnessDevice)
        }
    }
        
    func setImageforGroupStudiesButton() {
        if isGroupStudies {
            groupStudiesBtn.setImage(UIImage(named: "global_switch_on"), for: .normal)
        } else {
            groupStudiesBtn.setImage(UIImage(named: "global_switch_off"), for: .normal)
        }
    }
    
    func setImageforSmsRecieveButton() {
        if isSmsRecieve {
            smsRecieveBtn.setImage(UIImage(named: "global_switch_on"), for: .normal)
        } else {
            smsRecieveBtn.setImage(UIImage(named: "global_switch_off"), for: .normal)
        }
    }
    
    func setDevicesCollectionView() {
        if selectedFitnessDevice.count == 0 && selectedMedicalDevice.count == 0 {
            //devicesCollectionViewHeightConstraint.constant = 0
            //            selectModelHeightConstraint.constant = 0
            collectionViewSeparatorLabel.isHidden = true
            devicesCollectionView.isHidden = true
            selectModelButton.isHidden = true
        } else {
            selectModelButton.isHidden = false
            //            selectModelHeightConstraint.constant = 30
            devicesCollectionView.isHidden = false
            collectionViewSeparatorLabel.isHidden = false
            //            devicesCollectionViewHeightConstraint.constant = 120
            devicesCollectionView.reloadData()
        }
        
        self.getSelectedDevicesInCommonArray()
    }
    
    func getSelectedDevicesInCommonArray() {
        self.selectedDeviceNames.removeAll()
        
        for device in selectedFitnessDevice {
            selectedDeviceNames.append(device.name ?? "")
        }
        
        for device in selectedMedicalDevice {
            selectedDeviceNames.append(device.name ?? "")
        }
    }
    
    func setDevicesButtonImage() {
        //Fitness Device Button Image
        if selectedFitnessDevice.count == 0 {
            fitnessDeviceButton.setImage(UIImage(named: "fitnessDevices"), for: .normal)
        } else if selectedFitnessDevice.count != 0 {
            fitnessDeviceButton.setImage(UIImage(named: "fitnessDevice_Selected"), for: .normal)
        }
        
        //Medical Device Button Image
        if selectedMedicalDevice.count == 0 {
            medicalDeviceButton.setImage(UIImage(named: "medicalDevices"), for: .normal)
        } else if selectedMedicalDevice.count != 0 {
            medicalDeviceButton.setImage(UIImage(named: "profile_medical_selected"), for: .normal)
        }
    }
    
    @IBAction func raceDropDown(_ sender: Any) {
        IQKeyboardManager.shared.resignFirstResponder()
        initRaceDropDown()
    }
    
    @IBAction func ethnicityDropDown(_ sender: Any) {
        IQKeyboardManager.shared.resignFirstResponder()
        initEthnicityDropDown()
    }
}

//Text Fields Drop down methods
extension ProfileDetailsView {
    func initCaretakerTypeDropDown() {
        if countriesArray.caretakerTypes.count == 0 {
            Utility.shared.showToast(message: "No Caretaker types list available.")
            return
        }
        
        let caretakerTypesNames = countriesArray.caretakerTypes.map{$0.name}
        if caretakerTypesNames.count == 0 {
            Utility.shared.showToast(message: "No Caretaker types list available.")
            return
        }
        
        self.initdropDown(textField: caretakerTypeTextfield, dataSource: caretakerTypesNames as! [String])
    }
    
    func initRelationshipDropDown() {
        if countriesArray.relationship.count == 0 {
            Utility.shared.showToast(message: "No Relationship list available.")
            return
        }
        
        let relationsNames = countriesArray.relationship.map{$0.name}
        if relationsNames.count == 0 {
            Utility.shared.showToast(message: "No Relationship list available.")
            return
        }
        
        self.initdropDown(textField: relationshipTextField, dataSource: relationsNames as! [String])
    }
    
    func initRaceDropDown() {
        if countriesArray.race.count == 0 {
            Utility.shared.showToast(message: "No race list available.")
            return
        }
        
        let raceNames = countriesArray.race.map{$0.name}
        if raceNames.count == 0 {
            Utility.shared.showToast(message: "No race list available.")
            return
        }
        
        self.initdropDown(textField: raceTextField, dataSource: raceNames as! [String])
    }
    
    func initEthnicityDropDown() {
        if countriesArray.ethnicity.count == 0 {
            Utility.shared.showToast(message: "No ethnicity list available.")
            return
        }
        
        let ethnicityNames = countriesArray.ethnicity.map{$0.name}
        if ethnicityNames.count == 0 {
            Utility.shared.showToast(message: "No ethnicity list available.")
            return
        }
        self.initdropDown(textField: ethnicityTextField, dataSource: ethnicityNames as! [String])
    }
    
    //MARK:- Text Fields and Buttons Action's
    @IBAction func selectModelButtonAction(_ sender: Any) {
    }
    
    @IBAction func nextButtonAction(_ sender: Any)
    
    {
        //****************
        if isCaretakerProfile {
            if self.caretakerDetails?.minor_id != nil {
//                Utility.shared.showToast(message: "Coming Soon")
                self.updateMinorCaretakerProfile()
                return
            }
        }
        
        IQKeyboardManager.shared.resignFirstResponder()
        if !validateTextFields() {
            return
        }
        
        var userData = [String: String]()
        
        let firstName = firstNameTextField.text
        let middleName = middleNameTextField.text ?? ""
        let lastName = lastNameTextField.text
        let altPhoneNumber = alternateNumberTextField.text ?? ""
        
        userData["name"] = firstName! + " " + lastName!
        if oldMiddleName != middleName && middleName != "" {
            userData["custom:mid_name"] = middleName
        }
        
        if maleRadioButton.isSelected {
            userData["custom:gender"] = "Male"
        } else if femaleRadioButton.isSelected {
            userData["custom:gender"] = "Female"
        } else if otherRadioButton.isSelected {
            userData["custom:gender"] = "Other"
        }
        
        userData["custom:dob"] = dobTextField.text
        userData["custom:address1"] = (address1TextField.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:address2"] = (address2TextFiedl.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:region"] = (regionTextField.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:state"] = (stateTextField.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:city"] = (cityTextField.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:race"] = String(self.selectedRace.id ?? 0) //raceTextField.text ?? ""
        userData["custom:ethnicity"] = String(self.selectedEthnicity.id ?? 0) //ethnicityTextField.text ?? ""
        userData["custom:zip"] = zipTextField.text ?? ""    //zip
        userData["custom:alt_pn"] = altPhoneNumber
        userData["custom:emergency_name"] =  (txtfieldContactName.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:emergency_no"] = (txtfieldContactNumber.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:medical_alert_id"] = (txtfieldMedicalId.text ?? "").trimmingCharacters(in: .whitespaces)
        
        let smsRecieve = self.isSmsRecieve ? "true" : "false" //smsRecieveSwitch.isOn ? 1 : 0
        let groupStudies = self.isGroupStudies ? "true" : "false"  //groupStudiesSwitch.isOn ? 1 : 0
        let mfaEnabled = self.isMfaEnabled ? "true" : "false"
        
        userData["custom:sms_r"] = smsRecieve
        userData["custom:grp_stdy"] = groupStudies
        if !isCaretakerProfile {
            userData["custom:mfa_enabled"] = mfaEnabled
        }
        
        if isCaretakerProfile {
            userData["email"] = emailTextField.text ?? ""
            userData["relationshipId"] = String(selectedRelationship.id ?? 0)
        }
        
        delegate?.updateProfile(data: userData)
//        AWSAuth.shared.updateUserAttributes(userData: userData) { (result) in
//
//        }
    
        
        //**********************
        delegate?.setSelectedTab(index: 1)
//        Utility.shared.showToast(message: "Coming Soon")
    }
    
    @IBAction func addImageButtonAction(_ sender: Any) {
    
        self.delegate?.openImagePicker()
//        Utility.shared.showToast(message: "Coming Soon")
    }
    
    @IBAction func dobTextFieldAction(_ sender: Any) {
        self.showDatePicker()
    }
    
    @IBAction func ethnicityTextFieldAction(_ sender: Any) {
        self.endEditing(true)
    }
    
    @IBAction func fitnessDeviceButtonAction(_ sender: Any) {
        if devicesArray.fitness.count == 0 {
            Utility.shared.showToast(message: "No fitness devices available")
            return
        }
        
        delegate?.openDevicesList(listData: devicesArray, deviceType: "fitness")
    }
    
    @IBAction func medicalDeviceButtonAction(_ sender: Any) {
        if devicesArray.medical.count == 0 {
            Utility.shared.showToast(message: "No medical devices available")
            return
        }
        
        delegate?.openDevicesList(listData: devicesArray, deviceType: "medical")
//        showAlertWithBounceEffect(myView: selectDeviceView, onWindow: true)
    }
    
    @IBAction func changePasswordButtonAction(_ sender: Any) {
        self.delegate?.changePassword()
    }
    
    @IBAction func enableMFAButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.isMfaEnabled = sender.isSelected
        callEnableMfaApi(isEnabled: sender.isSelected)
    }
    
    func callEnableMfaApi(isEnabled: Bool) {
        Utility.shared.startLoader()
        MyProfileViewModel.enableMFA(isEnabled: isEnabled) { (sucess, message) in
            if sucess! {
                let mfaEnabled = self.isMfaEnabled ? "true" : "false"
                
                var userData = [String: String]()
                
                userData["custom:mfa_enabled"] = mfaEnabled
                AWSAuth.shared.updateUserAttributes(userData: userData) { (sucess) -> (Void) in
                    if sucess {
                        if self.isMfaEnabled {
                            DispatchQueue.main.async {
                                self.makeToast("OTP Authentication Enabled")
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.makeToast("OTP Authentication Disabled")
                            }
                        }
                    }
                }
            } else {
                self.makeToast("Something went wrong")
            }
        }
    }
    
    
    
    @IBAction func smsRecieveButtonAction(_ sender: Any) {
        if isSmsRecieve {
            self.isSmsRecieve = false
        } else {
            self.isSmsRecieve = true
        }
        self.setImageforSmsRecieveButton()
    }
    
    @IBAction func groupStudiesButtonAction(_ sender: Any) {
        if isGroupStudies {
            isGroupStudies = false
        } else {
            isGroupStudies = true
        }
        self.setImageforGroupStudiesButton()
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        //IF PROFILE IS MINOR CARETAKER
        if isCaretakerProfile {
            if self.caretakerDetails?.minor_id != nil {
//                Utility.shared.showToast(message: "Coming Soon")
                self.updateMinorCaretakerProfile()
                return
            }
        }
        
        IQKeyboardManager.shared.resignFirstResponder()
        if !validateTextFields() {
            return
        }
        
        var userData = [String: String]()
        let firstName = firstNameTextField.text
        let middleName = middleNameTextField.text ?? ""
        let lastName = lastNameTextField.text
        let altPhoneNumber = alternateNumberTextField.text ?? ""
        
        userData["name"] = firstName! + " " + lastName!
        if oldMiddleName != middleName && middleName != "" {
            userData["custom:mid_name"] = middleName
        }
        
        if maleRadioButton.isSelected {
            userData["custom:gender"] = "Male"
        } else if femaleRadioButton.isSelected {
            userData["custom:gender"] = "Female"
        } else if otherRadioButton.isSelected {
            userData["custom:gender"] = "Other"
        }
        
        userData["custom:dob"] = dobTextField.text
        userData["custom:address1"] = (address1TextField.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:address2"] = (address2TextFiedl.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:region"] = (regionTextField.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:state"] = (stateTextField.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:city"] = (cityTextField.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:race"] = String(self.selectedRace.id ?? 0) //raceTextField.text ?? ""
        userData["custom:ethnicity"] = String(self.selectedEthnicity.id ?? 0) //ethnicityTextField.text ?? ""
        userData["custom:zip"] = zipTextField.text ?? ""    //zip
        userData["custom:alt_pn"] = altPhoneNumber
        userData["custom:emergency_name"] =  (txtfieldContactName.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:emergency_no"] = (txtfieldContactNumber.text ?? "").trimmingCharacters(in: .whitespaces)
        userData["custom:medical_alert_id"] = (txtfieldMedicalId.text ?? "").trimmingCharacters(in: .whitespaces)
        
        let smsRecieve = self.isSmsRecieve ? "true" : "false" //smsRecieveSwitch.isOn ? 1 : 0
        let groupStudies = self.isGroupStudies ? "true" : "false"  //groupStudiesSwitch.isOn ? 1 : 0
        let mfaEnabled = self.isMfaEnabled ? "true" : "false"
        
        userData["custom:sms_r"] = smsRecieve
        userData["custom:grp_stdy"] = groupStudies
        if !isCaretakerProfile {
            userData["custom:mfa_enabled"] = mfaEnabled
        }
        
        if isCaretakerProfile {
            userData["email"] = emailTextField.text ?? ""
            userData["relationshipId"] = String(selectedRelationship.id ?? 0)
        }
        
        delegate?.updateProfile(data: userData)
//        AWSAuth.shared.updateUserAttributes(userData: userData) { (result) in
//
//        }
    }
}

//MARK:- API Methods
extension ProfileDetailsView {
    //UPDATE PROFILE DETAILS FOR MINOR CARETAKER
    func updateMinorCaretakerProfile() {
        IQKeyboardManager.shared.resignFirstResponder()
        if !validateTextFields() {
            return
        }
        
        var params = [String: AnyObject]()
        
        //FULL NAME OF CAREGIVER (USER)
        var senderFullName = String()
        let name = Constant.userProfile?.name
        let userMiddleName = Constant.userProfile?.middleName
        if userMiddleName != "" || userMiddleName != nil {
            let myStringArr = name?.components(separatedBy: " ")
            let firstName = (myStringArr?[0] ?? "")
            var lastName = ""
            if myStringArr?.count == 2 {
                lastName = (myStringArr?[1] ?? "")
            }
            senderFullName = firstName + " " + (userMiddleName ?? "") + " " + lastName
        } else {
            senderFullName = name ?? ""
        }
        
        //FULL NAME OF MINOR CARETAKER
        let firstName = firstNameTextField.text
        let lastName = lastNameTextField.text
        var fullName = firstName! + " " + lastName!
        let middleName = middleNameTextField.text
        if middleName != "" || middleName != nil {
            let fmName = (firstName! + " ") + middleName!
            fullName = fmName + (" " + lastName!)
        }
        
        //GET AGE OF CARETAKER FROM DATE OF BIRTH
        let age = self.calculateAge()
        let dob = dobTextField.text  //(dobTextField.text)?.convertToDateString(currentDateFormat: Constant.dateFormat, requiredDateFormat: "yyyy-MM-dd")
        
        //CREATE PARAMS TO ADD CARETAKER
        let userId = AWSMobileClient.default().username ?? ""
        params["user_id"] = userId as AnyObject
        params["relationship_id"] = selectedRelationship.id as AnyObject
        params["name"] = fullName as AnyObject
        params["dob"] = dob as AnyObject //ageTextField.text as AnyObject
        params["minor_id"] = self.caretakerDetails?.minor_id as AnyObject
        
        //IF MINOR CARETAKER
        if age < 18 {
            params["care_taker_type_id"] = 1 as AnyObject
        }
            //IF ADULT CARETAKER
        else {
            params["care_taker_type_id"] = 2 as AnyObject
            params["email_address"] = emailTextField.text as AnyObject
        }
        params["sender_name"] = senderFullName as AnyObject
        
        self.delegate?.updateMinorProfile(params: params)
    }
    
    //GET PROFILE DETAILS OF LOGGED IN USER FROM AWS
    func getUserAttributesFromAWS() {
        if AWSAuth.shared.userAWSStatus() == .signedIn {
            AWSAuth.shared.getUserAWSAttributes { (data, error) -> (Void) in
                Utility.shared.stopLoader()
                guard error == nil else {
                    return
                }
                let profile = ProfileDetail(value: data)
                self.setScreenPrefilled(userData: data)
            }
        }
    }
    
    //GET PROFILE DETAILS FOR ADULT CARETAKER
    func getCaretakerProfileAttributes() {
        MyProfileViewModel.getProfileDetails(email: self.caretakerDetails?.email_address ?? "") {result,error in
            Utility.shared.stopLoader()
            if result != nil {
                self.setPrefilledForCaretaker(profile: result!)
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong!")
            }
        }
    }
    
    //GET MASTER DATA
    func getCountriesList() {
        dispatchGroup.enter()
        RegisterViewModel.getMasterData() { (countries, error) in
            self.dispatchGroup.leave()
            if error == nil{
                self.countriesArray = countries
                self.countriesListArray = countries.countries
                self.getSelectedCountryFromCode(code: Int(self.countryCode)!)
                self.countryTextField.text = self.selectedCountry.name ?? ""
                self.alternateContactCode.text = "+" + (self.selectedCountry.phonecode ?? "1")
            }
        }
    }
    
    //GET LIST OF MEDICAL AND FITNESS DEVICES
    func getDevicesList() {
        self.dispatchGroup.enter()
        MyProfileViewModel.getDevicesList() { (devices, error) in
            self.dispatchGroup.leave()
            if error == nil {
                print(devices.medical.count)
                self.devicesArray = devices
            }
            else {
//                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
    
}

//MARK:- Methods to get from ID
extension ProfileDetailsView {
    func getSelectedCountryFromCode(code: Int) {
        for country in countriesListArray {
            if country.id == code {
                self.selectedCountry = country
                
                return
            }
        }
    }
    
    func getRaceFromCode(code: Int) {
        for race in countriesArray.race {
            if race.id == code {
                self.selectedRace = race
            }
        }
    }
    
    func getEthnicityFromCode(code: Int) {
        for ethnicity in countriesArray.ethnicity {
            if ethnicity.id == code {
                self.selectedEthnicity = ethnicity
            }
        }
    }
    
    func getRelationFromName(name: String) {
        for relation in countriesArray.relationship {
            if relation.name == name {
                self.selectedRelationship = relation
            }
        }
    }
    
    func getMedicalDeviceFromID(idArray: [String]) {
        for device in devicesArray.medical {
            for id in idArray {
                if Int(id) == device.id {
                    selectedMedicalDevice.append(device)
                    selectedDeviceNames.append(device.name ?? "")
                }
            }
        }
        
//        devicesArray.medical.filter( idArray.contains(Int($0.id)))
    }
    
    func getFitnessDeviceFromID(idArray: [String]) {
        for device in devicesArray.fitness {
            for id in idArray {
                if Int(id) == device.id {
                    selectedFitnessDevice.append(device)
                    selectedDeviceNames.append(device.name ?? "")
                }
            }
        }
    }
    
    func getCaretakerTypeFromID(id: Int) -> String{
        for type in countriesArray.caretakerTypes {
            if type.id == id {
                return type.name ?? ""
            }
        }
        return ""
    }
}


//MARK:- Drop Down
extension ProfileDetailsView {
    func initdropDown (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                if textField == self.regionTextField {
                    self.regionTextField.text  = item
                }
                else if textField == self.raceTextField {
                    self.raceTextField.text  = item
                    self.selectedRace = self.countriesArray.race[index]
                }
                else if textField == self.ethnicityTextField {
                    self.ethnicityTextField.text  = item
                    self.selectedEthnicity = self.countriesArray.ethnicity[index]
                }
                else if textField == self.caretakerTypeTextfield {
                    self.caretakerTypeTextfield.text  = item
                    self.selectedCaretakerType = self.countriesArray.caretakerTypes[index]
                }
                else if textField == self.relationshipTextField {
                    self.relationshipTextField.text  = item
                    self.selectedRelationship = self.countriesArray.relationship[index]
                }
                dropDown.hide()
            }
            dropDown.show()
        }
    }
}

//MARK:- MY Profile View Controller Delegate Methods
extension ProfileDetailsView: MyProfileVCDelegates {
    func selectedFitnessDevices(devices: [Fitness]?) {
        selectedFitnessDevice = devices ?? [Fitness]()
        setDevicesButtonImage()
    }
    
    func selectedMedicalDevices(devices: [Medical]?) {
        selectedMedicalDevice = devices ?? [Medical]()
        setDevicesButtonImage()
    }
}

//MARK:- Notification Observer Methods
extension ProfileDetailsView {
    @objc func getSelectedFitnessDevices(notification: Notification) {
        let devices = notification.object as? [Fitness]
        selectedFitnessDevice = devices ?? [Fitness]()
        setDevicesButtonImage()
        setDevicesCollectionView()
    }
    
    @objc func getSelectedMedicalDevices(notification: Notification) {
        let devices = notification.object as? [Medical]
        selectedMedicalDevice = devices ?? [Medical]()
        setDevicesButtonImage()
        setDevicesCollectionView()
    }
    
    @objc func setProfileImage(notification: Notification) {
        guard let profileImage = notification.object as? UIImage else {
            Utility.shared.showToast(message: "Something went wrong!")
            return
        }
//        self.profileImageView.image = profileImage ?? UIImage(named: "profile_no_image")
        var params = [String: AnyObject]()
        if !isCaretakerProfile {
            params["email"] = emailTextField.text as AnyObject
            params["cognito_user_id"] = (AWSMobileClient.default().username ?? "") as AnyObject
            params["minor_id"] = "" as AnyObject
            params["user_type"] = "consumer" as AnyObject
        }
        else if isCaretakerProfile {
            //In case of Minor caretaker
            if self.caretakerDetails?.care_taker_type_id == 1 {
                params["email"] = "" as AnyObject //Constant.userProfile?.email as AnyObject
                params["cognito_user_id"] = "" as AnyObject //(AWSMobileClient.default().username ?? "") as AnyObject
                params["minor_id"] = (String(caretakerDetails?.minor_id ?? 0)) as AnyObject
                params["user_type"] = "" as AnyObject
            }
            //In case of Adult caretaker
            else {
                params["email"] = caretakerDetails?.email_address as AnyObject
                params["cognito_user_id"] = (String(caretakerDetails?.caretakerSub ?? "")) as AnyObject
                params["minor_id"] = "" as AnyObject
                params["user_type"] = "consumer" as AnyObject
            }
        }
        
        self.uploadProfileImage(params: params, image: profileImage)
//        self.delegate?.updateProfileImage(params: params, image: self.profileImageView.image!)
    }
    
    func uploadProfileImage(params: [String : AnyObject], image: UIImage) {
        let noImageData = UIImage(named: "profile_no_image")?.pngData()
        let profileImage = image.pngData()
        
        if profileImage != noImageData {
            let imageParams = params
            Utility.shared.startLoader()
            MyProfileViewModel.uploadProfileImage(params: imageParams, profileImage: image) { (data, error) in
                Utility.shared.stopLoader()
                if error != nil {
                    Utility.shared.showToast(message: error!)
                } else {
                    Utility.shared.showToast(message: data!)
                    self.profileImageView.image = image
                }
            }
        }
    }
}
//MARK:- Text field Validations
extension ProfileDetailsView {
    
    func validateTextFields() -> Bool {
        let objValidate   = Validations()
        
        //First name field Validation
        var (status,message) = objValidate.validateField(firstNameTextField)
        if !status{
            switch message {
            case "Empty field":
                self.makeToast("Please enter your First name")
                break
                
            case "Short length text":
                self.makeToast("First name must be atleast \(firstNameTextField.minLength) characters")
                break
                
            case "Long length text":
                self.makeToast("First name must be maximum \(firstNameTextField.maxLength) characters")
                break
                
            default:
                break
            }
            return false
        }
        
        //Middle Name Field Validation
        if middleNameTextField.text?.count != 0{
            (status,message) = objValidate.validateField(middleNameTextField)
            if !status{
                switch message {
                case "Empty field":
                    self.makeToast("Please enter your Middle name")
                    break
                    
                case "Short length text":
                    self.makeToast("Middle name must be atleast \(middleNameTextField.minLength) characters")
                    break
                    
                case "Long length text":
                    self.makeToast("Middle name must be maximum \(middleNameTextField.maxLength) characters")
                    break
                    
                default:
                    break
                }
                return false
            }
        }
        
        //Last Name Field Validation
        (status,message) = objValidate.validateField(lastNameTextField)
        if !status{
            switch message {
            case "Empty field":
                self.makeToast("Please enter your Last name")
                
                break
            case "Short length text":
                self.makeToast("Last name must be atleast \(lastNameTextField.minLength) characters")
                
                break
            case "Long length text":
                self.makeToast("Last name must be maximum \(lastNameTextField.maxLength) characters")
                
                break
            default:
                break
            }
            return false
        }
        
        //IF MINOR CARETAKER PROFILE, NO NEED TO VALIDATE OTHER FIELDS
        if self.caretakerDetails?.care_taker_type_id == 1 {
            return true
        }
        
        //Email Field Validation
        (status,message) = objValidate.validateEmailField(emailTextField)
        if !status{
            switch message {
            case "Empty field":
                self.makeToast("Please enter your Email")
                break
                
            case "Invalid Email format":
                self.makeToast("Please enter a valid Email")
                break
                
            case "Short length text":
                self.makeToast("Email must be atleast \(emailTextField.minLength) characters")
                break
                
            case "Long length text":
                self.makeToast("Email must be maximum \(emailTextField.maxLength) characters")
                break
                
            default:
                break
            }
            return false
        }
        
        //Country Field Validation
        if countryTextField.text!.isEmpty || (countryTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0{
            self.makeToast("Please enter your country")
            return false
        }
        
        //Address Line 1
        if address1TextField.text!.isEmpty || (address1TextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0{
//            self.makeToast("Please enter your Address Line 1")
//            return false
            address1TextField.text = ""
        }
        
        //Address Line 1
        if address1TextField.text!.isEmpty || (address1TextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0{
//            self.makeToast("Please enter your Address Line 1")
//            return false
            address2TextFiedl.text = ""
        }
        
        //Region Field (Region)
//        if regionTextField.text!.isEmpty || (regionTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0{
//            self.makeToast("Please enter your Region")
//            return false
//        }
        
        //STATE FIELD
        
        
        //Race Field
//        if raceTextField.text!.isEmpty || (raceTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0{
//            self.makeToast("Please enter your Race")
//            return false
//        }
        
        //Ethnicity Field
//        if ethnicityTextField.text!.isEmpty || (ethnicityTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces)).count == 0{
//            self.makeToast("Please enter your Ethnicity")
//            return false
//        }
        
        //Zip Field Validation
        if !(zipTextField.text?.isEmpty)! {
            (status,message) = objValidate.validateField(zipTextField)
            if !status{
                switch message {
                case "Empty field":
                    self.makeToast("Please enter your Zip")
                    break
                    
                case "Short length text":
                    self.makeToast("Zip must be atleast \(zipTextField.minLength) digits")
                    break
                    
                case "Long length text":
                    self.makeToast("Zip must be maximum \(zipTextField.maxLength) digits")
                    break
                    
                default:
                    break
                }
                return false
            }
        }
        
            (status,message) = objValidate.validateField(txtfieldContactName)
            if !status{
                switch message {
                case "Empty field":
                    self.makeToast("Please enter your emergency contact name")
                    break
                    
                case "Short length text":
                    self.makeToast("Emergency contact name must be atleast \(txtfieldContactName.minLength) digits")
                    break
                    
                case "Long length text":
                    self.makeToast("Emergency contact name must be maximum \(txtfieldContactName.maxLength) digits")
                    break
                    
                default:
                    break
                }
                return false
            }
      
        
        
            (status,message) = objValidate.validateField(txtfieldContactNumber)
            if !status{
                switch message {
                case "Empty field":
                    self.makeToast("Please enter your emergency contact number")
                    break
                    
                case "Short length text":
                    self.makeToast("Emergency contact number must be atleast \(txtfieldContactNumber.minLength) digits")
                    break
                    
                case "Long length text":
                    self.makeToast("Emergency contact number must be maximum \(txtfieldContactNumber.maxLength) digits")
                    break
                    
                default:
                    break
                }
                return false
            }
        
        UserDefaults.standard.set(txtfieldContactName.text, forKey: "emerContactName")
        UserDefaults.standard.set(txtfieldContactNumber.text, forKey: "emerContactNumber")

       
       
        //Primary Contact Field
//        if primaryContactTextField.text!.isEmpty {
//            self.makeToast("Please enter your Primary Contact number")
//            return false
//        } else if primaryContactTextField.text!.count >= 15 {
//            self.makeToast("Primary Contact number cannot be more than 15 digits")
//            return false
//        } else if primaryContactTextField.text!.count <= 9 {
//            self.makeToast("Primary Contact number cannot be less than 9 digits")
//            return false
//        }
        
        if alternateNumberTextField.text?.count != 0 {
            if alternateNumberTextField.text!.count > alternateNumberTextField.maxLength {
                self.makeToast("Alternate Contact number cannot be more than \(alternateNumberTextField.maxLength) digits")
                return false
            } else if alternateNumberTextField.text!.count < alternateNumberTextField.minLength {
                self.makeToast("Alternate Contact number cannot be less than \(alternateNumberTextField.minLength) digits")
                return false
            }
        }
        

      
        return true
    }
}

extension ProfileDetailsView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == caretakerTypeTextfield {
//            IQKeyboardManager.shared.resignFirstResponder()
//            self.initCaretakerTypeDropDown()
            return false
        }
        
        //Relationship text field - Drop down init
        if textField == relationshipTextField {
//            IQKeyboardManager.shared.resignFirstResponder()
//            self.initRelationshipDropDown()
            return false
        }
        //Race text field -  drop down init
        if textField == raceTextField {
            IQKeyboardManager.shared.resignFirstResponder()
//            self.initRaceDropDown()
            return false
        }
        //Ethnicity Text field
        else if textField == ethnicityTextField {
            IQKeyboardManager.shared.resignFirstResponder()
//            self.initEthnicityDropDown()
            return false
        }
        else if textField == dobTextField {
            self.showDatePicker()
            return true
        }
        else if textField == regionTextField {
            return true
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //RESTRICT INITIAL EMPTY SPACES FOR ADDRESS, REGION, STATE AND CITY
        if (textField.text == "" && string == " ") && (textField == address1TextField || textField == address2TextFiedl || stateTextField == textField || cityTextField == textField || textField == regionTextField) {
            return false
        }
        
        //RESTRICT MORE THAN ONE SPACE CONSECUTIVELY FOR ADDRESS, REGION, STATE AND CITY
        if (string == " " && textField.text?.last == " ") && (textField == address1TextField || textField == address2TextFiedl || stateTextField == textField || cityTextField == textField || textField == regionTextField) {
            return false
        }
        
        //RESTRICT EMPTY SPACES
        if string == " " && textField != address1TextField && textField != address2TextFiedl && stateTextField == textField && cityTextField == textField{
            return false
        }
        
        //ADDRESS TEXT FIELD LIMITATIONS
        if textField == address1TextField || textField == address2TextFiedl {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 60, newCharacter: string)
            return result
        }
        
        //STATE AND CITY FIELD LIMITATIONS
        if textField == stateTextField || textField == cityTextField {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 32, newCharacter: string)
            
            if !result {
                return result
            }
            
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        
        //Check textfield limit for characters
        let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: nil, newCharacter: string)
        if !result {
            return result
        }
        
        //NAME FIELD LIMITATIONS
        if textField == firstNameTextField || textField == middleNameTextField || textField == lastNameTextField  {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 32, newCharacter: string)
            
            if !result {
                return result
            }
            
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        
        else if textField == txtfieldContactName {
            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 32, newCharacter: string)
            
            if !result {
                return result
            }
            
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
            
            //CONTACT AND ZIP CODE FIELD LIMITATTIONS
        else if textField == primaryContactTextField || textField == alternateNumberTextField || textField == zipTextField {
            var result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 12, newCharacter: string)
            
            if textField == zipTextField {
                result = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 8, newCharacter: string)
            }
            
            if !result {
                return result
            }
            
            let ACCEPTABLE_CHARACTERS = "1234567890"
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        
        
        return true
    }
}

extension ProfileDetailsView : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedDeviceNames.count  //selectedFitnessDevice.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DevicesCollectionViewCell", for: indexPath) as! DevicesCollectionViewCell
        cell.deviceTitleLabel.text = selectedDeviceNames[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: self.devicesCollectionView.frame.size.width / 4, height: self.devicesCollectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension ProfileDetailsView {
    func showDatePicker(){
        let formater = DateFormatter()
//        if self.caretakerDetails?.care_taker_type_id == 1 {
//            formater.dateFormat = "yyyy-MM-dd"
//        }
//        else {
            formater.dateFormat = Constant.dateFormat
//        }
         //"dd MMM, yyyy"
        var date = Date()
        if dobTextField.text != "" {
            date = formater.date(from: dobTextField.text!) ?? Date()
        }
        
        let screenWidth = UIScreen.main.bounds.width
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -110, to: Date())
        if self.caretakerDetails?.care_taker_type_id == 1 {
            datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
            datePicker.maximumDate = Date()
        } else {
            datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        }
        
        datePicker.setDate(date, animated: true)
        if #available(iOS 14.0, *)
        {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()

//            datePicker.datePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        dobTextField.inputAccessoryView = toolbar
        dobTextField.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
//        if self.caretakerDetails?.care_taker_type_id == 1 {
//            formatter.dateFormat = "yyyy-MM-dd"
//        }
//        else {
            formatter.dateFormat = Constant.dateFormat
//        }
        dobTextField.text = formatter.string(from: datePicker.date)
        self.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.endEditing(true)
    }

}
