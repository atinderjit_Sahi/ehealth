//
//  VitalStatusView.swift
//  E-Health
//
//  Created by Abhishek Chugh on 5/21/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient
import DropDown


class VitalStatusView: UIView {

    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var femaleRadioButton: UIButton!
    
    @IBOutlet weak var dobTextField: ExtendedTextField!
    @IBOutlet weak var heightFeetTextFiedl: ExtendedTextField!
    @IBOutlet weak var heightInchTextField: ExtendedTextField!
    @IBOutlet weak var chestTectFiedl: ExtendedTextField!
    @IBOutlet weak var weightTextField: ExtendedTextField!
    @IBOutlet weak var waistTextField: ExtendedTextField!
    @IBOutlet weak var hipsTextField: ExtendedTextField!
    @IBOutlet weak var bmiTextField: ExtendedTextField!
    @IBOutlet weak var lblNote: UILabel!
    
    @IBOutlet weak var wightLbsTextField: ExtendedTextField!
    @IBOutlet weak var chestInchTextField: ExtendedTextField!
    @IBOutlet weak var WaistInchTextField: ExtendedTextField!
    @IBOutlet weak var hipsInchTextField: ExtendedTextField!
    @IBOutlet weak var viewHeight: UIView!
  //  @IBOutlet weak var heightPicker: TYHeightPicker!
    var vitalStats = VitalsModel()
    var userId = String()
    var isCaretaker = false
    var caretakerDetails: CaretakerModel?
    var heightPicker: TYHeightPicker!
    public var firstTime : Bool = false
    
    //MARK:- View Hierarchy
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        heightPicker = TYHeightPicker()
        heightPicker.translatesAutoresizingMaskIntoConstraints = false
        heightPicker.delegate = self
        viewHeight.addSubview(heightPicker)
        
        heightPicker.leftAnchor.constraint(equalTo: viewHeight.leftAnchor).isActive = true
        heightPicker.rightAnchor.constraint(equalTo: viewHeight.rightAnchor).isActive = true
        heightPicker.centerYAnchor.constraint(equalTo: viewHeight.centerYAnchor).isActive = true
        heightPicker.heightAnchor.constraint(equalToConstant: 145).isActive = true
     
        
        if !isCaretaker {
            userId = AWSMobileClient.default().username ?? ""
        } else {
            if caretakerDetails?.minor_id != nil {
                userId = AWSMobileClient.default().username ?? ""
                self.getVitalStatsForMinor()
                return
            }
            userId = caretakerDetails!.caretakerSub!
        }
        getVitalStats()
    }
    
    @IBAction func maleRadiobuttonAction(_ sender: Any) {
    }
    
    @IBAction func femaleRadioButtonAction(_ sender: Any) {
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        self.endEditing(true)
       // viewHeight.isHidden = true
        if !validateTextField() {
            return
        }
        self.updateVitalStats()
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
       // Utility.shared.showToast(message: "Coming Soon")
      //  viewHeight.isHidden = true
        self.endEditing(true)
        if !validateTextField() {
            return
        }
        
        
        self.updateVitalStats()
    }
    
    @IBAction func actionDropDown(_ sender: UIButton) {
        
        if sender.tag == 101 {
            let dataSource = ["Inches", "Centimeters"]
            self.initdropDownHeight(textField: heightInchTextField, dataSource: dataSource )
        }
        
        else if sender.tag == 102 {
            viewHeight.isHidden = true
            let dataSource = ["Kgs", "Lbs"]
            self.initdropDownWeight(textField: wightLbsTextField, dataSource: dataSource )
        }
        
        else if sender.tag == 103 {
            viewHeight.isHidden = true
            let dataSource = ["Inches", "Centimeters"]
            self.initdropDownChest(textField: chestInchTextField, dataSource: dataSource )
        }
        
        else if sender.tag == 104 {
            viewHeight.isHidden = true
            let dataSource = ["Inches", "Centimeters"]
            self.initdropDownWaist(textField: WaistInchTextField, dataSource: dataSource )
        }
        
        else if sender.tag == 105 {
            viewHeight.isHidden = true
            let dataSource = ["Inches", "Centimeters"]
            self.initdropDownHip(textField: hipsInchTextField, dataSource: dataSource )
        }
    }
    
    
    func initdropDownHeight (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
               // textField.text = item
                    self.heightInchTextField.text  = item
                   self.firstTime = false
              //  cmBtn = createButton("Centimeter", tag: 0)
              //  feetInchBtn = createButton("Feet/inch", tag: 1)
                viewHeight.isHidden = false
                if item == "Inches" {
                    self.heightPicker.btnTapped(sender: self.heightPicker.feetInchBtn)

                }
                else {
                    self.heightPicker.btnTapped(sender: self.heightPicker.cmBtn)
                }
                
                ///****

                if heightFeetTextFiedl.text != ""  && weightTextField.text != "" && heightFeetTextFiedl.text != "0" && heightFeetTextFiedl.text != "0.0" {
                    if let weight = Double(weightTextField.text!), let feet = Double(heightFeetTextFiedl.text!){
                        
                        let finalFeet = Double(self.FeetConversion(str: heightFeetTextFiedl.text ?? "1"))
                        let finalInch = Double(self.InchConversion(str: heightFeetTextFiedl.text ?? "1"))
                        
                       if heightInchTextField.text == "Inches" &&  wightLbsTextField.text == "Lbs" {
                        heightPicker.setDefaultHeight(67, unit: .Inch)

                        let weightInKg = weight * 0.45359237
                        let totalInches = (finalFeet * 12) + finalInch
                        let heightInMeters = totalInches * 0.0254
                        let bmi = weightInKg / pow(heightInMeters, 2)
                        if bmi < 300 {
                            lblNote.isHidden = true
                        bmiTextField.text = String(format: "%.2f", bmi)
                        }
                        else {
                            lblNote.isHidden = false
                            bmiTextField.text = ""
                        }
                        }
                        
                       else if heightInchTextField.text == "Inches" &&  wightLbsTextField.text == "Kgs" {
                        heightPicker.setDefaultHeight(67, unit: .Inch)
                        let weightInKg = weight
                        let totalInches = (finalFeet * 12) + finalInch
                        let heightInMeters = totalInches * 0.0254
                        let bmi = weightInKg / pow(heightInMeters, 2)
                        if bmi < 300 {
                            lblNote.isHidden = true
                        bmiTextField.text = String(format: "%.2f", bmi)
                        }
                        else {
                            lblNote.isHidden = false
                            bmiTextField.text = ""
                        }
                        
                       }
                        
                       else if heightInchTextField.text == "Centimeters" &&  wightLbsTextField.text == "Kgs" {
                        heightPicker.setDefaultHeight(67, unit: .CM)

                        let weightInKg = weight
                        let totalInches = feet * 0.39370
                        let heightInMeters = totalInches * 0.0254
                        let bmi = weightInKg / pow(heightInMeters, 2)
                        if bmi < 300 {
                            lblNote.isHidden = true
                        bmiTextField.text = String(format: "%.2f", bmi)
                        }
                        else {
                            lblNote.isHidden = false
                            bmiTextField.text = ""
                        }
                       }
                       else if heightInchTextField.text == "Centimeters" &&  wightLbsTextField.text == "Lbs" {
                        heightPicker.setDefaultHeight(67, unit: .CM)

                        let weightInKg = weight * 0.45359237
                        let totalInches = feet * 0.39370
                        let heightInMeters = totalInches * 0.0254
                        let bmi = weightInKg / pow(heightInMeters, 2)
                        if bmi < 300 {
                        lblNote.isHidden = true
                        bmiTextField.text = String(format: "%.2f", bmi)
                        }
                        else {
                            lblNote.isHidden = false
                            bmiTextField.text = ""
                        }
                       }
                        
                        
                        
                        
                      // let bmi = self.bmiCalcImperialUnits(weightInPounds: weight, heightInFeet: feet, remainderInches: Double("0")!)
                      //  bmiTextField.text = bmi
                    }
                } else {
                    bmiTextField.text = ""
                }
                
                
                ///******
                
                
                dropDown.hide()
            }
            dropDown.show()
        }
    }
    
    func FeetConversion(str : String) -> Int{
        let numberString = str
        let numberComponent = numberString.components(separatedBy :".")
        if Int(numberComponent [0]) != nil {
            let integerNumber = Int(numberComponent [0]) ?? 0
            return integerNumber
        }
        return 0
    }
    func InchConversion(str : String) -> Int{
        let numberString = str
        let numberComponent = numberString.components(separatedBy :".")
        if numberComponent.count >= 2 {
        if Int(numberComponent [1]) != nil {
        let fractionalNumber = Int(numberComponent [1]) ?? 0
            return fractionalNumber
        }
        }
        return 0
    }
    
    func initdropDownWeight (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                    self.wightLbsTextField.text  = item
                
                
                ///****
                
                if heightFeetTextFiedl.text != ""  && weightTextField.text != "" && heightFeetTextFiedl.text != "0" && heightFeetTextFiedl.text != "0.0" {
                    if let weight = Double(weightTextField.text!), let feet = Double(heightFeetTextFiedl.text!){
                        let finalFeet = Double(self.FeetConversion(str: heightFeetTextFiedl.text ?? "1"))
                        let finalInch = Double(self.InchConversion(str: heightFeetTextFiedl.text ?? "1"))
                        
                       if heightInchTextField.text == "Inches" &&  wightLbsTextField.text == "Lbs" {
                          
                        let weightInKg = weight * 0.45359237
                        let totalInches = (finalFeet * 12) + finalInch;
                        let heightInMeters = totalInches * 0.0254
                        let bmi = weightInKg / pow(heightInMeters, 2)
                        if bmi < 300 {
                            lblNote.isHidden = true
                        bmiTextField.text = String(format: "%.2f", bmi)
                        }
                        else {
                            lblNote.isHidden = false
                            bmiTextField.text = ""
                        }
                        
                        }
                        
                       else if heightInchTextField.text == "Inches" &&  wightLbsTextField.text == "Kgs" {
                        
                        let weightInKg = weight
                        let totalInches = (finalFeet * 12) + finalInch ;
                        let heightInMeters = totalInches * 0.0254
                        let bmi = weightInKg / pow(heightInMeters, 2)
                        if bmi < 300 {
                            lblNote.isHidden = true
                        bmiTextField.text = String(format: "%.2f", bmi)
                        }
                        else {
                            lblNote.isHidden = false
                            bmiTextField.text = ""
                        }
                        
                       }
                        
                       else if heightInchTextField.text == "Centimeters" &&  wightLbsTextField.text == "Kgs" {
                        
                        let weightInKg = weight
                        let totalInches = feet * 0.39370
                        let heightInMeters = totalInches * 0.0254
                        let bmi = weightInKg / pow(heightInMeters, 2)
                        if bmi < 300 {
                            lblNote.isHidden = true
                        bmiTextField.text = String(format: "%.2f", bmi)
                        }
                        else {
                            lblNote.isHidden = false
                            bmiTextField.text = ""
                        }
                       }
                       else if heightInchTextField.text == "Centimeters" &&  wightLbsTextField.text == "Lbs" {
                        
                        let weightInKg = weight * 0.45359237
                        let totalInches = feet * 0.39370
                        let heightInMeters = totalInches * 0.0254
                        let bmi = weightInKg / pow(heightInMeters, 2)
                        if bmi < 300 {
                            lblNote.isHidden = true
                        bmiTextField.text = String(format: "%.2f", bmi)
                        }
                        else {
                            lblNote.isHidden = false
                            bmiTextField.text = ""
                        }
                       }
                        
                        
                        
                        
                      // let bmi = self.bmiCalcImperialUnits(weightInPounds: weight, heightInFeet: feet, remainderInches: Double("0")!)
                      //  bmiTextField.text = bmi
                    }
                } else {
                    bmiTextField.text = ""
                }
                
                
                ///******
                
                dropDown.hide()
            }
            dropDown.show()
        }
    }
    
    func initdropDownChest (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.chestInchTextField.text = item
                dropDown.hide()
            }
            dropDown.show()
        }
    }
    
    
    func initdropDownWaist (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.WaistInchTextField.text = item
                dropDown.hide()
            }
            dropDown.show()
        }
    }
    
    func initdropDownHip (textField: UITextField, dataSource: [String]) {
        let dropDown = DropDown()
        dropDown.bottomOffset = CGPoint(x: 0, y: textField.bounds.height)
        dropDown.anchorView = textField
        dropDown.dataSource = dataSource
        
        DispatchQueue.main.async {
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.hipsInchTextField.text = item
                dropDown.hide()
            }
            dropDown.show()
        }
    }
    
    
    //Show vital details on screen
    func showScreenPrefilled() {
      
        let heightString = vitalStats.height
        if  heightString != 0.0 && heightString != 0 {
            heightFeetTextFiedl.text = String (Float(heightString ?? 0))
            firstTime = true
        }
        
        if let weight = vitalStats.weight {
         if weight != 0 {
                weightTextField.text = String(Int(weight))
        }
        }
        
        if let chest = vitalStats.chest {
            if vitalStats.chest != 0 {
                chestTectFiedl.text = String(Int(chest))
        }
        }
       
        if let waist = vitalStats.waist {
            if waist != 0 {
                waistTextField.text = String(Int(waist))
        }
        }
        if let hips = vitalStats.hips {
            if  hips != 0 {
                hipsTextField.text = String(Int(hips))
        } }
        if let bmi = vitalStats.bmi {
            if  bmi != 0 || bmi != 0.0 {
            bmiTextField.text = (bmi.description)
            }
        }
        
        if let weight = vitalStats.weight_unit {
            if weight != "0" {
                wightLbsTextField.text = weight.trimmingCharacters(in: .whitespacesAndNewlines)}
        }
        if let chest = vitalStats.chest_unit {
            if chest != "0" {

                chestInchTextField.text = chest.trimmingCharacters(in: .whitespacesAndNewlines) }
        }
        if let waist = vitalStats.waist_unit {
            if waist != "0" {
                WaistInchTextField.text = waist.trimmingCharacters(in: .whitespacesAndNewlines)}
        }
        
        if let hips = vitalStats.hips_unit {
            if hips != "0" {
                hipsInchTextField.text = hips.trimmingCharacters(in: .whitespacesAndNewlines)}
        }
        
        if let height = vitalStats.height_unit {
            if height != "0" {
                heightInchTextField.text = height.trimmingCharacters(in: .whitespacesAndNewlines)
                viewHeight.isHidden = false
                if heightInchTextField.text == "Inches" {
                    self.heightPicker.btnTapped(sender: self.heightPicker.feetInchBtn)

                }
                else {
                    self.heightPicker.btnTapped(sender: self.heightPicker.cmBtn)
                }
            }
            else {
                viewHeight.isHidden = true
            }
        }
        
    }
}

//API Methods
extension VitalStatusView {
    func getVitalStats() {
        MyProfileViewModel.getVitalDetails(cognitoID: userId) { (result, error) in
            if result != nil {
                self.vitalStats = result!
                self.showScreenPrefilled()
            } else {
                print(error ?? "")
                //Utility.shared.showToast(message: error ?? "")
            }
        }
    }
    
    func getVitalStatsForMinor() {
        let minorID = String(self.caretakerDetails?.minor_id ?? 0)
        MyProfileViewModel.getVitalDetailsForMinor(minorID: minorID) { (result, error) in
            if result != nil {
                self.vitalStats = result!
                self.showScreenPrefilled()
            } else {
                print(error ?? "")
                //Utility.shared.showToast(message: error ?? "")
            }
        }
    }
    
    func updateVitalStats() {
        Utility.shared.startLoader()
        let params = self.getUpdateParams()
        
        let url = Constant.URL.saveVitalsV2 //Constant.URL.saveVitals
        
        MyProfileViewModel.saveVitals(params: params, url: url) { (result, error) in
            Utility.shared.stopLoader()
            if result != nil {
                self.vitalStats = result!
                Utility.shared.showToast(message: "Record has been updated successfully.")
                //self.showScreenPrefilled()

                if Constant.userProfile?.isNewUser == "1" {
                                       guard let vc = UIStoryboard.init(name: StoryboardNames.Report, bundle: Bundle.main).instantiateViewController(withIdentifier: DocumentListingViewController.className) as? DocumentListingViewController else {return}
                
                                                         vc.documentType = "4"
                                                            vc.fileType = "1"
                                                             vc.titleText = ""
              
                                                                  vc.selectedCareTaker = nil
                    vc.titleText = "Medications"
                                                             UIApplication.shared.topControllerInHierarchy()?.navigationController?.pushViewController(vc, animated: true)
                                                            return
                }
               
                
            } else {
                Utility.shared.showToast(message: error ?? "Something went wrong!")
            }
        }
    }
    
    func getUpdateParams() -> [String: AnyObject]{
        var params = [String: AnyObject]()
//        let heightText = (heightFeetTextFiedl.text ?? "") + "." + (heightInchTextField.text ?? "")
        params["height"] = heightFeetTextFiedl.text as AnyObject?
        params["weight"] = weightTextField.text as AnyObject?
        params["chest"] = chestTectFiedl.text as AnyObject?
        params["waist"] = waistTextField.text as AnyObject?
        params["hips"] = hipsTextField.text as AnyObject?
        params["bmi"] = bmiTextField.text as AnyObject?
        params["height_unit"] =  heightInchTextField.text as AnyObject?
        params["weight_unit"] = wightLbsTextField.text as AnyObject?
        params["chest_unit"] = chestInchTextField.text as AnyObject?
        params["hips_unit"] = hipsInchTextField.text as AnyObject?
        params["waist_unit"] =  WaistInchTextField.text as AnyObject?
        
        if self.vitalStats.id != nil {
            params["id"] = self.vitalStats.id as AnyObject?
        }
        
        //Height Param
        var height : String?
        if heightFeetTextFiedl.text != "" {
            let heightFeet = heightFeetTextFiedl.text
            var heightInch = "0"
            if heightInchTextField.text != "" {
                heightInch = heightInchTextField.text!
            }
            height = heightFeet! //+ "." + heightInch
        }
     //   params["height"] = (height ?? "") as AnyObject?
        params["user_id"] = userId as AnyObject?
        params["minor_id"] = "0" as AnyObject
        if caretakerDetails?.minor_id != nil {
            params["minor_id"] = caretakerDetails!.minor_id as AnyObject
        }
        return params
    }
}

//MARK:- Calculate BMI
extension VitalStatusView {
    func bmiCalcImperialUnits(weightInPounds : Double, heightInFeet: Double, remainderInches: Double) -> String {
        
        
        let weightInKg = weightInPounds * 0.45359237
        let totalInches = (heightInFeet * 12) + remainderInches
        let heightInMeters = totalInches * 0.0254
        let bmi = weightInKg / pow(heightInMeters, 2)
        let shortenedBmi = String(format: "%.2f", bmi)
        
//        let weightInKg = 55 * 0.45359237
//        let totalInches = (1 * 12) + 12
//        let heightInMeters = Double(totalInches) * 0.0254
//        let bmi = weightInKg / pow(heightInMeters, 2)
//        let shortenedBmi = String(format: "%.2f", bmi)
        
        return shortenedBmi
    }
}

//MARK:- Text field Validations
extension VitalStatusView {
    func validateTextField() -> Bool {
//        if heightFeetTextFiedl.text == "" && heightInchTextField.text == "" && weightTextField.text == "" && chestTectFiedl.text == "" && waistTextField.text == "" && hipsTextField.text == "" && bmiTextField.text == "" && wightLbsTextField.text == "" && chestInchTextField.text == "" && hipsInchTextField.text == "" && WaistInchTextField.text == "" {
//            Utility.shared.showToast(message: "All fields are empty")
//            return false
//        }

//        if heightFeetTextFiedl.text == ""  {
//            Utility.shared.showToast(message: "Please enter height")
//            return false
//        }
//
//        if heightInchTextField.text == "" {
//            Utility.shared.showToast(message: "Please enter unit for height")
//            return false
//        }
//
//
//        if weightTextField.text == ""  {
//            Utility.shared.showToast(message: "Please enter weight")
//            return false
//        }
//
//        if wightLbsTextField.text == "" {
//            Utility.shared.showToast(message: "Please enter unit for weight")
//            return false
//        }
//
//        if chestTectFiedl.text == ""  {
//            Utility.shared.showToast(message: "Please enter chest size")
//            return false
//        }
//
//        if chestInchTextField.text == "" {
//            Utility.shared.showToast(message: "Please enter unit for chest")
//            return false
//        }
//
//        if hipsTextField.text == ""  {
//            Utility.shared.showToast(message: "Please enter hips size")
//            return false
//        }
//
//        if hipsInchTextField.text == "" {
//            Utility.shared.showToast(message: "Please enter unit for hips")
//            return false
//        }
//
//        if waistTextField.text == ""  {
//            Utility.shared.showToast(message: "Please enter waist size")
//            return false
//        }
//
//        if WaistInchTextField.text == "" {
//            Utility.shared.showToast(message: "Please enter unit for waist")
//            return false
//        }
//
        
        //Validate Maximum feet
        if heightInchTextField.text == "Centimeters" {
            let feet = Int(heightFeetTextFiedl.text ?? "1")
            if feet! > 250 {
                Utility.shared.showToast(message: "Height should be less than 250 cms")
                return false
            }
            if feet! < 1 {
                Utility.shared.showToast(message: "Height should be minimum 1 cm")
                return false
            }
        }
        //Validate Maximum Inches
        
        if heightInchTextField.text == "Inches" {
            let inches = Int(heightFeetTextFiedl.text ?? "1")
            if inches != nil {
            if inches! > 96 {
                Utility.shared.showToast(message: "Height should be less or equal to 96 Inches")
                return false
            }
            if inches! < 1 {
                Utility.shared.showToast(message: "Height cannot be less than 1 Inch")
               return false
            }
        }
        }
        
        //Weight Maximum Validation
        if wightLbsTextField.text == "Kgs" {
            
            let weight = Int(weightTextField.text ?? "1")
            
            if weight != nil {
            if weight! > 320 {
                Utility.shared.showToast(message: "Weight cannot be more than 320 kgs")
                return false
            }
            if weight! < 1 {
                Utility.shared.showToast(message: "Weight should be minimum 1 kg")
                return false
            }
            }
        }
        
        if wightLbsTextField.text == "Lbs" {
            let weight = Int(weightTextField.text ?? "1")
            if weight != nil {
            if weight! > 700 {
                Utility.shared.showToast(message: "Weight cannot be more than 700 lbs")
                return false
            }
            if weight! < 1 {
                Utility.shared.showToast(message: "Weight should be minimum 1 lb")
                return false
            }
            }
        }
        
        //Chest Maximum Validation
        if chestInchTextField.text == "Centimeters" {
            let chest = Int(chestTectFiedl.text ?? "1")
            if chest != nil {
            if chest! > 134 {
                Utility.shared.showToast(message: "Chest cannot be more than 134 cms")
                return false
            }
            if chest! < 1 {
                Utility.shared.showToast(message: "Chest should be minimum 1 cm")
                return false
            }
            }
        }
        
        //Chest Maximum Validation
        if chestInchTextField.text == "Inches" {
            let chest = Int(chestTectFiedl.text ?? "1")
            if chest != nil {
            if chest! > 53 {
                Utility.shared.showToast(message: "Chest cannot be more than 53 Inches")
                return false
            }
            if chest! < 1 {
                Utility.shared.showToast(message: "Chest should be minimum 1 inch")
                return false
            }
            }
        }
        
        //waist Maximum Validation
        if WaistInchTextField.text == "Centimeters" {
            let waist = Int(waistTextField.text ?? "1")
            if waist != nil {
            if waist! > 124 {
                Utility.shared.showToast(message: "Waist cannot be more than 124 cms")
                return false
            }
            if waist! < 1 {
                Utility.shared.showToast(message: "Waist should be minimum 1 cm")
                return false
            }
            }
        }
        
        if WaistInchTextField.text == "Inches" {
            let waist = Int(waistTextField.text ?? "1")
            if waist != nil {
            if waist! > 49 {
                Utility.shared.showToast(message: "Waist cannot be more than 49 Inches")
                return false
            }
            if waist! < 1 {
                Utility.shared.showToast(message: "Waist should be minimum 1 inch")
                return false
            }
            }
        }
        
        //Hips Maximum Validation
        if hipsInchTextField.text == "Centimeters" {
            let hips = Int(hipsTextField.text ?? "1")
            if hips != nil {
            if hips! > 131 {
                Utility.shared.showToast(message: "Hips cannot be more than 131 cms")
                return false
            }
            if hips! < 1 {
                Utility.shared.showToast(message: "Hips should be minimum 1 cm")
                return false
            }
            }
        }
        
        
        if hipsInchTextField.text == "Inches" {
            let hips = Int(hipsTextField.text ?? "1")
            if hips != nil {
            if hips! > 52 {
                Utility.shared.showToast(message: "Hips cannot be more than 52 Inches")
                return false
            }
            if hips! < 1 {
                Utility.shared.showToast(message: "Hips should be minimum 1 inch")
                return false
            }
            }
        }
        
        return true
    }
    
    func calculate_BMI () {
        if heightFeetTextFiedl.text != ""  && weightTextField.text != "" && heightInchTextField.text != "" && wightLbsTextField.text != "" && heightFeetTextFiedl.text != "0" && heightFeetTextFiedl.text != "0.0" {
            if let weight = Double(weightTextField.text!), let feet = Double(heightFeetTextFiedl.text!){
                
                let finalFeet = Double(self.FeetConversion(str: heightFeetTextFiedl.text ?? "1"))
                let finalInch = Double(self.InchConversion(str: heightFeetTextFiedl.text ?? "1"))
                
               if heightInchTextField.text == "Inches" &&  wightLbsTextField.text == "Lbs" {
                let weightInKg = weight * 0.45359237
                let totalInches = (finalFeet * 12) + finalInch;
                let heightInMeters = totalInches * 0.0254
                let bmi = weightInKg / pow(heightInMeters, 2)
                if bmi < 300 {
                lblNote.isHidden = true
                bmiTextField.text = String(format: "%.2f", bmi)
                }
                else {
                    lblNote.isHidden = false
                    bmiTextField.text = ""
                }
                }
                
               else if heightInchTextField.text == "Inches" &&  wightLbsTextField.text == "Kgs" {
                
                let weightInKg = weight
                let totalInches = (finalFeet * 12) + finalInch;
                let heightInMeters = totalInches * 0.0254
                let bmi = weightInKg / pow(heightInMeters, 2)
                if bmi < 300 {
                    lblNote.isHidden = true
                bmiTextField.text = String(format: "%.2f", bmi)
                }
                else {
                    lblNote.isHidden = false
                    bmiTextField.text = ""
                }
               }
                
               else if heightInchTextField.text == "Centimeters" &&  wightLbsTextField.text == "Kgs" {
                
                let weightInKg = weight
                let totalInches = feet * 0.39370
                let heightInMeters = totalInches * 0.0254
                let bmi = weightInKg / pow(heightInMeters, 2)
                if bmi < 300 {
                lblNote.isHidden = true
                bmiTextField.text = String(format: "%.2f", bmi)
                }
                else {
                    lblNote.isHidden = false
                    bmiTextField.text = ""
                }
                
               }
               
               else if heightInchTextField.text == "Centimeters" &&  wightLbsTextField.text == "Lbs" {
                
                let weightInKg = weight * 0.45359237
                let totalInches = feet * 0.39370
                let heightInMeters = totalInches * 0.0254
                let bmi = weightInKg / pow(heightInMeters, 2)
                if bmi < 300 {
                lblNote.isHidden = true
                bmiTextField.text = String(format: "%.2f", bmi)
                }
                else {
                    lblNote.isHidden = false
                    bmiTextField.text = ""
                }
               }
            }
            
          
            
            
        } else {
            bmiTextField.text = ""
        }
    }
}

//MARK:- Text Field delegates
extension VitalStatusView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       
        if textField == bmiTextField {
            return false
        }
        if textField == heightFeetTextFiedl {
            viewHeight.isHidden = false
            return false
        }
        else{
            viewHeight.isHidden = true
        }
        
        let newPosition = textField.endOfDocument
        textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
            return false
        }
        
//        //Height -Feet Text Field
//        if textField == heightFeetTextFiedl  { //|| textField == bmiTextField
//            let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 2, newCharacter: string)
//
//            return result
//        }
        
        if (textField.text?.contains("."))! && string == "."{
            return false
        }
        
        
        let result  = Utility.shared.limitTextFieldCharacters(range: range, oldString: textField.text! as NSString, limit: 3, newCharacter: string)
        
        return result
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        if textField == heightFeetTextFiedl {
            firstTime = false
        }
        calculate_BMI()
    }
}
extension VitalStatusView: TYHeightPickerDelegate {
    
    func selectedHeight(height: CGFloat, unit: HeightUnit) {
        
        if  firstTime == false {
        
        if unit == .CM {
            heightFeetTextFiedl.text = "\(Int(height))"     //"\(Int(height)) \(unit)"
        }
        
        if unit == .Inch {
            let feet = Int(height / 12)
            let inch = Int(height) % 12
            
            if inch != 0 {
                heightFeetTextFiedl.text = "\(feet).\(inch)"//"\(feet) feet \(inch) inch"
                
            } else  {
                heightFeetTextFiedl.text = "\(feet).0"
            }
         }
        }
    }
    
    func pickerScrolled(bool_Value: Bool ) {
    firstTime = false
     calculate_BMI()
    }
    func  unitChanged (str: String) {
        if str == "Centimeters" {
            heightInchTextField.text = "Centimeters"
            if heightFeetTextFiedl.text != nil {
                if ((heightFeetTextFiedl.text?.contains(".")) != nil) {
                heightFeetTextFiedl.text = "0"
            }
        }
        }
        else {
            heightInchTextField.text = "Inches"
        }
    }
}


    

