//
//  MedicalReport.swift
//  E-Health
//
//  Created by Jaspreet Kaur on 9/17/20.
//  Copyright © 2020 AbhiShek Chugh. All rights reserved.
//

import UIKit
import AWSMobileClient

protocol MedicalReportTabDelegates: class {
    func addDocumentScreen(fileType: String, documentType: String)
    func viewMedicalReport(urlString: String)
    func deleteMedicalReport(params: [String: Any])
//    func reloadTableView()
}

class MedicalReport: UIView {

    //MARK: - Outlets
    @IBOutlet weak var medicalReportTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var textFieldFromDate: UITextField!
    @IBOutlet weak var textFieldToDate: UITextField!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var searchDoneButton: UIButton!
    @IBOutlet weak var searchClearButton: UIButton!
    @IBOutlet weak var fromToDateView: UIView!
    @IBOutlet weak var doneClearButtonView: UIView!
    
    //MARK: - Properties
    var pageSize = 500
    var pageNumber = 1
    var fileType: String?
    var titleText: String?
    var documentType: String?
    var searchText = String()
    var isDataLoading = false
    var activeTextField = UITextField()
    var selectedCareTaker: CaretakerModel?
    var documentsArray: [DocumentListingModel]?
    var insuranceRecordAray: [InsuranceRecord]?
    
    var datePicker = UIDatePicker()
    
    var medicalDelegate: MedicalReportTabDelegates?
    
    var myProfileVC : MyProfileViewController?
    var isCaretakerProfile = false
    var caretakerDetails: CaretakerModel?
    
    //MARK:- View Hierarchy
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
//        myProfileVC?.delegate = self
        medicalReportTableView.register(UINib(nibName: "MedicalReportTableViewCell", bundle: nil), forCellReuseIdentifier: MedicalReportTableViewCell.className)
        customizeTextFields()
        callSearchApi()
        medicalReportTableView.tableFooterView = UIView()
        medicalReportTableView.separatorStyle = .none
        medicalReportTableView.delegate = self
        medicalReportTableView.dataSource = self
        searchBar.delegate = self
        searchBar.setSearchBarUI()
        addButton.setImage(UIImage(named: "addDocuments"), for: .normal)
    }
    
    func customizeTextFields() {
        textFieldFromDate.delegate = self
        textFieldToDate.delegate = self
        textFieldFromDate.setLeftImage(leftImage: UIImage(named: "calendar")!)
        textFieldToDate.setLeftImage(leftImage: UIImage(named: "calendar")!)
        textFieldFromDate.attributedPlaceholder = NSAttributedString(string: "From", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
        textFieldToDate.attributedPlaceholder = NSAttributedString(string: "To", attributes: [NSAttributedString.Key.foregroundColor: Color.textFieldPlaceholderColor])
    }
    
    //MARK: - UIButton Actions
    @IBAction func buttonAddTapped(_ sender: UIButton) {
        self.medicalDelegate?.addDocumentScreen(fileType: "2", documentType: "6")
    }
    
    @IBAction func filterButtonTapped(_ sender: UIButton) {
        self.endEditing(true)
        self.searchBar.text = ""
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            print(sender.isSelected)
            hideShowSearchView(isHidden: false)
        } else {
            print(sender.isSelected)
            hideShowSearchView(isHidden: true)
            emptySearchByDateFields()
            callSearchApi()
        }
    }
    
    @IBAction func buttonSearchDoneTapped(_ sender: UIButton) {
        self.endEditing(true)
        if textFieldFromDate.text == "" {
            Utility.shared.showToast(message: "Please select from date")
            return
        } else if textFieldToDate.text == "" {
            Utility.shared.showToast(message: "Please select to date")
            return
        }
        searchDocumentByDate(toDate: textFieldToDate.text ?? "", fromDate: textFieldFromDate.text ?? "")
    }
    
    @IBAction func buttonSearchClearTapped(_ sender: UIButton) {
        self.endEditing(true)
        hideShowSearchView(isHidden: true)
        filterButton.isSelected = false
        emptySearchByDateFields()
        callSearchApi()
    }
    
    func hideShowSearchView(isHidden: Bool) {
        DispatchQueue.main.async {
            self.fromToDateView.isHidden = isHidden
            self.doneClearButtonView.isHidden = isHidden
        }
    }
    
    func emptySearchByDateFields() {
        textFieldToDate.text = ""
        textFieldFromDate.text = ""
    }
    
    func callSearchApi() {
        searchDocument(text: "")
    }

}
extension MedicalReport: DocumentListingProtocol {
    
    func viewReport(sender: UIButton) {
        self.endEditing(true)
        let indexpath = getSelectedIndexPath(sender: sender, tableView: medicalReportTableView)
        
        if indexpath != nil && documentsArray != nil {
            let document = documentsArray![indexpath!.row]
            let fileUrl = Constant.URL.baseURL + (document.filePath ?? "")
            medicalDelegate?.viewMedicalReport(urlString: fileUrl)
        }
    }
    
    func getSelectedIndexPath(sender: UIButton?, tableView: UITableView?) -> IndexPath? {
        let point = sender?.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView?.indexPathForRow(at: point!)
        return indexPath!
    }
}

//MARK: - Table View Delegate
extension MedicalReport: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK: - Table View Data Source
extension MedicalReport: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections = Int()
        if documentType == "3" {
            numOfSections = showNoRecordFound(dataArray: insuranceRecordAray ?? [], tableview: tableView)
        } else {
            numOfSections = showNoRecordFound(dataArray: documentsArray ?? [], tableview: tableView)
        }
        return numOfSections
    }
    
    func showNoRecordFound(dataArray: [Any], tableview: UITableView) -> Int {
        var noOfSections: Int = 0
        if !dataArray.isEmpty {
            tableview.separatorStyle = .singleLine
            noOfSections            = 1
            addButton.isHidden = true
        } else {
            addButton.isHidden = false
        }
        return noOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if documentsArray != nil {
            return documentsArray!.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MedicalReportTableViewCell.className, for: indexPath) as? MedicalReportTableViewCell else {
            fatalError()
        }
        cell.documentType = documentType ?? "0"
        let document = documentsArray![indexPath.row]
        cell.delegate = self
        //Added on june 5 2020
        let date = document.createdAt?.convertToDateString(currentDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", requiredDateFormat: Constant.dateFormat)
        cell.dateAddedLabel.text = "Added on " + (date ?? "")
        
        cell.scanUploadLabel.text = "Through Upload"
        cell.reportTitleLabel.text = document.name
        
        return cell
    }
    
    /*func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            Utility.shared.showCustomAlertWithCompletion("Delete Record", message: "Are you sure you want to delete this record?", withButtonTypes: ["No", "Yes"], withAlertCompletionBlock: { (alertAction, buttonTitle) in
                
                if buttonTitle == "Yes" {
                    self.deleteDocument(indexpath: indexPath)
                }
            })
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }*/
    
    func deleteDocument(indexpath: IndexPath) {
        Utility.shared.showToast(message: "Coming Soon")
    }
    
}

//MARK:- Search Bar Delegates
extension MedicalReport: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count >= 2 {
            self.searchDocument(text: searchText)
        } else {
            if searchText.count == 0 {
                searchBar.endEditing(true)
                self.searchDocument(text: searchText)
                medicalReportTableView.reloadData()
            } else {
                medicalReportTableView.reloadData()
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchDocument(text: String) {
        
        var params = [String: Any]()
        
        if isCaretakerProfile {
            if caretakerDetails?.minor_id != nil {
                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": caretakerDetails?.minor_id ?? 0, "file_type": "2", "document_type": "", "search_string": text, "page": pageNumber, "size": pageSize] as [String : Any]
            } else {
                params = ["cognito_user_id": caretakerDetails?.caretakerSub ?? "0", "minor_id": 0, "file_type": "2", "document_type": "", "search_string": text, "page": pageNumber, "size": pageSize] as [String : Any]
            }
        } else {
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "file_type": "2", "document_type": "", "search_string": text, "page": pageNumber, "size": pageSize] as [String : Any]
        }

        
//        if selectedCareTaker != nil {
//
//            if selectedCareTaker?.minor_id != nil {
//                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "file_type": "2", "document_type": "", "search_string": text, "page": pageNumber, "size": pageSize] as [String : Any]
//            } else {
//                params = ["cognito_user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "file_type": "2", "document_type": "", "search_string": text, "page": pageNumber, "size": pageSize] as [String : Any]
//            }
//        } else {
//            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "file_type": "2", "document_type": "", "search_string": text, "page": pageNumber, "size": pageSize] as [String : Any]
//        }
        
        Utility.shared.startLoader()
        DocumentListingViewModel.searchDocument(parameters: params) { (response, error) in
            Utility.shared.stopLoader()
            if response != nil {
                self.isDataLoading = false
                self.documentsArray = response
                self.medicalReportTableView.reloadData()
            } else {
                if text != "" {
                    self.documentsArray?.removeAll()
                    self.medicalReportTableView.reloadData()
                }
                if error != "No data found." {
                    Utility.shared.showToast(message: error ?? "Something went wrong")
                }
            }
        }
    }
    
    func searchDocumentByDate(toDate: String, fromDate: String) {
        var params = [String: Any]()
        if selectedCareTaker != nil {
            
            if selectedCareTaker?.minor_id != nil {
                params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": selectedCareTaker?.minor_id ?? 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": "", "page": pageNumber, "size": pageSize, "from_date" : fromDate, "to_date": toDate] as [String : Any]
            } else {
                params = ["cognito_user_id": selectedCareTaker?.caretakerSub ?? "0", "minor_id": 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": "", "page": pageNumber, "size": pageSize, "from_date" : fromDate, "to_date": toDate] as [String : Any]
            }
        } else {
            params = ["cognito_user_id": AWSMobileClient.default().username ?? "", "minor_id": 0, "file_type": fileType ?? "0", "document_type": documentType ?? "0", "search_string": "", "page": pageNumber, "size": pageSize, "from_date" : fromDate, "to_date": toDate] as [String : Any]
        }
        
        if documentType == "6" {
            params["document_type"] = ""
        }
        
        Utility.shared.startLoader()
        //        if documentType == "3" {
        DocumentListingViewModel.searchDocumentsByDate(parameters: params) { (response, records, error) in
            Utility.shared.stopLoader()
            if response != nil {
                //self.filterButton.isUserInteractionEnabled = true
                if self.documentType == "3" {
                    self.insuranceRecordAray = records
                    self.medicalReportTableView.reloadData()
                } else {
                    self.documentsArray = response
                    self.medicalReportTableView.reloadData()
                }
            } else {
                //self.filterButton.isUserInteractionEnabled = false
                if self.documentType == "3" {
                    self.insuranceRecordAray?.removeAll()
                } else {
                    self.documentsArray?.removeAll()
                }
                
                self.medicalReportTableView.reloadData()
                Utility.shared.showToast(message: error ?? "Something went wrong")
            }
        }
    }
                
}

extension MedicalReport: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        if textField == textFieldFromDate || textField == textFieldToDate {
            self.showDatePicker()
        }
        return true
    }
    
    func showDatePicker(){
        let formater = DateFormatter()
        formater.dateFormat = Constant.dateFormat //"MM/dd/yyyy"
        var date = Date()
        if activeTextField.text != "" {
            date = formater.date(from: activeTextField.text!) ?? Date()
        }
        
        let screenWidth = UIScreen.main.bounds.width
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -110, to: Date())
        datePicker.maximumDate = Date() //Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datePicker.setDate(date, animated: true)
        
        if #available(iOS 14.0, *)
        {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()

//            datePicker.datePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        textFieldFromDate.inputAccessoryView = toolbar
        textFieldToDate.inputAccessoryView = toolbar
        textFieldFromDate.inputView = datePicker
        textFieldToDate.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = Constant.dateFormat //"yyyy-MM-dd" //"dd MMM, yyyy"
        activeTextField.text = formatter.string(from: datePicker.date)
        self.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.endEditing(true)
    }
}
